DECLARE @start_period DATETIME = '2019-07-01 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-07-09 23:59:59.997'; 
DECLARE @key_word VARCHAR (max) = '%Independence%' 

SELECT TOP (100) * 
FROM [dbo].[Events]
WHERE ([String1] LIKE @key_word
      OR [String2] LIKE @key_word
      OR [String3] LIKE @key_word
      OR [String4] LIKE @key_word
      OR [String5] LIKE @key_word
      OR [String6] LIKE @key_word
      OR [String7] LIKE @key_word
      OR [String8] LIKE @key_word
      OR [String9] LIKE @key_word
      OR [String10] LIKE @key_word)
AND  [Timestamp] BETWEEN @start_period and @end_period
AND [EventId] != 8