DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform int

SET @startperiod = '2019-09-01 00:00:00.000';
SET @endperiod = '2019-09-01 23:59:59.999';

WITH
    AutumnUsers
    AS
    (
        SELECT  UserId  AS U_ID
        , [Created]
        FROM dbo.Users
        WHERE Created BETWEEN @startperiod AND @endperiod
        AND [RegistrationCountry] != 'UA'
          AND GameUserId NOT IN (103,	1240,	22,	1064,	0,	1002,	69,	482,	93,	671,	1050,	22,	1028,	647,	1004) -- testers
    )

 
SELECT CONVERT(date, Created) as 'Date'
, Count (*) AS NewUsers
, (SELECT Count(DISTINCT UserId) 
        FROM dbo.[EventsPurchase]  
        WHERE [PaymentsCount] = 1 
      --  AND [Timestamp] BETWEEN POU.[Created] AND POU.[Created] + 7
        AND  [UserId] = POU.[U_ID] ) AS '1 Purchase D30'

FROM AutumnUsers AS POU 
GROUP BY CONVERT(date, Created)
ORDER BY CONVERT(date, Created)
