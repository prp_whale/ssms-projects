DECLARE @st_per datetime,
        @end_per datetime

DECLARE @days_ago_st INT = 90;
DECLARE @days_ago_end INT = 0; -- 0 = yesterday

SET @st_per  =  DATEDIFF(dd, @days_ago_st, GETDATE())  ; 
SET @end_per  = DATEADD(ms, -3 ,  DATEDIFF(dd, @days_ago_end, GETDATE()) ) ; 


SELECT [Date]
     -- ,[ProjectName]
      ,case when [Platform]=1 then 'Android'
        when [Platform]=2 then 'iOS'
        else '-' end as OS
   --   ,[AdUnit]
      ,[AdvertisingID]
   --   ,[AdvertisingIDType]
   --   ,[UserID]
   --   ,[Segment]
   ,CASE WHEN [Placement] = '' THEN 'Other'   ELSE [Placement] END AS 'Placement'
      ,[AdNetwork]
      ,[Impressions]
      ,[Revenue]
     -- ,[ABTesting]
  FROM [dbo].[ISUserAdRevenue] 
WHERE [Date] BETWEEN @st_per AND @end_per
AND [ProjectName] = DB_NAME()


