DECLARE @id int = 1500;

--SELECT * 
--FROM [dbo].[Events]
--WHERE [UserId] = @id 
--ORDER BY [Timestamp]

SELECT [EventId] 
		,[Order]
       , [Timestamp] 
       , [ClientTime] 
       , [BackOfficeTime] 
       , [SessionId] 
	   ,[Int1]
	   ,[Int2]
	   ,[Int3]
	    ,[String1]
		 ,[String2]
		  ,[String3]
		    ,[String4]
			  ,[String5]
FROM   [dbo].[Events]
WHERE  [UserId] = @id 
AND [EventId] = 41
--ORDER  BY [Timestamp] 
ORDER  BY [ClientTime] 


SELECT
[Int1] as T
,count(*) as Amount
,count (distinct UserId) as U
FROM   [dbo].[Events]
WHERE  [EventId] = 41
GROUP BY [Int1]
Order by T 