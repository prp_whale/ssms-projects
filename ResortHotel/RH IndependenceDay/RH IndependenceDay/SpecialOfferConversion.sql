DECLARE @start_period DATETIME = '2019-07-04 00:00:00.000';  -- INDEPENDESE EVENT
DECLARE @end_period DATETIME = '2019-07-08 23:59:59.997'; 

--SET @start_period  =  DATEADD (WW, -2, @start_period); 
--SET @end_period =  DATEADD (WW, -2, @end_period); 

--SELEcT @start_period, @end_period

SELECT [segment_3]
,Count (DISTINCT UserId) AS Users
FROM   [dbo].[ev_OfferShowUp] AS AO 
       INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU  
               ON AO.[UserId] = POU.U_ID 
WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
  AND [ClientVersion] BETWEEN '1.14.0' AND '1.19.99' 
AND [segment_3] BETWEEN 1301 AND 1324
GROUP BY [segment_3]
ORDER BY [segment_3]


--SELECT [offers_showed_1]            AS ONE
--	  ,[offers_showed_2]            AS TWO
--       ,Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)                 AS Amount 
--FROM   [dbo].[ev_OfferShowUp] AS AO 
--       INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
--               ON AO.[UserId] = POU.U_ID 
--WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
--  -- AND [ClientVersion] BETWEEN '1.14.0' AND '1.19.99' 
--GROUP  BY [offers_showed_1], [offers_showed_2]
--ORDER  BY Users DESC

--SELECT [offers_showed_1]            AS ONE
--	  ,[offers_showed_2]            AS TWO
--       ,Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)                 AS Amount 
--FROM   [dbo].[ev_OfferPurchase] AS AO 
--       INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
--               ON AO.[UserId] = POU.U_ID 
--WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
--   AND [ClientVersion] BETWEEN '1.14.0' AND '1.19.99' 
--  AND [result] = 'OK'
--GROUP  BY [offers_showed_1], [offers_showed_2]
--ORDER  BY Users DESC

----SELECT TOP (100) * 
----FROM [dbo].[ev_OfferPurchase]
----WHERE [Result] = 'OK'
---- -- AND [segment_1] BETWEEN 1023 AND 1026
---- AND [segment_2] BETWEEN 1301 AND 1324
----ORDER BY [Timestamp] DESC

--SELECT [segment_3]
--, [segment_1]
--, [segment_2] 
--, Count (*) AS A
--FROM [dbo].[ev_OfferPurchase]
--WHERE [Timestamp] BETWEEN @start_period AND @end_period  
--AND [segment_3] = -1
--GROUP BY [segment_3], [segment_1] , [segment_2]
--ORDER  BY [segment_3], [segment_1] ,[segment_2]