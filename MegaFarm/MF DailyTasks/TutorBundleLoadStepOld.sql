
WITH StepTimeRaw
AS (
    SELECT  UserId, tutorial_step_id AS step_id
    , LEAD(tutorial_step_id, 1) OVER(PARTITION BY UserId ORDER BY Timestamp)  AS "NextStep"
    , DATEDIFF(ss,TIMESTAMP, LEAD(Timestamp) OVER(PARTITION BY UserId ORDER BY Timestamp) ) AS 'SecondsTimeStamp'
    , DATEDIFF(ss,ClientTime, LEAD(ClientTime) OVER(PARTITION BY UserId ORDER BY Timestamp) ) AS 'SecondsClientV'
FROM dbo.tutor_seq
WHERE  tutorial_step_id bEtween '1' AND '2'
AND ClientVersion = '1.4.2'
)
, StepTime 
AS 
(
    SELEcT * FROM StepTimeRaw WHERE step_id < NextStep
)

-- SELEcT 
--     distinct  PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY SecondsTimeStamp) OVER() as median_cont
-- From StepTime 

-- SELECT Distinct UserId
-- , PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY SecondsTimeStamp) OVER(PARTITION by UserId) as Q1_sec
-- , PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY SecondsTimeStamp) OVER(PARTITION by UserId) as median_sec
-- , PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY SecondsTimeStamp) OVER(PARTITION by UserId) as Q3_sec
-- FROM StepTime 
-- ORDER BY median_sec DESC

SELECT UserId 
, AVG(SecondsTimeStamp) AS 'AvgTime'
FROM StepTime 
GROUP BY UserId
ORDER BY  AvgTime DESC


