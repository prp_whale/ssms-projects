/*
3.Package % buyers ( 1st deposit- what percentage of users buys what type of package, same for second deposit).
Сколько процентов от зарегестрированных юзеров сделали первый депозит, в течении 30 ти дней, а сколько от тех которые сделали первый депозит , сделали и второй ? 

*/

DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform int,
        @day_shift int;

SET @day_shift = 30
SET @startperiod = '2019-07-01 00:00:00.000';
SET @endperiod = DATEADD(dd, -@day_shift, DATEADD(dd,  DATEDIFF(dd,0,GETDATE()),0));
SET @platform = 1 ;

WITH
    PoolOfUsers
    AS
    (
        SELECT  distinct(UserId)  AS U_ID
        , [Created] 
        FROM dbo.Users
        WHERE Created BETWEEN @startperiod AND @endperiod
        AND [RegistrationCountry] != 'UA'
          AND GameUserId NOT IN (103,	1240,	22,	1064,	0,	1002,	69,	482,	93,	671,	1050,	22,	1028,	647,	1004) -- testers
         -- AND [LTV] > 0
        --  AND [MediaSource] IS NOT  NULL
          AND [Platform] = @platform
      )

, UsersAndPur30D
 AS (
    SELECT [UserId]
    , Max([PaymentsCount]) AS Pay30Day
    , Convert(date, MIN(POU.[Created])) as Created
    FROM dbo.[EventsPurchase] as T
        INNER JOIN PoolOfUsers as POU ON t.[UserId] = POU.[U_ID]
    WHERE [Timestamp] BETWEEN POU.[Created] AND DATEADD(dd, @day_shift, POU.[Created])
   GROUP BY [UserId]
 )

SELECT
 t.Created
,t.NewUsers AS 'New Users'
, iif( t1.Depositors IS NULL, 0, t1.Depositors ) as '1+ Deposit'
, iif( t2.Depositors IS NULL, 0, t2.Depositors ) as '2+ Deposit'
, iif( t3.Depositors IS NULL, 0, t3.Depositors ) as '3+ Deposit'
, iif( t4.Depositors IS NULL, 0, t4.Depositors ) as '4+ Deposit'
, iif( t5.Depositors IS NULL, 0, t5.Depositors ) as '5+ Deposit'
, iif( t7.Depositors IS NULL, 0, t7.Depositors ) as '7+ Deposit'
, iif( t10.Depositors IS NULL, 0,t10.Depositors )   as '10+ Deposit'
FROM 
(
    SELECT  Convert(date, POU.[Created]) as Created
    , Count(*) AS NewUsers
    FROM PoolOfUsers as POU 
    GROUP BY  Convert(date, POU.[Created]) 
) AS T
LEFT JOIN 
(
    SELEcT [Created]
    , Count(*) as Depositors
    FROM UsersAndPur30D
    WHERE Pay30Day >= 1
    GROUP BY  [Created]
) AS T1 ON T.[Created] = T1.[Created]
LEFT JOIN 
(
    SELEcT [Created]
    , Count(*) as Depositors
    FROM UsersAndPur30D
    WHERE Pay30Day >= 2
    GROUP BY  [Created]
) AS T2 ON T.[Created] = T2.[Created]
LEFT JOIN 
(
    SELEcT [Created]
    , Count(*) as Depositors
    FROM UsersAndPur30D
    WHERE Pay30Day >= 3
    GROUP BY  [Created]
) AS T3 ON T.[Created] = T3.[Created]
LEFT JOIN 
(
    SELEcT [Created]
    , Count(*) as Depositors
    FROM UsersAndPur30D
    WHERE Pay30Day >= 4
    GROUP BY  [Created]
) AS T4 ON T.[Created] = T4.[Created]
LEFT JOIN 
(
    SELEcT [Created]
    , Count(*) as Depositors
    FROM UsersAndPur30D
    WHERE Pay30Day >= 5
    GROUP BY  [Created]
) AS T5 ON T.[Created] = T5.[Created]
LEFT JOIN 
(
    SELEcT [Created]
    , Count(*) as Depositors
    FROM UsersAndPur30D
    WHERE Pay30Day >= 7
    GROUP BY  [Created]
) AS T7 ON T.[Created] = T7.[Created]
LEFT JOIN 
(
    SELEcT [Created]
    , Count(*) as Depositors
    FROM UsersAndPur30D
    WHERE Pay30Day >= 10
    GROUP BY  [Created]
) AS T10 ON T.[Created] = T10.[Created]
ORDER BY Created