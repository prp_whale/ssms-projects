SELECT TOP (10000) [Created]      AS date_index 
                   ,[UserID]      AS player_id 
                   ,[MediaSource] AS source 
INTO   PRP_D2D_Installs 
FROM   [dbo].[Users] 
ORDER  BY [LastLogin] DESC 

SELECT TOP (100) * 
FROM   PRP_D2D_Installs 

SELECT [Timestamp] AS date_index 
       ,[UserID]   AS player_id 
INTO   PRP_D2D_Visits 
FROM   [dbo].[EventsAppOpen] AS AO 
       INNER JOIN PRP_D2D_Installs AS I 
               ON I.player_id = AO.[UserId] 

SELECT TOP (100) * 
FROM   PRP_D2D_Visits 

SELECT [Timestamp]          AS date_index 
       ,[UserID]            AS player_id 
       ,[ProductId]         AS package_name 
       ,[Amount]       AS amount 
       ,[AmountInUSD] * 0.3 AS tax 
INTO   PRP_D2D_Payments 
FROM   [dbo].[EventsPurchase] AS AO 
       INNER JOIN PRP_D2D_Installs AS I 
               ON I.player_id = AO.[UserId] 

SELECT TOP (100) * 
FROM   PRP_D2D_Payments 