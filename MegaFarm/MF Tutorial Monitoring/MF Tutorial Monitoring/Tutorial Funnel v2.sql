--SideQuest 1.5 month
DECLARE @start_period DATETIME = '2019-02-15 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-09-30 23:59:59.993'; 

--March month
--DECLARE @start_period DATETIME = '2019-03-01 00:00:00.000'; 
--DECLARE @end_period DATETIME = '2019-03-31 23:59:59.993'; 

--April Week One
--DECLARE @start_period DATETIME = '2019-04-01 00:00:00.000'; 
--DECLARE @end_period DATETIME = '2019-04-07 23:59:59.993'; 

DECLARE @test_country Varchar(2) = 'UA'; 

WITH POOL_OF_USERS 
AS ( 
		SELECT [UserId] as U_ID
		FROM   [dbo].[Users] AS U 
		WHERE  --[Created] BETWEEN @start_period AND @end_period AND
		     [RegistrationCountry] != @test_country 
			--AND [ClientVersion] BETWEEN '1.1.3' AND '1.1.3'
		--	AND [RegistrationClientVersion] BETWEEN '1.1.1' AND '1.1.3'
			
			-- AND [LTV] = 0
	) 

--SELECT Count (DISTINCT U_ID) AS REGISTRATIONS 
--       , Count (*)           AS A 
--FROM   POOL_OF_USERS 

SELECT tutorial_step_id            AS STEP 
       , Count (DISTINCT [UserId]) AS Users 
       , Count (*)                 AS Amount 
FROM   [dbo].[tutor_seq] AS TS 
       INNER JOIN POOL_OF_USERS AS POU 
               ON TS.[UserId] = POU.U_ID 
--WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
GROUP  BY tutorial_step_id 
ORDER  BY tutorial_step_id 


