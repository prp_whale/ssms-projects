--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
DECLARE @act_not_completed INT =14;
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------
DECLARE @sql_0 VARCHAR (max);
DECLARE @sql_1 VARCHAR (max);
DECLARE @sql_2 VARCHAR (max);
--------------------------------------------------------------------------------------------------------------
SET @sql_0 = 'DROP TABLE IF EXISTS PRP_Pool_StoryLine_Leave_On_Act_'+Cast( @act_not_completed AS varchar)+';'

SET @sql_1  = '
DECLARE @start_period DATETIME = ''2019-06-01 00:00:00.000'';
DECLARE @end_period DATETIME = ''2019-09-01 23:59:59.997'';

DECLARE @act_completed INT = cast('+Cast( @act_not_completed AS varchar)+' as Int) - 1; 

WITH POOL_OF_USERS
AS (
    SELECT [UserId] 
  --  , [Created]
    FROM dbo.[Users]
    WHERE [Created] BETWEEN @start_period AND @end_period
)

SELECT DISTINCT SP.[UserID] AS U_ID
  --  , [Created]
INTO PRP_Pool_StoryLine_Leave_On_Act_'+Cast( @act_not_completed AS varchar)+'
FROM [dbo].[ev_StorylineProgress] AS SP 
INNER JOIN POOL_OF_USERS AS POU ON SP.[UserId] = POU.[UserId]
LEFT JOIN ( SELECT SP.[UserID]  FROM [dbo].[ev_StorylineProgress] AS SP WHERE  ActIdInt = cast('+Cast( @act_not_completed AS varchar)+' as Int) ) AS SP2
                                ON SP.[UserId] = SP2.[UserId] 
WHERE  ActIdInt =  @act_completed 
    AND SP2.[UserID] IS NULL'

SET @sql_2 = 'SELECT COUNT(*) FROM PRP_Pool_StoryLine_Leave_On_Act_'+Cast( @act_not_completed AS varchar)
--------------------------------------------------------------------------------------------------------------
EXEC (@sql_0)
EXEC (@sql_1)
EXEC (@sql_2)
--------------------------------------------------------------------------------------------------------------



