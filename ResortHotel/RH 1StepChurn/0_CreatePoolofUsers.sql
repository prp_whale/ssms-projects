DECLARE @start_period DATETIME = '2019-09-01 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-10-01 23:59:59.997'; 

--------------------------------------------------
-- STEP 1 BELOW
-------------------------------------------------

DROP TABLE IF EXISTS PRP_Pool_Of_Users_Android;
SELECT DISTINCT [userid]
, [Created]
, [DeviceName]
, [RamSize]
, [GPUType]
, [LTV]
, [Retention1D]
, [Retention2D]
, [registrationcountry]
, [ClientVersion]
, [MediaSource]
, [OsVersion]
INTO   PRP_Pool_Of_Users_Android 
FROM   [dbo].[users] 
WHERE  [created] BETWEEN @start_period AND @end_period 
       AND [registrationcountry] != 'UA' 
       AND platform = 1

--------------------------------------------------
-- STEP 2 BELOW
--------------------------------------------------

DROP TABLE IF EXISTS PRP_Pool_Of_Deviators_Android;
SELECT DISTINCT U.[userid]
, U.[Created]
, [DeviceName]
, [RamSize]
, [GPUType]
, U.[LTV]
, [Retention1D]
, U.[registrationcountry]
, U.[ClientVersion]
, [MediaSource]
, [OsVersion]
INTO   PRP_Pool_Of_Deviators_Android 
FROM   [dbo].[users] AS U LEFT JOIN [dbo].[EventsLevelAttempts] a ON a.[UserId] = U.[UserId]
WHERE  U.[created] BETWEEN @start_period AND @end_period 
       AND U.[registrationcountry] != 'UA' 
       AND  a.[UserId] IS NULL
        AND U.[platform] = 1

