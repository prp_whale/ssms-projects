DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform int

SET @startperiod = '2019-09-01 00:00:00.000';
SET @endperiod = '2019-10-01 23:59:59.999';
SET @platform = 1;
WITH
    AutumnUsers
    AS
    (
        SELECT UserId  AS U_ID
        , [LTV30D]
        FROM dbo.Users
        WHERE Created BETWEEN @startperiod AND @endperiod
            -- testers
            AND GameUserId NOT IN (103,	1240,	22,	1064,	0,	1002,	69,	482,	93,	671,	1050,	22,	1028,	647,	1004)
            AND LTV > 0
            AND Platform = @platform
      
    )

    -- SELEcT 
    -- Sum(LTV30D) AS TotalRevenue
    -- ,Count(U_ID) AS Payers
    -- ,Sum(LTV30D)/Count(U_ID) AS Avarage
    -- From AutumnUsers 


    SELEcT 
      distinct  PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY LTV30D) OVER() as median_cont
    From AutumnUsers 
