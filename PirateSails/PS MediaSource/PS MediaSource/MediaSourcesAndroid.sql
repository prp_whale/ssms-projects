DECLARE @start_period DATETIME = '2019-03-10 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-04-10 23:59:59.997'; 

DECLARE @test_country VARCHAR(2) = 'UA'; 
DECLARE @platofrm INT = 1; -- 1 - Android; 2 iOS 


WITH POOL_OF_USERS 
AS ( 
		SELECT *
		FROM   [dbo].[Users] AS U 
		WHERE 
		    [Created] BETWEEN @start_period AND @end_period 
		    --AND [RegistrationCountry] != @test_country 
			AND [Platform] = @platofrm
			
			--AND [MediaSource] IS NULL
		  -- AND [LTV] = 0
	) 


SELECT [MediaSource] 
, Count (*) AS Users
FROM POOL_OF_USERS
GROUP BY [MediaSource]
ORDER BY [MediaSource]
--ORDER BY Users desc