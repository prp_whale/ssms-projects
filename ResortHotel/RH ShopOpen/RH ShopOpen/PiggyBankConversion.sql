DECLARE @start_period DATETIME = '2019-03-09 00:00:00.000'; 
--DECLARE @end_period DATETIME = '2019-04-19 00:00:00.000'; 

--DECLARE @start_period DATETIME = '2019-04-19 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-05-09 23:59:59.997'; 

--SELECT Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)            AS Amount 
--FROM   [dbo].[ev_PiggyBankShow]
--WHERE [TimeStamp] BETWEEN @start_period AND @end_period

--SELECT Count (DISTINCT U_ID) AS Users 
--       ,Count (*)            AS Amount 
--FROM  [dbo].[PRP_Pool_Of_ACTIVE_USERS]

--SELECT Count (DISTINCT U_ID) AS Users 
--       ,Count (*)            AS Amount 
--FROM  [dbo].[PRP_Pool_Of_USERS]


SELECT Count (DISTINCT [UserId]) AS Users 
       ,Count (*)            AS Amount 
FROM   [dbo].[ev_PiggyBankShow] AS PB
		INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS_20_PLUS_LVL]  AS AU
			ON AU.U_ID = PB.[UserId]
WHERE [TimeStamp] BETWEEN @start_period AND @end_period
AND ([ClientVersion] >= '1.9.0' OR ([ClientVersion] BETWEEN '1.11.0' AND  '1.19.99'))
--AND [is_auto] = 0


--SELECT Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)            AS Amount 
--FROM   [dbo].[ev_PiggyBankPurchase] AS PB
--		INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS_20_PLUS_LVL]  AS AU
--			ON AU.U_ID = PB.[UserId]
--WHERE [TimeStamp] BETWEEN @start_period AND @end_period
--AND ([ClientVersion] >= '1.9.0' OR ([ClientVersion] BETWEEN '1.11.0' AND  '1.19.99'))
--AND [Result] = 'OK'

--SELECT [level_id]
--		,Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)            AS Amount 
--FROM   [dbo].[ev_PiggyBankShow]
--WHERE [TimeStamp] BETWEEN @start_period AND @end_period
--GROUP BY [level_id]
--ORDER BY [level_id]