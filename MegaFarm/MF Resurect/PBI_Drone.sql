/*Вертолет
В данной таблице интересует статистика выполнения заказов на вертолетной площадке, а также наполнение заказов и вознаграждение за них. Это нужно для того, чтобы проанализировать динамику выполнения заказов и отмены заказов. 
Главные выводы из этой таблицы: как часто игроки выполняют заказы в разрезе уровней и сессии; как часто отменяют заказы и по какой причине (сложный к выполнению заказ, недостойное вознаграждение)
При построении графика нужно учитывать следующие параметры:
кол-во выполняемых заказов на уровень (сам заказ и получаемая награда)
кол-во выполняемых заказов за сессию (сам заказ и получаемая награда)
кол-во удаленных заказов за сессию
кол-во удаленных заказов по уровням
разделение на депозиторов и недепозиторов
возможность выбирать версии и временные промежутки
*/

declare @days int = 28;

declare @days_multi int = 1;
declare @st_per DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days*@days_multi, GETDATE())));
declare @end_per DATETIME  = DATEADD(ms, -3, CONVERT(datetime, CONVERT(date, GETDATE())));

declare @versa_limit int = 100;


WITH Versa
AS (
    SELECT registrationclientversion AS RegVer
    FROM  dbo.Users
   -- WHERE  created BETWEEN @st_per AND @end_per
    GROUP BY registrationclientversion
    HAVING Count(*) >=  @versa_limit
)


SELECT CONVERT(date, [TimeStamp]) AS 'Date', ClientVersion, level_id
,CASE   WHEN ltv > 0 THEN 'Depos'  ELSE 'ND'  END AS 'IsPay'
,CASE   WHEN order_status = 1 THEN 'Completed'  WHEN order_status = 2 THEN 'Canceled'  ELSE 'Err'  END AS 'Orders Status'
, Count(Distinct UserID) AS Users
, Count(Distinct SessionId) AS Sessionss
, Count(UserID) AS Events

FROM order_drone 
WHERE ClientVersion IN (SELECT * FROM Versa)
AND [Timestamp] BETWEEN @st_per AND @end_per

AND order_status IN (1,2)
AND [level_id] IS NOT NULL

GROUP BY CONVERT(date, [TimeStamp]) , ClientVersion,  level_id, order_status
,CASE   WHEN ltv > 0 THEN 'Depos'  ELSE 'ND'  END 

ORDER BY CONVERT(date, [TimeStamp]), ClientVersion, level_id, [Events]

