DECLARE @date DATETIME = '2019-07-24 00:00:00.000' ;

;WITH
    MediaSourceUsers
    AS
    (
        SELECT UserId AS U_ID
        FROM Users
        --WHERE MediaSource IS NULL  -- organic
        WHERE MediaSource = 'gurutraff_int' -- guru traffic
        --WHERE MediaSource != 'gurutraff_int' -- User Acquisition
        AND UserId NOT IN (103,	1240,	22,	1064,	0,	1002,	69,	482,	93,	671,	1050,	22,	1028,	647,	1004) -- testers
    )


SELECT *
FROM EventsPurchase  AS T
        INNER JOIN MediaSourceUsers ON MediaSourceUsers.[U_ID] =  T.[UserId]
WHERE [Timestamp] BETWEEN @date AND @date+1


SELECT TOP (100) * 
FROM EventsSessionFinished
WHERE [Timestamp] BETWEEN DATEADD(dd, -7, GETDATE()) AND GETDATE()
ORDER BY Duration DESC

