/*
Привет. Есть игроки, которые после того, как закончится энергия, не покупают ее, а выходят из игры
вот хотелось бы понять, как часто сталкиваются с тем что она заканчивается, и как часто эти игроки просто выходят.
*/

DECLARE @st_per DATETIME = '2020-03-01 00:00:00.000';
DECLARE @end_per DATETIME =  '2020-03-28 23:59:59.997'; --22

WITH PoU 
AS(
    SELECT UserId AS U_ID
    , CASE 
        WHEN dbo.Getchecksum(advertisingid) % 4 = 0 THEN 'A'
        WHEN dbo.Getchecksum(advertisingid) % 4 = 1 THEN 'B'
        WHEN dbo.Getchecksum(advertisingid) % 4 = 2 THEN 'C'
        WHEN dbo.Getchecksum(advertisingid) % 4 = 3 THEN 'D'
        ELSE 'ERROR' END AS 'Group'
    , CASE 
        WHEN ltv = 0 THEN 'ND'
        ELSE 'Depos' END AS 'IsPay'
    FROM   dbo.Users 
    WHERE  created BETWEEN '2020-03-01 00:00:00.000' AND '2020-03-21 23:59:59.997' 
        AND registrationclientversion IN ( '1.0.182', '1.0.184', '1.0.244' ) 
)
,CTE1 
AS (
    SELECT UserID , [Group] , [IsPay]
    , TriggerSourceId AS WorldId
    , TriggerId AS RunNumber
    , [Timestamp] AS start_katka
    FROM EventsProductMovement JOIN PoU ON UserId = U_ID
    WHERE ProductType = 'Energy'
    AND ProductBalance <= 7
    AND [Action] = 'spend'
    AND TriggerSourceId BETWEEN '1' AND '8'
    AND [Timestamp] BETWEEN @st_per AND @end_per
)
,CTE2
AS (
    SELECT FR.UserID , [Group] , [IsPay]
       ,  Timestamp AS end_katka

    FROM FinishRun AS FR INNER JOIN CTE1 AS C1 
        ON FR.UserId = C1.UserId AND FR.WorldId = C1.WorldId AND FR.Runnumber = C1.RunNumber
    WHERE  Timestamp BETWEEN start_katka AND DATEADD(hh, 1, start_katka) 
)
,CTE3
AS (
    SELECT VS.UserID , [Group] , [IsPay]
  --  , Count(*) AS Trans
    FROM VideoAdStatus  AS VS INNER JOIN CTE2 AS C2 
        ON VS.UserId = C2.UserId  AND Timestamp BETWEEN end_katka AND DATEADD(mi, 10, end_katka)
        AND TriggerSourceId = 'ReceiveEnergy' 
        AND AdStatus = 1
--    GROUP BY VS.UserID 
)
,CTE4
AS (
    SELECT VS.UserID , [Group] , [IsPay]
  --  , Count(*) AS Trans
    FROM EventsProductMovement  AS VS INNER JOIN CTE2 AS C2 
        ON VS.UserId = C2.UserId  AND Timestamp BETWEEN end_katka AND DATEADD(mi, 10, end_katka)
        AND ProductType = 'Gems'
        AND [Action] = 'spend'
        AND TriggerSourceId = 'buy_energy' 
--    GROUP BY VS.UserID 
)

SELECT [IsPay] , [Group]
, (SELECT  COUNT(DISTINCT UserId) FROM CTE1 AS C WHERE  C.IsPay = P.IsPay AND C.[Group] = P.[Group]) AS 'Start Users'
--, (SELECT  COUNT(*)               FROM CTE1 AS C WHERE  C.IsPay = P.IsPay AND C.[Group] = P.[Group]) AS 'Start Events'

, (SELECT  COUNT(DISTINCT UserId) FROM CTE2 AS C WHERE  C.IsPay = P.IsPay AND C.[Group] = P.[Group]) AS 'End Users'
--, (SELECT  COUNT(*)               FROM CTE2 AS C WHERE  C.IsPay = P.IsPay AND C.[Group] = P.[Group]) AS 'End Events'

, (SELECT  COUNT(DISTINCT UserId) FROM CTE3 AS C WHERE  C.IsPay = P.IsPay AND C.[Group] = P.[Group]) AS 'Ads Users'
--, (SELECT  COUNT(*)               FROM CTE3 AS C WHERE  C.IsPay = P.IsPay AND C.[Group] = P.[Group]) AS 'Ads Events'

, (SELECT  COUNT(DISTINCT UserId) FROM CTE4 AS C WHERE  C.IsPay = P.IsPay AND C.[Group] = P.[Group]) AS 'IAP Users'
--, (SELECT  COUNT(*)               FROM CTE4 AS C WHERE  C.IsPay = P.IsPay AND C.[Group] = P.[Group]) AS 'IAP Events'


FROM POU AS P 
GROUP BY [IsPay] , [Group]
ORDER BY [IsPay] , [Group]




-- SELECT DISTINCT TriggerSourceId
-- FROM EventsProductMovement 
-- WHERE ProductType = 'Energy'
-- AND TriggerSourceId  BETWEEN '1' AND '8'
-- ORDER BY TriggerSourceId

