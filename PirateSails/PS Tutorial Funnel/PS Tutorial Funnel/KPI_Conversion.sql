DECLARE @start_period DATETIME = '2019-06-01 00:00:00.000';
DECLARE @end_period DATETIME = '2019-06-14 23:59:59.997';

--SET @start_period  = '2019-06-15 00:00:00.000';
--SET @end_period  = '2019-06-28 23:59:59.997';

SET @start_period  = '2019-07-01 00:00:00.000';
SET @end_period   = '2019-07-14 23:59:59.997';

 SET @start_period  = '2019-07-14 00:00:00.000';
 SET @end_period   = '2019-07-28 23:59:59.997';

SET @start_period  = '2019-08-01 00:00:00.000';
SET @end_period   = '2019-08-14 23:59:59.997';

SELECT Count (DISTINCT [UserId]) AS 'Registration'
,Count(Iif([LTV1D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C1'
,Count(Iif([LTV2D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C2'
,Count(Iif([LTV3D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C3'
,Count(Iif([LTV4D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C4'
,Count(Iif([LTV5D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C5'
,Count(Iif([LTV6D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C6'
,Count(Iif([LTV7D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C7'
,Count(Iif([LTV8D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C8'
,Count(Iif([LTV9D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C9'
,Count(Iif([LTV10D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C10'
,Count(Iif([LTV11D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C11'
,Count(Iif([LTV12D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C12'
,Count(Iif([LTV13D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C13'
,Count(Iif([LTV14D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C14'
,Count(Iif([LTV15D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C15'
,Count(Iif([LTV16D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C16'
,Count(Iif([LTV17D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C17'
,Count(Iif([LTV18D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C18'
,Count(Iif([LTV19D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C19'
,Count(Iif([LTV20D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C20'
,Count(Iif([LTV21D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C21'
,Count(Iif([LTV22D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C22'
,Count(Iif([LTV23D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C23'
,Count(Iif([LTV24D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C24'
,Count(Iif([LTV25D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C25'
,Count(Iif([LTV26D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C26'
,Count(Iif([LTV27D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C27'
,Count(Iif([LTV28D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C28'
,Count(Iif([LTV29D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C29'
,Count(Iif([LTV30D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT [UserId]) AS 'C30'
FROM [dbo].[Users] 
WHERE  [Created] BETWEEN @start_period AND @end_period 
AND [Platform] = 1