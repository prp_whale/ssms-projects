DECLARE @start_period DATETIME = '2019-03-01 00:00:00.000';
DECLARE @end_period DATETIME = '2019-09-01 23:59:59.997';


SELECT Cast ([timestamp] as date) AS 'day'
       ,Count(DISTINCT userid) AS users 
FROM   [dbo].[ChestOpen] 
WHERE  timestamp BETWEEN created AND Dateadd(dd, 2, created) 
       AND chestlevel = 1 
       AND timestamp BETWEEN @start_period AND @end_period
       AND [isfreeopen] = 0
GROUP BY Cast ([timestamp] as date)  
ORDER BY day asc