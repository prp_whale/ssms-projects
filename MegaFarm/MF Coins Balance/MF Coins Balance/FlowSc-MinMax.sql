declare @start_date datetime	= '2019-02-11 00:00:00';
declare @end_date datetime		= '2019-02-24 23:59:59';
declare @cheater int			= 7249;
declare @depositor int			= 0;
 
 SELECT [level] 
		,MAX([coins]) as max_coins
		,MIN([coins]) as min_coins
		,MAX([coins])- MIN([coins]) as delta
		,[UserId] as U_id
	  FROM [dbo].[balance] 
	  WHERE  [timestamp] between @start_date and @end_date
	  AND [UserId] != @cheater
	  --AND [is_depositor] = @depositor
	  GROUP BY [UserId], [level]
	  ORDER BY U_id