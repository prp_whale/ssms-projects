




SELECT Count (DISTINCT [UserId]) 
       ,Sum (LTV) AS TOTAL_LTV 
FROM   [dbo].[Users] AS U 
       INNER JOIN (SELECT U_ID 
                   FROM   [dbo].[PRP_Terminal_InviteD_LESS_1_6_0]
                   UNION 
                   SELECT U_ID 
                   FROM   [dbo].[PRP_Terminal_InviteD_MORE_1_6_0]) AS 
                  POOL_USERS 
               ON POOL_USERS.[U_ID] = U.[UserId] 
	