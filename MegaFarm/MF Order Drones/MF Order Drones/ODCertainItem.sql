DECLARE @start_date Datetime = '2019-02-11 00:00:00.000'; 
DECLARE @end_date Datetime = '2019-02-24 23:59:59.999'; 
DECLARE @cheater Int = 7249; 
DECLARE @test_country Varchar(2) = 'UA' 



--DECLARE @product_in_slot VARCHAR(10) = 'Milk Shake'
  DECLARE @product_in_slot VARCHAR(20) = 'Cucumber Fresh Juice'
--DECLARE @product_in_slot VARCHAR(10) = 'Chair'
--DECLARE @product_in_slot VARCHAR(10) = 'Fried Corn'
--DECLARE @product_in_slot VARCHAR(10) = 'Hot Dog'

SELECT [order_status] as STATUS
	   , COUNT (*) as AMOUNT
	  ,  COUNT (DISTINCT [UserId]) as USERS
FROM   [dbo].[order_drone] 
 WHERE  [Timestamp] BETWEEN @start_date AND  @end_date 
              AND    [UserId] != @cheater 
              AND    [RegistrationCountry] != @test_country 
              AND    [LTV] = 0 
			AND ( [slot1_description] = @product_in_slot 
				OR  [slot2_description] = @product_in_slot
				OR  [slot3_description] = @product_in_slot
				OR  [slot4_description] = @product_in_slot
				OR  [slot5_description] = @product_in_slot
				OR  [slot6_description] = @product_in_slot
				)
GROUP BY [order_status]
ORDER BY [order_status]

--SELECT *
-- FROM   [dbo].[order_drone] 
-- WHERE  [Timestamp] BETWEEN @start_date AND  @end_date 
--              AND    [UserId] != @cheater 
--              AND    [RegistrationCountry] != @test_country 
--              AND    [LTV] = 0 
--			AND ( [slot1_description] = @product_in_slot 
--				OR  [slot2_description] = @product_in_slot
--				OR  [slot3_description] = @product_in_slot
--				OR  [slot4_description] = @product_in_slot
--				OR  [slot5_description] = @product_in_slot
--				OR  [slot6_description] = @product_in_slot
--				)