--DECLARE @start_period DATETIME = '2019-03-09 00:00:00.000'; 
--DECLARE @end_period DATETIME = '2019-04-19 00:00:00.000'; 

DECLARE @start_period DATETIME = '2019-04-19 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-05-09 23:59:59.997'; 


--DROP TABLE IF EXISTS PRP_Pool_Of_ACTIVE_USERS_20_PLUS_LVL;

--SELECT U.*
--INTO PRP_Pool_Of_ACTIVE_USERS_20_PLUS_LVL
--FROM   [dbo].[PRP_Pool_Of_ACTIVE_USERS] AS U 
--       INNER JOIN (
--					SELECT DISTINCT [UserId] 
--					FROM [dbo].[EventsLevelAttempts]
--					WHERE [TimeStamp] BETWEEN @start_period AND @end_period
--					AND [Completed] = 1
--					AND [LevelId] >= 20
--					AND ([ClientVersion] >= '1.9.0' OR ([ClientVersion] BETWEEN '1.11.0' AND  '1.19.99'))
--					)AS AO 
--              ON AO.[UserId] = U.U_ID 



--SELECT Count (DISTINCT U_ID) AS Users 
--       ,Count (*)                 AS Amount 
--FROM   [dbo].[PRP_Pool_Of_ACTIVE_USERS_20_PLUS_LVL] 


SELECT Count (DISTINCT U_ID) AS Users 
       ,Count (*)                 AS Amount 
FROM   [dbo].[PRP_Pool_Of_ACTIVE_USERS] AS U 
       INNER JOIN (
					SELECT DISTINCT [UserId] 
					FROM [dbo].[EventsLevelAttempts]
					WHERE [TimeStamp] BETWEEN @start_period AND @end_period
					AND [Completed] = 1
					AND [LevelId] >= 20
					AND ([ClientVersion] >= '1.9.0' OR ([ClientVersion] BETWEEN '1.11.0' AND  '1.19.99'))
					)AS AO 
              ON AO.[UserId] = U.U_ID 