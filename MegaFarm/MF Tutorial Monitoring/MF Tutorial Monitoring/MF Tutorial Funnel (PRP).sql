--Dates of samples
-- between '2019-02-08 00:00:00' and '2019-02-14 23:59:59' without side_quests
-- between '2019-02-08 00:00:00' and '2019-02-14 23:59:59' side_quests
-- BETWEEN '2019-02-15 00:00:00' AND '2019-02-21 23:59:57' side_quests week 2 
SELECT TOP (1000) [eventid], 
                  [userid], 
                  [timestamp], 
                  [clienttime], 
                  [backofficetime], 
                  [order], 
                  [clientversion], 
                  [isoffline], 
                  [platform], 
                  [created], 
                  [ltv], 
                  [retention1d], 
                  [retention2d], 
                  [retention7d], 
                  [firstdepositdate], 
                  [ltv7d], 
                  [ltv2d], 
                  [ltv1d], 
                  [ltv30d], 
                  [registrationcountry], 
                  [tutorial_step_id] 
FROM   [dbo].[tutor_seq] 

-- amount of each event 
SELECT tutorial_step_id  as ID, count (*) as Amount
FROM   [dbo].[tutor_seq] 
GROUP  BY tutorial_step_id
ORDER  BY tutorial_step_id 


-- amount of each event by period 
SELECT tutorial_step_id  as ID, count (*) as Amount
FROM   [dbo].[tutor_seq] 
WHERE  [created] between '2019-02-08 00:00:00' and '2019-02-14 23:59:59'
       AND registrationcountry <> 'ua' 
GROUP  BY tutorial_step_id
ORDER  BY tutorial_step_id 

--registrations 
SELECT Count (userid) AS registrations 
FROM   [dbo].[users] 
WHERE  [created] between '2019-02-08 00:00:00' and '2019-02-14 23:59:59'
       AND registrationcountry <> 'ua' 



--tutorial steps  
SELECT tutorial_step_id       AS tutorial_step_id, 
       Count(DISTINCT userid) AS users 
FROM   [dbo].[tutor_seq] 
WHERE  [timestamp] between '2019-02-08 00:00:00' and '2019-02-14 23:59:59'
       AND registrationcountry <> 'ua' 
       AND userid IN (SELECT userid 
                      FROM   users 
                      WHERE  created between '2019-02-08 00:00:00' and '2019-02-14 23:59:59') 
GROUP  BY tutorial_step_id 
ORDER  BY tutorial_step_id 


--appopen 
SELECT Count (DISTINCT userid) AS AppOpens 
FROM   [dbo].[eventsappopen] 
WHERE  [timestamp] between '2019-02-08 00:00:00' and '2019-02-14 23:59:59'
       AND country <> 'ua' 