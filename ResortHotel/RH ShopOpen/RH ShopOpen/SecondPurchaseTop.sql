DECLARE @start_period DATETIME = '2019-03-09 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-05-09 23:59:59.997'; 

--SELECT [ProductId] 
--       ,Count (DISTINCT [UserId]) AS Depositors 
--       ,Count (*)                 AS Amount 
--FROM   [dbo].[EventsPurchase]  AS PBP 
--       INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS] AS POAU 
--               ON POAU.U_ID = PBP.[UserId] 
--WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
--   AND [PaymentsCount] = 2 
--GROUP  BY [ProductId] 
--ORDER  BY Depositors  DESC


SELECT Count (DISTINCT [UserId]) AS Depositors 
       ,Count (*)                 AS Amount 
FROM   [dbo].[EventsPurchase]  AS PBP 
       INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS] AS POAU 
               ON POAU.U_ID = PBP.[UserId] 
WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
   AND [PaymentsCount] = 1  