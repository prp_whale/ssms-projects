WITH LastSucKatka
AS (
    SELECT DISTINCT fr.UserId AS U_ID
    ,MAX([Timestamp]) AS LastSucKatka
    FROM FinishRun as fr join users on users.userid=fr.userid 
    WHERE RoomName != '-2' 
    AND registrationclientversion='0.0.124' 
    GROUP BY fr.UserId
)


SELECT TOP (1)
    count(distinct e.userid) users,
    count(*) spins,
    sum(productamount) as TotalGold
    ,avg(productamount) as AvgGold
  FROM [dbo].[EventsProductMovement] as e INNER JOIN LastSucKatka on UserId = U_ID
  where 
   action='temp_received'
    and triggersourceid='ads_wheel' 
	and triggerid like '4%' 
	and Timestamp <= LastSucKatka
	and producttype='gold'
    and productamount<9999

