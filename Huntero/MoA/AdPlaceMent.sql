DECLARE @st_per DATETIME = '2020-04-01 00:00:00.000';
DECLARE @end_per DATETIME =  '2020-04-15 23:59:59.997'; --22

WITH PoU
AS (
    SELECT DISTINCT UserId AS U_ID
    FROM dbo.Users 
    --WHERE RegistrationClientVersion IN ('1.0.214','1.0.217','1.0.302', '1.0.289')
)
, NEM
 AS (
     SELECT UserID
     , AccountLevel AS lvl
     , [Timestamp] AS e_time
      FROM NotEnoughMinerals JOIN PoU ON UserID = U_ID
      WHERE [Timestamp] BETWEEN @st_per AND @end_per  
      AND ClientVersion IN ('1.0.214','1.0.217','1.0.302', '1.0.289')
      AND ResourceName = 'energy'
 )
, CTE_AD  AS 
(
    SELECT T.UserId 
   , AccountLevel AS lvl
    FROM VideoAdStatus AS T 
        INNER JOIN NEM ON T.UserId = NEM.UserID  AND T.AccountLevel = NEM.lvl
    AND [Timestamp] BETWEEN e_time AND DATEADD(MINUTE, 10, e_time)
    AND ClientVersion IN ('1.0.214','1.0.217','1.0.302', '1.0.289')
    AND TriggerSourceId = 'ReceiveEnergy'
)

, CTE_GEM  AS 
(
    SELECT T.UserId 
    , NEM.lvl AS lvl
    FROM EventsProductMovement AS T 
        INNER JOIN NEM ON T.UserId = NEM.UserID  AND T.LevelId = NEM.lvl
    AND [Timestamp] BETWEEN e_time AND DATEADD(MINUTE, 10, e_time)
    AND ClientVersion IN ('1.0.214','1.0.217','1.0.302', '1.0.289')
    AND TriggerSourceId = 'buy_energy'
    AND ProductType = 'Gems'
)

SELECT t.lvl 
, Count(*) AS Actions 
, Count (DISTINCT t.UserId ) AS Players

-- , Count(c1.UserId) AS ADActions 
-- , Count (DISTINCT c1.UserId ) AS AdPlayers

-- , Count(c2.UserId) AS GemActions 
-- , Count (DISTINCT c2.UserId ) AS GemPlayers
FROM NEM AS t 
          --   LEFT JOIN CTE_AD as c1 ON t.UserId = c1.UserId AND t.lvl = c1.lvl
          --     LEFT JOIN CTE_GEM as c2 ON t.UserId = c2.UserId AND t.lvl = c2.lvl
WHERE t.lvl <=50
GROUP BY t.lvl 
ORDER BY t.lvl 

-- SELECT Count(*) AS A 
-- , Count (DISTINCT UserId ) AS U 
-- FROM  CTE_GEM

-- SELECT ClientVersion
-- , Count(*) AS Y
--  FROM NotEnoughMinerals
-- WHERE Timestamp BETWEEN @st_per AND @end_per 
-- GROUP BY ClientVersion

-- -- SELECT Count(*) 
-- -- FROM NotEnoughMinerals
-- -- WHERE [Timestamp] BETWEEN @st_per AND @end_per
-- -- AND ResourceName = 'energy'

-- SELECT Count(*) 
-- FROM VideoAdStatus
-- WHERE [Timestamp] BETWEEN @st_per AND @end_per
-- AND TriggerSourceId = 'ReceiveEnergy'