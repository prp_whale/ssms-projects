-- DECLARE @start_period DATETIME = '2019-06-01 00:00:00.000';
-- DECLARE @end_period DATETIME = '2019-06-14 23:59:59.997';
 
-- DECLARE @start_period DATETIME = '2019-06-15 00:00:00.000';
-- DECLARE @end_period DATETIME = '2019-06-28 23:59:59.997';

DECLARE @start_period DATETIME = '2019-07-01 00:00:00.000';
DECLARE @end_period DATETIME = '2019-07-14 23:59:59.997';

SET @start_period  = '2019-06-01 00:00:00.000';
SET @end_period   = '2019-09-01 23:59:59.997';

DECLARE @test_country VARCHAR(2) = 'UA'; -- виключаємо Україну що б не попасти на тестові девайси
 
DECLARE  @TestDevices TABLE (UserID INT PRIMARY KEY);
INSERT INTO @TestDevices VALUES  (6), (14), (23), (105), (239), (247), (256), (257), (333), (1331), (1366), (1367), (1368), (1375), (1381), (1382), (3487), (3737), (4244), (4253), (4255), (4265), (4266), (4270), (5517), (5906), (2), (3), (7), (8), (9), (12), (16), (17), (21), (22), (25), (26), (27), (51), (117), (249), (293), (598), (666), (670), (1190), (1192), (1193), (1337), (5568);
 
 
;WITH POOL_OF_USERS  -- беремо користувачів за потрібними параметрами
AS (
        SELECT U.[UserId] AS ID
        FROM   [dbo].[Users] AS U
               LEFT JOIN @TestDevices AS TD
                      ON U.[UserId] = TD.UserID
        WHERE
            TD.UserID IS NULL -- виключаємо тестові девайси
            AND  [Created] BETWEEN @start_period AND @end_period
       --     AND [RegistrationCountry] != @test_country  -- виключаємо Україну що б не попасти на тестові девайси
)

, USERS_STEPS
AS (
	SELECT T.[UserId] AS U_ID
	,  [step_key] AS T_STEP
	FROM [dbo].[Tutorial] AS T 
	INNER JOIN POOL_OF_USERS AS POU ON T.[UserId] = POU.ID
	WHERE [Timestamp] BETWEEN @start_period AND @end_period 
	)

SELECT T_STEP
	, Count(DISTINCT U_ID) AS USERS
FROM USERS_STEPS
GROUP BY T_STEP 
ORDER BY USERS DESC