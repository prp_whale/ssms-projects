declare @days int = 28;

declare @days_multi int = 1;
declare @st_per DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days*@days_multi, GETDATE())));
declare @end_per DATETIME  = DATEADD(ms, -3, CONVERT(datetime, CONVERT(date, GETDATE())));

declare @versa_limit int = 100;


WITH Versa
AS (
    SELECT registrationclientversion AS RegVer
    FROM  dbo.Users
   -- WHERE  created BETWEEN @st_per AND @end_per
    GROUP BY registrationclientversion
    HAVING Count(*) >=  @versa_limit
)
, Opens
AS  (
    SELECT Convert(date, [Timestamp] ) as ao
    , UserId AS U_ID
    FROM EventsAppOpen
    WHERE [Timestamp] BETWEEN @st_per AND @end_per
    AND ClientVersion IN (SELECT RegVer FROM Versa)
    GROUP BY  Convert(date, [Timestamp] ) 
    , UserId 

)
, DepositorOpens
AS (
    SELECT ao , UserID 
    , Convert(date, MAX([Timestamp])) AS last_pay
    , SUM (AmountInUsd) AS total_gross
    , Count(UserID) AS transactions
    FROM EventsPurchase JOIN Opens on UserId = U_ID
    WHERE [Timestamp] BETWEEN dateadd(dd, -28, ao) and ao
    GROUP BY  ao , UserID 

)
, RFM 
AS (
    SELECT ao , UserID  
    , CASE 
      WHEN  DATEDIFF(dd, last_pay, ao) = 1 THEN '3' 
      WHEN  DATEDIFF(dd, last_pay, ao) BETWEEN 2 AND 7 THEN '2' 
      ELSE '1'
        END AS R

    , CASE 
      WHEN  transactions = 1 THEN '1' 
      WHEN  transactions BETWEEN 2 AND 7 THEN '2' 
      WHEN  transactions > 7 THEN '3' 
      ELSE 'err'
        END AS F

    , CASE 
      WHEN  total_gross <= 7 THEN '1' 
      WHEN  total_gross between 7.0001 and 70 THEN '2' 
      WHEN  total_gross > 70.0001 THEN '3' 
      ELSE 'err'
        END AS M
    FROM DepositorOpens
)



SELECT ao, R, F, M
, COUNT(UserId) AS Depositors
FROM RFM
WHERE ao = '2020-04-24'
GROUP BY ao, R, F, M
ORDER BY ao, R , F, M

-- SELECT * FROM Opens WHERE U_ID = 214651
-- ORDER BY ao


-- SELECT TOP 100 * FROM RFM
-- WHERE UserID = 198656

