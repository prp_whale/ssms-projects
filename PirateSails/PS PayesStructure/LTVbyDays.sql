SELECT cast ([Created] as date) AS 'Date'
, Count(*) as Users
, Sum ([LTV]) as LTV
FROM dbo.[UsersLtvCoeff]
WHERE [Platform] = 1 
--AND MediaSource = 'gurutraff_int' 
AND MediaSource IS NULL
--AND  MediaSource IN ('googleadwords_int', 'Facebook Ads', 'ironsource_int', 'unityads_int')
GROUP BY cast ([Created] as date)
ORDER BY cast ([Created] as date)