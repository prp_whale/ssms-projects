DECLARE @start_period DATETIME = '2019-06-01 00:00:00.000';
DECLARE @end_period DATETIME = '2019-06-14 23:59:59.997';

SET @start_period  = '2019-06-15 00:00:00.000';
SET @end_period  = '2019-06-28 23:59:59.997';

SET @start_period  = '2019-07-01 00:00:00.000';
SET @end_period   = '2019-07-14 23:59:59.997';

WITH MEDIA_COUNTRY
AS ( 
    SELECT [MediaSource] 
            ,[RegistrationCountry] 
            ,Count(DISTINCT [UserId]) AS Users 
        FROM   DBO.[Users] 
        WHERE  [Created] BETWEEN @start_period AND @end_period 
        GROUP  BY [MediaSource] 
                ,[RegistrationCountry] 
        HAVING Count(DISTINCT [UserId]) > 200 
)
, POOL_OF_USERS
AS (
    SELECT [UserId]
         , U.[MediaSource] 
         , U.[RegistrationCountry] 
      FROM   DBO.[Users] AS U 
        INNER JOIN  MEDIA_COUNTRY AS MC
            ON (U.[MediaSource] = MC.[MediaSource] OR (U.[MediaSource]is NULL and MC.[MediaSource] is NULL) )
            AND U.[RegistrationCountry] = MC.[RegistrationCountry] 
     WHERE  [Created] BETWEEN @start_period AND @end_period 
)



SELECT [MediaSource] 
            ,[RegistrationCountry] 
            ,Count(DISTINCT [UserId]) AS Registration
            , (SELECT     Count(DISTINCT T.[UserId]) 
                FROM       [dbo].[Tutorial] AS T 
                INNER JOIN POOL_OF_USERS    AS POU 
                ON         T.[UserId] = POU.[UserId] 
                WHERE      [step_key] = '10_tutorial_end'
                AND ( POU.[MediaSource] = P.[MediaSource]  OR ( POU.[MediaSource] IS NULL AND P.[MediaSource] IS NULL ) ) 
                AND POU.[RegistrationCountry]  = P.[RegistrationCountry]  
                 ) AS CompletedTutorial
            , (SELECT Count(DISTINCT B.[UserId]) 
                FROM   [dbo].[Building] AS B 
                INNER JOIN POOL_OF_USERS AS POU 
                        ON B.[UserId] = POU.[UserId] 
                WHERE  [BuildingId] = 'Citadel' 
                AND [BuildingLevel] = 2 
                AND ( POU.[MediaSource] = P.[MediaSource]  OR ( POU.[MediaSource] IS NULL AND P.[MediaSource] IS NULL ) ) 
                AND POU.[RegistrationCountry] = P.[RegistrationCountry] 
            ) AS 'Level 2'
             , (SELECT Count(DISTINCT B.[UserId]) 
                FROM   [dbo].[Building] AS B 
                INNER JOIN POOL_OF_USERS AS POU 
                        ON B.[UserId] = POU.[UserId] 
                WHERE  [BuildingId] = 'Citadel' 
                AND [BuildingLevel] = 3 
                AND ( POU.[MediaSource] = P.[MediaSource]  OR ( POU.[MediaSource] IS NULL AND P.[MediaSource] IS NULL ) ) 
                AND POU.[RegistrationCountry] = P.[RegistrationCountry] 
            ) AS 'Level 3'  
            , (SELECT Count(DISTINCT B.[UserId]) 
                FROM   [dbo].[Building] AS B 
                INNER JOIN POOL_OF_USERS AS POU 
                        ON B.[UserId] = POU.[UserId] 
                WHERE  [BuildingId] = 'Citadel' 
                AND [BuildingLevel] = 4 
                AND ( POU.[MediaSource] = P.[MediaSource]  OR ( POU.[MediaSource] IS NULL AND P.[MediaSource] IS NULL ) ) 
                AND POU.[RegistrationCountry] = P.[RegistrationCountry] 
            ) AS 'Level 4'  
            , (SELECT Count(DISTINCT B.[UserId]) 
                FROM   [dbo].[Building] AS B 
                INNER JOIN POOL_OF_USERS AS POU 
                        ON B.[UserId] = POU.[UserId] 
                WHERE  [BuildingId] = 'Citadel' 
                AND [BuildingLevel] = 5 
                AND ( POU.[MediaSource] = P.[MediaSource]  OR ( POU.[MediaSource] IS NULL AND P.[MediaSource] IS NULL ) ) 
                AND POU.[RegistrationCountry] = P.[RegistrationCountry] 
            ) AS 'Level 5'      
             , (SELECT Count(DISTINCT B.[UserId]) 
                FROM   [dbo].[Building] AS B 
                INNER JOIN POOL_OF_USERS AS POU 
                        ON B.[UserId] = POU.[UserId] 
                WHERE  [BuildingId] = 'Citadel' 
                AND [BuildingLevel] = 10 
                AND ( POU.[MediaSource] = P.[MediaSource]  OR ( POU.[MediaSource] IS NULL AND P.[MediaSource] IS NULL ) ) 
                AND POU.[RegistrationCountry] = P.[RegistrationCountry] 
            ) AS 'Level 10'       
        FROM  POOL_OF_USERS AS P
        GROUP  BY [MediaSource] 
                ,[RegistrationCountry] 
        ORDER  BY [MediaSource] 
                ,[RegistrationCountry] 

