 DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform int

SET @startperiod = '2019-08-14 00:00:00.000';
SET @endperiod = '2019-11-14 23:59:59.999';
SET @platform = 2;

 
With lvls as (
    SELECT DISTINCT UserId as U_ID
    FROM  dbo.[EventsLevelAttempts]
    WHERE  LevelId > 30	
    AND [Platform] = @platform
)

SELECT Convert(date, Timestamp)
, Count (DISTINCT UserID) AS Users
 FROM  dbo.[EventsAppOpen] AS ao INNER JOIN lvls ON lvls.U_ID = ao.UserId
    WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
    AND [Platform] = @platform
GROUP BY Convert(date, Timestamp)
ORDER BY Convert(date, Timestamp)


