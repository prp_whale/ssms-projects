WITH POOL_OF_USERS
AS (
 
    SELECT UserId AS U_ID
    --SELECT TOP (100000) UserId AS U_ID
    FROM dbo.Users
    WHERE [RegistrationClientVersion] IN ('0.0.105', '0.0.108', '0.0.124','0.0.177', '0.0.189')
  --  AND Platform = 2
    AND Created < '2020-02-19'
)
--SELEcT Count(*) FROM POOL_OF_USERS
-- SELEcT Count (*)
-- FROM POOL_OF_USERS LEFT JOIN  dbo.[UiButtonStatus] AS US  ON U_ID = UserId 
--     AND ButtonName = 'CameraBtn'
-- WHERE UserId IS NULL
, ClicksBtn
AS (
    SELECT UserId
        , ButtonStatus
        ,[Timestamp]
        , [AccountLevel]
    FROM dbo.[UiButtonStatus] INNER JOIN POOL_OF_USERS ON U_ID = UserId 
    AND ButtonName = 'CameraBtn'
    
)
, UsersAndClicks
AS (
    SELECT UserId
    , Count(*)  AS Clicks
    FROM ClicksBtn
    GROUP BY UserID
)

, LevelsAndClicks
AS (
    SELECT AccountLevel
    ,CASE 
     WHEN ButtonStatus = 'Orthographic'  THEN 'ALTERNATIVE'
     WHEN ButtonStatus = 'Perspective'  THEN 'STANDART'
     ELSE 'Error'
     END AS ButtonStatus
    , Count(*)  AS Clicks
     , Count(DISTINCT UserId)  AS Users
    FROM ClicksBtn
    GROUP BY AccountLevel
    , ButtonStatus
)
 , ModClicksOver10
 AS (
     SELECT UserId
    ,   Clicks %2  AS 'ModClicks'
    FROM UsersAndClicks
    WHERE Clicks > 10
  )

SELECT *
FROM LevelsAndClicks
ORDER BY ButtonStatus
    , AccountLevel 

-- SELECT  Clicks 
-- , Count(UserId) AS Users
-- FROM UsersAndClicks
-- GROUP BY Clicks
-- HAVING Clicks <= 10
-- ORDER BY Clicks




--  SELECT  ModClicks 
-- , Count(UserId) AS Users
-- FROM ModClicksOver10
-- GROUP BY ModClicks
