declare @days int = 28;

declare @days_multi int = 3;
declare @st_per DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days*@days_multi, GETDATE())));
declare @end_per DATETIME  = DATEADD(ms, -3, CONVERT(datetime, CONVERT(date, GETDATE())));

declare @versa_limit int = 1000;


WITH Versa
AS (
        SELECT ClientVersion AS Ver
        FROM  dbo.EventsAppOpen
        WHERE [Timestamp] BETWEEN @st_per AND @end_per 
        AND ClientVersion IS NOT NULL
        GROUP BY ClientVersion
        HAVING Count(*) >=  @versa_limit
)



    SELECT CONVERT(date, [TimeStamp]) AS 'DATE', ClientVersion, level_id, bought_before, banner_id, icon_id, is_auto, purchase_name, price
        ,CASE   WHEN ltv > 0 THEN 1 ELSE 0 END AS 'IsPay'
        , Count(Distinct UserID) AS Users
        , Count(Distinct SessionId) AS Sessionss
        , Count(UserID) AS Events
        --, AVG(time_to_end) as SecondsLeft
    FROM trigger_purchase 
    WHERE ClientVersion IN (SELECT * FROM Versa)
    AND [Timestamp] BETWEEN @st_per AND @end_per
    AND [level_id] IS NOT NULL
    AND result = 'ProductCredited'
    GROUP BY CONVERT(date, [TimeStamp]) , ClientVersion,  level_id, bought_before, banner_id, icon_id, is_auto, purchase_name, price
    ,CASE    WHEN ltv > 0 THEN 1 ELSE 0 END 
    ORDER BY CONVERT(date, [TimeStamp]) , ClientVersion,  level_id, bought_before, banner_id, icon_id, is_auto, purchase_name, price



