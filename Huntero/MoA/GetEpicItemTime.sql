/*сколько часов тратит в среднем, чтобы получить первую зеленую/синюю/фиолетовую вещь?*/
DECLARE @startperiod DATETIME = '2020-02-21 00:00:00.000';
DECLARE @endperiod DATETIME = '2020-02-25 23:59:59.997';

;WITH POOL_OF_USERS
AS (
 
     SELECT UserId AS U_ID
    -- SELECT TOP (100) UserId AS U_ID
    FROM dbo.Users
    -- WHERE [RegistrationClientVersion] IN ('0.0.105', '0.0.108', '0.0.124','0.0.177', '0.0.189')
    -- AND Created < '2020-02-23'
    WHERE [RegistrationClientVersion] = '0.0.124'
    AND Created BETWEEN @startperiod AND @endperiod
    AND dbo.getchecksum(advertisingid) % 4 in (3,2)
) 
,CTE2
AS (
    SELECT DISTINCT UserID
    , Created
    , (Select MIN([Timestamp]) FROM EquipmentMovement AS em WHERE  EquipmentValuable = 'epic' AND em.UserId = EMM.UserID) AS 'FirstEpic'
    , (Select MIN([Timestamp]) FROM EquipmentMovement AS em WHERE  EquipmentValuable = 'rare' AND em.UserId = EMM.UserID) AS 'FirstRare'
    , (Select MIN([Timestamp]) FROM EquipmentMovement AS em WHERE  EquipmentValuable = 'uncommon' AND em.UserId = EMM.UserID) AS 'FirstUncommon'
    FROM EquipmentMovement AS EMM INNER JOIN POOL_OF_USERS ON UserId = U_ID
    
),
CTE3
AS (
    SELEcT UserID
    , DATEDIFF(ss, Created, [FirstUncommon] )AS 'uncommon_sec'
    , DATEDIFF(ss, Created, [FirstRare] )    AS 'rare_sec'
    , DATEDIFF(ss, Created, [FirstEpic] )    AS 'epic_sec'
    FROM CTE2 
    WHERE FirstUncommon IS NOT NULL 
    AND FirstRare IS NOT NULL
    AND FirstEpic IS NOT NULL
),
 
CTE4
AS (
    SELECT TOP(1)
     PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY uncommon_sec) OVER( ) as Q1_uncommon_sec
    , PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY uncommon_sec) OVER( ) as median_uncommon_sec
    , PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY uncommon_sec) OVER( ) as Q3_uncommon_sec

    , PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY rare_sec) OVER() as Q1_rare_sec
    , PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY rare_sec) OVER() as median_rare_sec
    , PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY rare_sec) OVER() as Q3_rare_sec

    , PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY epic_sec) OVER() as Q1_epic_sec
    , PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY epic_sec) OVER() as median_epic_sec
    , PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY epic_sec) OVER() as Q3_epic_sec
    
    FROM CTE3
)


SELECT 
  Q1_uncommon_sec       *1.0/3600 AS 'TOP 25%' -- Green
, median_uncommon_sec   *1.0/3600 AS 'Median Hours' -- Green
, Q3_uncommon_sec       *1.0/3600 AS 'TOP 75%' -- Green
FROM CTE4 
UNION ALL
SELECT 
 Q1_rare_sec         *1.0/3600 AS 'Blue Q1 Hours'
, median_rare_sec       *1.0/3600 AS 'Blue Median Hours'
, Q3_rare_sec           *1.0/3600 AS 'Blue Q3 Hours'
FROM CTE4 
UNION ALL
SELECT
  Q1_epic_sec           *1.0/3600 AS 'Purple Q1 Hours'
, median_epic_sec       *1.0/3600 AS 'Purple Median Hours'
, Q3_epic_sec           *1.0/3600 AS 'Purple Q3 Hours'
 FROM CTE4
 UNION ALL
SELECT 
Count(Distinct UserId), (SELECT COunt(*) FROM POOL_OF_USERS),'' 
FROM CTE3


-- SELECT 
--   Q1_uncommon_sec       *1.0/3600 AS 'Green Q1 Hours'
-- , median_uncommon_sec   *1.0/3600 AS 'Green Median Hours'
-- , Q3_uncommon_sec       *1.0/3600 AS 'Green Q3 Hours'

-- ,Q1_rare_sec            *1.0/3600 AS 'Blue Q1 Hours'
-- , median_rare_sec       *1.0/3600 AS 'Blue Median Hours'
-- , Q3_rare_sec           *1.0/3600 AS 'Blue Q3 Hours'

-- ,Q1_epic_sec            *1.0/3600 AS 'Purple Q1 Hours'
-- , median_epic_sec       *1.0/3600 AS 'Purple Median Hours'
-- , Q3_epic_sec           *1.0/3600 AS 'Purple Q3 Hours'
--  FROM CTE4