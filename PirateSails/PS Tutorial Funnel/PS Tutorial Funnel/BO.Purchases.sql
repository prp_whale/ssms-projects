DECLARE @start_registration DATETIME = '2019-05-01 00:00:00.000';
DECLARE @end_registration DATETIME = '2019-08-01 23:59:59.997';

-- DECLARE @start_registration DATETIME = '2019-07-31 00:00:00.000';
-- DECLARE @end_registration DATETIME = '2019-07-31 23:59:59.997';

DECLARE @revenue_day  int = 1;
DECLARE @platform INt = 2 ;


WITH U_PUR
AS (
    SELECT 
    DISTINCT [UserId] 
       ,(SELECT Sum([AmountInUsd]) 
         FROM   [dbo].[EventsPurchase] as E 
         WHERE  EPC.[UserId] = E.[UserId] 
                 AND [Timestamp] BETWEEN [Created] AND  Dateadd (day, @revenue_day, E.[Created])) AS 'D_LTV' 
    FROM [dbo].[eventspurchase]  AS EPC
    WHERE [Created] BETWEEN @start_registration AND @end_registration
    AND Platform = @platform
)

SELECT Sum(D_LTV*0.7) AS Revenue
FROM U_PUR