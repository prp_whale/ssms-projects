SELECT [eventid], 
       [dbo].[eventsinfo].[name], 
       [dbo].[eventsparamsinfo].[name] AS TypeName, 
       [type], 
       [typenumber] 
FROM   [dbo].[eventsinfo] 
       LEFT JOIN [dbo].[eventsparamsinfo] 
              ON [dbo].[eventsinfo].[id] = [dbo].[eventsparamsinfo].[eventid] 
ORDER  BY id, 
          [type], 
          typenumber 