DECLARE @start_date DATETIME = '2019-02-11 00:00:00'; 
DECLARE @end_date DATETIME = '2019-02-24 23:59:59'; 
DECLARE @cheater INT = 7249; 

SELECT DISTINCT[userid], 
               [is_depositor], 
               [ltv] 
FROM   [dbo].[balance] 
WHERE  [registrationcountry] != 'UA' 
       AND [timestamp] BETWEEN @start_date AND @end_date 
       AND [userid] != @cheater 
       AND [is_depositor] = 0 
       AND ltv > 0 
ORDER  BY ltv DESC 