DECLARE @start_registration DATETIME = '2019-03-01 00:00:00.000';
DECLARE @end_registration DATETIME = '2019-08-01 23:59:59.997';

SELECT L.* 
,All_Users
,L1.[Leavers] AS leave_month_1
,L2.[Leavers] AS leave_month_2
,L3.[Leavers] AS leave_month_3
,L4.[Leavers] AS leave_month_4
,L5.[Leavers] AS leave_month_5
,L6.[Leavers] AS leave_month_6
,L7.[Leavers] AS leave_month_7
,[All_Leavers]*1.0 / [All_Users] AS churn
,L1.[Leavers]*1.0 / U1.[Users] AS churn_month_1
,L2.[Leavers]*1.0 / U2.[Users] AS churn_month_2
,L3.[Leavers]*1.0 / U3.[Users] AS churn_month_3
,L4.[Leavers]*1.0 / U4.[Users] AS churn_month_4
,L5.[Leavers]*1.0 / U5.[Users] AS churn_month_5
,L6.[Leavers]*1.0 / U6.[Users] AS churn_month_6
,L7.[Leavers]*1.0 / U7.[Users] AS churn_month_7
FROM
(SELECT [ClientVersion]
, Count(*) AS All_Leavers
FROM PRP_Pool_Of_Deviators_Android
GROUP BY [ClientVersion]
 ) AS L
INNER JOIN 
(SELECT [ClientVersion]
, Count(*) AS All_Users
FROM PRP_Pool_Of_Users_Android
GROUP BY [ClientVersion]
HAVING Count(*) > 5) AS U
ON U.[ClientVersion] =  L.[ClientVersion]

LEFT JOIN
(SELECT [ClientVersion]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN @start_registration AND DATEADD(mm,1,@start_registration)
GROUP BY [ClientVersion]
 ) AS L1 ON L.[ClientVersion] = L1.[ClientVersion]
LEFT JOIN 
(SELECT [ClientVersion]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN @start_registration AND DATEADD(mm,1,@start_registration)
GROUP BY [ClientVersion]
) AS U1
ON L.[ClientVersion] = U1.[ClientVersion] 

LEFT JOIN
(SELECT [ClientVersion]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,1,@start_registration) AND DATEADD(mm,2,@start_registration)
GROUP BY [ClientVersion]
 ) AS L2 ON L.[ClientVersion] = L2.[ClientVersion]
LEFT JOIN 
(SELECT [ClientVersion]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,1,@start_registration) AND DATEADD(mm,2,@start_registration)
GROUP BY [ClientVersion]
) AS U2
ON L.[ClientVersion] = U2.[ClientVersion] 

LEFT JOIN
(SELECT [ClientVersion]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,2,@start_registration) AND DATEADD(mm,3,@start_registration)
GROUP BY [ClientVersion]
 ) AS L3 ON L.[ClientVersion] = L3.[ClientVersion]
LEFT JOIN 
(SELECT [ClientVersion]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,2,@start_registration) AND DATEADD(mm,3,@start_registration)
GROUP BY [ClientVersion]
) AS U3
ON L.[ClientVersion] = U3.[ClientVersion] 

LEFT JOIN
(SELECT [ClientVersion]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,3,@start_registration) AND DATEADD(mm,4,@start_registration)
GROUP BY [ClientVersion]
 ) AS L4 ON L.[ClientVersion] = L4.[ClientVersion]
LEFT JOIN 
(SELECT [ClientVersion]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,3,@start_registration) AND DATEADD(mm,4,@start_registration)
GROUP BY [ClientVersion]
) AS U4
ON L.[ClientVersion] = U4.[ClientVersion] 

LEFT JOIN
(SELECT [ClientVersion]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,4,@start_registration) AND DATEADD(mm,5,@start_registration)
GROUP BY [ClientVersion]
 ) AS L5 ON L.[ClientVersion] = L5.[ClientVersion]
LEFT JOIN 
(SELECT [ClientVersion]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,4,@start_registration) AND DATEADD(mm,5,@start_registration)
GROUP BY [ClientVersion]
) AS U5
ON L.[ClientVersion] = U5.[ClientVersion] 

LEFT JOIN
(SELECT [ClientVersion]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,5,@start_registration) AND DATEADD(mm,6,@start_registration)
GROUP BY [ClientVersion]
 ) AS L6 ON L.[ClientVersion] = L6.[ClientVersion]
LEFT JOIN 
(SELECT [ClientVersion]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,5,@start_registration) AND DATEADD(mm,6,@start_registration)
GROUP BY [ClientVersion]
) AS U6
ON L.[ClientVersion] = U6.[ClientVersion] 

LEFT JOIN
(SELECT [ClientVersion]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,7-1,@start_registration) AND DATEADD(mm,7,@start_registration)
GROUP BY [ClientVersion]
 ) AS L7 ON L.[ClientVersion] = L7.[ClientVersion]
LEFT JOIN 
(SELECT [ClientVersion]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,7-1,@start_registration) AND DATEADD(mm,7,@start_registration)
GROUP BY [ClientVersion]
) AS U7
ON L.[ClientVersion] = U7.[ClientVersion] 
ORDER BY All_Leavers DESC
