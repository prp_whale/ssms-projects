/*
Acceleration
*/

declare @days int = 28;

declare @days_multi int = 1;
declare @st_per DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days*@days_multi, GETDATE())));
declare @end_per DATETIME  = DATEADD(ms, -3, CONVERT(datetime, CONVERT(date, GETDATE())));

declare @versa_limit int = 100;


WITH Versa
AS (
    SELECT registrationclientversion AS RegVer
    FROM  dbo.Users
   -- WHERE  created BETWEEN @st_per AND @end_per
    GROUP BY registrationclientversion
    HAVING Count(*) >=  @versa_limit
)


SELECT CONVERT(date, [TimeStamp]) AS 'Date', ClientVersion, LevelId , TriggerId
,CASE   WHEN ltv > 0 THEN 'Depos'  ELSE 'ND'  END AS 'IsPay'

, Count(Distinct UserID) AS Users
, Count(Distinct SessionId) AS Sessionss
, Count(UserID) AS Events
, Sum(ProductAmount) AS CashSpended
FROm EventsProductMovement
 
WHERE ClientVersion IN (SELECT * FROM Versa)
AND [Timestamp] BETWEEN @st_per AND @end_per

AND ProductType = 'cash'
AND [Action] = 'spend'
AND [TriggerSourceId] = 'accelerate'

GROUP BY CONVERT(date, [TimeStamp]) , ClientVersion, LevelId , TriggerId
,CASE   WHEN ltv > 0 THEN 'Depos'  ELSE 'ND'  END

ORDER BY TriggerId, CONVERT(date, [TimeStamp]), ClientVersion, LevelId 

