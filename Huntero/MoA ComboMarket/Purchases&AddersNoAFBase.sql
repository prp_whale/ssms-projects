 DECLARE @start_created datetime,
  --      @end_created datetime, 
        @startperiod datetime,
        @endperiod datetime,
		@platform int,
        @days_ago int ,
        @projectName NVARCHAR(20)

SET @days_ago = 1

SET @start_created  = '2020-03-02 00:00:00.000'; -- DONT CHANGE
--SET @end_created = '2020-04-01 00:00:00.000'; -- fisrt day of month
--SET @end_created = DATEADD(ms,   -3, @end_created); -- day before


SET @startperiod = '2020-03-02 00:00:00.000';
--SET @endperiod = DATEADD(dd, 14 , @startperiod) -- next week
--SET @endperiod =  DATEADD(ms,   -3, @endperiod); -- day before

SET @endperiod = DATEADD(ms,   -3, DATEADD(dd,  DATEDIFF(dd,0,GETDATE()),-@days_ago));
SET @endperiod  = '2020-04-12 23:59:59.997';
--SET @endperiod = DATEADD(dd,  DATEDIFF(dd,0,GETDATE()),-@days_ago);
SET @projectName = 'Hunter';

--SET @platform = 2;

WITH PoolOFUsers
AS (

    SELECT UserId  AS U_ID
        , [Created] AS InstallTime
        , AdvertisingId AS Adv_ID
        , Platform
        , ISNULL([MediaSource], 'Organic') AS MediaSource
        , ISNULL([Campaign], 'None') AS Campaign
        FROM dbo.[Users] as u  
        WHERE [Created] >= @start_created 
)


, Purchasers AS (
    SELEcT 
    Convert(date, InstallTime) AS IntsalledDate
    , MediaSource
    , Campaign
    , PoU.Platform
    , DATEDIFF(dd, InstallTime, Timestamp ) AS DayOfLife
    , Sum(PaymentsCount) AS TotalPayments
    , Sum(AmountInUsd )* 0.7 AS IAPDailyNetRevenue

    FROM EventsPurchase AS P 
        INNER JOIN PoolOFUsers AS PoU ON PoU.U_ID = P.UserId AND PoU.Platform = P.Platform
    WHERE DATEDIFF(dd, InstallTime, Timestamp ) >= 0
    AND [Timestamp] BETWEEN @startperiod AND @endperiod
    GROUP BY PoU.Platform,  Convert(date, InstallTime), MediaSource, Campaign , DATEDIFF(dd, InstallTime, Timestamp ) 
)

,Aders
AS 
(
    SELECT     
     Convert(date, InstallTime) AS IntsalledDate   
    , MediaSource
    , Campaign
    , PoU.Platform
    , DATEDIFF(dd, InstallTime, ad.Date ) AS DayOfLife
    , Sum (Revenue) AdDailyNetRevenue 
    FROM [dbo].[ISUserAdRevenue] as  ad
        INNER JOIN PoolOFUsers AS PoU  ON PoU.Adv_ID = ad.AdvertisingId AND PoU.Platform = ad.Platform  AND  ad.ProjectName = @projectName
    WHERE DATEDIFF(dd, InstallTime, ad.Date ) >= 0
        AND ad.Date BETWEEN @startperiod AND @endperiod
    GROUP BY PoU.Platform,  Convert(date, InstallTime), MediaSource, Campaign , DATEDIFF(dd, InstallTime, ad.Date ) 
)



SELECT ISNULL(a.IntsalledDate, p.IntsalledDate) AS 'InstalledDates'
, ISNULL(a.MediaSource, p.MediaSource) AS 'MediaSource'
, ISNULL(a.Campaign, p.Campaign) + '   [' + ISNULL(a.MediaSource, p.MediaSource)   + '][Platform: ' +  ISNULL(a.Platform, p.Platform)  +']'   AS 'Campaign' 
--, ISNULL(a.Campaign, p.Campaign) AS 'Campaign'
, ISNULL(a.Platform, p.Platform) AS 'Platform' 
, ISNULL(a.DayOfLife, p.DayOfLife) AS 'DayOfLife'
, ISNULL(p.IAPDailyNetRevenue, 0) AS 'IAPDailyNetRevenue' 
, ISNULL(a.AdDailyNetRevenue, 0) AS 'AdDailyNetRevenue'
, Convert(date,@endperiod) AS 'LastDataUpdate'

FROM Purchasers AS p FULL OUTER JOIN Aders AS a
    ON p.IntsalledDate = a.IntsalledDate AND 
        p.MediaSource = a.MediaSource AND 
        p.Campaign = a.Campaign AND 
        p.Platform = a.Platform AND
        p.DayOfLife = a.DayOfLife

WHERE ISNULL(p.IAPDailyNetRevenue, 0) > 0  OR ISNULL(a.AdDailyNetRevenue, 0) > 0
ORDER BY [InstalledDates] DESC,  Platform, MediaSource, Campaign, DayOfLife


