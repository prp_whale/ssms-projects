DECLARE @start_period DATETIME = '2019-03-01 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-08-01 23:59:59.997'; 

DROP TABLE IF EXISTS PRP_Pool_Of_Users;

SELECT DISTINCT [userid] 
INTO   PRP_Pool_Of_Users 
FROM   [dbo].[users] 
WHERE  [created] BETWEEN @start_period AND @end_period 
       AND [registrationcountry] != 'UA' 


SELECT Count (*) AS U FROM PRP_Pool_Of_Users