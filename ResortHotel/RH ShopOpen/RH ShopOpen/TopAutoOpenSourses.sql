--SELECT [trigger_source_id]            AS Sourse 
--     , [trigger_id] AS Triger
--       ,Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)                 AS Amount 
--FROM   [dbo].[PRP_ShopPurchase_March_May] 
----WHERE  [is_auto] = 1 
--  -- AND [is_auto] = 1 
--  -- AND [new_depositor] = 0 
--GROUP  BY [trigger_source_id] , [trigger_id]
--ORDER  BY Amount DESC 


DECLARE @start_period DATETIME = '2019-04-09 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-05-09 23:59:59.997'; 

SELECT [trigger_source_id]             AS Sourse 
     --, [trigger_id] AS Triger
       ,Count (DISTINCT [UserId]) AS Users 
       ,Count (*)                 AS Amount 
FROM   [dbo].[ev_ShopOpen]
WHERE [Timestamp] BETWEEN @start_period AND @end_period
AND [ClientVersion] = '1.13.4'
    --AND [is_auto] = 0 
  -- AND [trigger_source_id] = 'none'
  -- AND [new_depositor] = 0 
GROUP  BY [trigger_source_id]  --, [trigger_id]
ORDER  BY Amount DESC 

