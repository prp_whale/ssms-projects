 DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform int,
        @days_ago int ,
        @projectName NVARCHAR(20)

SET @days_ago = 1
SET @startperiod = '2020-01-06 00:00:00.000';

--SET @endperiod = '2020-02-12 23:59:59.997';
SET @endperiod = DATEADD(ms,   -3, DATEADD(dd,  DATEDIFF(dd,0,GETDATE()),-@days_ago));
SET @projectName = 'Hunter';

--SET @platform = 2;

SELECT   [Date]
       ,[MediaSource]
       , [Ad]  + '   [' + [MediaSource]  + '][Platform: ' +  [Platform] +']'   AS 'Ad'
       , [Platform]
       , SUm([TotalCost]) AS SumCosts
        , SUm([Impressions]) AS SumImpressions
        , SUm([Clicks]) AS SumClicks
        , SUm([Installs]) AS SumInstalls
FROM [dbo].[AFPartnersDailyReport]
where     projectname = @projectName
AND Date BETWEEN @startperiod AND @endperiod
AND MediaSource = 'unityads_int'
GROUP BY  [Date] ,[MediaSource] ,[Ad], [Platform]
ORDER BY  [Platform], [Date] ,[MediaSource] ,[Ad]



