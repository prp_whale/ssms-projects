declare @days int = 28;

declare @days_multi int = 3;
declare @st_per DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days*@days_multi, GETDATE())));
declare @end_per DATETIME  = DATEADD(ms, -3, CONVERT(datetime, CONVERT(date, GETDATE())));

declare @versa_limit int = 1000;


WITH Versa
AS (
        SELECT ClientVersion AS Ver
        FROM  dbo.EventsAppOpen
        WHERE [Timestamp] BETWEEN @st_per AND @end_per 
        AND ClientVersion IS NOT NULL
        GROUP BY ClientVersion
        HAVING Count(*) >=  @versa_limit
)

, Opens
AS (
    SELECT CONVERT(date, [TimeStamp]) AS 'Date', ClientVersion, level_id, bought_before, banner_id, icon_id, is_auto
        ,CASE   WHEN ltv > 0 THEN 1 ELSE 0 END AS 'IsPay'
        , Count(Distinct UserID) AS Users
        , Count(Distinct SessionId) AS Sessionss
        , Count(UserID) AS Events

    FROM trigger_open 
    WHERE ClientVersion IN (SELECT * FROM Versa)
    AND [Timestamp] BETWEEN @st_per AND @end_per
    AND [level_id] IS NOT NULL

    GROUP BY CONVERT(date, [TimeStamp]) , ClientVersion,  level_id, bought_before, banner_id, icon_id, is_auto
    ,CASE    WHEN ltv > 0 THEN 1 ELSE 0 END 
)

, Purchases
AS (
    SELECT CONVERT(date, [TimeStamp]) AS 'Date', ClientVersion, level_id, bought_before, banner_id, icon_id, is_auto
        ,CASE   WHEN ltv > 0 THEN 1 ELSE 0 END AS 'IsPay'
        , Count(Distinct UserID) AS Users
        , Count(Distinct SessionId) AS Sessionss
        , Count(UserID) AS Events

    FROM trigger_purchase 
    WHERE ClientVersion IN (SELECT * FROM Versa)
    AND [Timestamp] BETWEEN @st_per AND @end_per
    AND [level_id] IS NOT NULL
    AND result = 'ProductCredited'
    GROUP BY CONVERT(date, [TimeStamp]) , ClientVersion,  level_id, bought_before, banner_id, icon_id, is_auto
    ,CASE    WHEN ltv > 0 THEN 1 ELSE 0 END 
)


SELECT o.DATE
, o.ClientVersion
, o.level_id
, o.IsPay
, o.bought_before
, o.banner_id
, o.icon_id
, o.is_auto
, o.Users as 'Open Users'
, o.Sessionss as 'Open Sessionss'
, o.Events as 'Open Events'
, ISNULL ( p.Users, 0) as 'Purchase Users'
, ISNULL ( p.Sessionss, 0) as 'Purchase Sessionss'
, ISNULL ( p.Events, 0) as 'Purchase Events'
FROM Opens AS o 
LEFT JOIN Purchases AS P 
    ON o.[Date] =p.[Date] AND o.ClientVersion = p.ClientVersion AND o.level_id = p.level_id AND o.IsPay = p.IsPay
        AND o.bought_before = p.bought_before AND o.banner_id = p.banner_id AND o.icon_id = p.icon_id AND o.is_auto = p.is_auto
          
ORDER BY o.DATE
, o.ClientVersion
, o.level_id
, o.IsPay
, o.bought_before
, o.banner_id
, o.icon_id
, o.is_auto

