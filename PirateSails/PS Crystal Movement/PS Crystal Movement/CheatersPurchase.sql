

WITH PURCHASE99
AS (
   SELECT [UserId]
      ,[Timestamp]
      ,[ClientTime]
      ,[BackOfficeTime]
      ,[Order]
      ,[ClientVersion]
      ,[IsOffline]
      ,[Amount]
      ,[AmountInUSD]
      ,[Currency]
      ,[FirstDepositThisDay]
      ,[ProductId]
      ,[SecondDepositThisDay]
      ,[FirstDeposit]
      ,[ReturnToDeposit]
      ,[ReturnToDepositWeek]
      ,[ReturnToDepositMonth]
      ,[FirstDepositDate]
      ,[PaymentsCount]
      ,[Created]
      ,[Country]
      ,[Platform]
      ,[TransactionId]
      ,[SessionId]
  FROM [dbo].[EventsPurchase]
  WHERE [AmountInUSD] = 99.99


)


--SELECT Count (DISTINCT [UserId]) AS U 
--       ,Count(*)           AS A 
--FROM   PURCHASE99 
SELECT * FROM PURCHASE99
ORDER BY [Timestamp]

SELECT *
FROM dbo.Users
WHERE UserId = 8080