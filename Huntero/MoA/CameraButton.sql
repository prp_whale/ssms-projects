
WITH POOL_OF_USERS
AS (
   SELECT 35945 AS U_ID FROM dbo.Users
    -- SELECT TOP (10) UserId AS U_ID
    -- FROM dbo.Users
    -- WHERE [RegistrationClientVersion] IN ('0.0.105', '0.0.108')
)

, ALL_RUN_AND_CLICK AS (
    SELECT UserId
    ,'StartRun' AS 'Action'
    ,[Timestamp]
    , [AccountLevel]
    FROM dbo.[StartRun] INNER JOIN POOL_OF_USERS ON U_ID = UserId
    
    UNION
    SELECT UserId
    ,'FinishRun'
    ,[Timestamp]
    , [AccountLevel]
    FROM dbo.[FinishRun] INNER JOIN POOL_OF_USERS ON U_ID = UserId
         AND  RoomName != '-2'
    UNION
    SELECT UserId
    , ButtonStatus
    ,[Timestamp]
    , [AccountLevel]
    FROM dbo.[UiButtonStatus] INNER JOIN POOL_OF_USERS ON U_ID = UserId 
        AND ButtonName = 'CameraBtn'
     
   -- ORDER BY UserId, [Timestamp]
)
, STEPS_ACTION AS (
    SELECT UserId , [Timestamp]
    , LAG (Action) OVER (PARTITION  BY UserId ORDER BY [Timestamp]) AS PrewAction
    ,[Action]
    , LEAD (Action) OVER (PARTITION  BY UserId ORDER BY [Timestamp]) AS NextAction
    , DATEDIFF(ss, [Timestamp], LEAD (Timestamp) OVER (PARTITION  BY UserId ORDER BY [Timestamp])) AS Sec
    , [AccountLevel] 
    FROM ALL_RUN_AND_CLICK
)
, FILTERED_ACTION AS  (
    SELECT   UserId , [Timestamp]
    , PrewAction
    ,CASE 
        WHEN [Action] = 'FinishRun'                                      THEN NextAction 
        WHEN [PrewAction] IS NULL   AND [Action] = 'StartRun'            THEN 'Perspective'
        WHEN [Action] = 'StartRun'  AND [NextAction] =  'Orthographic'   THEN 'Perspective'
        WHEN [Action] = 'StartRun'  AND [NextAction] = 'Perspective'     THEN 'Orthographic'
    ELSE [Action]
    END AS 'Action'
    , NextAction 
    , Sec
    , AccountLevel
    FROM STEPS_ACTION
    WHERE NextAction != 'StartRun'
)

, RECC
AS (
    SELECT 
     UserId , [Timestamp]
    , LAG([Action]) OVER (PARTITION  BY (UserId) ORDER BY [Timestamp]) AS 'PrewAction'
    , [Action]
    FROM FILTERED_ACTION
   
)

--If(OBJECT_ID('tempdb..#temp_click') Is Null)
--Begin
   SELECT TOP(50) * 
   INTO #temp_click
   FROM RECC
 --  ORDER BY [Timestamp]
--End


SELEcT * FROM #temp_click
ORDER BY [Timestamp]

DECLARE @temp VARCHAR(100)
DECLARE @i int
;WHILE (i < Count(*) FROM #temp_click )
BEGIN
    SET @temp = [Action]
    
END 


;WHILE (SELEcT COUNT ([Action])  FROM #temp_click WHERE [Action] = 'StartRun') > 0
BEGIN
        UPDATE #temp_click
        SET [Action] =  CASE 
            WHEN [Action] = 'StartRun'   THEN PrewAction 
            ELSE [Action] 
            END
        , PrewAction = Pew 
        (
         SELECT LAG([Action]) OVER (PARTITION  BY (UserId) ORDER BY [Timestamp]) AS 'Pew'
         FROM FILTERED_ACTION
        ) 
END 

UPDATE  UpdateTarget
SET Avg14GreenP = Displaced
    FROM 
    (SELECT 
    a.Avg14GreenP, 
    LAG(a.Avg14Green) OVER (PARTITION BY a.Ticker ORDER BY a.[Date] 
    )  AS Displaced
    FROM Avg14RSI a) 
AS UpdateTarget;




-- If(OBJECT_ID('tempdb..#temp_click') Is Not Null)
 --Begin Drop Table #temp_click End


    -- INSERT #temp_dates([date]) SELECT d FROM ( 
    --     SELECT d = DATEADD(DAY, rn - 1, @startperiod) FROM  (
    --             SELECT TOP (DATEDIFF(DAY, @startperiod, @endperiod+1)) rn = ROW_NUMBER() OVER (ORDER BY s1.[object_id])
    --             FROM sys.all_objects AS s1 CROSS JOIN sys.all_objects AS s2
    --             ORDER BY s1.[object_id] ) AS x ) AS y;

-- SELECT [Int1] AS 'AccountLevel'
-- , Count(Distinct UserId) AS Users
-- , Count(*) AS Clicks
-- FROM Events
-- WHERE EventId = 9
-- AND [String2] = 'CameraBtn'
-- AND ClientVersion IN ('0.0.105', '0.0.108')
-- GROUP BY [Int1]
-- ORDER BY [Int1]