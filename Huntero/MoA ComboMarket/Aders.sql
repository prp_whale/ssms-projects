 DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform int

SET @startperiod = '2020-01-07 00:00:00.000';
SET @endperiod = '2020-01-08 23:59:59.999';
SET @platform = 2;

WITH PoolOfUsers
AS (
    SELECT u.UserId  AS U_ID
    , CONVERT(date, Created) AS InstallDay
    , Date AS AdViewDate
    , ad.AdvertisingID
    , u.MediaSource
    , u.Campaign
    , Impressions
    , Revenue AS AdRevenue
    , ad.Platform
    FROm [dbo].[ISUserAdRevenue] as ad INNER JOIN dbo.Users as u 
        ON u.AdvertisingId = ad.AdvertisingId AND u.Platform = ad.Platform  AND ProjectName = 'Hunter' 
    AND Created BETWEEN @startperiod  AND @endperiod
)

-- SELECT InstallDay AS CohortInstallDay
-- , Dateadd(dd, 2, InstallDay)
-- , DATEDIFF(dd, InstallDay, AdViewDate ) AS DayOfLife
-- , ISNULL(MediaSource, 'Organic') AS MediaSources
-- , MediaSource
-- , Campaign
-- , MediaSource + '|' + Campaign AS Campaigns
-- , Sum (AdRevenue)
-- FROM PoolOfUsers
-- WHERE Dateadd(dd, 3, InstallDay) = AdViewDate
-- GROUP BY InstallDay, MediaSource, Campaign, AdViewDate
-- ORDER BY InstallDay, MediaSource, Campaign


SELECT InstallDay AS CohortInstallDay
, ISNULL(MediaSource, 'Organic') AS MediaSource
, ISNULL(Campaign, 'None') AS Campaign
, MediaSource + '|' + Campaign AS ExtCampaign
, DATEDIFF(dd, InstallDay, AdViewDate ) AS DayOfLife
, Sum (AdRevenue) AS RevenueInThisDay
, Platform
FROM PoolOfUsers
WHERE Dateadd(dd, 3, InstallDay) = AdViewDate
GROUP BY InstallDay, Platform, MediaSource, Campaign, AdViewDate 
ORDER BY InstallDay, MediaSource, Campaign


SELECT TOp (100) * FROM [dbo].[ISUserAdRevenue] 