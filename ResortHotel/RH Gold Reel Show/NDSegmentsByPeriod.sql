DECLARE @startperiod datetime,
        @endperiod datetime,
        @segment int,
        @segment_alt int,
        @segment_3 int,
	    	@platform int;
        
    SET @startperiod = '2019-08-15 00:00:00.000';
    SET @endperiod = '2019-11-07 23:59:59.999';
    SET @platform = 1 ;

    SET @segment = 1110;
    SET @segment_alt = 1110  ;
--SELECT  DISTINCT segment_2     FROM [dbo].[OfferPurchase]     WHERE [Timestamp]  BETWEEN @startperiod AND @endperiod


WITH PayersBySegment
AS (
    SELECT  UserId AS U_ID
    FROM [dbo].[OfferPurchase]
    WHERE [Timestamp]  BETWEEN @startperiod AND @endperiod
    AND result = 'OK'
    AND [segment_1] = @segment
    UNION 
    SELECT UserId 
    FROM [dbo].[ShopPurchase]
    WHERE [Timestamp]  BETWEEN @startperiod AND @endperiod
    AND result = 'OK'
    AND [segment_1] = @segment
    UNION 
    SELECT UserId 
    FROM [dbo].[TriggerOfferPurchase]
    WHERE [Timestamp]  BETWEEN @startperiod AND @endperiod
    AND result = 'OK'
    AND [segment] = @segment
    AND [price] > 0
    UNION 
    SELECT UserId 
    FROM [dbo].[MultiOfferPurchase]
    WHERE [Timestamp]  BETWEEN @startperiod AND @endperiod
    AND result = 'OK'
    AND [segment_2] = @segment
    AND  purchase_name != 'multi_offer_free_pack_1'
   )
--SELECT Count (*) FROM PayersBySegment
SELECT t.U AS Active_U
, t2.U  AS Purchase_U
, t3.U  AS Golden_U
, t2.U*1.0 / t.U AS 'P/A'
, t3.U*1.0 / t.U AS 'P/A'
, t3.U*1.0 / t2.U AS 'G/P'
FROM 
(
Select '1' as link 
 ,Count(Distinct UserId) AS U
FROM [dbo].[EventsAppOpen] AS T 
        INNER JOIN PayersBySegment AS POU ON T.[UserId] = POU.[U_ID]
WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
) AS t 
INNER JOIN 
(
    Select '1' as link 
    ,Count(Distinct UserId) AS U
    FROM [dbo].[EventsPurchase] AS T 
        INNER JOIN PayersBySegment AS POU ON T.[UserId] = POU.[U_ID]
    WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
)
AS t2 ON t.link = t2.link 
INNER JOIN 
(
    Select '1' as link 
    ,Count(Distinct UserId) AS U
    FROM [dbo].[EventsPurchase] AS T 
        INNER JOIN PayersBySegment AS POU ON T.[UserId] = POU.[U_ID]
    WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
    AND ProductId LIKE 'golden_ors_%'
)
AS t3 ON t.link = t3.link 

