 DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform int

SET @startperiod = '2019-08-14 00:00:00.000';
SET @endperiod = '2019-11-14 23:59:59.999';
SET @platform = 1;

 WITH sgm
 AS (
 SELECT UserId  as U_ID
    , CASE
        WHEN LTV = 0 THEN  'non-depositors'
        WHEN LTV between 0.0001 and 7 THEN  'minnows'
        WHEN LTV between 7.0001 and 70 THEN  'dolphins'
        WHEN LTV between 70.0001 and 300 THEN  'grand dolphins'
        WHEN LTV > 300 THEN  'whales'
        ELSE 'Error occured'
     END AS segment
     , LTV
    FROM [dbo].[Users]
    WHERE [ClientVersion] >= '1.14.7'	 AND [ClientVersion] < '1.2.0'
    AND  [Platform] = @platform
    AND [RegistrationCountry] != 'UA'
     )
, lvls as (
    SELECT DISTINCT UserId 
    FROM  dbo.[EventsLevelAttempts]
    WHERE  LevelId > 30	
    AND [Platform] = @platform
)
, opn AS (
    SELECT DISTINCT UserId 
    FROM  dbo.[EventsAppOpen]
    WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
    AND [Platform] = @platform
)

--SELECT count(* ) FROM lvls

 SELEcT segment 
  , Count(*)  AS Players
  , Sum (LTV) AS Gross
 FROM sgm INNER JOIN opn ON sgm.U_ID = opn.UserId
 INNER JOIN lvls ON lvls.UserId = opn.UseriD
 GROUP BY segment
 ORDER BY Players  desc

