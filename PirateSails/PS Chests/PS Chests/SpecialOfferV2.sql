DECLARE @start_period DATETIME = '2019-04-26 00:00:00.000';
DECLARE @end_period DATETIME = '2019-05-01 23:59:59.997';
 
DECLARE @test_country VARCHAR(2) = 'UA'; -- ��������� ������ �� � �� ������� �� ������ �������
 
DECLARE  @TestDevices TABLE (UserID INT PRIMARY KEY);
INSERT INTO @TestDevices VALUES (6), (14), (23), (105), (239), (247), (256), (257), (333), (1331), (1366), (1367), (1368), (1375), (1381), (1382), (3487), (3737), (4244), (4253), (4255), (4265), (4266), (4270), (5517), (5906), (2), (3), (7), (8), (9), (12), (16), (17), (21), (22), (25), (26), (27), (51), (117), (249), (293), (598), (666), (670), (1190), (1192), (1193), (1337), (5568);
 
DECLARE @bank_price VARCHAR (15) = 'bank%price15';
 
 
WITH POOL_OF_USERS  -- ������ ������������ �� ��������� �����������
AS (
        SELECT U.[UserId] AS ID
        FROM   [dbo].[Users] AS U
               LEFT JOIN @TestDevices AS TD
                      ON U.[UserId] = TD.UserID
        WHERE
            TD.UserID IS NULL -- ��������� ������ �������
        --  AND  [Created] BETWEEN @start_period AND @end_period  --
            AND [RegistrationCountry] != @test_country  -- ��������� ������ �� � �� ������� �� ������ �������
        --    AND [LTV] = 0 -- ��� ����������� �� ������������ �� �� ������������
    )
 
, BANK_BUYERS
AS (
    SELECT [ProductId]
           , [UserId]
           , [TIMESTAMP]
    FROM   [dbo].[EventsPurchase] AS EP
           INNER JOIN POOL_OF_USERS AS POU
                   ON POU.ID = EP.[UserId]
    WHERE  [ProductId] LIKE '%bank%'
    )
,  BANK_BUYERS_CERTAIN_PRICE
AS (
    SELECT [ProductId] AS ProdId
            , [UserId] AS U_ID
    FROM   BANK_BUYERS
    WHERE  [ProductId] LIKE @bank_price
      --  AND [TIMESTAMP] BETWEEN '2019-04-01 00:00:00.000' AND '2019-04-25 23:59:59.997'  -- ����������� ��� ���� ���� ������� ����� ���� � �����
    )
 
SELECT [ProductId] 
       , Count(DISTINCT [UserId])                               AS Users 
       , Count (DISTINCT [TransactionId]) * Max ([AmountInUSD]) AS SumInUSD 
       , Count (DISTINCT [TransactionId])                       AS PaymentsCount 
FROM   [dbo].[EventsPurchase] AS EP 
       INNER JOIN BANK_BUYERS_CERTAIN_PRICE AS BBCP 
               ON BBCP.U_ID = EP.[UserId] 
WHERE  [ProductId] LIKE '%special%' 
--  AND [TIMESTAMP] BETWEEN @start_period AND @end_period -- ����������� ��� ���� ���� ������� ����� special offer  
GROUP  BY [ProductId] 
ORDER  BY paymentscount DESC 

--SELECT * 
--FROM   [dbo].[EventsPurchase] AS EP
--       INNER JOIN BANK_BUYERS_CERTAIN_PRICE AS BBCP
--               ON BBCP.U_ID = EP.[UserId]
--WHERE  [ProductId] LIKE '%special%'
--   AND [TIMESTAMP] BETWEEN @start_period AND @end_period -- ����������� ��� ���� ���� ������� ����� special offer