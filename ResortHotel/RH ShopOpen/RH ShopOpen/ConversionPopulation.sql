DECLARE @start_period DATETIME = '2019-03-09 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-05-09 23:59:59.997'; 


--SELECT Count (DISTINCT [UserId]) AS Depositors 
--       ,Count (*)                AS Amount 
--FROM   [dbo].[ev_PiggyBankPurchase] AS PBP 
--       INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS] AS POAU 
--               ON POAU.U_ID = PBP.[UserId] 
--WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
--   AND [result] = 'OK' 
 --  AND [new_depositor] = 1 


SELECT Count (DISTINCT [UserId]) AS Depositors 
       ,Count (*)                AS Amount 
FROM   [dbo].[ev_OfferPurchase] AS PBP 
       INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS] AS POAU 
               ON POAU.U_ID = PBP.[UserId] 
WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
   AND [result] = 'OK' 
   AND [new_depositor] = 1 

--SELECT Count (DISTINCT [UserId]) AS Depositors 
--       ,Count (*)                AS Amount 
--FROM   [dbo].[ev_ShopPurchase] AS PBP 
--       INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS] AS POAU 
--               ON POAU.U_ID = PBP.[UserId] 
--WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
--   AND [result] = 'OK' 
  -- AND [new_depositor] = 1 



--SELECT [special_offer_available] 
--       ,Count (DISTINCT [UserId]) AS Depositors 
--       ,Count (*)                 AS Amount 
--FROM   [dbo].[ev_PiggyBankPurchase] AS PBP 
--       INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS] AS POAU 
--               ON POAU.U_ID = PBP.[UserId] 
--WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
--   AND [result] = 'OK' 
--   AND [new_depositor] = 1 
--GROUP  BY [special_offer_available] 
--ORDER  BY [special_offer_available] 


--SELECT [special_offer_available] 
--       ,Count (DISTINCT [UserId]) AS Depositors 
--       ,Count (*)                 AS Amount 
--FROM   [dbo].[ev_ShopPurchase] AS PBP 
--       INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS] AS POAU 
--               ON POAU.U_ID = PBP.[UserId] 
--WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
--   AND [result] = 'OK' 
--   AND [new_depositor] = 1  
--GROUP  BY [special_offer_available] 
--ORDER  BY [special_offer_available] 

