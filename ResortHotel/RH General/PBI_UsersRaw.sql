DECLARE @startperiod datetime,
        @endperiod datetime

SET @startperiod =  DATEADD(dd,  DATEDIFF(dd,0,GETDATE())-90,0)
SET @endperiod = DATEADD(ms, -3, DATEADD(dd,  DATEDIFF(dd,0,GETDATE()),0))

;WITH
    appopen
    AS
    (
        SELECT distinct UserId AS U_ID
        FROM EventsAppOpen
        WHERE [Timestamp] between  @startperiod and @endperiod
		and Country != 'ua'
    ) 

select 
convert (date, created) as created
,userid
,RegistrationClientVersion
,ClientVersion
, AdvertisingId
,isnull(RegistrationCountry,'-') as Country
,case when MediaSource is null then 'Organic'
	  when MediaSource  like '%None%' then 'Organic'
	  when MediaSource  like '%ironsource%' then 'Ironsource'
	  when MediaSource  like '%googleadwords%' then 'Google'
	  when MediaSource  like '%Facebook%' then 'Facebook'
	  when MediaSource  like '%fb_rh_play_button%' then 'Facebook'
	  when MediaSource  like '%fbpage%' then 'Facebook'
	  when MediaSource  like '%gurutraff_int%' then 'Gurutraff'
	  when MediaSource  like '%fy_guru%' then 'Gurutraff'
	  when MediaSource  like '%cross_moregames%' then 'Gurutraff' 
	  when MediaSource  like '%vungle_int%' then 'Vungle' 
	  when MediaSource  like '%unityads_int%' then 'Unity' 
	  when MediaSource  like '%adaction_int%' then 'Adaction' 
          when MediaSource  like '%tapjoy_int%' then 'Tapjoy' 
	  when MediaSource  like '%applovin_int%' then 'Applovin' 
          when MediaSource  like '%test%' then 'Organic'
	  when MediaSource  = '' then 'Organic'
	  else 'Other' end as Source

,    case when PLATFORM=1 then 'Android'
     when PLATFORM=2 then 'iOS'
	 else '-' end as OS
from users join appopen on UserId = u_id
where RegistrationCountry != 'ua'

  SELECT distinct AdNetwork   FROM [dbo].[ISUserAdRevenue] 