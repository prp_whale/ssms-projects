declare @st_per DATETIME = '2020-05-05 00:00:00:000';
declare @end_per DATETIME  = '2020-05-05 23:59:59:997';

WITH PoU 
As (
    SELECT TOP (100)  UserId AS U_ID
    FROM dbo.Users 
    WHERE Created BETWEEN @st_per AND @end_per
        AND [Retention1D] IS NULL
        AND [Retention2D] IS NULL
        AND [Retention3D] IS NULL
        AND [Retention4D] IS NULL
        AND [Retention5D] IS NULL
)
, LastEvents
AS (
    SELECT  UserID , [String1] AS S1, EventID , Name as e_name
    ,  DENSE_RANK( ) OVER (PARTITION BY UserID ORDER BY [TIMESTAMP] DESC) AS Rnk
    FROM dbo.Events 
    JOIN EventsInfo ON EventID = ID 
    JOIN PoU ON UserID = U_ID
)

SELECT UserID
, (SELECT e_name FROM LastEvents WHERE UserID = le.UserID AND Rnk = 1 ) AS LastEvent
, (SELECT s1 FROM LastEvents WHERE UserID = le.UserID AND Rnk = 1 ) AS LastEvent
, (SELECT e_name FROM LastEvents WHERE UserID = le.UserID AND Rnk = 2 ) AS EvMinus2
, (SELECT s1 FROM LastEvents WHERE UserID = le.UserID AND Rnk = 2 ) AS EvMinus2
, (SELECT e_name FROM LastEvents WHERE UserID = le.UserID AND Rnk = 3 ) AS EvMinus
, (SELECT s1 FROM LastEvents WHERE UserID = le.UserID AND Rnk = 3 ) AS EvMinus2
FROM LastEvents AS le
GROUP BY UserID


--Count(*) FROM PoU