declare @time_begin datetime = '2019-03-01 00:00:00';
declare @time_end   datetime = '2019-06-13 23:59:59';

declare @date_chage_prices   datetime = '2019-04-19 00:00:00';
declare @date_chage_prices2   datetime = '2019-05-22 00:00:00';

declare @country   varchar(2) = 'RU';



--RU
declare @koef_0_99	float = 0.33;
declare @koef_1_99	float = 0.34;
declare @koef_2_99	float = 0.34;
declare @koef_4_99	float = 0.32;
declare @koef_7_99	float = 0.33;
declare @koef_9_99	float = 0.34;
declare @koef_14_99	float = 0.34;--22 ���
declare @koef_15_99	float = 0.34;
declare @koef_19_99	float = 0.34;
declare @koef_29_99	float = 1.0;
declare @koef_31_99	float = 0.34;
declare @koef_44_99	float = 0.34;--22 ���
declare @koef_49_99	float = 1.0;
declare @koef_59_99	float = 0.34;--22 ���
declare @koef_99_99	float = 0.34;--22 ���


/*--PL
declare @koef_0_99	float = 0.43;
declare @koef_1_99	float = 0.44;
declare @koef_2_99	float = 0.40;
declare @koef_4_99	float = 0.41;
declare @koef_7_99	float = 0.42;
declare @koef_9_99	float = 0.39;
declare @koef_15_99	float = 0.40;
declare @koef_19_99	float = 0.40;
declare @koef_31_99	float = 0.40;
declare @koef_49_99	float = 1.0;
declare @koef_59_99	float = 1.0;
declare @koef_99_99	float = 1.0;*/


WITH ActiveUsersTableTemp as (
	SELECT [UserId] as user_id
			,MIN([FirstDepositDate]) as first_dep_date
			,CONVERT(date, [Timestamp]) as appopen_timestamp

	FROM [dbo].[EventsAppOpen] 
	WHERE [Country] = @country AND [Timestamp] BETWEEN @time_begin AND @time_end AND [Platform] = 1

	GROUP BY CONVERT(date, [Timestamp]), [UserId]
)


,ActiveUsersTable as (
	SELECT appopen_timestamp as _date
			,COUNT(DISTINCT user_id) as active_users
			,COUNT(DISTINCT IIF(first_dep_date <= appopen_timestamp, user_id, null)) as active_dep

	FROM ActiveUsersTableTemp LEFT JOIN  [Users] ON [Users].[UserId] = user_id --AND [MediaSource] IS NULL

	GROUP BY appopen_timestamp
	--ORDER BY CONVERT(date, [Timestamp])
)

,GrossTable as (
	SELECT CONVERT(date, [Timestamp]) as _date
			,COUNT(DISTINCT [EventsPurchase].[UserId]) as depositors
			,COUNT(DISTINCT IIF([FirstDeposit] = 1, [EventsPurchase].[UserId], null)) as new_depositors
			,COUNT([EventsPurchase].[UserId]) as transactions

			,SUM(iif([Amount] = 0.99, IIF([Timestamp] >= @date_chage_prices, [Amount] * @koef_0_99, [Amount]), 0.0)) as product1
			,SUM(iif([Amount] = 1.99, IIF([Timestamp] >= @date_chage_prices, [Amount] * @koef_1_99, [Amount]), 0.0)) as product2
			,SUM(iif([Amount] = 2.99, IIF([Timestamp] >= @date_chage_prices, [Amount] * @koef_2_99, [Amount]), 0.0)) as product3 
			,SUM(iif([Amount] = 4.99, IIF([Timestamp] >= @date_chage_prices, [Amount] * @koef_4_99, [Amount]), 0.0)) as product4 
			,SUM(iif([Amount] = 9.99, IIF([Timestamp] >= @date_chage_prices, [Amount] * @koef_9_99, [Amount]), 0.0)) as product5 
			,SUM(iif([Amount] = 14.99, IIF([Timestamp] >= @date_chage_prices2, [Amount] * @koef_14_99, [Amount]), 0.0)) as product6 
			,SUM(iif([Amount] = 19.99, IIF([Timestamp] >= @date_chage_prices, [Amount] * @koef_19_99, [Amount]), 0.0)) as product7 
			,SUM(iif([Amount] = 29.99, IIF([Timestamp] >= @date_chage_prices, [Amount] * @koef_29_99, [Amount]), 0.0)) as product8
			,SUM(iif([Amount] = 44.99, IIF([Timestamp] >= @date_chage_prices2, [Amount] * @koef_44_99, [Amount]), 0.0)) as product9 
			,SUM(iif([Amount] = 59.99, IIF([Timestamp] >= @date_chage_prices2, [Amount] * @koef_59_99, [Amount]), 0.0)) as product10
			,SUM(iif([Amount] = 99.99, IIF([Timestamp] >= @date_chage_prices2, [Amount] * @koef_99_99, [Amount]), 0.0)) as product11



	FROM [dbo].[EventsPurchase] LEFT JOIN [Users] ON [Users].[UserId] = [EventsPurchase].[UserId]

	WHERE [EventsPurchase].[Country] = @country AND [Timestamp] BETWEEN @time_begin AND @time_end --AND [MediaSource] IS NULL 
	AND [EventsPurchase].[Platform] = 1

	GROUP BY CONVERT(date, [Timestamp])
	--ORDER BY CONVERT(date, [Timestamp])
)
/*-------------------------------------------------------------------------------------------------------------*/
SELECT ActiveUsersTable._date
		,active_users
		,active_dep

		,depositors
		,new_depositors
		,transactions
		--,gross

		,product1
		,product2
		,product3
		,product4

		,product5
		,product6
		,product7
		,product8
		,product9

		,product10
		,product11

FROM GrossTable JOIN ActiveUsersTable ON ActiveUsersTable._date = GrossTable._date

ORDER BY ActiveUsersTable._date