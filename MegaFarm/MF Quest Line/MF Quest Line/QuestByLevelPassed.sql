
DECLARE @start_date Datetime = '2019-02-01 00:00:00.000'; 
DECLARE @end_date Datetime = '2019-02-28 23:59:59.999'; 
DECLARE @test_country Varchar(2) = 'UA'; 


declare @DuplicateTable table(UserID int primary key);
insert into @DuplicateTable values  (7249), (335),	(724),	(746),	(814),	(1091),	(1194),	(1279),	(1343),	(1699),	(1779),	(2049),	(2137),	(2469),	(2667),	(2712),	(2764),	(2799),	(3006),	(3116),	(3148),	(3162),	(3425),	(3527),	(3586),	(3634),	(3639),	(3679),	(3784),	(3794),	(3808),	(3814),	(3866),	(3869),	(3880),	(3918),	(3923),	(3941),	(3994),	(4005),	(4012),	(4019),	(4028),	(4076),	(4085),	(4092),	(4115),	(4128),	(4150),	(4160),	(4176),	(4190),	(4220),	(4231),	(4241),	(4250),	(4276),	(4292),	(4307),	(4308),	(4317),	(4405),	(4413),	(4466),	(4470),	(4472),	(4474),	(4488),	(4528),	(4534),	(4538),	(4586),	(4598),	(4619),	(4625),	(4628),	(4643),	(4659),	(4661),	(4667),	(4694),	(4698),	(4719),	(4774),	(4778),	(4798),	(4802),	(4831),	(4843),	(4869),	(4872),	(4928),	(4937),	(4949),	(4951),	(4961),	(4987),	(4994),	(4998),	(5003),	(5056),	(5062),	(5077),	(5085),	(5103),	(5106),	(5144),	(5147),	(5149),	(5153),	(5183),	(5215),	(5268),	(5269),	(5271),	(5276),	(5302),	(5309),	(5340),	(5346),	(5364),	(5365),	(5419),	(5440),	(5502),	(5569),	(5642),	(5649),	(5707),	(5732),	(5763),	(5798),	(5812),	(5816),	(5821),	(5823),	(5848),	(5874),	(5879),	(5884),	(5893),	(5902),	(5908),	(5918),	(5926),	(5961),	(5991),	(5996),	(6001),	(6018),	(6020),	(6074),	(6093),	(6101),	(6128),	(6132),	(6156),	(6158),	(6186),	(6197),	(6199),	(6240),	(6293),	(6301),	(6310),	(6314),	(6318),	(6333),	(6350),	(6357),	(6362),	(6388),	(6399),	(6405),	(6443),	(6445),	(6464),	(6493),	(6497),	(6517),	(6539),	(6569),	(6594),	(6604),	(6633),	(6671),	(6676),	(6692),	(6710),	(6748),	(6755),	(6771),	(6788),	(6795),	(6820),	(6834),	(6844),	(6888),	(6891),	(6928),	(6929),	(6969),	(6989),	(6994),	(7017),	(7050),	(7057),	(7062),	(7067),	(7070),	(7126),	(7134),	(7163),	(7190),	(7201),	(7212),	(7225),	(7226),	(7229),	(7239),	(7241),	(7248),	(7265),	(7268),	(7283),	(7329),	(7347),	(7350),	(7363),	(7370),	(7378),	(7382),	(7424),	(7464),	(7467),	(7471),	(7504),	(7510),	(7511),	(7514),	(7540),	(7547),	(7564),	(7602),	(7612),	(7617),	(7627),	(7628),	(7635),	(7636),	(7639),	(7657),	(7676),	(7700),	(7711),	(7755),	(7765),	(7798),	(7804),	(7827),	(7830),	(7831),	(7852),	(7859),	(7878),	(7902),	(7909),	(7915),	(7924),	(7933),	(7943),	(7944),	(7997),	(8033),	(8053),	(8055),	(8072),	(8073),	(8160),	(8187),	(8208),	(8250),	(8282),	(8285),	(8293),	(8297),	(8326),	(8336),	(8376),	(8410),	(8454),	(8458),	(8494),	(8504),	(8512),	(8518),	(8527),	(8530),	(8562),	(8626),	(8659),	(8666),	(8669),	(8703),	(8709),	(8745),	(8764),	(8783),	(8802),	(8804),	(8862),	(8886),	(8933),	(9011),	(9013),	(9028),	(9032),	(9064),	(9072),	(9086),	(9087),	(9114),	(9138),	(9152),	(9164),	(9169),	(9175),	(9197),	(9226),	(9230),	(9231),	(9251),	(9252),	(9258),	(9279),	(9290),	(9292),	(9301),	(9305),	(9324),	(9326),	(9327),	(9364),	(9397),	(9404),	(9405),	(9409),	(9410),	(9413),	(9484);

--DECLARE @task_id VARCHAR(20) = 'OneCleanExp'; 
--DECLARE @task_id VARCHAR(20) = 'TwoFirstBridge'; 
DECLARE @task_id VARCHAR(20) = 'ThreeCleanExp'; 
--DECLARE @task_id VARCHAR(20) = 'FourCommandCentre'; 
--DECLARE @task_id VARCHAR(20) = 'SevenProvision'; 
--DECLARE @task_id VARCHAR(20) = 'EightFabric'; 
--DECLARE @task_id VARCHAR(20) = 'CanteenParking'; 
--DECLARE @task_id VARCHAR(20) = 'RaisePopulation'; 
--DECLARE @task_id VARCHAR(20) = 'AreaOfAstr1'; 
--DECLARE @task_id VARCHAR(20) = 'AreaOfAstr2'; 
--DECLARE @task_id VARCHAR(20) = 'AreaOfAstr3'; 
--DECLARE @task_id VARCHAR(20) = 'AreaOfAstr4'; 
--DECLARE @task_id VARCHAR(20) = 'RocketSign'; 
--DECLARE @task_id VARCHAR(20) = 'BuildBusStop'; 
--DECLARE @task_id VARCHAR(20) = 'BuildWorkshop1'; 
--DECLARE @task_id VARCHAR(20) = 'BuildWorkshop2'; 
--DECLARE @task_id VARCHAR(20) = 'BuildWorkshop3'; 
--DECLARE @task_id VARCHAR(20) = 'BuildTrainingCenter1'; 
--DECLARE @task_id VARCHAR(20) = 'BuildTrainingCenter2'; 

WITH FEBRUARY_USERS 
AS ( 
		SELECT U.[UserId] AS FU_ID 
		FROM   [dbo].[Users] AS U 
			   LEFT JOIN @DuplicateTable AS DT 
					  ON U.[UserId] = DT.[UserId] 
		WHERE  DT.[UserId] IS NULL 
		   AND [Created] BETWEEN @start_date AND @end_date 
		   AND [RegistrationCountry] != @test_country 
		   AND [LTV] = 0
	) 
--, QUEST_FUNNEL 
--AS ( 
		SELECT TOP (100) EQ.[UserId]           AS U_ID 
			   , [task_id]           AS TASK_NAME 
			   , [task_id_available] AS GIVEN 
			   , [task_id_passed]    AS DONE 
			   , [level]             AS LVL 
		FROM   [dbo].[event_quest] AS EQ 
			   INNER JOIN FEBRUARY_USERS AS FU 
					   ON EQ.[UserId] = FU.[FU_ID] 
		WHERE  [Timestamp] BETWEEN @start_date AND @end_date 
--)
			

		--SELECT LVL 
		----	   , Count (*)             AS AMOUNT 
		--	   , Count (DISTINCT U_ID) AS USERS 
		--FROM   QUEST_FUNNEL 
		--WHERE  DONE = 1 
		--AND TASK_NAME = @task_id
		--GROUP BY LVL
		--ORDER  BY LVL ASC 

