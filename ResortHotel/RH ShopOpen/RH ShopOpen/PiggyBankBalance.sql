DECLARE @start_period DATETIME = '2019-03-09 00:00:00.000'; 
--DECLARE @end_period DATETIME = '2019-04-19 00:00:00.000'; 

--DECLARE @start_period DATETIME = '2019-04-19 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-05-09 23:59:59.997'; 

--SELECT [type]
--		,Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)            AS Amount 
--FROM   [dbo].[ev_PiggyBankPurchase] AS PB
--		INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS_20_PLUS_LVL]  AS AU
--			ON AU.U_ID = PB.[UserId]
--WHERE [TimeStamp] BETWEEN @start_period AND @end_period
--AND ([ClientVersion] >= '1.9.0' OR ([ClientVersion] BETWEEN '1.11.0' AND  '1.19.99'))
--AND [Result] = 'OK'
--GROUP BY [type]
--ORDER BY [type]

SELECT [type]
		,Count (DISTINCT [UserId]) AS Users 
       ,Count (*)            AS Amount 
FROM   [dbo].[ev_PiggyBankShow]  AS PB
		INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS_20_PLUS_LVL]  AS AU
			ON AU.U_ID = PB.[UserId]
WHERE [TimeStamp] BETWEEN @start_period AND @end_period
AND ([ClientVersion] >= '1.9.0' OR ([ClientVersion] BETWEEN '1.11.0' AND  '1.19.99'))
AND [is_auto] = 0
GROUP BY [type]
ORDER BY [type]


--SELECT [ClientVersion]
--		,Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)            AS Amount 
--FROM   [dbo].[ev_PiggyBankShow]  AS PB
--	--	INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS_20_PLUS_LVL]  AS AU
--		--	ON AU.U_ID = PB.[UserId]
--WHERE [TimeStamp] BETWEEN @start_period AND @end_period
--AND ([ClientVersion] >= '1.9.0' OR ([ClientVersion] BETWEEN '1.11.0' AND  '1.19.99'))
----AND [type] = 'craig_safe_1'
--GROUP BY [ClientVersion]
--ORDER BY [ClientVersion]


--SELECT [type]
--       ,Avg ([progress])            AS AVG_COINS 
--FROM   [dbo].[ev_PiggyBankPurchase] AS PB
--		INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS_20_PLUS_LVL]  AS AU
--			ON AU.U_ID = PB.[UserId]
--WHERE [TimeStamp] BETWEEN @start_period AND @end_period
--AND [ClientVersion] >= '1.9.0'
--AND [Result] = 'OK'
--GROUP BY [type]
--ORDER BY [type]


