DECLARE @DateFrom datetime = '2019-11-03 00:00:00.000';--начало анализированного периода
DECLARE @DateTo datetime = DATEADD(ms, -3, DATEADD(dd,  DATEDIFF(dd,0,GETDATE()),0)) -- конец анализированного периода


SELECT CONVERT(date, [Created]) as date
		,COUNT(*) AS 'Users'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 1, IIF([LTV1D] > 0, [LTV1D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 1, 1, null)) * 1.0, 0) as 'Day1'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 2, IIF([LTV2D] > 0, [LTV2D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 2, 1, null)) * 1.0, 0) as 'Day2'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 3, IIF([LTV3D] > 0, [LTV3D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 3, 1, null)) * 1.0, 0) as 'Day3'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 4, IIF([LTV4D] > 0, [LTV4D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 4, 1, null)) * 1.0, 0) as 'Day4'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 5, IIF([LTV5D] > 0, [LTV5D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 5, 1, null)) * 1.0, 0) as 'Day5'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 6, IIF([LTV6D] > 0, [LTV6D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 6, 1, null)) * 1.0, 0) as 'Day6'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 7, IIF([LTV7D] > 0, [LTV7D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 7, 1, null)) * 1.0, 0) as 'Day7'
		
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 8, IIF([LTV8D] > 0, [LTV8D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 8, 1, null)) * 1.0, 0) as 'Day8'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 9, IIF([LTV9D] > 0, [LTV9D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 9, 1, null)) * 1.0, 0) as 'Day9'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 10, IIF([LTV10D] > 0, [LTV10D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 10, 1, null)) * 1.0, 0) as 'Day10'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 11, IIF([LTV11D] > 0, [LTV11D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 11, 1, null)) * 1.0, 0) as 'Day11'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 12, IIF([LTV12D] > 0, [LTV12D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 12, 1, null)) * 1.0, 0) as 'Day12'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 13, IIF([LTV13D] > 0, [LTV13D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 13, 1, null)) * 1.0, 0) as 'Day13'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 14, IIF([LTV14D] > 0, [LTV14D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 14, 1, null)) * 1.0, 0) as 'Day14'
		
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 15, IIF([LTV15D] > 0, [LTV15D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 15, 1, null)) * 1.0, 0) as 'Day15'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 16, IIF([LTV16D] > 0, [LTV16D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 16, 1, null)) * 1.0, 0) as 'Day16'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 17, IIF([LTV17D] > 0, [LTV17D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 17, 1, null)) * 1.0, 0) as 'Day17'
        ,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 18, IIF([LTV18D] > 0, [LTV18D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 18, 1, null)) * 1.0, 0) as 'Day18'
        ,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 19, IIF([LTV19D] > 0, [LTV19D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 19, 1, null)) * 1.0, 0) as 'Day19'
        ,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 20, IIF([LTV20D] > 0, [LTV20D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 20, 1, null)) * 1.0, 0) as 'Day20'
        ,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 21, IIF([LTV21D] > 0, [LTV21D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 21, 1, null)) * 1.0, 0) as 'Day21'
        ,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 22, IIF([LTV22D] > 0, [LTV22D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 22, 1, null)) * 1.0, 0) as 'Day22'
        ,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 23, IIF([LTV23D] > 0, [LTV23D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 23, 1, null)) * 1.0, 0) as 'Day23'
        ,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 24, IIF([LTV24D] > 0, [LTV24D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 24, 1, null)) * 1.0, 0) as 'Day24'
        ,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 25, IIF([LTV25D] > 0, [LTV25D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 25, 1, null)) * 1.0, 0) as 'Day25'
        ,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 26, IIF([LTV26D] > 0, [LTV26D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 26, 1, null)) * 1.0, 0) as 'Day26'
        ,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 27, IIF([LTV27D] > 0, [LTV27D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 27, 1, null)) * 1.0, 0) as 'Day27'
        ,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 28, IIF([LTV28D] > 0, [LTV28D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 28, 1, null)) * 1.0, 0) as 'Day28'
        ,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 29, IIF([LTV29D] > 0, [LTV29D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 29, 1, null)) * 1.0, 0) as 'Day29'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 30, IIF([LTV30D] > 0, [LTV30D], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 30, 1, null)) * 1.0, 0) as 'Day30'
        
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 60, IIF([LTV2M] > 0, [LTV2M], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 60, 1, null)) * 1.0, 0) as 'Day60'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 120, IIF([LTV4m] > 0, [LTV4m], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 120, 1, null)) * 1.0, 0) as 'Day120'
		,COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 180, IIF([LTV6M] > 0, [LTV6M], null), null)) / NULLIF(COUNT(IIF(DATEDIFF(day, [Created], GETDATE()) >= 180, 1, null)) * 1.0, 0) as 'Day180'

FROM [dbo].[Users] 

WHERE [Created] BETWEEN @DateFrom AND @DateTo 
AND [Platform] = 1 
--AND [RegistrationCountry] = 'DE'

AND [MediaSource] = 'Facebook Ads'
--AND [Campaign] = 'mohicanlab_family_zoo_barel_lal_inp_01'
--AND [MediaSource] IS NULL

GROUP BY CONVERT(date, [Created])
ORDER BY CONVERT(date, [Created])



