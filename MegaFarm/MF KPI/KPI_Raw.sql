DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform varchar(4)
--,@regcountry varchar(4)

-- дата (START)
SET @startperiod = '2019-01-28 00:00:00.000'
-- дата (END)
--SET @endperiod = '2019-11-03 23:59:59.999'
SET @endperiod = DATEADD(ms, -3, DATEADD(wk,  DATEDIFF(wk,0,GETDATE()),0))
SET @platform = 1 -- 2 -- 3 --
--set @regcountry =  'fr'--'us'--'gb' --'de' --'fr' --'jp'

If(OBJECT_ID('tempdb..#temp_dates') Is Null)
Begin
    CREATE TABLE #temp_dates ( [date] DATE PRIMARY KEY );
    INSERT #temp_dates([date]) SELECT d FROM ( 
        SELECT d = DATEADD(DAY, rn - 1, @startperiod) FROM  (
                SELECT TOP (DATEDIFF(DAY, @startperiod, @endperiod+1)) rn = ROW_NUMBER() OVER (ORDER BY s1.[object_id])
                FROM sys.all_objects AS s1 CROSS JOIN sys.all_objects AS s2
                ORDER BY s1.[object_id] ) AS x ) AS y;
End

;WITH
    MediaSourceUsers
    AS
    (
        SELECT UserId AS U_ID
        FROM Users
        WHERE MediaSource IS NULL  -- organic
        --WHERE MediaSource = 'gurutraff_int' -- guru traffic
        --WHERE MediaSource != 'gurutraff_int' -- User Acquisition
        --AND GameUserId NOT IN (103, 1240,   22, 1064,   0,  1002,   69, 482,    93, 671,    1050,   22, 1028,   647,    1004, 38999, 746) -- testers
        AND UserId NOT IN (165,	464, 291,	216, 13, 262,	1,	44327,198)-- testers
    )

-- SELEcT MediaSource, Count(*) AS A FROM Users  WHERE MediaSource != 'guru_traf'  GROUP BY MediaSource ORDER BY A DESC
SELECT t0.[Date]
, t2.[DAU]
, t1.[Number of Users]
, t1.[TotalMinutesDuration]  
, t2.[TotalSessions]
, t3.NewUsers
, t3.RetentionD1+0.0 AS 'TRetention D1'
, t3.RetentionD3+0.0 AS 'TRetention D3'
, t3.RetentionD7+0.0 AS 'TRetention D7'
, t3.RetentionD14+0.0 AS 'TRetention D14'
, t3.RetentionD28+0.0 AS 'TRetention D28'
, t4.trans 'Trans'
, t4.Gross+0.0 'Gross'
, t4.PAYers  AS 'PAYingUsers'
, t4.Total_New_PAYers AS 'Total New PAYers'
, t3.[Conversion 1d] 'TConversion 1d'
, t3.[Conversion 2d] 'TConversion 2d'
, t3.[Conversion 7d] 'TConversion 7d'
, t3.[Conversion 14d] 'TConversion 14d'
, t3.[Conversion 28d] 'TConversion 28d'
, t3.T_LTV_1D AS 'tLTV 1d'
, t3.T_LTV_2D AS 'tLTV 2d'
, t3.T_LTV_7D AS 'tLTV 7d'
, t3.T_LTV_14D AS 'tLTV 14d'
, t3.T_LTV_28D AS 'tLTV 28d'
, t3.T_LTV_ALL AS 'tLTV All'
, t5.[MAU]
FROM
    (
    SELECT CONVERT(date, [Timestamp]) AS 'Date'
         	 , COUNT(DISTINCT userid) AS 'Number of Users'
             , 1.0*(cast((sum(duration)+0.0) AS bigint))/60  AS 'TotalMinutesDuration'-- /(60*COUNT(distinct userid)) as 'AVGDuration',
    FROM EventsSessionFinished AS T
        INNER JOIN MediaSourceUsers ON MediaSourceUsers.[U_ID] =  T.[UserId]
    WHERE Platform = @platform
        AND Timestamp BETWEEN @startperiod AND @endperiod
        AND duration<2000000
    --and Country = @regcountry
    GROUP BY CONVERT(date, [Timestamp])
) 
AS t1
    INNER JOIN
    (
SELECT
        CONVERT(date, [Timestamp]) AS 'Date'
             , COUNT(DISTINCT UserId) AS 'DAU'
             , count(timestamp) AS 'TotalSessions' --/(count(iif (firstloginthisday=1,1,NULL))+0.01) [AvgSessionsPerUser]
     FROM EventsAppOpen AS T
        INNER JOIN MediaSourceUsers ON MediaSourceUsers.[U_ID] =  T.[UserId]
    WHERE Timestamp BETWEEN @startperiod AND @endperiod
        AND Platform = @platform
    --and Country = @regcountry
    GROUP BY CONVERT(date, [Timestamp])
)
AS t2 ON t1.[Date] = t2.[Date]
    LEFT JOIN
    (
    SELECT
        CONVERT(date, [created]) AS 'Date'
	    , count(DISTINCT UserId) AS 'NewUsers'
		, sum(cast(Retention1D AS int)) AS 'RetentionD1'
		, sum(cast(Retention3D AS int)) AS 'RetentionD3'
		, sum(cast(Retention7D AS int)) AS 'RetentionD7'
		, sum(cast(Retention14D AS int)) AS 'RetentionD14'
		, sum(cast(Retention28D AS int)) AS 'RetentionD28'
		, count(iif(ltv1d>0,1,NULL))*1.0 AS [Conversion 1d]
        , count(iif(ltv2d>0,1,NULL))*1.0 AS [Conversion 2d]
        , count(iif(ltv7d>0,1,NULL))*1.0 AS [Conversion 7d]
        , count(iif(ltv14d>0,1,NULL))*1.0 AS [Conversion 14d]
        , count(iif(ltv28d>0,1,NULL))*1.0 AS [Conversion 28d]
        , sum(iif(ltv1d>0,ltv1d,NULL))*1.0 AS T_LTV_1D
        , sum(iif(ltv2d>0,ltv2d,NULL))*1.0 AS T_LTV_2D
        , sum(iif(ltv7d>0,ltv7d,NULL))*1.0 AS T_LTV_7D
        , sum(iif(ltv14d>0,ltv14d,NULL))*1.0 AS T_LTV_14D
        , sum(iif(ltv28d>0,ltv28d,NULL))*1.0 AS T_LTV_28D
        , sum(iif(ltv>0,ltv,NULL))*1.0 AS T_LTV_ALL
    FROM users AS T
        INNER JOIN MediaSourceUsers ON MediaSourceUsers.[U_ID] =  T.[UserId]
    WHERE Platform = @platform
        AND created BETWEEN @startperiod AND @endperiod
    --and [RegistrationCountry] = @regcountry
    GROUP BY CONVERT(date, [created])
        )
AS t3 ON t1.[Date] = t3.[Date]
    LEFT JOIN
    ( 
    SELECT
        CONVERT(date, [Timestamp]) AS 'Date'
			, COUNT(UserId) AS 'Trans'
			, SUM(AmountInUSD) AS 'Gross'
            , COUNT(DISTINCT UserId) AS 'PAYers'
			, sum(cast([FirstDeposit] AS int)) AS 'Total_New_PAYers'
    FROM EventsPurchase AS T
        INNER JOIN MediaSourceUsers ON MediaSourceUsers.[U_ID] =  T.[UserId]
    WHERE Platform = @platform
        AND Timestamp BETWEEN @startperiod AND @endperiod
    --and Country = @regcountry
    GROUP BY CONVERT(date, [Timestamp])
) 
AS t4 ON t1.[Date] = t4.[Date]
 LEFT JOIN
    (
    SELECT t.[Date]
   ,(
        SELECT Count(DISTINCT UserId) 
        FROM EventsAppOpen AS TT
        INNER JOIN MediaSourceUsers ON MediaSourceUsers.[U_ID] =  TT.[UserId]
        WHERE Timestamp >= DATEADD(dd, -29, t.[Date]) AND  Timestamp < DATEADD(dd, 1, t.[Date])
    ) AS MAU
    FROM #temp_dates AS t
    GROUP BY  t.[Date]
)
AS t5 ON t1.[Date] = t5.[Date]
RIGHT JOIN #temp_dates AS t0 ON t1.[Date] = t0.[Date]
  
ORDER BY [Date]




If(OBJECT_ID('tempdb..#temp_dates') Is Not Null)
Begin Drop Table #temp_dates End