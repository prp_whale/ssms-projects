DECLARE @start_period DATETIME = '2019-06-01 00:00:00.000';
DECLARE @end_period DATETIME = '2019-06-14 23:59:59.997';


-- SET @start_period  = '2019-06-15 00:00:00.000';
-- SET @end_period  = '2019-06-28 23:59:59.997';

-- SET @start_period  = '2019-07-01 00:00:00.000';
-- SET @end_period   = '2019-07-14 23:59:59.997';

DECLARE @bottom_limit INT = 200;


WITH MEDIA_COUNTRY
AS ( 
    SELECT [MediaSource] 
            ,[RegistrationCountry] 
            ,Count(DISTINCT [UserId]) AS Users 
        FROM   DBO.[Users] 
        WHERE  [Created] BETWEEN @start_period AND @end_period 
        AND  [Platform] = 1
        GROUP BY [MediaSource] 
                ,[RegistrationCountry] 
        HAVING Count(DISTINCT [UserId]) > @bottom_limit
)
,   POU -- AS POOL_OF_USERS
AS (
    SELECT [UserId]
         , U.[MediaSource] 
         , U.[RegistrationCountry] 
      FROM   DBO.[Users] AS U 
        INNER JOIN  MEDIA_COUNTRY AS MC
            ON (U.[MediaSource] = MC.[MediaSource] OR (U.[MediaSource]is NULL and MC.[MediaSource] is NULL) )
            AND U.[RegistrationCountry] = MC.[RegistrationCountry] 
     WHERE  [Created] BETWEEN @start_period AND @end_period 
)

  SELECT
        P.[MediaSource] 
        ,P.[RegistrationCountry]
        ,Count (DISTINCT P.UserId) AS 'Registration'
        ,Sum(Cast([Retention1D] AS INT))*1.0 / Count (DISTINCT P.UserId) AS 'Ret Day1'
        ,Sum(Cast([Retention7D] AS INT))*1.0 / Count (DISTINCT P.UserId) AS 'Ret Day7'
        ,Sum(Cast([Retention14D] AS INT))*1.0 / Count (DISTINCT P.UserId) AS 'Ret Day14'
        ,Sum(Cast([Retention30D] AS INT)) *1.0 / Count (DISTINCT P.UserId) AS 'Ret Day30' 
        ,Count(Iif([LTV1D]> 0, U.[UserId], NULL) )*1.0 / Count (DISTINCT P.UserId) AS 'C1 Day1'
        ,Count(Iif([LTV7D]> 0, U.[UserId], NULL) )*1.0 / Count (DISTINCT P.UserId) AS 'C1 Day7'
        ,Count(Iif([LTV14D]> 0, U.[UserId], NULL) )*1.0 / Count (DISTINCT P.UserId) AS 'C1 Day14'
        ,Count(Iif([LTV30D]> 0, U.[UserId], NULL) )*1.0 / Count (DISTINCT P.UserId) AS 'C1 Day30'
        ,Sum([LTV1D]) / Count (DISTINCT P.UserId) AS 'CARPU Day1'
        ,Sum([LTV7D]) / Count (DISTINCT P.UserId) AS 'CARPU Day7'
        ,Sum([LTV14D]) / Count (DISTINCT P.UserId) AS 'CARPU Day14'
        ,Sum([LTV30D]) / Count (DISTINCT P.UserId) AS 'CARPU Day30'
    FROM [dbo].[Users] AS U 
        INNER JOIN POU AS P  ON U.[UserId] = P.[UserId] 
        AND (U.[MediaSource] = P.[MediaSource] OR (P.[MediaSource] is NULL and u.[MediaSource] is NULL) )
            AND U.[RegistrationCountry] = P.[RegistrationCountry] 
        GROUP BY P.[MediaSource], P.[RegistrationCountry] 
        ORDER BY P.[MediaSource] , P.[RegistrationCountry] 
        
