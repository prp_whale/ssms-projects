--до какого уровня в среднем первой главы игроки доходят с первой попытки. Со второй попытки. С третьей попытки и так до 10-й. По каждой главе.

DECLARE @st_per DATETIME = '2020-03-01 00:00:00.000';
DECLARE @end_per DATETIME =  '2020-03-31 23:59:59.997'; --22

WITH PoU 
AS(
    SELECT UserId AS U_ID
    , CASE 
        WHEN dbo.Getchecksum(advertisingid) % 4 = 0 THEN 'A'
        WHEN dbo.Getchecksum(advertisingid) % 4 = 1 THEN 'B'
        WHEN dbo.Getchecksum(advertisingid) % 4 = 2 THEN 'C'
        WHEN dbo.Getchecksum(advertisingid) % 4 = 3 THEN 'D'
        ELSE 'ERROR' END AS 'Group'
    , CASE 
        WHEN ltv = 0 THEN 'ND'
        ELSE 'Depos' END AS 'IsPay'
    , registrationclientversion AS CreatedVersion
    FROM   dbo.Users 
    WHERE  created BETWEEN '2020-03-01 00:00:00.000' AND '2020-03-21 23:59:59.997' 
        AND registrationclientversion IN ( '1.0.182', '1.0.184', '1.0.244' ) 
)


, CTE1 
AS (
    SELECT UserID , [Group] , [IsPay] , [CreatedVersion]
        , WorldId AS LocationId
        , RunNumber
        , RoomNumber
        , [Timestamp] aS katka_end
    FROM dbo.[Finishrun] INNER JOIN POU ON UserId = U_ID
    WHERE [Timestamp] BETWEEN @st_per AND @end_per
    and IsLeft = 0 and   roomnumber!=0 and roomname!='-2' and runlevel<=11
),



CTE2 
AS (
    SELECT C.UserId, [Group] , [IsPay] , [CreatedVersion]
        , C.LocationId AS WorldId
        , C.RunNumber
        , C.RoomNumber AS end_room
        , SR.[Timestamp]  AS katka_start
    FROM dbo.[StartRun] AS SR INNER JOIN CTE1 AS C  ON SR.UserId = C.UserId AND SR.LocationId = C.LocationId AND SR.Runnumber = C.Runnumber 
    WHERE SR.[Timestamp] BETWEEN @st_per AND @end_per 
    AND SR.[Timestamp] < katka_end
    AND healthpoint < 9999 and attackpoint < 9999 
 
),

 FIRST_SUCCESS 
AS (
    SELECT C.UserId , [Group] , [IsPay] , [CreatedVersion]
        , C.WorldId 
        , C.RunNumber
        , Max(RoomNumber) AS end_room
        , Min([Timestamp]) aS first_complete
    FROM dbo.[Finishrun] AS FR INNER JOIN CTE2 AS C  ON FR.UserId = C.UserId AND FR.WorldId = C.WorldId AND FR.Runnumber = C.Runnumber 
    WHERE RoomNumber = '-1'
    
    GROUP BY C.UserId , [IsPay], [Group] , [CreatedVersion], C.WorldId, C.RunNumber
)



SELECT [IsPay] , [Group]  , [CreatedVersion], WorldId, RunNumber

,  COUNT(DISTINCT UserId) AS 'Start Users'
, AVG(end_room)         AS 'Avg end level'
, (SELECT  COUNT(DISTINCT UserId) FROM FIRST_SUCCESS AS C WHERE  C.IsPay = T.IsPay AND C.[Group] = T.[Group] AND C.WorldId = T.WorldId AND C.Runnumber = '-1') AS 'Finishers'
FROM CTE2 AS T 
WHERE RunNumber BETWEEN 1 AND 20
GROUP BY [IsPay], [Group] , [CreatedVersion], WorldId, RunNumber
ORDER BY [IsPay], [Group] , [CreatedVersion], WorldId, RunNumber




