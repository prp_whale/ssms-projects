declare @startperiod as datetime
declare @endperiod as datetime
 
​
Declare @Hours  int = 36;
Declare @Group int = 3
​
​
set @startperiod = '2020-02-08 00:00:00.000'; --начало анализированного периода
SET @endperiod = '2020-02-09 23:59:59.997'; -- конец анализированного периода
​
WITH PoolOfUsers
AS (
    SELECT DISTINCT UserID AS U_ID
    FROM dbo.EquipmentMovement
    WHERE  userid in (select userid from users where created between @startperiod and @endperiod and dbo.getchecksum(advertisingid) % 4 = @group)  
    and timestamp between created and dateadd(hh,@Hours,created)
    and created between @startperiod and @endperiod
    and platform=1
    --and triggersourceid!='fuse'
     and UserID not IN (SELECT DISTINCT UserID  FROM dbo.EquipmentMovement WHERE TriggerSourceId = 'Fuse' )
)
 
, WantNotFuse
AS (
    SELECT UserID,equipmentid,heroslotcategoryid
    , Count(*) AS Amount
    FROM dbo.EquipmentMovement AS EM INNER JOIN PoolOfUsers AS pou ON pou.U_ID = em.UserId
    where  userid in (select userid from users where created between @startperiod and @endperiod and dbo.getchecksum(advertisingid) % 4 = @group)  
   -- and timestamp between created and dateadd(hh,@Hours,created)
    and created between @startperiod and @endperiod
      and platform=1
     --and equipmentbalance>=3
   -- WHERE TriggerSourceId = 'Fuse'
    GROUP BY UserID,equipmentid,heroslotcategoryid
    HAVING Count(*) >= 3
)
 
SELECT Count (Distinct UserId) FROM WantNotFuse