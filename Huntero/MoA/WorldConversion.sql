declare @periodstart datetime = '2020-03-12 00:00:00:000';
declare @periodend datetime = '2020-03-13 23:59:59:997';

;WITH POU
AS (
   SELECT UserID as U_ID
    FROM dbo.Users 
    WHERE dbo.getchecksum(advertisingid)%4 IN (0)
    AND ClientVersion IN ('1.0.182')
    AND Created between @periodstart and @periodend 
) 
, ANTI_CHEAT 
AS (
    SELECT DISTINCT  UserID AS U_ID
    FROM dbo.StartRun INNER JOIN POU ON UserId = U_ID
    WHERE healthpoint < 9999 and attackpoint < 9999 
)
 


SELECT 0 AS WORLD
 , Count(*) AS Users
    FROM ANTI_CHEAT
UNION ALL
    SELECT
        WorldId
, Count(DISTINCT UserID)
    FROM dbo.[Finishrun] INNER JOIN ANTI_CHEAT ON UserId = U_ID
    WHERE RoomNumber = -1
    AND [Timestamp] BETWEEN Created AND DATEADD(dd, 4, Created)
    and IsLeft = 0 and   roomnumber!=0 and roomname!='-2' and runlevel<=11
    GROUP BY WorldId
ORDER BY 1 

