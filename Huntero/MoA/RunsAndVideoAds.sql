	;WITH Rooms
	AS (
		SELECT SUBSTRING(TriggerId,3,LEN(TriggerId)) AS RoomName
		, TriggerSourceId AS AdReason
		, UserId
		, TalentsLevelid
		, AccountLevel
		 FROM VideoAdStatus
		 WHERE  TriggerId like '1_%'
		 AND AdStatus = 0
	)
, AdReasonViews AS 
(
	SELECT RoomNumber 
	, AdReason
	, Count(*) AS AdViews
	FROM RunRoomInfo AS rri INNER JOIN Rooms as r on r.UserId = rri.UserId AND r.TalentsLevelid = rri.TalentsLevelid AND r.AccountLevel = rri.AccountLevel AND r.RoomName = rri.RoomName
	WHERE WorldId = 1
	--AND Runnumber = 1
	GROUP BY AdReason,  RoomNumber
	--ORDER BY AdReason, RoomNumber

)

, AdViews AS 
(
	SELECT RoomNumber 
	--, AdReason
	, Count(*) AS AdViews
	FROM RunRoomInfo AS rri INNER JOIN Rooms as r on r.UserId = rri.UserId AND r.TalentsLevelid = rri.TalentsLevelid AND r.AccountLevel = rri.AccountLevel AND r.RoomName = rri.RoomName
	WHERE WorldId = 1
	--AND Runnumber = 1
	GROUP BY  RoomNumber
	--ORDER BY AdReason, RoomNumber
)
, Runs AS
(
   SELECT RoomNumber
   , Count(*) AS Runs
   
   FROM RunRoomInfo
   	WHERE WorldId = 1
	--AND Runnumber = 1
	GROUP BY RoomNumber
)

SELECT r.*
, AdReason, AdViews 
FROM Runs AS r INNER JOIN AdReasonViews as aw ON r.RoomNumber = aw.RoomNumber 
ORDER BY r.RoomNumber

