DECLARE @start_date Datetime		= '2019-02-11 00:00:00.000'; 
DECLARE @end_date Datetime			= '2019-02-24 23:59:59.999'; 
DECLARE @cheater Int				= 7249; 
DECLARE @test_country Varchar(2)	= 'UA' 
DECLARE @user_start_date Datetime	= '2019-02-11 00:00:00.000'; 
DECLARE @user_end_date Datetime		= '2019-02-11 23:59:59.999'; 

SELECT Cast ([Timestamp] AS Date)  AS DAY 
       , Count (*)                 AS AMOUNT 
       , Count (DISTINCT [UserID]) AS Users 
  FROM [dbo].[order_drone] AS OD 
       INNER JOIN (SELECT [UserId] AS ID 
                     FROM [dbo].[users] 
                    WHERE [RegistrationCountry] != @test_country 
                      AND [CreatedBackOfficeTime] BETWEEN 
                          @user_start_date AND @user_end_date 
                      AND [UserId] != @cheater 
                      AND [LTV] = 0) AS USERS 
               ON USERS.ID = OD.[UserId] 
 WHERE [Timestamp] BETWEEN @start_date AND @end_date 
   AND [order_status] = 0 
 GROUP BY Cast ([Timestamp] AS Date) 
 ORDER BY DAY 