DECLARE @start_period DATETIME = '2019-08-01 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-08-25 23:59:59.997'; 

SELECT [FriendLevelId]
  , Count (DISTINCT UserId) AS U
  , Count (*) AS A
  FROM [dbo].[ev_FriendsAction]
  WHERE ActionId = 'get_reward'
  AND [Timestamp] BETWEEN @start_period AND @end_period
GROUP BY FriendLevelId
ORDER BY FriendLevelId ASC           


