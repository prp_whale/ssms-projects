DECLARE @start_date Datetime = '2019-04-01 00:00:00.000'; 
DECLARE @end_date Datetime = '2019-04-30 23:59:59.999'; 
DECLARE @cheater Int = 7249; 
DECLARE @cheater2 Int = 10144; 
DECLARE @cheater3 Int = 20652; 
DECLARE @test_country Varchar(2) = 'UA' 


	--declare @source varchar(25)		= 'complete_building'
	--declare @source varchar(25)		= 'daily_bonus'
	--declare @source varchar(25)		= 'get_achievement'
	--declare @source varchar(25)		= 'gift'
	--declare @source varchar(25)		= 'iap'
	declare @source varchar(25)		= 'level_up'
	--declare @source varchar(25)		= 'reward_leaderboard'
	--declare @source varchar(25)		= 'reward_quest'
	--declare @source varchar(25)		= 'reward_video'
	--declare @source varchar(25)		= 'reward_wheel_fortune'

SELECT	[Level_id] as lvl 
		, SUM	([coins]) as COINS
		, COUNT (DISTINCT [UserId]) as USERS_ON_LVL
		, COUNT (*) as TRANSACTIONS_ON_LVL
  FROM [dbo].[currency_received]
  WHERE   [RegistrationCountry] != @test_country 
   AND [UserID] NOT IN (7249, 10144, 20652)
   AND [timestamp] between @start_date and @end_date
  -- AND [source_type_id] = @source
   AND [coins] != 0
   --AND [LTV] > 0
  GROUP BY [Level_id]
  ORDER BY lvl 

  --SELECT [UserId] 
  --, SUM	([coins]) as COINS
  --  FROM [dbo].[currency_received]
  --WHERE   [RegistrationCountry] != @test_country 
  --AND [UserID] NOT IN (7249, 10144, 20652)
  -- AND [timestamp] between @start_date and @end_date
  --  AND [coins] != 0
  --GROUP BY [UserId]
  --ORDER BY COINS  desc