declare @time_begin datetime = '2020-05-10 00:00:00';
declare @time_end   datetime = '2020-05-17 23:59:59';

SELECT Convert(Date, Created) AS 'Date'
,Count(Distinct UserId)  UserId
, Count(Distinct AdvertisingId) AdvertisingId
, Count(Distinct InstallUID) InstallUID
, Count(Distinct WhaleappID) WhaleappID
, Count(Distinct GameUserId) GameUserId
, Count(Distinct DeviceId) DeviceId
  FROM [dbo].[Users] 
  WHERE [Created] BETWEEN @time_begin AND @time_end
  AND Platform = 1
  GROUP BY Convert(Date, Created)
  ORDER BY Convert(Date, Created)
  


-- declare @time_begin datetime = '2020-05-13 00:00:00';
-- declare @time_end   datetime = '2020-05-13 23:59:59';

-- WITH CTE
-- AS (
--     SELECT Distinct AdvertisingId AS AdvID
--     FROM dbo.Users
--     WHERE [Created] < @time_begin
-- )


-- SELECT Distinct AdvertisingId 
-- FROM [dbo].[Users] 
--     LEFT JOIN CTE ON AdvId = AdvertisingId 
-- WHERE Created BETWEEN @time_begin AND @time_end
-- AND AdvId IS NULL