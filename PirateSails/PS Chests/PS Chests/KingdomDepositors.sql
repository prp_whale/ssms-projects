DECLARE @start_period DATETIME = '2019-05-05 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-05-22 23:59:59.997'; 


DECLARE @test_country Varchar(2) = 'UA'; -- ��������� ������ �� � �� ������� �� ������ �������

DECLARE  @TestDevices TABLE (UserID INT PRIMARY KEY);
INSERT INTO @TestDevices VALUES  (6), (14), (23), (105), (239), (247), (256), (257), (333), (1331), (1366), (1367), (1368), (1375), (1381), (1382), (3487), (3737), (4244), (4253), (4255), (4265), (4266), (4270), (5517), (5906), (2), (3), (7), (8), (9), (12), (16), (17), (21), (22), (25), (26), (27), (51), (117), (249), (293), (598), (666), (670), (1190), (1192), (1193), (1337), (5568);

DECLARE @KingdomId int = 4;

;WITH POOL_OF_USERS  -- ������ ������������ �� ��������� �����������
AS ( 
		SELECT U.[UserId] AS ID 
		FROM   [dbo].[Users] AS U 
			   LEFT JOIN @TestDevices AS TD 
					  ON U.[UserId] = TD.UserID
		WHERE 
			TD.UserID IS NULL -- ��������� ������ �������
			AND  [Created] BETWEEN @start_period AND @end_period 
		    AND [RegistrationCountry] != @test_country  -- ��������� ������ �� � �� ������� �� ������ �������
) 

, KINGDOMS_USER 
 AS (
	  SELECT DISTINCT [UserId] 
      ,[KingdomId]
	    FROM [dbo].[UnitsBalance] AS UB 
		 INNER JOIN POOL_OF_USERS AS POU 
					   ON POU.ID = UB.[UserId] 
		  WHERE [KingdomId] IS NOT NULL
		AND [Timestamp] BETWEEN @start_period AND @end_period 
 )

 --SELECT Top (100) * FROM KINGDOMS_USER
 --WHERE [KingdomId] = @KingdomId 

SELECT [ProductId] 
       ,Count(DISTINCT EP.[UserId] ) AS Users 
       ,Sum ([AmountInUSD])                   AS AmountInUSD 
       ,Count (*)                             AS PaymentsCount 
FROM   [dbo].[EventsPurchase] AS EP
      INNER JOIN KINGDOMS_USER AS KU 
		ON EP.[UserId] = KU.[UserId]
		AND [KingdomId] = @KingdomId

WHERE [Timestamp] BETWEEN @start_period AND @end_period 
GROUP  BY [ProductId] 
ORDER  BY Users DESC 



