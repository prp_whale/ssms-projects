/*Посевы
В данной таблице интересует статистика по засеиванию и сбора урожая. Это нужно для того, чтобы понять какой урожая игроки часто засеивают, а какой реже, чтобы можно было анализировать баланс по ним и выявить причину неиспользования того или иного вида урожая (реже попадается в заказах, слишком дорогой и т .д.)
При построении графика нужно учитывать следующие параметры:
кол-во и виды урожая в разрезе уровня игрока
кол-во и виды урожая за сессию в разрезе уровня игрока
кол-во ускоренный посевов --later
разделение на депозиторов и недепозиторов
возможность выбирать версии и временные промежутки
*/
declare @days int = 28;

declare @days_multi int = 1;
declare @st_per DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days*@days_multi, GETDATE())));
declare @end_per DATETIME  = DATEADD(ms, -3, CONVERT(datetime, CONVERT(date, GETDATE())));

declare @versa_limit int = 100;


WITH Versa
AS (
    SELECT registrationclientversion AS RegVer
    FROM  dbo.Users
   -- WHERE  created BETWEEN @st_per AND @end_per
    GROUP BY registrationclientversion
    HAVING Count(*) >=  @versa_limit
)


SELECT CONVERT(date, [TimeStamp]) AS 'Date', ClientVersion, TriggerId, LevelId
,CASE   WHEN ltv > 0 THEN 'Depos'  ELSE 'ND'  END AS 'IsPay'
, Count(Distinct UserID) AS Users
, Count(Distinct SessionId) AS Sessionss
, Count(UserID) AS Events

FROM EventsProductMovement 
WHERE ClientVersion IN (SELECT * FROM Versa)
AND [Timestamp] BETWEEN @st_per AND @end_per

AND ProductType = 'Coins'
AND [Action] = 'spend'
AND [TriggerSourceId] = 'sow_crops'
AND [level_id] IS NOT NULL

GROUP BY CONVERT(date, [TimeStamp]) , ClientVersion, TriggerId, LevelId
,CASE   WHEN ltv > 0 THEN 'Depos'  ELSE 'ND'  END 

ORDER BY CONVERT(date, [TimeStamp]), ClientVersion, LevelId, [Events]

