declare @st_per DATETIME = '2020-05-01 00:00:00:000';
declare @end_per DATETIME  = '2020-05-05 23:59:59:997';
DECLARE @cv NVARCHAR(10) = '1.17';

WITH PoU 
As (
    SELECT  UserId AS U_ID
    FROM dbo.Users 
    WHERE Created BETWEEN @st_per AND @end_per
        AND [Retention1D] IS NULL
        AND [Retention2D] IS NULL
        AND [Retention3D] IS NULL
        AND [Retention4D] IS NULL
        AND [Retention5D] IS NULL
    AND RegistrationClientVersion = @cv
)

SELECT Count(*) FROM PoU

SELECT ResourceTrigger,  ResourceRequired
, Count(Distinct UserId) AS Users
, Count(*) AS Transactions
FROM NotEnoughMinerals JOIN PoU ON UserID = U_ID
GROUP BY ResourceTrigger,  ResourceRequired

SELECT ClientVersion, 
Count(*) AS A
FROM EventsAppOpen
GROUP BY ClientVersion