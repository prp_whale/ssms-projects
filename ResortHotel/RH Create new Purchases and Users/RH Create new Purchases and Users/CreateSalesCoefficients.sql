DROP TABLE IF EXISTS PRP_Sales_Coefficients;

DECLARE @Sales_Coefficients TABLE 
  ( 
     [Timestamp] datetime, 
     [Amount]    float, 
	 [Platform]	 int,
     [Country]   varchar(2), 
     [Coeff]     float 
	
  ); 

INSERT INTO @Sales_Coefficients VALUES 
			(CAST('2019-04-19 00:00:00.000' AS datetime),0.99,1,'RU',0.33)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),1.99,1,'RU',0.34)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),2.99,1,'RU',0.34)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),4.99,1,'RU',0.32)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),7.99,1,'RU',0.33)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),9.99,1,'RU',0.34)
			,(CAST('2019-05-22 00:00:00.000' AS datetime),14.99,1,'RU',0.34)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),15.99,1,'RU',0.34)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),19.99,1,'RU',0.34)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),29.99,1,'RU',1.0)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),31.99,1,'RU',0.34)
			,(CAST('2019-05-22 00:00:00.000' AS datetime),44.99,1,'RU',0.34)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),49.99,1,'RU',1.0)
			,(CAST('2019-05-22 00:00:00.000' AS datetime),59.99,1,'RU',0.34)
			,(CAST('2019-05-22 00:00:00.000' AS datetime),99.99,1,'RU',0.34)

			,(CAST('2019-04-19 00:00:00.000' AS datetime),0.99,1,'PL',0.43)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),1.99,1,'PL',0.44)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),2.99,1,'PL',0.40)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),4.99,1,'PL',0.41)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),7.99,1,'PL',0.42)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),9.99,1,'PL',0.39)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),15.99,1,'PL',0.40)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),19.99,1,'PL',0.40)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),31.99,1,'PL',0.40)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),49.99,1,'PL',1.0)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),59.99,1,'PL', 1.0)
			,(CAST('2019-04-19 00:00:00.000' AS datetime),99.99,1,'PL', 1.0)

SELECT * 
INTO   PRP_Sales_Coefficients 
FROM   @Sales_Coefficients 

