DECLARE @start_period DATETIME = '2018-12-01 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-06-01 23:59:59.997'; 


DROP TABLE IF EXISTS PRP_Inviters_LESS_1_6_0;

SELECT [UserId]					AS U_ID
       ,Count (*)                AS Invitations  
INTO PRP_Inviters_LESS_1_6_0
FROM  [dbo].[ev_InvitationAction]  AS IA
	INNER JOIN PRP_Pool_Of_Active_Users_6_Month_LESS_1_6_0 AS POU
		ON	IA.[UserId] = POU.U_ID
WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
   AND ( ( [ClientVersion] BETWEEN '1.0.0' AND '1.1.99' )  OR ( [ClientVersion] BETWEEN '1.2.0' AND '1.5.99' ) ) -- LESS_1_6_0 
   AND ([ActionId] = 1 OR [ActionId] = 2)
GROUP  BY [UserId] 
ORDER  BY Invitations DESC 

SELECT Count (DISTINCT U_ID) AS Users 
       ,Count (*)            AS Amount 
FROM   PRP_Inviters_LESS_1_6_0


------------------------------------------------------------------------------------

--DROP TABLE IF EXISTS PRP_Inviters_MORE_1_6_0;

--SELECT [UserId]					AS U_ID
--       ,Count (*)                AS Invitations  
--INTO PRP_Inviters_MORE_1_6_0
--FROM  [dbo].[ev_InvitationAction]  AS IA
--	INNER JOIN PRP_Pool_Of_Active_Users_6_Month_MORE_1_6_0 AS POU
--		ON	IA.[UserId] = POU.U_ID
--WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
--   AND ([ClientVersion] >= '1.6.0' OR ([ClientVersion] BETWEEN '1.11.0' AND  '1.19.99')) -- MORE_1_6_0
--   AND ([ActionId] = 1)
--GROUP  BY [UserId] 
--ORDER  BY Invitations DESC 

--SELECT Count (DISTINCT U_ID) AS Users 
--       ,Count (*)                 AS Amount 
--FROM   PRP_Inviters_MORE_1_6_0


------------------------------------------------------------------------------------

DROP TABLE IF EXISTS PRP_Inviters;


SELECT DISTINCT [UserId]  AS U_ID
 INTO PRP_Inviters
   FROM [dbo].[ev_FriendsWindow] 
   WHERE  [ActionId] = 'add_friend'
UNION
   SELECT DISTINCT [UserId] 
 SELECT DISTINCT [UserId] 
   FROM [dbo].[ev_InvitationAction]
   WHERE  [ActionId] = 1 OR [ActionId] = 2


SELECT Count (DISTINCT U_ID) AS Users 
      ,Count (*)                 AS Amount 
FROM   PRP_Inviters

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------


-- SELECT Count(DISTINCT [UserId])  AS U_ID
-- FROM [dbo].[ev_FriendsWindow] AS FW  JOIN PRP_Pool_Of_Active_Users_MORE_1_14_2 AS POU ON POU.[U_ID] = FW.[UserId]
-- WHERE  [ActionId] = 'add_friend'


SELECT Count(DISTINCT I.[U_ID])  AS U_ID
FROM PRP_Inviters  AS I INNER  JOIN PRP_Pool_Of_Active_Users_MORE_1_14_2 AS POU ON POU.[U_ID] = I.[U_ID]

-- SELECT Count(DISTINCT I.[U_ID])  AS U_ID
-- FROM PRP_Pool_Of_Real_Invatiors  AS I INNER  JOIN PRP_Pool_Of_Active_Users_MORE_1_14_2 AS POU ON POU.[U_ID] = I.[U_ID]

------------------------------------------------------------------
------------------------------------------------------------------

SELECT Count(DISTINCT [UserId])  AS U_ID
   FROM [dbo].[ev_InvitationAction] AS FW  INNER JOIN PRP_Pool_Of_Active_Users_6_Month_MORE_1_6_0 AS POU ON POU.[U_ID] = FW.[UserId]

   WHERE  ([ActionId] = 1 OR [ActionId] = 2)
   AND [TimeStamp] BETWEEN  '2019-06-01 00:00:00.000' AND '2019-07-31 23:59:59.997'

SELECT Count(DISTINCT I.[U_ID])  AS U_ID
FROM PRP_Pool_Of_Real_Invatiors  AS I INNER  JOIN PRP_Pool_Of_Active_Users_6_Month_MORE_1_6_0 AS POU ON POU.[U_ID] = I.[U_ID]


SELECT Count(DISTINCT U.[UserId])  AS U_ID
FROM  [dbo].[ev_InvitationFriends] AS I 
		INNER JOIN [dbo].[Users] AS U ON U.[GameUserId] =  NULLIF( I.[UserIdInvitationSender],'null') 
		LEFT JOIN PRP_Inviters AS IA  ON	IA.[U_ID] = U.[UserID]
      
WHERE IA.[U_ID] IS NOT NULL 
AND NOT ( (U.[UserID] = I.[UserID]) AND (NULLIF( I.[UserIdInvitationSender],'null') = U.[GameUserId] ))
AND (I.[ClientVersion] >= '1.6.0' OR (I.[ClientVersion] BETWEEN '1.11.0' AND  '1.14.01')) -- MORE_1_6_0
 AND I.[TimeStamp] BETWEEN  '2019-06-01 00:00:00.000' AND '2019-07-31 23:59:59.997'