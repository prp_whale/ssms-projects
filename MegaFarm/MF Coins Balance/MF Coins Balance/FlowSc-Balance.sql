declare @start_date datetime	= '2019-02-11 00:00:00';
declare @end_date datetime		= '2019-02-24 23:59:59';
declare @cheater int			= 7249;
declare @depositor int			= 0;
--  SELECT [level ] as LVL
--	,MAX([coins]) as MAX_
--	,[UserId]
--  FROM [dbo].[balance]
--  WHERE  [timestamp] between @start_date and @end_date
--  AND [UserId] != @cheater
--  --AND [is_depositor] = 0
--  GROUP BY [UserId], [level]
--  ORDER BY LVL

--  SELECT a.*
--FROM [dbo].[balance] a
--LEFT OUTER JOIN [dbo].[balance] b
--    ON a.[UserId] = b.[UserId] AND a.[coins] < b.[coins]
--WHERE b.id IS NULL;

--SELECT a.id, a.rev, a.contents
--FROM YourTable a
--INNER JOIN (
--    SELECT id, MAX(rev) rev
--    FROM YourTable
--    GROUP BY id
--) b ON a.id = b.id AND a.rev = b.rev

--WITH UNSINGED_DELTA AS(
SELECT	a.[level]		as LVL
	,	a.[UserId]		as U
	,	a.[Timestamp]	as timest
	,	a.[coins]		as COINS
	,	b.delta			as DELTA
FROM [dbo].[balance]	as a
	INNER JOIN (
	   SELECT [level] 
		,MAX([coins]) as max_coins
		,min([coins]) as min_coins
		,[UserId] as U_id
		,MAX([coins])- MIN([coins]) as delta
	  FROM [dbo].[balance] 
	  WHERE  [timestamp] between @start_date and @end_date
	  AND [UserId] != @cheater
	  --AND [is_depositor] = @depositor
	  GROUP BY [UserId], [level]
) as b ON a.[UserId] = b.U_id AND (a.[coins] = b.max_coins OR a.[coins] = b.min_coins  )
	WHERE  [timestamp] between @start_date and @end_date
	AND DELTA != 0
	  ORDER BY LVL, U, timest desc
--)

--SELECT DISTINCT U
--	,LVL
--	, DELTA
--	FROM UNSINGED_DELTA
--	ORDER BY LVL, U

--SELECT LVL
--	,AVG(DELTA) as Average
--	FROM UNSINGED_DELTA
--	GROUP BY LVL
--	ORDER BY LVL

 -- SELECT [level ] 
	--,MAX([coins])
	--,[UserId]
 -- FROM [dbo].[balance]
 -- WHERE  [timestamp] between @start_date and @end_date
 -- AND [UserId] != @cheater
 -- --AND [is_depositor] = 0
 -- GROUP BY [UserId], [level]
 -- ORDER BY LVL