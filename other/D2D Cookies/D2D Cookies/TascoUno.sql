
WITH SOURSES_USERS_PAYMENTS
AS (
SELECT Installs.source AS 'source'
		,Payments.player_id 
        ,(SELECT Sum (P.amount) - Sum (P.tax) 
			FROM   PRP_D2D_Payments AS P 
				   INNER JOIN PRP_D2D_Installs AS I 
						   ON P.player_id = I.player_id 
			WHERE  I.date_index < Dateadd(MM, -1, Getdate()) 
			   AND P.date_index BETWEEN I.date_index AND Dateadd(DD, 1, I.date_index) 
			   AND P.player_id = Payments.player_id 
			GROUP  BY P.player_id 
				) AS PAY_DAY_0
		  ,(SELECT Sum (P.amount) - Sum (P.tax) 
				FROM   PRP_D2D_Payments AS P 
					   INNER JOIN PRP_D2D_Installs AS I 
							   ON P.player_id = I.player_id 
				WHERE  I.date_index < Dateadd(MM, -1, Getdate()) 
				   AND P.date_index BETWEEN Dateadd(DD, 1, I.date_index) AND 
											Dateadd(DD, 2, I.date_index) 
				   AND P.player_id = Payments.player_id 
				GROUP  BY P.player_id 
 					) AS PAY_DAY_1
		  ,(SELECT Sum (P.amount) - Sum (P.tax) 
				FROM   PRP_D2D_Payments AS P 
					   INNER JOIN PRP_D2D_Installs AS I 
							   ON P.player_id = I.player_id 
				WHERE  I.date_index < Dateadd(MM, -1, Getdate()) 
				   AND P.date_index BETWEEN Dateadd(DD, 2, I.date_index) AND 
											Dateadd(DD, 3, I.date_index) 
				   AND P.player_id = Payments.player_id 
				GROUP  BY P.player_id 
 					) AS PAY_DAY_2
		  ,(SELECT Sum (P.amount) - Sum (P.tax) 
				FROM   PRP_D2D_Payments AS P 
					   INNER JOIN PRP_D2D_Installs AS I 
							   ON P.player_id = I.player_id 
				WHERE  I.date_index < Dateadd(MM, -1, Getdate()) 
				   AND P.date_index BETWEEN Dateadd(DD, 3, I.date_index) AND 
											Dateadd(DD, 4, I.date_index) 
				   AND P.player_id = Payments.player_id 
				GROUP  BY P.player_id 
 					) AS PAY_DAY_3
FROM   PRP_D2D_Payments AS Payments 
 INNER JOIN PRP_D2D_Installs AS Installs 
				ON Payments.player_id = Installs.player_id
     
)

SELECT [source]
 , SUM(PAY_DAY_0)/ COUNT (DISTINCT player_id) AS CARPU_DAY_0
  , SUM(PAY_DAY_1)/ COUNT (DISTINCT player_id) AS CARPU_DAY_1
   , SUM(PAY_DAY_2)/ COUNT (DISTINCT player_id) AS CARPU_DAY_2
    , SUM(PAY_DAY_3)/ COUNT (DISTINCT player_id) AS CARPU_DAY_3
FROM SOURSES_USERS_PAYMENTS
GROUP BY [source]
ORDER BY [source]