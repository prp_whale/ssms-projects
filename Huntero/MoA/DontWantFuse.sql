
;WITH Fusers
AS  (
    SELECT DISTINCT UserID  AS U_ID
    FROM dbo.EquipmentMovement 
    WHERE TriggerSourceId = 'Fuse'
    AND ClientVersion IN ('0.0.105', '0.0.108')
)
,PoolOfUsers
AS (
    SELECT DISTINCT UserID AS U_ID
    FROM dbo.EquipmentMovement 
    WHERE  ClientVersion IN ('0.0.105', '0.0.108')
    AND UserID NOT IN (SELECT * FROM Fusers )
)

, WantNotFuse 
AS (
    SELECT UserID, HeroSlotCategoryId, EquipmentId
    , Count(*) AS Amount
    FROM dbo.EquipmentMovement AS EM INNER JOIN PoolOfUsers AS pou ON pou.U_ID = em.UserId
    WHERE  ClientVersion IN ('0.0.105', '0.0.108')
    GROUP BY UserID, HeroSlotCategoryId, EquipmentId
    HAVING Count(*) >= 3
) 

SELECT Count (Distinct UserId) FROM WantNotFuse 
--SELECT Count (Distinct U_ID) FROM PoolOfUsers 
--SELEcT TOP (100) * FROM WantNotFuse