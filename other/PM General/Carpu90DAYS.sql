declare @st_per DATETIME = '2020-01-01 00:00:00:000';
declare @end_per DATETIME  = '2020-06-01 23:59:59:997';

declare @rc NVARCHAR(5) = 'us' -- RegistrationCountry
declare @ms NVARCHAR(40) = 'facebook ads' -- MediaSource
declare @pl INT  = 1; -- Platform


WITH Cohort
AS (
    SELECT Convert(date, [Created]) AS DateCreate
    , UserID AS U_ID
    FROM dbo.Users
    WHERE [Created] BETWEEN @st_per AND @end_per 
    and MediaSource = @ms
    and Platform = @pl
    and RegistrationCountry = @rc

)

SELECT DateCreate AS Created
    , (Count(DISTINCT U_ID)) AS Users
    , (Count(DISTINCT UserId)) AS Payers
    , SUM ([AmountInUSD])/ (Count(DISTINCT U_ID)) AS LTV
    , (SELECT SUM ([AmountInUSD])   FROM [dbo].[EventsPurchase] WHERE EXISTS (SELECT * FROM Cohort) AND  Convert(date, [Created]) = DateCreate AND [Timestamp] BETWEEN Created AND DATEADD(dd ,1 , Created)) / (Count(DISTINCT U_ID)) AS CARPU_D1
    , (SELECT SUM ([AmountInUSD])   FROM [dbo].[EventsPurchase] WHERE EXISTS (SELECT * FROM Cohort) AND   Convert(date, [Created]) = DateCreate AND [Timestamp] BETWEEN Created AND DATEADD(dd, 2 , Created)) / (Count(DISTINCT U_ID)) AS CARPU_D2
FROM [dbo].[EventsPurchase]  AS EP RIGHT JOIN Cohort ON UserID = U_ID 
WHERE DateCreate BETWEEN @st_per AND @end_per
GROUP BY DateCreate
ORDER BY DateCreate

-- SELECT Convert(date, [Created]) AS Created
--     , SUM ([AmountInUSD])/ (Count(DISTINCT UserID)) AS LTV
--     , (SELECT SUM ([AmountInUSD])   FROM [dbo].[EventsPurchase]   WHERE Convert(date, [Created]) = Convert(date, EP.[Created]) AND [Timestamp] BETWEEN Created AND DATEADD(dd ,1 , Created)) / (Count(DISTINCT UserID)) AS CARPU_D1
--     , (SELECT SUM ([AmountInUSD])   FROM [dbo].[EventsPurchase]   WHERE Convert(date, [Created]) = Convert(date, EP.[Created]) AND [Timestamp] BETWEEN Created AND DATEADD(dd, 2 , Created)) / (Count(DISTINCT UserID)) AS CARPU_D2
-- FROM [dbo].[EventsPurchase]  AS EP 
-- WHERE [Created] BETWEEN @st_per AND @end_per
-- GROUP BY Convert(date, [Created])

-- SELECT Convert(date, [Created]), 
-- Count (DISTINCT UserID) AS A 
-- FROM EventsPurchase
-- GROUP BY Convert(date, [Created])
-- ORDER BY Convert(date, [Created])