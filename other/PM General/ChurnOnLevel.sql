declare @st_per DATETIME = '2020-05-01 00:00:00:000';
declare @end_per DATETIME  = '2020-05-10 23:59:59:997';
--DECLARE @cv NVARCHAR(10) = '1.17';

WITH PoU 
As (
    SELECT  UserId AS U_ID
    FROM dbo.Users 
    WHERE Created BETWEEN @st_per AND @end_per
        AND [Retention1D] IS NULL
        AND [Retention2D] IS NULL
        AND [Retention3D] IS NULL
        AND [Retention4D] IS NULL
        AND [Retention5D] IS NULL
  --  AND RegistrationClientVersion = @cv
)

, LastKatka
AS (
    SELECT UserId AS U_ID
    ,Max([TIMESTAMP]) as ls_time 
     FROM 
    EventsLevelAttempts
    JOIN PoU ON UserId = U_ID
    GROUP BY UserId
)
 , CTE3 AS  
(
    SELECT LevelId
    , CompletedType
    , Completed
    , Crown
    , AddSteps
    , UserId
    FROM EventsLevelAttempts JOIN LastKatka ON UserId = U_ID AND [Timestamp] = ls_time
    WHERE LevelId <= 10
)

, LastMove 
AS (
    SELECT UserID as U_ID
    , Max([Timestamp]) AS LastMove
    FROM EventsProductMovement AS ela JOIN PoU ON UserID = U_ID 
    GROUP BY UserId
)

, Coins
as (
    SELECT CTE3.LevelID
    , AVG (ProductBalance) AS AvgCoins
    FROM EventsProductMovement as EMP 
            JOIN CTE3 ON CTE3.UserId = EMP.UserId
            JOIN LastMove ON EMP.UserId = U_ID AND [Timestamp] = LastMove
    GROUP BY CTE3.LevelID
 )


SELECT * FROM Coins ORDER BY LevelID


-- SELECT LevelId , Completed  , Crown AS CoinsPrepaid , AddSteps * 90 AS CoinsOnSteps
-- ,   Count(UseriD) AS users
-- FROM CTE3

-- GROUP BY  LevelId, Completed, Crown, AddSteps
-- HAVING Count(UseriD)  > 1 
-- ORDER BY Completed DESC, LevelId, Crown, AddSteps