DECLARE @start_date Datetime = '2019-02-11 00:00:00.000'; 
DECLARE @end_date Datetime = '2019-02-24 23:59:59.999'; 
DECLARE @cheater Int = 7249; 
DECLARE @test_country Varchar(2) = 'UA' 

SELECT [slot6_description]        AS SLOT 
       , Count(*)                 AS Amount 
       , Count(DISTINCT [UserId]) AS USERS 
FROM   [dbo].[order_drone] 
WHERE  [Timestamp] BETWEEN @start_date AND @end_date 
   AND [UserId] != @cheater 
   AND [RegistrationCountry] != @test_country 
   AND [LTV] = 0 
   AND [order_status] = 1
   AND [slot6_description]  IS NOT NULL
GROUP  BY [slot6_description] 
ORDER  BY SLOT 