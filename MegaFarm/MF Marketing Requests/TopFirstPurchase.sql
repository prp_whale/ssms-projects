DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform varchar(4)

SET @startperiod = '2019-09-01 00:00:00.000';
SET @endperiod = '2019-11-01 23:59:59.999';

WITH
    AutumnUsers
    AS
    (
        SELECT UserId  AS U_ID
        FROM dbo.Users
        WHERE Created BETWEEN @startperiod AND @endperiod
          AND GameUserId NOT IN (103,	1240,	22,	1064,	0,	1002,	69,	482,	93,	671,	1050,	22,	1028,	647,	1004) -- testers
    )

SELECT ProductId 
, COUNT(DISTINCT UserId) AS Depositors
--, COUNT (*) AS Trans
FROM [dbo].[EventsPurchase] AS EP INNER JOIN AutumnUsers AS POU ON EP.[UserId]  = POU.[U_ID]
WHERE PaymentsCount > 3
GROUP BY ProductId 
ORDER BY Depositors DESC

SELEcT UserId,  Count( *)  AS A  FROM [dbo].[EventsPurchase]  GROUP BY  UserId Order by A DESC

SELEcT *  FROM [dbo].[EventsPurchase] WHERE UserId = 88888	ORDER BY [Timestamp]