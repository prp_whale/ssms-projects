/*Отвал игроков
В данной таблице интересует пользователи, которые ушли из игры (можно сделать несколько временных рамок ухода - 1 неделя, 2 недели... 1 месяц). 
При построении графика нужно учитывать следующие параметры:
уровень игрока
баланс игроков (мин/макс/средний)
разделение на депозиторов и недепозиторов
возможность выбирать версии и временные промежутки  
при необходимости, дать возможность получить ID отвалившихся игроков с учетом выбранных параметров
*/

declare @days int = 28;

declare @days_multi int = 1;
declare @st_per DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days*@days_multi, GETDATE())));
declare @end_per DATETIME  = DATEADD(ms, -3, CONVERT(datetime, CONVERT(date, GETDATE())));

declare @versa_limit int = 100;


WITH Versa
AS (
    SELECT registrationclientversion AS RegVer
    FROM  dbo.Users
   -- WHERE  created BETWEEN @st_per AND @end_per
    GROUP BY registrationclientversion
    HAVING Count(*) >=  @versa_limit
)

, LvlUp
AS (
    SELECT UserId
        , CONVERT(date,Timestamp) as lvlup_date
        , TriggerId AS lvl
        , ClientVersion
        ,CASE   WHEN ltv > 0 THEN 'Yes'  ELSE 'No'  END AS 'IsPay'
        , FIRST_VALUE (TriggerId) OVER (PARTITION BY UserId, CONVERT(date,Timestamp) ORDER BY Timestamp DESC) as last_lvlup
        , ProductBalance AS 'GoldBalanse'
    FROM EventsProductMovement
    WHERE ClientVersion IN (SELECT * FROM Versa)
        AND [Timestamp] BETWEEN @st_per AND @end_per
        AND ProductType = 'coins'
        AND [Action] = 'received'
        AND [TriggerSourceId] = 'level_up'
)

,GameOpen 
AS (
    SELECT UserID AS U_ID
            , CONVERT(date,Timestamp) as first_date
            ,  LEAD (CONVERT(date,Timestamp),1,CONVERT(date,getdate())) OVER (PARTITION BY UserId ORDER BY CONVERT(date,Timestamp) ASC)  as next_date
    FROM balance 
   
    WHERE [ClientVersion] IN (SELECT * FROM Versa)
        AND [Timestamp] BETWEEN @st_per AND @end_per
        AND [level] IS NOT NULL
    GROUP BY UserId , CONVERT(date,Timestamp)
)



SELECT lvlup_date   , ClientVersion , [IsPay] , lvl
, COUNT(Distinct UserId) AS Users
, COUNT(Distinct (iif (datediff(dd, lvlup_date, next_date)>1, UserID, null))) AS Churn_1D
, COUNT(Distinct (iif (datediff(dd, lvlup_date, next_date)>3, UserID, null))) AS Churn_3D
, COUNT(Distinct (iif (datediff(dd, lvlup_date, next_date)>7, UserID, null))) AS Churn_7D
, COUNT(Distinct (iif (datediff(dd, lvlup_date, next_date)>14, UserID, null))) AS Churn_14D
, COUNT(Distinct (iif (datediff(dd, lvlup_date, next_date)>28, UserID, null))) AS Churn_28D
, min([GoldBalanse] ) AS MinCoin
, avg([GoldBalanse] ) AS AvGCoin
, max([GoldBalanse] ) AS MaxCoin
FROM LvlUp LEFT JOIN GameOpen ON UserId = U_ID AND lvlup_date = first_date AND lvl = last_lvlup
GROUP BY lvlup_date , ClientVersion , [IsPay] , lvl
ORDER BY lvlup_date   , ClientVersion , [IsPay] , Convert(int, lvl)


