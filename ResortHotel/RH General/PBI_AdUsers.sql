DECLARE @st_per datetime,
        @end_per datetime

DECLARE @days_ago_st INT = 90;
DECLARE @days_ago_end INT = 0; -- 0 = yesterday

SET @st_per  =  DATEDIFF(dd, @days_ago_st, GETDATE())  ; 
SET @end_per  = DATEADD(ms, -3 ,  DATEDIFF(dd, @days_ago_end, GETDATE()) ) ; 

SELECT TOP 100 AD.[AdvertisingID]
   , MAX(U.UserId) AS BOUserID
   , SUM(Impressions) AS TotalView
   , SUM (Revenue) AS TotalRevenue
   FROM [dbo].[ISUserAdRevenue] AS AD  LEFT JOIN dbo.Users AS U ON AD.AdvertisingID = U.AdvertisingID AND [Created] BETWEEN @st_per AND @end_per
WHERE [Date] BETWEEN @st_per AND @end_per
AND [ProjectName] = DB_NAME()
GROUP BY AD.[AdvertisingID]