declare @start_date datetime	= '2019-02-11 00:00:00';
declare @end_date datetime		= '2019-02-24 23:59:59';
declare @cheater int			= 7249

SELECT TOP (1000) [SessionId]
      ,[EventId]
      ,[UserId]
      ,[Timestamp]
      ,[ClientTime]
      ,[BackOfficeTime]
      ,[Order]
      ,[ClientVersion]
      ,[IsOffline]
      ,[Platform]
      ,[Created]
      ,[LTV]
      ,[Retention1D]
      ,[Retention2D]
      ,[Retention7D]
      ,[FirstDepositDate]
      ,[LTV7D]
      ,[LTV2D]
      ,[LTV1D]
      ,[LTV30D]
      ,[RegistrationCountry]
      ,[cash]
      ,[coins]
      ,[friends]
      ,[is_depositor]
      ,[level]
      ,[max_population]
      ,[population]
      ,[silage_capacity]
      ,[silage_filled]
      ,[storage_capacity]
      ,[storage_filled]
      ,[xp]
      ,[gems1]
      ,[gems2]
      ,[gems3]
      ,[gems4]
      ,[orders_fulfilled]
      ,[planes_sent]
      ,[products_produced]
      ,[trucks_sent]
  FROM [dbo].[balance]
  WHERE  [timestamp] between @start_date and @end_date
   AND [UserId] = 3859
  ORDER BY [Timestamp]


--  SELECT [level ] as LVL
----	,AVG(iif([coins]>0,coins,null)) as AVERAGE
--	,AVG([coins]) as AVERAGE
--	--,SUM([coins])*1.0 / nullif(count (distinct [UserId]),0) as BALANCE_PER_USER
--	,COUNT(*) as C
--  FROM [dbo].[balance]
--  WHERE  [timestamp] between @start_date and @end_date
--  AND [UserId] != @cheater
--  AND [is_depositor] = 0
--  GROUP BY [level]
--  ORDER BY LVL
--SELECT [UserId]
--,count(*) as AMOUNT
--FROM [dbo].[balance]
--  WHERE  [timestamp] between @start_date and @end_date
--  GROUP BY [UserId]
--  ORDER BY AMOUNT DESC



  SELECT [level ] as LVL
	,MAX([coins]) as MAX_
	,[UserId]
	--,MIN([coins]) as MIN_
	--,[Timestamp] as time
	--,SUM([coins])*1.0 / nullif(count (distinct [UserId]),0) as BALANCE_PER_USER
	,COUNT(*) as C
	,COUNT(DISTINCT [UserID]) as U
  FROM [dbo].[balance]
  WHERE  [timestamp] between @start_date and @end_date
  --AND [UserId] = 3859
  AND [UserId] != @cheater
  --AND [is_depositor] = 0
  GROUP BY [UserId], [level]
  ORDER BY LVL