DECLARE @start_period DATETIME = '2019-07-04 00:00:00.000';  -- INDEPENDESE EVENT
DECLARE @end_period DATETIME = '2019-07-08 23:59:59.997'; 

-- SET @start_period  =  DATEADD (WW, -1, @start_period); 
--SET @end_period =  DATEADD (WW, -1, @end_period); 

--SET @start_period  =  DATEADD (WW, -2, @start_period); 
--SET @end_period =  DATEADD (WW, -2, @end_period); 

--SELEcT @start_period, @end_period

SELECT [segment_2]
,	Count (DISTINCT UserId) AS 'All Users'
,	(
		SELECT Count (DISTINCT UserId) 
		FROM   [dbo].[ev_OfferPurchase] AS TT 
			   INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
					   ON TT.[UserId] = POU.U_ID 
		WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
		   AND [ClientVersion] BETWEEN '1.14.0' AND '1.19.99' 
		   AND TT.[segment_2] = MQ.[segment_2] 
		GROUP  BY [segment_2] 
	)  AS Users 
,	(
		SELECT Count (DISTINCT UserId) 
		FROM   [dbo].[ev_OfferPurchase] AS TT 
			   INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
					   ON TT.[UserId] = POU.U_ID 
		WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
		   AND [ClientVersion] BETWEEN '1.14.0' AND '1.19.99' 
		   AND [Result] = 'OK' 
		   AND TT.[segment_2] = MQ.[segment_2] 
		GROUP  BY [segment_2] 
	)  AS Depositors
FROM   [dbo].[ev_OfferShowUp] AS MQ 
       INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
               ON MQ.[UserId] = POU.U_ID 
WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
   AND [ClientVersion] BETWEEN '1.14.0' AND '1.19.99' 
   AND [segment_2] BETWEEN 1201 AND 1204 
GROUP  BY [segment_2] 
ORDER  BY [segment_2] 
--------------------------------------------------------------------------------------------------
-- without version
--SELECT [segment_2]
--,	Count (DISTINCT UserId) AS 'All Users'
--,	(
--		SELECT Count (DISTINCT UserId) 
--		FROM   [dbo].[ev_OfferPurchase] AS TT 
--			   INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
--					   ON TT.[UserId] = POU.U_ID 
--		WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
--		   AND TT.[segment_2] = MQ.[segment_2] 
--		GROUP  BY [segment_2] 
--	)  AS Users 
--,	(
--		SELECT Count (DISTINCT UserId) 
--		FROM   [dbo].[ev_OfferPurchase] AS TT 
--			   INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
--					   ON TT.[UserId] = POU.U_ID 
--		WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
--		   AND [Result] = 'OK' 
--		   AND TT.[segment_2] = MQ.[segment_2] 
--		GROUP  BY [segment_2] 
--	)  AS Depositors
--FROM   [dbo].[ev_OfferShowUp] AS MQ 
--       INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
--               ON MQ.[UserId] = POU.U_ID 
--WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
--   AND [segment_2] BETWEEN 1201 AND 1204 
--GROUP  BY [segment_2] 
--ORDER  BY [segment_2] 
