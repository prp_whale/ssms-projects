DECLARE @st_per DATETIME = '2020-03-01 00:00:00.000';
DECLARE @end_per DATETIME =  '2020-03-31 23:59:59.997';

DECLARE @days_tail INT = 10;


/*
кто может тогда сделать запрос и найти количество депозиторов, которые стали депозиторами в первые 3 дня жизни в марте 20 года на проекте ресорт?)
*/

-- WITH PoU 
-- AS (
--     SELECT 1 AS U_ID
--     , event_time AS [Created]
--     FROM [dbo].[AFEvents]
--     WHERE event_time BETWEEN @st_per AND @end_per
--     AND event_name = 'install'
-- )

SELECT platform 
    ,Count(DISTINCT advertising_id)
  FROM [dbo].[AFEvents]
  WHERE event_name = 'af_purchase'
  AND [event_time] BETWEEN [install_time] AND DATEADD(dd, @days_tail, install_time)
  AND [ProjectName] = 'ResortHotel'
  AND [install_time] BETWEEN @st_per AND @end_per
  GROUP BY platform

