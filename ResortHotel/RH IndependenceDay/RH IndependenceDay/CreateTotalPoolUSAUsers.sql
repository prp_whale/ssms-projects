DECLARE @certain_country Varchar(2) = 'US';

DROP TABLE IF EXISTS PRP_TOTAL_Pool_Of_Active_USA_Users;

SELECT U.[UserId] AS U_ID
INTO PRP_TOTAL_Pool_Of_Active_USA_Users
FROM   [dbo].[Users] AS U 
		INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS AU 
              ON AU.U_ID = U.[UserId]
WHERE [RegistrationCountry] = @certain_country  
 
SELECT Count (DISTINCT U_ID) AS Users 
       ,Count (*)                 AS Amount 
FROM   PRP_TOTAL_Pool_Of_Active_USA_Users