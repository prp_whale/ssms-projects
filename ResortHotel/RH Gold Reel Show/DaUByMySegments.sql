DECLARE @startperiod datetime,
        @endperiod datetime,
        @segment VARCHAR(100),
    	@platform int,
        @ua VARCHAR (2),
        @first_deposit int;

    SET @startperiod = '2019-08-15 00:00:00.000';
    SET @endperiod = '2019-11-07 23:59:59.999';
    SET @ua = 'UA';

    SET @segment = 'non-depositors';
    SET @segment ='minnows';
    SET @segment = 'dolphins';
    SET @segment = 'grand dolphins';
   -- SET @segment = 'whales';
    
    SET @first_deposit = 1;
    SET @platform = 1 ;

WITH UsersAndSegments
AS(
    SELECT UserId  as U_ID
    , CASE 
        WHEN LTV = 0 THEN  'non-depositors'
        WHEN LTV between 0.0001 and 7 THEN  'minnows'
        WHEN LTV between 7.0001 and 70 THEN  'dolphins'
        WHEN LTV between 70.0001 and 300 THEN  'grand dolphins'
        WHEN LTV > 300 THEN  'whales'
        ELSE 'Error occured'
     END AS segment
    FROM [dbo].[Users]
    WHERE [RegistrationCountry] != @ua
 )

-- SELECT @segment as segment
-- , t.U AS Active_U
-- , t2.U  AS Purchase_U
-- , t3.U  AS Golden_U
-- , IIF(t2.U = 0,0,t2.U*1.0 / t.U) AS 'P/A'
-- , IIF(t2.U = 0,0,t3.U*1.0 / t.U) AS 'P/A'
-- , IIF(t2.U = 0,0, t3.U*1.0 / t2.U) AS 'G/P'
-- FROM 
-- (
-- Select '1' as link 
--  ,Count(Distinct UserId) AS U
-- FROM [dbo].[EventsAppOpen] AS T 
--         INNER JOIN UsersAndSegments AS POU ON T.[UserId] = POU.[U_ID] AND POU.[segment] = @segment
-- WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
-- ) AS t 
-- INNER JOIN 
-- (
--     Select '1' as link 
--     ,Count(Distinct UserId) AS U
--     FROM [dbo].[EventsPurchase] AS T 
--         INNER JOIN UsersAndSegments AS POU ON T.[UserId] = POU.[U_ID] AND POU.[segment] = @segment
--     WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
--     AND [FirstDeposit] = @first_deposit
-- )
-- AS t2 ON t.link = t2.link 
-- INNER JOIN 
-- (
--     Select '1' as link 
--     ,Count(Distinct UserId) AS U
--     FROM [dbo].[EventsPurchase] AS T 
--         INNER JOIN UsersAndSegments AS POU ON T.[UserId] = POU.[U_ID] AND POU.[segment] = @segment
--     WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
--     AND ProductId LIKE 'golden_ors_%'
--     AND [FirstDeposit] = @first_deposit
-- )
-- AS t3 ON t.link = t3.link 

SELECT * 
FROM dbo.Users  AS U
INNER JOIN  [dbo].[EventsPurchase]  AS T ON U.UserId = T.UserId
        INNER JOIN UsersAndSegments AS POU ON T.[UserId] = POU.[U_ID] AND POU.[segment] = @segment
WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
    AND ProductId LIKE 'golden_ors_%'
    AND [FirstDeposit] = @first_deposit

    SELECT * FROM [dbo].[EventsPurchase]  WHERE UserId = 5589956 AND ProductId LIKE 'golden_ors_%' 
--5589956	
--5386399	