SELECT [UserId] 
       ,(SELECT Sum([AmountInUsd]) 
         FROM   [dbo].[EventsPurchaseCoeff] as E 
         WHERE  EPC.[UserId] = E.[UserId] 
                AND E.[Timestamp] BETWEEN E.[Created] AND Getdate ()) AS 'LTV' 
       ,(SELECT Sum([AmountInUsd]) 
         FROM   [dbo].[EventsPurchaseCoeff] as E 
         WHERE  EPC.[UserId] = E.[UserId] 
                AND E.[Timestamp] BETWEEN E.[Created] AND 
                Dateadd (hh, 1, E.[Created])) AS 'LTV1H' 
		,(SELECT Sum([AmountInUsd]) 
         FROM   [dbo].[EventsPurchaseCoeff] as E 
         WHERE  EPC.[UserId] = E.[UserId] 
                AND E.[Timestamp] BETWEEN E.[Created] AND 
                Dateadd (hh, 3, E.[Created])) AS 'LTV3H' 
		,(SELECT Sum([AmountInUsd]) 
         FROM   [dbo].[EventsPurchaseCoeff] as E 
         WHERE  EPC.[UserId] = E.[UserId] 
                AND E.[Timestamp] BETWEEN E.[Created] AND 
                Dateadd (hh, 6, E.[Created])) AS 'LTV6H' 
		,(SELECT Sum([AmountInUsd]) 
         FROM   [dbo].[EventsPurchaseCoeff] as E 
         WHERE  EPC.[UserId] = E.[UserId] 
                AND E.[Timestamp] BETWEEN E.[Created] AND 
                Dateadd (hh, 12, E.[Created])) AS 'LTV12H' 
		,(SELECT Sum([AmountInUsd]) 
         FROM   [dbo].[EventsPurchaseCoeff] as E 
         WHERE  EPC.[UserId] = E.[UserId] 
                AND E.[Timestamp] BETWEEN E.[Created] AND 
                Dateadd (D, 1, E.[Created])) AS 'LTV1D' 
		,(SELECT Sum([AmountInUsd]) 
         FROM   [dbo].[EventsPurchaseCoeff] as E 
         WHERE  EPC.[UserId] = E.[UserId] 
                AND E.[Timestamp] BETWEEN E.[Created] AND 
                Dateadd (D, 2, E.[Created])) AS 'LTV2D' 
		,(SELECT Sum([AmountInUsd]) 
         FROM   [dbo].[EventsPurchaseCoeff] as E 
         WHERE  EPC.[UserId] = E.[UserId] 
                AND E.[Timestamp] BETWEEN E.[Created] AND 
                Dateadd (D, 3, E.[Created])) AS 'LTV3D' 
		,(SELECT Sum([AmountInUsd]) 
         FROM   [dbo].[EventsPurchaseCoeff] as E 
         WHERE  EPC.[UserId] = E.[UserId] 
                AND E.[Timestamp] BETWEEN E.[Created] AND 
                Dateadd (D, 4, E.[Created])) AS 'LTV4D'  
		,(SELECT Sum([AmountInUsd]) 
         FROM   [dbo].[EventsPurchaseCoeff] as E 
         WHERE  EPC.[UserId] = E.[UserId] 
                AND E.[Timestamp] BETWEEN E.[Created] AND 
                Dateadd (D, 5, E.[Created])) AS 'LTV5D' 
		,(SELECT Sum([AmountInUsd]) 
         FROM   [dbo].[EventsPurchaseCoeff] as E 
         WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 6, E.[Created])) AS 'LTV6D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 7, E.[Created])) AS 'LTV7D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 8, E.[Created])) AS 'LTV8D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 9, E.[Created])) AS 'LTV9D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 10, E.[Created])) AS 'LTV10D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 11, E.[Created])) AS 'LTV11D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 12, E.[Created])) AS 'LTV12D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 13, E.[Created])) AS 'LTV13D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 14, E.[Created])) AS 'LTV14D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 15, E.[Created])) AS 'LTV15D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 16, E.[Created])) AS 'LTV16D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 17, E.[Created])) AS 'LTV17D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 18, E.[Created])) AS 'LTV18D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 19, E.[Created])) AS 'LTV19D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 20, E.[Created])) AS 'LTV20D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 21, E.[Created])) AS 'LTV21D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 22, E.[Created])) AS 'LTV22D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 23, E.[Created])) AS 'LTV23D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 24, E.[Created])) AS 'LTV24D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 25, E.[Created])) AS 'LTV25D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 26, E.[Created])) AS 'LTV26D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 27, E.[Created])) AS 'LTV27D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 28, E.[Created])) AS 'LTV28D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 29, E.[Created])) AS 'LTV29D' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (D, 30, E.[Created])) AS 'LTV30D' 
				,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (M, 2, E.[Created])) AS 'LTV2M' 
						,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (M, 4, E.[Created])) AS 'LTV4M' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (M, 6, E.[Created])) AS 'LTV6M' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (Y, 1, E.[Created])) AS 'LTV1Y' 
		,(SELECT Sum([AmountInUsd]) 
					FROM   [dbo].[EventsPurchaseCoeff] as E 
					WHERE  EPC.[UserId] = E.[UserId] 
						AND E.[Timestamp] BETWEEN E.[Created] AND 
		Dateadd (Y, 2, E.[Created])) AS 'LTV12' 

FROM   [dbo].[EventsPurchaseCoeff] AS EPC 
GROUP  BY [UserId] 
ORDER BY [UserId]