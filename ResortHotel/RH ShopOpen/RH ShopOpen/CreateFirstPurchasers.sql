DECLARE @start_period DATETIME = '2019-03-09 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-05-09 23:59:59.997'; 


DROP TABLE IF EXISTS PRP_FIRST_PURCHASERS;

SELECT EP.[UserId]
, EP.[ProductId] As FIRST_PURCHASE
INTO PRP_FIRST_PURCHASERS
FROM   [dbo].[EventsPurchase]  AS EP 
       INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS] AS AU 
              ON EP.[UserId] = AU.U_ID 
WHERE [TimeStamp] BETWEEN @start_period AND @end_period
AND [PaymentsCount] = 1


SELECT Count (DISTINCT [UserId]) AS Users 
       ,Count (*)            AS Amount 
FROM   [dbo].[PRP_FIRST_PURCHASERS] 

