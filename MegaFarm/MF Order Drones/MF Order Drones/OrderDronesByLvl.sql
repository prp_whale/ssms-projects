DECLARE @start_date Datetime = '2019-02-11 00:00:00'; 
DECLARE @end_date Datetime = '2019-02-24 23:59:59'; 
DECLARE @cheater Int = 7249; 

WITH DRONS_AND_LVL 
     AS (SELECT OD.[userid]          AS U_ID 
                , OD.[timestamp] 
                , [order_status]     AS ORD_ST 
                , [slot1_quant] 
                , [slot2_quant] 
                , [slot3_quant] 
                , [slot4_quant] 
                , [slot5_quant] 
                , [slot6_quant] 
                , [customerid] 
                , [slot1_description] AS SLOT_1
                , [slot2_description] as SLOT_2 
                , [slot3_description] as SLOT_3
                , [slot4_description] As SLOT_4 
                , [slot5_description] 
                , [slot6_description] 
                , Max(CR.[level_id]) AS LVL 
         FROM   [dbo].[order_drone] AS OD 
                INNER JOIN (SELECT [userid] 
                                   , [timestamp] 
                                   , [level_id] 
                            FROM   [dbo].[currency_received] 
                            WHERE  [source_type_id] = 'level_up' 
                               AND [coins] != 0 
                               ) AS CR 
                        ON OD.[userid] = CR.[userid] 
                           AND OD.[timestamp] >= CR.[timestamp] 
			WHERE OD.[timestamp] BETWEEN @start_date AND @end_date 
                               AND OD.[UserId] != @cheater 
                               AND [LTV] = 0
         GROUP  BY OD.[userid] 
                   , OD.[timestamp] 
                   , [order_status] 
                   , [slot1_quant] 
                   , [slot2_quant] 
                   , [slot3_quant] 
                   , [slot4_quant] 
                   , [slot5_quant] 
                   , [slot6_quant] 
                   , [customerid] 
                   , [slot1_description] 
                   , [slot2_description] 
                   , [slot3_description] 
                   , [slot4_description] 
                   , [slot5_description] 
                   , [slot6_description] 
        --ORDER  BY OD.[timestamp] 
        ) 

SELECT LVL 
       , Count(*)             AS Amount 
       , Count(DISTINCT U_ID) AS USERS 
FROM   DRONS_AND_LVL 
WHERE  ORD_ST = 1 
GROUP  BY LVL 
ORDER  BY LVL

--SELECT SLOT_1 
--       , Count(*)             AS Amount 
--       , Count(DISTINCT U_ID) AS USERS 
--FROM   DRONS_AND_LVL 
--WHERE  ORD_ST = 2 
----AND SLOT_1 = 'Egg'
----AND SLOT_2 IS NOT NULL
--GROUP  BY SLOT_1 
--ORDER  BY SLOT_1  
--ORDER  BY Amount  desc