-- DECLARE @start_registration DATETIME = '2019-08-01 00:00:00.000';
-- DECLARE @end_registration DATETIME = '2019-08-01 23:59:59.997';

DECLARE @start_registration DATETIME = '2019-07-31 00:00:00.000';
DECLARE @end_registration DATETIME = '2019-07-31 23:59:59.997';


DECLARE @platform INt = 2 


SELEcT Count (Distinct U.UserId) AS Users
    FROM dbo.Users  AS U
    WHERE U.[Created] BETWEEN @start_registration AND @end_registration
    AND U.Platform = @platform 

--Commulative Revenue
SELEct Sum([LTV1D]*0.7) AS LTV1D
,  Sum([LTV2D]*0.7) AS LTV2D
,  Sum([LTV3D]*0.7) AS LTV3D
,  Sum([LTV4D]*0.7) AS LTV4D
FROM [dbo].[Users]  
 WHERE [Created] BETWEEN @start_registration AND @end_registration


 -- Revenue
SELEct Sum([LTV1D]*0.7) AS D1
,  Sum([LTV2D]*0.7) - Sum([LTV1D]*0.7)  AS D2
,  Sum([LTV3D]*0.7) -  Sum([LTV2D]*0.7)  AS D3
,  Sum([LTV4D]*0.7) -  Sum([LTV3D]*0.7)  AS  D4
FROM [dbo].[Users]  
 WHERE [Created] BETWEEN @start_registration AND @end_registration

