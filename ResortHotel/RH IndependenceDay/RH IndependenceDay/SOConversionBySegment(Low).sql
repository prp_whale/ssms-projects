DECLARE @start_period DATETIME = '2019-07-04 00:00:00.000';  -- INDEPENDESE EVENT
DECLARE @end_period DATETIME = '2019-07-08 23:59:59.997'; 

--SET @start_period  =  DATEADD (WW, -1, @start_period); 
--SET @end_period =  DATEADD (WW, -1, @end_period); 

SET @start_period  =  DATEADD (WW, -2, @start_period); 
SET @end_period =  DATEADD (WW, -2, @end_period); 

--SELEcT @start_period, @end_period

--SELECT [segment_1]
--,	Count (DISTINCT UserId) AS 'All Users'
--,	(
--		SELECT Count (DISTINCT UserId) 
--		FROM   [dbo].[ev_OfferPurchase] AS TT 
--			   INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
--					   ON TT.[UserId] = POU.U_ID 
--		WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
--		   AND [ClientVersion] BETWEEN '1.14.0' AND '1.19.99' 
--		   AND TT.[segment_1] = MQ.[segment_1] 
--		GROUP  BY [segment_1] 
--	)  AS Users 
--,	(
--		SELECT Count (DISTINCT UserId) 
--		FROM   [dbo].[ev_OfferPurchase] AS TT 
--			   INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
--					   ON TT.[UserId] = POU.U_ID 
--		WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
--		   AND [ClientVersion] BETWEEN '1.14.0' AND '1.19.99' 
--		   AND [Result] = 'OK' 
--		   AND TT.[segment_1] = MQ.[segment_1] 
--		GROUP  BY [segment_1] 
--	)  AS Depositors
--FROM   [dbo].[ev_OfferShowUp] AS MQ 
--       INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
--               ON MQ.[UserId] = POU.U_ID 
--WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
--   AND [ClientVersion] BETWEEN '1.14.0' AND '1.19.99' 
--   AND [segment_1] BETWEEN 1011 AND 1324 
--   AND [segment_1] != 1110
--GROUP  BY [segment_1] 
--ORDER  BY [segment_1] 
--------------------------------------------------------------------------------------------------
-- without version
SELECT [segment_1]
,	Count (DISTINCT UserId) AS 'All Users'
,	(
		SELECT Count (DISTINCT UserId) 
		FROM   [dbo].[ev_OfferPurchase] AS TT 
			   INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
					   ON TT.[UserId] = POU.U_ID 
		WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
		   AND TT.[segment_1] = MQ.[segment_1] 
		GROUP  BY [segment_1] 
	)  AS Users 
,	(
		SELECT Count (DISTINCT UserId) 
		FROM   [dbo].[ev_OfferPurchase] AS TT 
			   INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
					   ON TT.[UserId] = POU.U_ID 
		WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
		   AND [Result] = 'OK' 
		   AND TT.[segment_1] = MQ.[segment_1] 
		GROUP  BY [segment_1] 
	)  AS Depositors
FROM   [dbo].[ev_OfferShowUp] AS MQ 
       INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
               ON MQ.[UserId] = POU.U_ID 
WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
   AND [segment_1] BETWEEN 1011 AND 1032 
    --AND [segment_1] != 1110
GROUP  BY [segment_1] 
ORDER  BY [segment_1] 