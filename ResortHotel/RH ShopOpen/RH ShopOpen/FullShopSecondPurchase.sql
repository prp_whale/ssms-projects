--SELECT [segment_1] 
--       ,[segment_2] 
--       ,Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)                 AS Amount 
--FROM   [dbo].[PRP_ShopPurchase_March_May] 
--WHERE  [full_shop] = 1 
--   AND [new_depositor] = 0 
--GROUP  BY [segment_1] 
--          ,[segment_2] 
--ORDER  BY Users DESC 


----------------------------------------------
SELECT [purchase_name]
       ,Count (DISTINCT [UserId]) AS Users 
       ,Count (*)                 AS Amount 
FROM   [dbo].[PRP_ShopPurchase_March_May] 
WHERE  [full_shop] = 1 
   AND [new_depositor] = 0 
   AND [segment_1] = 1012
   AND [segment_2] = 1117
GROUP  BY [purchase_name] 
ORDER  BY Users DESC 
