DECLARE @str_per AS datetime = '2020-03-01 00:00:00.000'; 
DECLARE @end_per AS datetime = '2020-03-24 23:59:59.997'; 

WITH POU 
AS (
SELECT UserId AS U_ID 
FROM   dbo.Users 
WHERE  created BETWEEN '2020-03-01 00:00:00.000' AND '2020-03-21 23:59:59.997' 
   AND registrationclientversion IN ( '1.0.182', '1.0.184', '1.0.244' ) 
   AND ltv = 0 
   AND dbo.Getchecksum(advertisingid) % 4 IN ( 0, 1 ) 
)

-- SELECT WorldID 
--        ,Count(DISTINCT Iif(roomnumber != '-1', UserID, NULL)) AS Fail 
--        ,Count(DISTINCT Iif(roomnumber = '-1', UserID, NULL))  AS Complete 
--        ,Count(DISTINCT UserId)                                AS 'All' 
-- FROM   dbo.FinishRun  INNER JOIN POU  ON UserId = U_ID 
-- WHERE  IsLeft = 0 
--    AND RoomName != '-2' 
--    AND [Timestamp] BETWEEN @str_per AND @end_per 
-- GROUP BY WorldID 
-- ORDER BY WorldID 

SELECT Runnumber 
       ,Count(DISTINCT UserId)                                AS 'All' 
       ,Count(DISTINCT Iif(roomnumber != '-1', UserID, NULL)) AS Fail 
       ,Count(DISTINCT Iif(roomnumber = '-1', UserID, NULL))  AS Complete 
FROM   dbo.FinishRun  INNER JOIN POU  ON UserId = U_ID 
WHERE  IsLeft = 0 
   AND RoomName != '-2' 
   AND [Timestamp] BETWEEN @str_per AND @end_per 
   AND WorldID = '2'
   AND Runnumber BETWEEN 1 AND 50
GROUP BY Runnumber 
ORDER BY Runnumber 



-- select 
-- runnumber,
-- count(distinct f.userid) users,
-- avg(roomnumber*1.0) as AvgRoom,
-- count(distinct iif(roomnumber='-1', f.userid,null)) as CompleteWorld
-- from finishrun as f join users on users.userid=f.userid where 
-- f.created between @startperiod and @endperiod 
-- and registrationclientversion in ('1.0.182','1.0.184','1.0.244')
-- and f.ltv=0 and isleft=0 and roomname!='-2'
-- and worldid='4'
--  and dbo.getchecksum(advertisingid) % 4 in (0,1)	
-- group by runnumber
-- having count(distinct f.userid)>5
-- order by runnumber
