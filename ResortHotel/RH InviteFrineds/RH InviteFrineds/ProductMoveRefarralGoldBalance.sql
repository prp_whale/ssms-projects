DECLARE @start_period DATETIME = '2019-08-01 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-08-31 23:59:59.997'; 

-- WITH SUCCSESFULL_INVITERS
-- AS (
--     SELEcT DISTINCT  UserId
--     FROM   [dbo].[EventsProductMovement] 
--     WHERE  [TriggerSourceId] = 'referrals' 
--     AND [ProductType] = 'coins'
--     --AND [ProductAmount]
--     AND [Timestamp] BETWEEN @start_period AND @end_period
--     AND [RegistrationCountry] != 'UA'
-- )


SELECT [ProductAmount]
, Count (DISTINCT EMP.[UserId]) AS Users
, Count (*) AS Amount 
FROM   [dbo].[EventsProductMovement] AS EMP  --  INNER JOIN PRP_Pool_Of_Real_Invatiors AS RI On RI.[U_ID] = EMP.[UserId] 
WHERE  [TriggerSourceId] = 'referrals' 
AND [ProductType] = 'coins'
AND [Timestamp] BETWEEN @start_period AND @end_period
AND [RegistrationCountry] != 'UA'
GROUP BY [ProductAmount]
ORDER BY [ProductAmount]



-- SELEcT * 
-- FROM   [dbo].[EventsProductMovement] 
-- WHERE  [TriggerSourceId] = 'referrals' 
-- AND [ProductType] = 'coins'
-- AND [Timestamp] BETWEEN @start_period AND @end_period
-- AND [ProductAmount] = 300
-- select *
-- from [dbo].[EventsProductMovement]
-- where triggersourceid = 'referrals'


-- SELECT * 
-- FROM   [dbo].[EventsProductMovement] 
-- WHERE  [TriggerSourceId] = 'referrals' 
-- AND [ProductType] = 'coins'
-- --AND [Timestamp] BETWEEN @start_period AND @end_period
-- AND [userid] = 4761942
-- ORDER BY [Timestamp]


-- SELEcT  [TriggerId]
-- , Count (DISTINCT [UserId]) AS Users
-- , Count (*) AS Amount 
-- , SUM([ProductAmount]) AS Gold
-- FROM   [dbo].[EventsProductMovement] 
-- WHERE [Action] = 'received'
-- AND [ProductType] = 'coins' 
-- AND [Timestamp] BETWEEN '2019-08-01' AND '2019-09-01'
-- AND [TriggerSourceId] = 'FUE'
-- --AND RegistrationCountry != 'CN' AND   RegistrationCountry != 'UA' 
-- --AND ProductAmount <= 1000
-- GROUP BY TriggerId 
-- ORDER BY Users DESC

-- SELEcT  [TriggerSourceId]
-- , Count (DISTINCT [UserId]) AS Users
-- , Count (*) AS Amount 
-- , SUM([ProductAmount]) AS Gold
-- FROM   [dbo].[EventsProductMovement] 
-- WHERE [Action] = 'received'
-- AND [ProductType] = 'coins' 
-- AND [Timestamp] BETWEEN '2019-08-01' AND '2019-09-01'
-- AND [TriggerSourceId] = 'Default'
-- --AND RegistrationCountry != 'CN' AND   RegistrationCountry != 'UA' 
-- AND ProductAmount <= 1000
-- GROUP BY TriggerSourceId 
-- ORDER BY Users DESC