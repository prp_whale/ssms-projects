DECLARE @start_date Datetime = '2019-02-11 00:00:00.000'; 
DECLARE @end_date Datetime = '2019-02-24 23:59:59.999'; 
DECLARE @cheater Int = 7249; 
DECLARE @test_country Varchar(2) = 'UA'; 

WITH USERS_AND_LVL 
     AS (SELECT MQ.[userid]     AS U_ID 
                , SQ.[level_id] AS LVL 
         FROM   [dbo].[eventsappopen] AS MQ 
                INNER JOIN (SELECT [userid] 
                                   , [timestamp] 
                                   , [level_id] 
                            FROM   [dbo].[currency_received] 
                            WHERE  [source_type_id] = 'level_up' 
                               AND [coins] != 0 
                               AND [LTV] = 0 
                               AND [RegistrationCountry] != @test_country) AS SQ 
                        ON MQ.[userid] = SQ.[userid] 
         WHERE  MQ.[timestamp] BETWEEN @start_date AND @end_date 
            AND MQ.[UserId] != @cheater 
         GROUP  BY MQ.[userid] 
                   , SQ.[level_id]) 
SELECT LVL 
       , Count(*)             AS Amount 
       , Count(DISTINCT U_ID) AS USERS 
FROM   USERS_AND_LVL 
GROUP  BY LVL 
ORDER  BY LVL 