DECLARE @start_period DATETIME = '2018-12-01 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-06-13 23:59:59.997'; 


--SELECT [ActionId]
--      ,Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)                AS Amount 
--FROM  [dbo].[ev_InvitationAction]
--WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
--GROUP BY [ActionId]
--ORDER BY [ActionId] ASC

--SELECT [TimeStamp]
--,[ClientTime]
--,[BackOfficeTime]
--,[ActionId]
--FROM  [dbo].[ev_InvitationAction]
--WHERE [UserId] =  '4191376'
--ORDER BY [ClientTime] DESC

SELECT [ClientVersion]
		,Count (DISTINCT [UserId]) AS Users 
       ,Count (*)                AS Amount 
FROM  [dbo].[ev_InvitationAction]
WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
AND [ActionId] = 2
GROUP BY [ClientVersion]
ORDER BY [ClientVersion] ASC