declare @st_per DATETIME = '2020-05-01 00:00:00:000';
declare @end_per DATETIME  = '2020-05-10 23:59:59:997';
DECLARE @cv NVARCHAR(10) = '1.17';

WITH PoU 
As (
    SELECT  UserId AS U_ID
    FROM dbo.Users 
    WHERE Created BETWEEN @st_per AND @end_per
        AND [Retention1D] IS NULL
        AND [Retention2D] IS NULL
        AND [Retention3D] IS NULL
        AND [Retention4D] IS NULL
        AND [Retention5D] IS NULL
   -- AND RegistrationClientVersion = @cv
   and RegistrationClientVersion IN ('1.17', '1.16')
)

, LastMove 
AS (
    SELECT UserID as U_ID
    , Max([Timestamp]) AS LastMove
    FROM EventsProductMovement AS ela JOIN PoU ON UserID = U_ID 
    GROUP BY UserId
)

 SELECT TriggerSourceId, AVG(ProductBalance) AS avg
 FROM EventsProductMovement 
 JOIN LastMove ON UserID = U_ID ANd [Timestamp] = LastMove
 GROUP BY TriggerSourceId
 ORDER BY avg
 

-- SELECT 
-- LevelId
-- , Count(distinct UserID) AS 'ALL'
-- , (SELECT Count(distinct UserID) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND Completed = 1 AND ClientVersion = @cv AND UserID = ela.UserID ) AS Sucsess
-- , (SELECT Count(distinct UserID) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND CompletedType = 0 AND ClientVersion = @cv ) AS Lose
-- , (SELECT Count(distinct UserID) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND CompletedType = 2 AND ClientVersion = @cv  ) AS LeftMenu
-- , (SELECT Count(distinct UserID) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND CompletedType = 3 AND ClientVersion = @cv ) AS LeftGame
-- , (SELECT Count(distinct UserID) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND AddSteps > 0  AND ClientVersion = @cv ) AS SpendCoinsTimes
-- --,  1.0*Sum(CoinsSpent)/Count(distinct UserID) AS SpendCoins
-- FROM EventsLevelAttempts AS ela JOIN PoU ON UserID = U_ID
-- WHERE ClientVersion = @cv -- 17
-- AND LevelId <= 30
-- GROUP BY LevelId
-- ORDER BY LevelID