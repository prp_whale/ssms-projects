
DROP TABLE IF EXISTS PRP_Pool_Of_Real_Invatiors;

-- -- OLD INVITATION SYSTEM
-- SELECT DISTINCT [UserIdInvitationSender] AS U_ID
-- --INTO PRP_Pool_Of_Real_Invatiors
-- FROM  [dbo].[ev_InvitationFriends] AS I
-- 		LEFT JOIN(SELECT DISTINCT [UserId] 
-- 					FROM [dbo].[ev_InvitationAction]
-- 					WHERE  [ActionId] = 1 OR [ActionId] = 2) AS IA
-- 					ON IA.[UserId] = NULLIF(I.[UserIdInvitationSender],'null')
--  --    	INNER JOIN PRP_TOTAL_Pool_Of_Active_Users AS TPOU
-- 	--		ON TPOU.U_ID = I.[UserIdInvitationSender]
-- WHERE IA.[UserId] IS NOT NULL
-- AND I.[ClientVersion] BETWEEN '1.14.2' AND '1.15.0'


-- -- NEW INVITATION SYSTEM
-- SELECT DISTINCT [UserIdInvitationSender] AS U_ID
-- INTO PRP_Pool_Of_Real_Invatiors
-- FROM  [dbo].[ev_InvitationFriends] AS I
-- 		LEFT JOIN (SELECT DISTINCT [UserId] 
-- 					   FROM [dbo].[ev_FriendsWindow]
-- 						WHERE  [ActionId] = 'add_friend'
-- 				UNION
-- 					SELECT DISTINCT [UserId] 
-- 						FROM [dbo].[ev_InvitationAction]
-- 						WHERE  [ActionId] = 1 OR [ActionId] = 2
-- 						) AS FW
-- 						ON FW.[UserId] = Cast(NULLIF(I.[UserIdInvitationSender],'null') as Int)
-- WHERE FW.[UserId] IS NOT NULL
-- AND I.[ClientVersion] BETWEEN '1.14.2' AND '1.15.0'



-- TOTAL INVITATION SYSTEM
SELECT DISTINCT U.[UserID] AS U_ID
,  [UserIdInvitationSender] AS U_INVITE
INTO PRP_Pool_Of_Real_Invatiors
FROM  [dbo].[ev_InvitationFriends] AS I 
		LEFT JOIN [dbo].[Users] AS U ON U.[GameUserId] =  NULLIF( I.[UserIdInvitationSender],'null') 
		LEFT JOIN PRP_Inviters AS IA
						--ON IA.[U_ID] = CONVERT (  INT  , NULLIF( I.[UserIdInvitationSender],'null'))
					 ON	IA.[U_ID] = U.[UserID]
 --    	INNER JOIN PRP_TOTAL_Pool_Of_Active_Users AS TPOU
	--		ON TPOU.U_ID = I.[UserIdInvitationSender]
WHERE IA.[U_ID] IS NOT NULL 
AND NOT ( (U.[UserID] = I.[UserID]) AND (NULLIF( I.[UserIdInvitationSender],'null') = U.[GameUserId] ))
--AND I.[ClientVersion] BETWEEN '1.14.2' AND '1.15.0'




SELECT Count (DISTINCT U_ID) AS Users 
       ,Count (*)             AS Amount 
FROM   PRP_Pool_Of_Real_Invatiors


-- Select Count(DISTINCT [UserIdInvitationSender])
-- FROM  [dbo].[ev_InvitationFriends]
-- WHERE [ClientVersion] BETWEEN '1.14.2' AND '1.15.0'