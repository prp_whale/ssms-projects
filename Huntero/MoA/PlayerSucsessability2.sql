--до какого уровня в среднем первой главы игроки доходят с первой попытки. Со второй попытки. С третьей попытки и так до 10-й. По каждой главе.

DECLARE @st_per DATETIME = '2020-03-01 00:00:00.000';
DECLARE @end_per DATETIME =  '2020-03-21 23:59:59.997'; --22

WITH PoU 
AS(
    SELECT UserId AS U_ID 
    FROM   dbo.Users 
    WHERE  created BETWEEN '2020-03-01 00:00:00.000' AND '2020-03-21 23:59:59.997' 
        AND registrationclientversion IN ( '1.0.182', '1.0.184', '1.0.244' ) 
        AND ltv = 0 
        AND dbo.Getchecksum(advertisingid) % 4 IN ( 0, 1 ) 
)

, CTE1 
AS (
    SELECT UserId
        , WorldId AS LocationId
        , RunNumber
        , RoomNumber
        , [Timestamp] aS katka_end
    FROM dbo.[Finishrun] INNER JOIN POU ON UserId = U_ID
    WHERE [Timestamp] BETWEEN @st_per AND @end_per
    and IsLeft = 0 and   roomnumber!=0 and roomname!='-2' and runlevel<=11
),



CTE2 
AS (
    SELECT C.UserId
        , C.LocationId AS WorldId
        , C.RunNumber
        , C.RoomNumber AS end_room
        , SR.[Timestamp]  AS katka_start
    FROM dbo.[StartRun] AS SR INNER JOIN CTE1 AS C  ON SR.UserId = C.UserId AND SR.LocationId = C.LocationId AND SR.Runnumber = C.Runnumber 
    WHERE SR.[Timestamp] BETWEEN @st_per AND @end_per 
    AND SR.[Timestamp] < katka_end
    AND healthpoint < 9999 and attackpoint < 9999 
 
),

 FIRST_SUCCESS 
AS (
    SELECT C.UserId
        , C.WorldId 
        , C.RunNumber
        , Max(RoomNumber) AS end_room
        , Min([Timestamp]) aS first_complete
    FROM dbo.[Finishrun] AS FR INNER JOIN CTE2 AS C  ON FR.UserId = C.UserId AND FR.WorldId = C.WorldId AND FR.Runnumber = C.Runnumber 
    WHERE RoomNumber = '-1'
    
    GROUP BY C.UserId, C.WorldId, C.RunNumber
),

CTE4
AS (
    SELECT C2.* 
    FROM CTE2 AS C2 INNER JOIN FIRST_SUCCESS AS FS ON C2.UserId = FS.UserId AND C2.WorldId = FS.WorldId 
    WHERE katka_start < first_complete
)


-- SELECT UserId
-- , Count(*) AS F 
-- FROM CTE2
-- WHERE WorldId = 1 
-- AND Runnumber = 10
-- GROUP BY UserId
-- HAVING COUNT(*)>1



SELECT WorldID 
, RunNumber
--, AVG(end_room) AS avg_end_room
--, Count(RunNumber) AS Runs
, Count(DISTINCT UserID) AS Users
FROM FIRST_SUCCESS
WHERE end_room BETWEEN -1 AND 50
AND RunNumber BETWEEN 1 AND 20
GROUP BY WorldID , RunNumber
ORDER BY WorldID, RunNumber


-- SELECT WorldID
-- , RunNumber
-- , AVG(end_room) AS avg_end_room
-- , Count(RunNumber) AS Runs
-- , Count(DISTINCT UserID) AS Users
-- FROM CTE2 AS C2 LEFT JOIN CTE4 AS C4 ON 
-- WHERE end_room BETWEEN 1 AND 50
-- AND RunNumber BETWEEN 1 AND 20
-- GROUP BY WorldID , RunNumber
-- ORDER BY 1, 2

