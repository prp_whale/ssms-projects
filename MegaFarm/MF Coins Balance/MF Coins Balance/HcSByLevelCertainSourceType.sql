DECLARE @start_date Datetime = '2019-02-11 00:00:00.000'; 
DECLARE @end_date Datetime = '2019-02-24 23:59:59.999'; 
DECLARE @cheater Int = 7249; 
DECLARE @test_country Varchar(2) = 'UA' 


	--declare @source varchar(25)		= 'accelerate_build'
	--declare @source varchar(25)		= 'accelerate_expansion'
	--declare @source varchar(25)		= 'accelerate_factory'
	--declare @source varchar(25)		= 'accelerate_farm'
	--declare @source varchar(25)		= 'accelerate_field'
	--declare @source varchar(25)		= 'accelerate_plain'
	--declare @source varchar(25)		= 'accelerate_stock_exch'
	--declare @source varchar(25)		= 'accelerate_wheel_fortune'
	--declare @source varchar(25)		= 'buy_coins_for_cash'
	--declare @source varchar(25)		= 'buy_material'
	--declare @source varchar(25)		= 'buy_pack_material'
	--declare @source varchar(25)		= 'factory_new_slot'
	declare @source varchar(25)		= 'gems_buy'
	
SELECT	[Level_id] as lvl 
		, SUM	([cash]) as CASH
		, COUNT (DISTINCT [UserId]) as USERS_ON_LVL
		, COUNT (*) as TRANSACTIONS
		 FROM [dbo].[currency_spent]
  WHERE   [RegistrationCountry] != @test_country
   AND [UserID] != @cheater
   AND [timestamp] between @start_date and @end_date
  -- AND [source_type_id] = @source
   AND [cash] != 0
   AND LTV > 0
  GROUP BY [Level_id]
  ORDER BY lvl 