DECLARE @v NVARCHAR(30) = '1.0.289';
WITH PoU
AS (
    SELECT DISTINCT E.UserID AS U_ID
    FROM StartRun AS E INNER JOIN Users  AS U
    ON E.UserId = U.UserId 
    --AND  E.ClientVersion = @v
    AND U.RegistrationClientVersion = @v
    )

SELECT 0 as step_id , COUNT(*) AS Users
from PoU
UNION ALL
SELECT step_id
, Count (DISTINCT UserID) 
FROM Tutorial JOIN PoU ON UserId = U_ID
--WHERE ClientVersion = '1.0.289'

GROUP BY step_id
ORDER BY step_id