DECLARE @version NVARCHAR(10) = '1.0.244'

;WITH POU 
AS (
    SELECT 
UserID
, avg(TotalLoadingTime) AS  AvgTotalLoad
, avg(LoadTime) AS  HunterScreen
, avg(PreloadTime) AS  WhaleScreen
FROM dbo.[LoadingTime]	
WHERE TotalLoadingTime <= 300
AND ClientVersion = @version
GROUP BY UserId
--ORDER BY AvgTotalLoad DESC
)

SELECT TOP (1) @version AS 'version'
, (SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 0 AND AvgTotalLoad < 5) AS '0 to 5 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 5 AND AvgTotalLoad < 10) AS '5 to 10 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 10 AND AvgTotalLoad < 15) AS '10 to 15 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 15 AND AvgTotalLoad < 20) AS '15 to 20 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 20 AND AvgTotalLoad < 25) AS '20 to 25 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 25 AND AvgTotalLoad < 30) AS '25 to 30 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 30 AND AvgTotalLoad < 35) AS '30 to 35 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 35 AND AvgTotalLoad < 40) AS '35 to 40 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 40 AND AvgTotalLoad < 45) AS '40 to 45 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 45 AND AvgTotalLoad < 50) AS '45 to 50 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 50 AND AvgTotalLoad < 55) AS '50 to 55 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 55 AND AvgTotalLoad < 60) AS '55 to 60 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 60 AND AvgTotalLoad < 65) AS '60 to 65 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 65 AND AvgTotalLoad < 70) AS '65 to 70 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 70 AND AvgTotalLoad < 75) AS '70 to 75 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 75 AND AvgTotalLoad < 80) AS '75 to 80 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 80 AND AvgTotalLoad < 85) AS '80 to 85 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 85 AND AvgTotalLoad < 90) AS '85 to 90 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 90 AND AvgTotalLoad < 95) AS '90 to 95 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 95 AND AvgTotalLoad < 100) AS '95 to 100 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 100 AND AvgTotalLoad < 150) AS '100 to 150 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 150 AND AvgTotalLoad < 200) AS '150 to 200 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 200 AND AvgTotalLoad < 250) AS '200 to 250 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 250 AND AvgTotalLoad < 300) AS '250 to 300 '
,(SELECT Count(DISTINCT UserID) FROM POU WHERE AvgTotalLoad >= 300 AND AvgTotalLoad < 999999) AS 'Over 500 '
FROM POU