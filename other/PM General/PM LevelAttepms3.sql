declare @st_per DATETIME = '2020-05-01 00:00:00:000';
declare @end_per DATETIME  = '2020-05-10 23:59:59:997';
DECLARE @cv NVARCHAR(10) = '1.17';


WITH PoU 
--WITH Churn 
As (
    SELECT  UserId AS CU_ID
    FROM dbo.Users 
    WHERE Created BETWEEN @st_per AND @end_per
        AND [Retention1D] IS NULL
        AND [Retention2D] IS NULL
        AND [Retention3D] IS NULL
        AND [Retention4D] IS NULL
        AND [Retention5D] IS NULL
   -- AND RegistrationClientVersion = @cv
   and RegistrationClientVersion IN ('1.17', '1.16')
)

-- , PoU
-- As (
--     SELECT  UserId AS U_ID
--     FROM dbo.Users LEFT JOIN Churn ON UserID = CU_ID
--     WHERE CU_ID IS NULL
--    and RegistrationClientVersion IN ('1.17', '1.16')
-- )



SELECT 
LevelId
, Count(*) AS 'ALL'
, (SELECT Count(*) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND Completed = 1 AND ClientVersion IN ('1.16', '1.17') AND UserId IN (SELECT * FROM PoU) ) AS Sucsess
, (SELECT Count(*) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND CompletedType = 0 AND ClientVersion IN ('1.16', '1.17') AND UserId IN (SELECT * FROM PoU) ) AS Lose
, (SELECT Count(*) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND CompletedType = 2 AND ClientVersion IN ('1.16', '1.17') AND UserId IN (SELECT * FROM PoU)  ) AS LeftMenu
, (SELECT Count(*) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND CompletedType = 3 AND ClientVersion IN ('1.16', '1.17') AND UserId IN (SELECT * FROM PoU) ) AS LeftGame
, (SELECT Count(*) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND AddSteps > 0  AND ClientVersion IN ('1.16', '1.17') AND UserId IN (SELECT * FROM PoU) ) AS SpendCoinsTimes
--,  1.0*Sum(CoinsSpent)/Count(*) AS SpendCoins
FROM EventsLevelAttempts AS ela
WHERE ClientVersion IN ('1.16', '1.17') AND UserId IN (SELECT * FROM PoU) -- 17
AND LevelId <= 30
GROUP BY LevelId
ORDER BY LevelID


-- SELECT 
-- LevelId
-- , Count(distinct UserID) AS 'ALL'
-- , (SELECT Count(distinct UserID) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND Completed = 1 AND ClientVersion IN ('1.16', '1.17') ) AS Sucsess
-- , (SELECT Count(distinct UserID) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND CompletedType = 0 AND ClientVersion IN ('1.16', '1.17') ) AS Lose
-- , (SELECT Count(distinct UserID) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND CompletedType = 2 AND ClientVersion IN ('1.16', '1.17')  ) AS LeftMenu
-- , (SELECT Count(distinct UserID) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND CompletedType = 3 AND ClientVersion IN ('1.16', '1.17') ) AS LeftGame
-- , (SELECT Count(distinct UserID) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND AddSteps > 0  AND ClientVersion IN ('1.16', '1.17') ) AS SpendCoinsTimes
-- --,  1.0*Sum(CoinsSpent)/Count(distinct UserID) AS SpendCoins
-- FROM EventsLevelAttempts AS ela
-- WHERE ClientVersion IN ('1.16', '1.17') -- 17
-- AND LevelId <= 30
-- GROUP BY LevelId
-- ORDER BY LevelID
