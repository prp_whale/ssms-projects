--???? ??????? 
DECLARE @time_begin DATETIME = '2019-05-14 00:00:00'; 
DECLARE @time_end DATETIME = '2019-05-16 23:59:59'; 
--???? ????? ???????????? ?????? ?? ????????????????? ? ???????? ?????? ???? ???????
DECLARE @LevelDataVer NVARCHAR(20) = '2019-05-14 00:00:00'; 
DECLARE @LevelIdMin INT = 1; 
DECLARE @LevelIdMax INT = 300; 
DECLARE @TypeId INT = 1; 

declare @VersionsTable table(ClientVersion NVARCHAR(20) primary key);
insert into @VersionsTable values ('1.11.0'),('1.12.0'),('1.13.0'); 

declare @DuplicateTable table(UserID int primary key);
insert into @DuplicateTable values (4531), (923803), (511012), (14696), (106604), (2003806), (661541), (386530), (116309), (7395), (156911), (1778), (41630), (30734), (156316), (12329), (136361), (89407), (45893), (91), (1418525), (106667), (116100), (204706), (1775195), (66427), (123487), (353229), (57203), (1820225), (593466), (86142), (41310), (4240), (31247), (91233), (81726), (225732), (62548), (692173), (186278), (53520), (2531), (93180), (44949), (62512), (56200), (15), (12307), (225037), (106640), (17832), (68610), (9477), (847663), (27), (216473), (2818), (360659), (923840), (10253), (129752), (213355), (58573), (20658), (72418), (19244), (52931), (908735), (149771), (33966), (5090), (102496), (19319), (17742), (326473), (114600), (12305), (418755), (100397), (729), (68116), (102969), (83515), (36995), (318), (16), (1761), (7968), (1019868), (367698), (69158), (330631), (1779962), (95937), (25708), (35421), (95066), (7602), (610945), (1357770), (10), (18750), (134929), (1844385), (850031), (40746), (202760), (36086), (20158), (13), (103986), (524636), (56885), (1150), (34885), (1513741), (50205), (13198), (270740), (3861), (12846), (166), (104536), (36932), (284052), (874561), (2276), (1726), (79503), (16726), (204472), (56327), (5192), (48740), (345746), (54962), (8), (986918), (707587), (89230), (67302), (30639), (5599), (1607), (8607), (9631), (611858), (734214), (774), (261428), (7), (78573), (73043), (34565), (47945), (662), (8201), (7753), (2123), (704209), (465078), (43743), (26799), (892198), (4682), (3949), (14514), (17848), (33739), (73027), (22757), (1354), (4896), (3537), (401), (6922), (499383), (15025), (3901), (20362), (3439), (1871), (69732), (1765), (608710), (8836), (24130), (5151), (30763), (74482), (595489), (35337), (1117), (15609), (724), (761442), (940731), (5674), (12102), (6036), (95276), (6302), (4568), (1704), (2157), (25450), (420395), (405), (499838), (208531), (407), (1223), (808884), (960), (2130), (467459), (10008), (2245), (16019), (3823), (863), (480216), (616435), (1397), (952265), (941379), (6663), (1493), (1139), (4455), (426075), (400431), (408587), (405613), (422127), (414623), (881988), (3999), (434897), (6298), (9737), (408850), (521), (263201), (285), (417531), (401011), (399700), (616710), (467841), (410414);

WITH ACTIVEDEPOSITORS 
     AS (SELECT ELA.UserID 
                --,max(ela.LTV) ltv 
                , Min(ELA.LevelID) min_level 
                , Max(ELA.LevelID) max_level 
         FROM   EventsLevelAttempts ELA 
                JOIN @DuplicateTable DT 
                  ON ELA.UserID <> DT.UserID 
                     --and ela.LTV > 0  
                     AND ELA.TypeId = 1 
                     AND ABgroup = 'G0V1' 
         GROUP  BY ELA.UserID 
        --having  max(ela.LevelID) - min(ela.LevelID) > 10  
        --    and 1.0 * max(ela.LTV) / nullif((max(ela.LevelID) - min(ela.LevelID)), 0) > 0.1  
        --    and 1.0 * max(ela.LTV) / nullif((max(ela.LevelID) - min(ela.LevelID)), 0) < 1.0 
        ) 
SELECT ELA.LevelID 
       , Count(DISTINCT ELA.UserID)                                                       AS 
         active_users 
       --,count(ela.UserID)                          as attempts 
       --,count(iif(ela.UserID > 0  and BoostsOnLvl=0 and BoostsBeforeLvl=0 and AddSteps=0 and (Crown=0 OR Crown IS NULL), 1, null))  as clear_attempts
       --,nullif(count (distinct iif(ela.UserID > 0  and BoostsOnLvl=0 and BoostsBeforeLvl=0 and AddSteps=0 and (Crown=0 OR Crown IS NULL), ela.UserID, null)), 0)
       , 1.0 * Count(ELA.UserID) / NULLIF(Count(DISTINCT ELA.UserID), 0)                  AS 
         attempt_per_user 
       , 1.0 * Count(Iif(ELA.UserID > 0 
                         AND BoostsOnLvl = 0 
                         AND BoostsBeforeLvl = 0 
                         AND AddSteps = 0 
                         AND ( Crown = 0 
                                OR Crown IS NULL ), 1, NULL)) / NULLIF( 
           Count (DISTINCT Iif(ELA.UserID > 0 
                               AND 
           BoostsOnLvl = 0 
                               AND BoostsBeforeLvl = 0 
                               AND AddSteps = 0 
                               AND ( Crown = 0 
                                      OR Crown IS NULL ), ELA.UserID, NULL)), 0)          AS 
         clear_attempt_per_user 
       , Avg(Iif(ELA.userid > 0 
                 AND completed = 0 
                 AND TilesRec1 > 0 
                 AND BoostsOnLvl = 0 
                 AND BoostsBeforeLvl = 0 
                 AND AddSteps = 0 
                 AND ( Crown = 0 
                        OR Crown IS NULL ), TilesRec1, 0)) * 1.0                          AS 
         g1_uncollected 
       -- / nullif(sum(iif(ela.userid>0 and completed=0 and TilesRec1>0 and BoostsOnLvl=0 and BoostsBeforeLvl=0 and AddSteps=0 and (Crown=0 OR Crown IS NULL),Goal1,0)),0)            
       , Avg(Iif(ELA.userid > 0 
                 AND completed = 0 
                 AND TilesRec2 > 0 
                 AND BoostsOnLvl = 0 
                 AND BoostsBeforeLvl = 0 
                 AND AddSteps = 0 
                 AND ( Crown = 0 
                        OR Crown IS NULL ), TilesRec2, 0)) * 1.0                          AS 
         g2_uncollected 
       -- / nullif(sum(iif(ela.userid>0 and completed=0 and TilesRec2>0 and BoostsOnLvl=0 and BoostsBeforeLvl=0 and AddSteps=0 and (Crown=0 OR Crown IS NULL), Goal1,0)),0)            as g2_uncollected
       , Avg(Iif(ELA.userid > 0 
                 AND completed = 1 
                 AND unusedsteps >= 0 
                 AND tilesrec1 > 0 
                 AND addsteps = 0 
                 AND BoostsOnLvl = 0 
                 AND BoostsBeforeLvl = 0 
                 AND ( Crown = 0 
                        OR Crown IS NULL ), unusedsteps, 0)) * 1.0                        AS 
         stepsleft 
       --/ nullif(count(distinct(iif(ela.userid>0 and unusedsteps>=0 and completed=1 and tilesrec1>0 and addsteps=0 and BoostsOnLvl=0 and BoostsBeforeLvl=0 and (Crown=0 OR Crown IS NULL),ela.userid,null))), 0)  as stepsleft
       , Count(DISTINCT( Iif(ELA.Completed = 1, ELA.UserId, NULL) )) * 1.0 / 
           ( Count(DISTINCT( ELA.UserId )) ) * 1.0                                        AS 
         pass_rate 
       , Sum(Iif(ELA.Completed = 0, 1, 0)) * 1.0 / Count(Iif(ELA.UserId > 0, 1, 0)) * 1.0 AS diff
FROM   EventsLevelAttempts ELA 
       JOIN ACTIVEDEPOSITORS AD 
         ON ELA.UserID = AD.UserID 
            AND ELA.Timestamp BETWEEN @time_begin AND @time_end 
            AND TypeId = @TypeId 
            AND ( @LevelDataVer IS NULL 
                   OR @LevelDataVer = @LevelDataVer ) 
            AND ELA.LevelID >= @LevelIdMin 
            AND ELA.LevelID <= @LevelIdMax 
            AND ClientVersion IN (SELECT ClientVersion 
                                  FROM   @VersionsTable) 
GROUP  BY ELA.LevelID 
ORDER  BY ELA.LevelID 