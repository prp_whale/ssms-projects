declare @days int = 28;

declare @days_multi int = 3;
declare @st_per DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days*@days_multi, GETDATE())));
declare @end_per DATETIME  = DATEADD(ms, -3, CONVERT(datetime, CONVERT(date, GETDATE())));

declare @versa_limit int = 1000;


WITH Versa
AS (
        SELECT ClientVersion AS Ver
        FROM  dbo.EventsAppOpen
        WHERE [Timestamp] BETWEEN @st_per AND @end_per 
        AND ClientVersion IS NOT NULL
        GROUP BY ClientVersion
        HAVING Count(*) >=  @versa_limit
)

, Regs
AS (
    SELECT UserID AS U_ID
    , Convert(date, [timestamp]) AS RegDate
    FROM EventsAppOpen 
    WHERE Registration = 1 
    AND [Timestamp] BETWEEN DATEADD(dd, -@days, @st_per) AND DATEADD(dd, -@days, @end_per)
)
, Opens 
AS (
    SELECT UserID
     , Convert(date, [timestamp]) AS VisitDate
    FROM EventsAppOpen JOIN Regs ON UserId = U_ID
    WHERE Registration = 0 
        AND [Timestamp] BETWEEN RegDate AND DATEADD(dd, @days, RegDate)
    GROUP BY UserID
     , Convert(date, [timestamp]) 
)
, OpensCount 
AS (
   SELECT UserID, RegDate
   , Count(UserID)  AS DaysInMonth 
   FROM Opens JOIN Regs ON UserId = U_ID
   GROUP BY UserId, RegDate
) 

SELECT RegDate, DaysInMonth
, Count (UserID) AS Visits
FROM OpensCount
GROUP BY RegDate, DaysInMonth
ORDER BY RegDate, DaysInMonth


-- SELECT RegDate
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 1 ) AS '1'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 2 ) AS '2'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 3 ) AS '3'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 4 ) AS '4'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 5 ) AS '5'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 6 ) AS '6'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 7 ) AS '7'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 8 ) AS '8'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 9 ) AS '9'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 10 ) AS '10'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 11 ) AS '11'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 12 ) AS '12'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 13 ) AS '13'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 14 ) AS '14'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 15 ) AS '15'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 16 ) AS '16'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 17 ) AS '17'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 18 ) AS '18'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 19 ) AS '19'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 20 ) AS '20'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 21 ) AS '21'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 22 ) AS '22'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 23 ) AS '23'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 24 ) AS '24'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 25 ) AS '25'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 26 ) AS '26'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 27 ) AS '27'
-- , (SELECT COUNT(UserID) FROM OpensCount WHERE oc.RegDate = RegDate AND DaysInMonth = 28 ) AS '28'
-- FROM OpensCount AS oc
-- GROUP BY RegDate
-- ORDER BY RegDate

