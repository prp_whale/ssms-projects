DECLARE @start_date Datetime = '2019-02-11 00:00:00.000'; 
DECLARE @end_date Datetime = '2019-02-24 23:59:59.999'; 
DECLARE @cheater Int = 7249; 
DECLARE @test_country Varchar(2) = 'UA' 


DECLARE @building VARCHAR(20) = 'Furniture Factory'
DECLARE @product_in_slot VARCHAR(10) = 'Chair'

--DECLARE @building VARCHAR(20) = 'Fast Food Restaurant'
--DECLARE @product_in_slot VARCHAR(10) = 'Fried Corn'
--DECLARE @product_in_slot VARCHAR(10) = 'Hot Dog'

--DECLARE @building VARCHAR(20) = 'Drinks Factory'
--DECLARE @product_in_slot VARCHAR(10) = 'Milk Shake'
--DECLARE @product_in_slot VARCHAR(20) = 'Cucumber Fresh Juice'

SELECT [order_status] as STATUS
		,COUNT (*) as AMOUNT
		, COUNT (DISTINCT [UserId]) as USERS
FROM   [dbo].[order_drone] AS OD 
    INNER JOIN  ( 
              SELECT [UserId] AS U_ID , 
                     [building_id] , 
                     [action_id] 
              FROM   [dbo].[building] 
              WHERE   [building_id] = @building 
              AND    [action_id] = 3 
			  ) AS BUILDING 
ON OD.[UserId] = BUILDING.U_ID 
WHERE  [Timestamp] BETWEEN @start_date AND  @end_date 
              AND    [UserId] != @cheater 
              AND    [RegistrationCountry] != @test_country 
              AND    [LTV] = 0 
              AND ( [slot1_description] = @product_in_slot 
				OR  [slot2_description] = @product_in_slot
				OR  [slot3_description] = @product_in_slot
				OR  [slot4_description] = @product_in_slot
				OR  [slot5_description] = @product_in_slot
				OR  [slot6_description] = @product_in_slot
				)
GROUP BY [order_status]
ORDER BY [order_status]



--SELECT[action_id]
--	,COUNT (*) as Amount
--  FROM [dbo].[building]
--  WHERE  [Timestamp] BETWEEN @start_date AND @end_date 
--   AND [UserId] != @cheater 
--   AND [LTV] = 0 
--   AND [building_id] = @building

--  GROUP BY  [action_id] 