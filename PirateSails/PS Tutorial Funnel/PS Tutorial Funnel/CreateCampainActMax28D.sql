
DROP TABLE IF EXISTS PRP_CampainAct_Max28D;

DECLARE @start_period DATETIME = '2019-05-01 00:00:00.000';  -- FOR  PRP_CampainAct_Max28D


SELEcT SP.[UserId]
, Max(SP.[IsDepositorClient]) AS IsDepositorClient
, Max(SP.[ActIdInt]) AS MaxAct
INTO PRP_CampainAct_Max28D
FROM [dbo].[ev_StorylineProgress]  AS SP INNER JOIN PRP_Pool_Of_Users AS POU ON POU.[UserId] = SP.[UserId] 
    INNER JOIN  [dbo].[users] AS U ON POU.[UserId] = U.[UserId] 
WHERE SP.[Timestamp] BETWEEN U.[Created] AND DATEADD (DD , 28 , U.[Created] )  
AND U.[Created] > @start_period
GROUP BY SP.[UserId]
ORDER BY MaxAct DESC


SELECT Count(*), Count (MaxAct) FROM PRP_CampainAct_Max28D 
SELECT TOP (1000) * FROM PRP_CampainAct_Max28D


 