
declare @sql_1 varchar(max)
declare @sql_2 varchar(max)
declare @sql_3 varchar(max)
declare @sql_4 varchar(max)
declare @sql_5 varchar(max)
declare @sql_6 varchar(max)
declare @sql_7 varchar(max)
declare @sql_8 varchar(max)
declare @sql_9 varchar(max)
declare @sql_10 varchar(max)
declare @sql_11 varchar(max)
declare @sql_12 varchar(max)
declare @sql_13 varchar(max)
declare @sql_14 varchar(max)
declare @sql_15 varchar(max)
declare @sql_16 varchar(max)

--declare @sql_17 varchar(max)
--declare @sql_18 varchar(max)
--declare @sql_19 varchar(max)
--declare @sql_20 varchar(max)

declare @base_name varchar(max)

--SET @base_name   = '[dbo].[PRP_Pool_Of_Active_Users_6_Month_LESS_1_6_0]';
--SET @base_name   = '[dbo].[PRP_Terminal_Inviters_LESS_1_6_0]';
--SET @base_name   = '[dbo].[PRP_Terminal_InviteD_LESS_1_6_0]';
 --SET @base_name   = '[dbo].[PRP_Pool_Of_Active_Users_6_Month_MORE_1_6_0]';
--SET @base_name   = '[dbo].[PRP_Terminal_Inviters_MORE_1_6_0]';
SET @base_name   = '[dbo].[PRP_Terminal_InviteD_MORE_1_6_0]';

SET @sql_1 = '
SELECT Count (DISTINCT [UserId]) AS NOT_PAYERS
FROM [dbo].[Users] AS U
	INNER JOIN'+ @base_name+' AS POOL_USERS
	ON POOL_USERS.[U_ID] = U.[UserId]
	WHERE [LTV] = 0'


SET @sql_2 = '
SELECT Count (DISTINCT [UserId]) AS PAYERS
FROM [dbo].[Users] AS U
	INNER JOIN'+ @base_name+' AS POOL_USERS
	ON POOL_USERS.[U_ID] = U.[UserId]
	WHERE [LTV] > 0'

SET @sql_3 = '
SELECT Avg ([LTV3D]) AS ARPU_D3
FROM [dbo].[Users] AS U
	INNER JOIN'+ @base_name+' AS POOL_USERS
	ON POOL_USERS.[U_ID] = U.[UserId]'

SET @sql_4 = '
SELECT Avg ([LTV7D]) AS ARPU_D7
FROM [dbo].[Users] AS U
	INNER JOIN'+ @base_name+' AS POOL_USERS
	ON POOL_USERS.[U_ID] = U.[UserId]'

SET @sql_5 = '
SELECT Avg ([LTV28D]) AS ARPU_D28
FROM [dbo].[Users] AS U
	INNER JOIN'+ @base_name+' AS POOL_USERS
	ON POOL_USERS.[U_ID] = U.[UserId]'

SET @sql_6 = '
SELECT Avg ([LTV3D]) AS ARPPU_D3
FROM [dbo].[Users] AS U
	INNER JOIN'+ @base_name+' AS POOL_USERS
	ON POOL_USERS.[U_ID] = U.[UserId]
	WHERE [LTV] > 0'

SET @sql_7 = '
SELECT Avg ([LTV7D]) AS ARPPU_D7
FROM [dbo].[Users] AS U
	INNER JOIN'+ @base_name+' AS POOL_USERS
	ON POOL_USERS.[U_ID] = U.[UserId]
	WHERE [LTV] > 0'

SET @sql_8 = '
SELECT Avg ([LTV28D]) AS ARPPU_D28
FROM [dbo].[Users] AS U
	INNER JOIN'+ @base_name+' AS POOL_USERS
	ON POOL_USERS.[U_ID] = U.[UserId]
	WHERE [LTV] > 0'

SET @sql_9 = '
SELECT Count (DISTINCT [UserId]) AS ROLLING_D1
FROM [dbo].[Users] AS U
	INNER JOIN'+ @base_name+' AS POOL_USERS
	ON POOL_USERS.[U_ID] = U.[UserId]
	WHERE Retention1D IS NOT NULL 
	OR Retention2D IS NOT NULL
	OR Retention3D IS NOT NULL
	OR Retention4D IS NOT NULL
	OR Retention5D IS NOT NULL
	OR Retention6D IS NOT NULL
	OR Retention7D IS NOT NULL
	OR Retention8D IS NOT NULL
	OR Retention9D IS NOT NULL
	OR Retention10D IS NOT NULL
	OR Retention11D IS NOT NULL
	OR Retention12D IS NOT NULL
	OR Retention13D IS NOT NULL
	OR Retention14D IS NOT NULL
	OR Retention15D IS NOT NULL
	OR Retention16D IS NOT NULL
	OR Retention17D IS NOT NULL
	OR Retention18D IS NOT NULL
	OR Retention19D IS NOT NULL
	OR Retention20D IS NOT NULL
	OR Retention21D IS NOT NULL
	OR Retention22D IS NOT NULL
	OR Retention23D IS NOT NULL
	OR Retention24D IS NOT NULL
	OR Retention25D IS NOT NULL
	OR Retention26D IS NOT NULL
	OR Retention27D IS NOT NULL
	OR Retention28D IS NOT NULL
	OR Retention29D IS NOT NULL
	OR Retention30D IS NOT NULL
	OR Retention2M IS NOT NULL
	OR Retention4M IS NOT NULL
	OR Retention6M IS NOT NULL
	OR Retention1Y IS NOT NULL
	OR Retention2Y IS NOT NULL
	'

	SET @sql_10 = '
SELECT Count (DISTINCT [UserId]) AS ROLLING_D3
FROM [dbo].[Users] AS U
	INNER JOIN'+ @base_name+' AS POOL_USERS
	ON POOL_USERS.[U_ID] = U.[UserId]
	WHERE Retention3D IS NOT NULL
	OR Retention4D IS NOT NULL
	OR Retention5D IS NOT NULL
	OR Retention6D IS NOT NULL
	OR Retention7D IS NOT NULL
	OR Retention8D IS NOT NULL
	OR Retention9D IS NOT NULL
	OR Retention10D IS NOT NULL
	OR Retention11D IS NOT NULL
	OR Retention12D IS NOT NULL
	OR Retention13D IS NOT NULL
	OR Retention14D IS NOT NULL
	OR Retention15D IS NOT NULL
	OR Retention16D IS NOT NULL
	OR Retention17D IS NOT NULL
	OR Retention18D IS NOT NULL
	OR Retention19D IS NOT NULL
	OR Retention20D IS NOT NULL
	OR Retention21D IS NOT NULL
	OR Retention22D IS NOT NULL
	OR Retention23D IS NOT NULL
	OR Retention24D IS NOT NULL
	OR Retention25D IS NOT NULL
	OR Retention26D IS NOT NULL
	OR Retention27D IS NOT NULL
	OR Retention28D IS NOT NULL
	OR Retention29D IS NOT NULL
	OR Retention30D IS NOT NULL
	OR Retention2M IS NOT NULL
	OR Retention4M IS NOT NULL
	OR Retention6M IS NOT NULL
	OR Retention1Y IS NOT NULL
	OR Retention2Y IS NOT NULL
	'
	SET @sql_11 = '
SELECT Count (DISTINCT [UserId]) AS ROLLING_D7
FROM [dbo].[Users] AS U
	INNER JOIN'+ @base_name+' AS POOL_USERS
	ON POOL_USERS.[U_ID] = U.[UserId]
	WHERE Retention7D IS NOT NULL
	OR Retention8D IS NOT NULL
	OR Retention9D IS NOT NULL
	OR Retention10D IS NOT NULL
	OR Retention11D IS NOT NULL
	OR Retention12D IS NOT NULL
	OR Retention13D IS NOT NULL
	OR Retention14D IS NOT NULL
	OR Retention15D IS NOT NULL
	OR Retention16D IS NOT NULL
	OR Retention17D IS NOT NULL
	OR Retention18D IS NOT NULL
	OR Retention19D IS NOT NULL
	OR Retention20D IS NOT NULL
	OR Retention21D IS NOT NULL
	OR Retention22D IS NOT NULL
	OR Retention23D IS NOT NULL
	OR Retention24D IS NOT NULL
	OR Retention25D IS NOT NULL
	OR Retention26D IS NOT NULL
	OR Retention27D IS NOT NULL
	OR Retention28D IS NOT NULL
	OR Retention29D IS NOT NULL
	OR Retention30D IS NOT NULL
	OR Retention2M IS NOT NULL
	OR Retention4M IS NOT NULL
	OR Retention6M IS NOT NULL
	OR Retention1Y IS NOT NULL
	OR Retention2Y IS NOT NULL
	'
	SET @sql_12 = '
SELECT Count (DISTINCT [UserId]) AS ROLLING_D28
FROM [dbo].[Users] AS U
	INNER JOIN'+ @base_name+' AS POOL_USERS
	ON POOL_USERS.[U_ID] = U.[UserId]
	WHERE Retention28D IS NOT NULL
	OR Retention29D IS NOT NULL
	OR Retention30D IS NOT NULL
	OR Retention2M IS NOT NULL
	OR Retention4M IS NOT NULL
	OR Retention6M IS NOT NULL
	OR Retention1Y IS NOT NULL
	OR Retention2Y IS NOT NULL
	'

SET @sql_13 = '
SELECT Avg ([TotalSessionLength3D]*1.0) AS TOTAL_SESION_D3
FROM [dbo].[Users] AS U
	INNER JOIN'+ @base_name+' AS POOL_USERS
	ON POOL_USERS.[U_ID] = U.[UserId]'

SET @sql_14 = '
SELECT Avg ([TotalSessionLength7D]*1.0) AS TOTAL_SESION_D7
FROM [dbo].[Users] AS U
	INNER JOIN'+ @base_name+' AS POOL_USERS
	ON POOL_USERS.[U_ID] = U.[UserId]'
	
SET @sql_15 = '
SELECT Avg ([TotalSessionLength28D]*1.0) AS TOTAL_SESION_D28
FROM [dbo].[Users] AS U
	INNER JOIN'+ @base_name+' AS POOL_USERS
	ON POOL_USERS.[U_ID] = U.[UserId]'

SET @sql_16  = '
SELECT SUM (LTV) AS TOTAL_LTV
FROM [dbo].[Users] AS U
	INNER JOIN'+ @base_name+' AS POOL_USERS
	ON POOL_USERS.[U_ID] = U.[UserId]'

	exec (@sql_16)

--exec (@sql_1)
--exec (@sql_2)
--exec (@sql_3)
--exec (@sql_4)
--exec (@sql_5)
--exec (@sql_6)
--exec (@sql_7)
--exec (@sql_8)
--exec (@sql_9)
--exec (@sql_10)
--exec (@sql_11)
--exec (@sql_12)
--exec (@sql_13)
--exec (@sql_14)
--exec (@sql_15)
--exec (@sql_16)

--exec (@sql_17)
--exec (@sql_18)
--exec (@sql_19)
--exec (@sql_20)