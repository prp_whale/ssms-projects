DECLARE @st_per DATETIME = '2020-04-01 00:00:00.000';
DECLARE @end_per DATETIME =  '2020-04-15 23:59:59.997'; --22

WITH PoU
AS (
    SELECT DISTINCT UserId AS U_ID
    FROM dbo.Users 
    --WHERE RegistrationClientVersion IN ('1.0.214','1.0.217','1.0.302', '1.0.289')
)

, AO 
AS (
    SELECT UserID 
    , [Timestamp] AS e_time
    ,  LEAD ([Timestamp]) OVER (PARTITION BY UserId ORDER BY [Timestamp] ASC)  as next_e_time
    
    FROM EventsAppOpen
    WHERE [Timestamp] BETWEEN @st_per AND @end_per  
        AND ClientVersion IN ('1.0.214','1.0.217','1.0.302', '1.0.289')
)
, AO_5H
AS (
    SELECT UserID 
    , e_time AS 'times'
    FROM AO
    WHERE DATEDIFF(ss, e_time, next_e_time ) > 5*60*60
    OR next_e_time IS NULL
   -- GROUP BY UserID
)

, CHEST_AD
AS (
    SELECT T.UserId 
        , AccountLevel AS lvl
    FROM VideoAdStatus AS T 
    --    INNER JOIN AO ON T.UserId = AO.UserID  
   -- AND [Timestamp] BETWEEN e_time AND next_e_time
   WHERE [Timestamp] BETWEEN @st_per AND @end_per  
    AND ClientVersion IN ('1.0.214','1.0.217','1.0.302', '1.0.289')
    AND TriggerSourceId = 'OpenChest'
)

SELECT TOP(100)  * FROM AO_5H 
WHERE UserID = 5145

-- SELECT Count(*) AS Actions 
--     , Count (DISTINCT UserId ) AS Players
--  FROM AO_5H  


-- SELECT t.lvl 
--     , Count(*) AS Actions 
--     , Count (DISTINCT t.UserId ) AS Players
-- FROM CHEST_AD AS t 

-- WHERE t.lvl BETWEEN 1  AND  50
-- GROUP BY t.lvl 
-- ORDER BY t.lvl 