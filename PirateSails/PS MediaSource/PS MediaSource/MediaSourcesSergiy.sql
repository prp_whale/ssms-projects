DECLARE @start_period DATETIME = '2019-03-10 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-04-10 23:59:59.997'; 

SELECT CONVERT(DATE, [Created]) 
       , Count(Iif([MediaSource] IS NULL, [UserId], NULL))               AS organik 
       , Count(Iif([MediaSource] = 'googleadwords_int', [UserId], NULL)) AS googleadwords_int 
       , Count(Iif([MediaSource] = 'gurutraff_int', [UserId], NULL))     AS gurutraff_int 
       , Count(Iif([MediaSource] = 'unityads_int', [UserId], NULL))      AS unityads_int 
FROM   [dbo].[Users] 
WHERE  [Created] BETWEEN @start_period AND @end_period 
   AND [Platform] = 1
GROUP  BY CONVERT(DATE, [Created]) 
ORDER  BY CONVERT(DATE, [Created]) 