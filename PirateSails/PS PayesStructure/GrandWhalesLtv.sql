Declare @GrandWhales table (Game_U_id int)
insert into @GrandWhales VALUES (161411), (150874), (108879), (145185), (131093), (130652), (127446), (115616), (26414), (110795), (103806), (108879), (105788), (103806), (103574), (103115), (98230), (118521), (87725), (155143), (86314), (83300), (79262), (75262), (73956), (80020), (71893), (67572), (66749), (63730), (63455), (61029), (40326), (38599), (37919), (32312), (31542), (97098), (28600), (26414), (22951), (23138), (22616), (22543), (22105), (20352), (59475), (18571), (16155), (739), (15782), (11693), (9989), (5873), (5958), (3667), (2162), (1378), (961);

Declare @GrandWhalesNoID table (U_id int)
insert into @GrandWhalesNoID VALUES (56158), (40011), (36337), (27362);

Declare @GrandWhales2Accounts table (U_id int)
insert into @GrandWhales2Accounts VALUES (86194), (84740), (69316), (61197), (26272), (16201);

WiTH Pool_Of_Whale
as(
    SELECT DISTINCT  U.UserId FROM DBO.[Users] AS U INNER JOIN @GrandWhales As GW ON U.[GameUserId] = GW.[Game_U_id]
     UNION  SELECT  * FROM @GrandWhales2Accounts
     UNION  SELECT  * FROM @GrandWhalesNoID
)


SELEcT 
 SELECT  * FROM  Pool_Of_Whale