DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform varchar(4)

SET @startperiod = '2019-09-01 00:00:00.000';
SET @endperiod = '2019-11-01 23:59:59.999';

WITH
    AutumnUsers
    AS
    (
        SELECT UserId  AS U_ID
        FROM dbo.Users
        WHERE Created BETWEEN @startperiod AND @endperiod
            -- testers
            AND GameUserId NOT IN (103,	1240,	22,	1064,	0,	1002,	69,	482,	93,	671,	1050,	22,	1028,	647,	1004)
      
    )

SELECT item_id
  , COUNT(DISTINCT UserId) AS Users
  , COunt (*) AS Trans
FROM [dbo].[currency_received] 
    AS CT INNER JOIN AutumnUsers AS POU ON CT.[UserId]  = POU.[U_ID]
WHERE source_type_id = 'level_up'
GROUP BY item_id
ORDER BY item_id