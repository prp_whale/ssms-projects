DECLARE @start_date DATETIME = '2019-02-25 00:00:00.000'; 
DECLARE @end_date DATETIME = '2019-03-06 23:59:59.999'; 
--DECLARE @cheater Int = 7249;   
DECLARE @test_country VARCHAR(2) = 'UA'; 

SELECT * 
FROM   [dbo].[EventsAppOpen] 
WHERE [UserId] = 9133

--SELECT [UserId]   AS U_ID 
--       , Count(*) AS AMOUNT 
--FROM   [dbo].[EventsAppOpen] 
--WHERE  [Timestamp] BETWEEN @start_date AND @end_date 
--GROUP  BY [UserId] 
--ORDER  BY U_ID
----ORDER  BY AMOUNT DESC 

SELECT EAO.[UserId]   AS U_ID 
       , Count(*) AS AMOUNT 
FROM   [dbo].[EventsAppOpen] as EAO 
		LEFT JOIN (
		 SELECT *
		 FROM  [dbo].[balance] 
		 WHERE [Timestamp] BETWEEN @start_date AND @end_date 
		 )as B ON EAO.[UserId]=B.[UserId]
WHERE  B.[UserId] IS NULL
AND EAO.[Timestamp] BETWEEN @start_date AND @end_date 
GROUP  BY EAO.[UserId] 
--ORDER  BY U_ID
ORDER  BY AMOUNT DESC

