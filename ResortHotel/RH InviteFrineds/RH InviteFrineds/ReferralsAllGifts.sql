DECLARE @start_period DATETIME = '2019-08-01 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-08-31 23:59:59.997'; 

WITH SUCCSESFULL_INVITERS
AS (
    SELEcT DISTINCT  EMP.[UserId]
    FROM   [dbo].[EventsProductMovement]  AS EMP  -- INNER JOIN PRP_Pool_Of_Real_Invatiors AS RI On RI.[U_ID] = EMP.[UserId] 
    WHERE  [TriggerSourceId] = 'referrals' 
    AND [ProductType] = 'coins'
    --AND [ProductAmount]
    AND [Timestamp] BETWEEN @start_period AND @end_period
    AND [RegistrationCountry] != 'UA'
)

SELECT Count (*) FROM  PRP_Pool_Of_Real_Invatiors


SELEcT UserId 
, (SELECT Count(*) 
    FROM   [dbo].[EventsProductMovement] 
    WHERE  [TriggerSourceId] = 'referrals' 
    AND [ProductType] = 'coins'
    AND [ProductAmount] = 200
    AND UserId = SI.[UserId] ) AS '200'
    , (SELECT Count(*) 
    FROM   [dbo].[EventsProductMovement] 
    WHERE  [TriggerSourceId] = 'referrals' 
    AND [ProductType] = 'coins'
    AND [ProductAmount] = 300
    AND UserId = SI.[UserId] ) AS '300'
    , (SELECT Count(*) 
    FROM   [dbo].[EventsProductMovement] 
    WHERE  [TriggerSourceId] = 'referrals' 
    AND [ProductType] = 'coins'
    AND [ProductAmount] = 500
    AND UserId = SI.[UserId] ) AS '500'
, (SELECT Count(*) 
    FROM   [dbo].[EventsProductMovement] 
    WHERE  [TriggerSourceId] = 'referrals' 
    AND [ProductType] = 'coins'
    AND [ProductAmount] = 700
    AND UserId = SI.[UserId] ) AS '700'
FROM SUCCSESFULL_INVITERS AS SI 
ORDER BY 5 desc ,  4 desc ,  3 desc, 2 desc  


-- SELEcT * 
-- FROM   [dbo].[EventsProductMovement] 
-- WHERE  [TriggerSourceId] = 'referrals' 
-- AND [ProductType] = 'coins'
-- AND [Timestamp] BETWEEN @start_period AND @end_period
-- AND [ProductAmount] = 300