/*
Також, потрібна нова аналітика по колесам фортуни, ми пофіксили вєси в колесах.
*/

WITH FS
AS (
    SELECT UserId AS U_ID
    ,  Min([Timestamp]) AS first_angel
    FROM EventsProductMovement
    WHERE TriggerSourceId = 'ads_wheel'
    AND ClientVersion IN ( '1.0.289', '1.0.214')
    AND TriggerId LIKE '1_Angels%'
    GROUP BY UserID

)

SELECT ProductType , ProductAmount, TriggerId 
, Count(*) AS 'evnts'
FROM EventsProductMovement INNER JOIN FS ON U_ID = UserId AND [Timestamp] = first_angel
WHERE TriggerSourceId = 'ads_wheel'
--AND ClientVersion = '1.0.289'
GROUP BY ProductType , ProductAmount , TriggerId 
ORDER BY ProductType , ProductAmount , TriggerId 


