DECLARE @start_date Datetime = '2019-02-01 00:00:00.000'; 
DECLARE @end_date Datetime = '2019-03-06 23:59:59.999'; 
--DECLARE @cheater Int = 7249;  
DECLARE @test_country Varchar(2) = 'UA' ;
DECLARE @source Varchar(25) = 'level_up' ;

WITH DUPLICATE_LVL_UP_USERS
     AS ( 
SELECT [level_id]  AS LVL 
       , [UserId]  AS U_ID 
       , Count (*) AS TRANSACTIONS 
FROM   [dbo].[currency_received] 
WHERE  [RegistrationCountry] != @test_country 
   AND [timestamp] BETWEEN @start_date AND @end_date 
   AND [source_type_id] = @source 
   AND [coins] != 0 
 --  AND [level_id] = 4 
GROUP  BY [level_id] 
          , [UserId] 
HAVING Count (*) > 1 
--ORDER  BY LVL
) --ORDER  BY U_ID

SELECT  U_ID
,COUNT(*) AS AMOUNT  
FROM DUPLICATE_LVL_UP_USERS
GROUP BY U_ID
ORDER BY U_ID 
--ORDER BY AMOUNT desc