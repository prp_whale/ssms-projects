DECLARE @start_period DATETIME = '2019-03-09 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-05-09 23:59:59.997'; 


DROP TABLE IF EXISTS PRP_Pool_Of_ACTIVE_USERS;

SELECT U.*
INTO PRP_Pool_Of_ACTIVE_USERS
FROM   [dbo].[PRP_Pool_Of_USERS] AS U 
       INNER JOIN (
					SELECT DISTINCT [UserId] 
					FROM [dbo].[EventsAppOpen] 
					WHERE [TimeStamp] BETWEEN @start_period AND @end_period
					)AS AO 
              ON AO.[UserId] = U.U_ID 



--SELECT Count (DISTINCT U_ID) AS Users 
--       ,Count (*)                 AS Amount 
--FROM   [dbo].[PRP_Pool_Of_USERS2] 

SELECT Count (DISTINCT U_ID) AS Users 
       ,Count (*)                 AS Amount 
FROM   [dbo].[PRP_Pool_Of_ACTIVE_USERS] 