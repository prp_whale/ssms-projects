--dates for report

declare @days int = 28;
​declare @days_multi int = 3;
declare @DateFrom DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days*@days_multi, GETDATE())));
declare @DateTo DATETIME  = DATEADD(ms, -3, CONVERT(datetime, CONVERT(date, GETDATE())));

SET DateFirst 1
;

WITH USERS as (select 
                userid as UserId,
                registrationcountry as country,
                isnull(MediaSource,'Organic') as Source,
                case 
                 when ltv >= 300 then 'whale'  
                 when ltv >= 70 and ltv < 300 then 'grand dolphin'
                 when ltv >=  7 and ltv < 70 then 'dolphin'
                 when ltv >   0 and ltv < 7 then 'minnow'
                 when ltv = 0 then 'non-depositor'
                 else null
                end as Segment,
		        iif(PLATFORM = 1, 'Android', 'iOS') as OS
                from dbo.Users
                    where created between @DateFrom and @DateTo
                    --and registrationcountry not in ('ua', 'cn')
                     
                )

select 
        CONVERT(date,epm.timestamp) as 'Date'
        ,DATEPART(ww, epm.timestamp) as 'Week'
        ,u.country
        ,u.Source
        ,u.Segment
        ,u.OS
        ,ProductType
        ,iif(epm.action = 'spent', 'Spent', 'Received')  as Action       
        ,epm.LevelId as Level
        ,epm.TriggerSourceID as  TriggerSource
        ,epm.TriggerID as Triggers
        ,count(distinct u.userid ) as Users
        ,sum (epm.ProductAmount​) as coins
        ,count( epm.[Order] ) as orders
from [dbo].[EventsProductMovement] epm join USERS u on epm.userid=u.userid
    where [timestamp] between @DateFrom and @DateTo     
    and epm.ProductAmount between 1 and 92000
group by CONVERT(date,epm.timestamp) 
        ,DATEPART(ww, epm.timestamp)
        ,u.country
        ,u.Source
        ,u.Segment
        ,u.OS
        ,ProductType
        ,iif(epm.action = 'spent', 'Spent', 'Received')     
        ,epm.LevelId 
        ,epm.TriggerSourceID 
        ,epm.TriggerID 
