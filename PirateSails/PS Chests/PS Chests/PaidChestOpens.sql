DECLARE @start_period DATETIME = '2019-03-01 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-04-10 23:59:59.997'; 


DECLARE @test_country Varchar(2) = 'UA'; -- ��������� ������ �� � �� ������� �� ������ �������

DECLARE  @TestDevices TABLE (UserID INT PRIMARY KEY);
INSERT INTO @TestDevices VALUES  (6), (14), (23), (105), (239), (247), (256), (257), (333), (1331), (1366), (1367), (1368), (1375), (1381), (1382), (3487), (3737), (4244), (4253), (4255), (4265), (4266), (4270), (5517), (5906), (2), (3), (7), (8), (9), (12), (16), (17), (21), (22), (25), (26), (27), (51), (117), (249), (293), (598), (666), (670), (1190), (1192), (1193), (1337), (5568);



WITH POOL_OF_USERS  -- ������ ������������ �� ��������� �����������
AS ( 
		SELECT U.[UserId] AS ID 
		FROM   [dbo].[Users] AS U 
			   LEFT JOIN @TestDevices AS TD 
					  ON U.[UserId] = TD.UserID
		WHERE 
			TD.UserID IS NULL -- ��������� ������ �������
			AND  [Created] BETWEEN @start_period AND @end_period 
		    AND [RegistrationCountry] != @test_country  -- ��������� ������ �� � �� ������� �� ������ �������
		--    AND [LTV] = 0
	) 

, CHESTS_OPEN -- ������ �������� �������� �������� �������������
 AS (
	  SELECT [UserId] 
      ,[Timestamp]
	  ,[ChestValue]
      ,[ChestLevel]
      ,[IsFreeOpen]
      ,[ChestRarity]
	   FROM [dbo].[ChestOpen] AS CO 
		 INNER JOIN POOL_OF_USERS AS POU 
					   ON POU.ID = CO.[UserId] 
		WHERE [Timestamp] BETWEEN @start_period AND @end_period 
 )

SELECT [ChestLevel] 
      -- , Count (*)                 AS Amount 
       , Count (DISTINCT [UserId]) AS '0'
       , (SELECT Count ([UserId]) 
          FROM   (SELECT [UserId] 
                         , Count (*) AS BuyAmount 
                  FROM   CHESTS_OPEN AS CO 
                  WHERE  [IsFreeOpen] = 0 
                     AND CO.[ChestLevel] = CHESTS_OPEN.[ChestLevel] 
                  GROUP  BY [UserId]) AS U 
          WHERE  BuyAmount = 1)    AS '1' 
		  , (SELECT Count ([UserId]) 
          FROM   (SELECT [UserId] 
                         , Count (*) AS BuyAmount 
                  FROM   CHESTS_OPEN AS CO 
                  WHERE  [IsFreeOpen] = 0 
                     AND CO.[ChestLevel] = CHESTS_OPEN.[ChestLevel] 
                  GROUP  BY [UserId]) AS U 
          WHERE  BuyAmount = 2)    AS '2' 
		  , (SELECT Count ([UserId]) 
          FROM   (SELECT [UserId] 
                         , Count (*) AS BuyAmount 
                  FROM   CHESTS_OPEN AS CO 
                  WHERE  [IsFreeOpen] = 0 
                     AND CO.[ChestLevel] = CHESTS_OPEN.[ChestLevel] 
                  GROUP  BY [UserId]) AS U 
          WHERE  BuyAmount = 3)    AS '3' 
		  , (SELECT Count ([UserId]) 
          FROM   (SELECT [UserId] 
                         , Count (*) AS BuyAmount 
                  FROM   CHESTS_OPEN AS CO 
                  WHERE  [IsFreeOpen] = 0 
                     AND CO.[ChestLevel] = CHESTS_OPEN.[ChestLevel] 
                  GROUP  BY [UserId]) AS U 
          WHERE  BuyAmount = 4)    AS '4' 
		  , (SELECT Count ([UserId]) 
          FROM   (SELECT [UserId] 
                         , Count (*) AS BuyAmount 
                  FROM   CHESTS_OPEN AS CO 
                  WHERE  [IsFreeOpen] = 0 
                     AND CO.[ChestLevel] = CHESTS_OPEN.[ChestLevel] 
                  GROUP  BY [UserId]) AS U 
          WHERE  BuyAmount = 5)    AS '5' 
		  , (SELECT Count ([UserId]) 
          FROM   (SELECT [UserId] 
                         , Count (*) AS BuyAmount 
                  FROM   CHESTS_OPEN AS CO 
                  WHERE  [IsFreeOpen] = 0 
                     AND CO.[ChestLevel] = CHESTS_OPEN.[ChestLevel] 
                  GROUP  BY [UserId]) AS U 
          WHERE  BuyAmount = 6)    AS '6' 
		  , (SELECT Count ([UserId]) 
          FROM   (SELECT [UserId] 
                         , Count (*) AS BuyAmount 
                  FROM   CHESTS_OPEN AS CO 
                  WHERE  [IsFreeOpen] = 0 
                     AND CO.[ChestLevel] = CHESTS_OPEN.[ChestLevel] 
                  GROUP  BY [UserId]) AS U 
          WHERE  BuyAmount = 7)    AS '7' 
		  , (SELECT Count ([UserId]) 
          FROM   (SELECT [UserId] 
                         , Count (*) AS BuyAmount 
                  FROM   CHESTS_OPEN AS CO 
                  WHERE  [IsFreeOpen] = 0 
                     AND CO.[ChestLevel] = CHESTS_OPEN.[ChestLevel] 
                  GROUP  BY [UserId]) AS U 
          WHERE  BuyAmount = 8)    AS '8' 
		  , (SELECT Count ([UserId]) 
          FROM   (SELECT [UserId] 
                         , Count (*) AS BuyAmount 
                  FROM   CHESTS_OPEN AS CO 
                  WHERE  [IsFreeOpen] = 0 
                     AND CO.[ChestLevel] = CHESTS_OPEN.[ChestLevel] 
                  GROUP  BY [UserId]) AS U 
          WHERE  BuyAmount = 9)    AS '9' 
		  , (SELECT Count ([UserId]) 
          FROM   (SELECT [UserId] 
                         , Count (*) AS BuyAmount 
                  FROM   CHESTS_OPEN AS CO 
                  WHERE  [IsFreeOpen] = 0 
                     AND CO.[ChestLevel] = CHESTS_OPEN.[ChestLevel] 
                  GROUP  BY [UserId]) AS U 
          WHERE  BuyAmount >= 10)    AS '10+' 
FROM   CHESTS_OPEN 
WHERE  [IsFreeOpen] = 1
GROUP  BY [ChestLevel] 
ORDER  BY [ChestLevel] 



