DECLARE @start_date Datetime = '2019-02-11 00:00:00.000'; 
DECLARE @end_date Datetime = '2019-02-24 23:59:59.999'; 
DECLARE @cheater Int = 7249; 
DECLARE @test_country Varchar(2) = 'UA'; 


WITH AIRPLANES_AND_LVL 
     AS (
			SELECT MQ.[userid]          AS U_ID 
				   , MQ.[timestamp]		AS TS
				   , Max(SQ.[level_id]) AS LVL 
				 --  ,MQ.[arrived] AS ARRIVED
				   ,MQ.[sends] AS SENDS
				   ,MQ.[acceleration] AS ACC
				   	FROM   [dbo].[airport] AS MQ 
				   INNER JOIN (
								SELECT [userid] 
										  , [timestamp] 
										  , [level_id] 
								   FROM   [dbo].[currency_received] 
								   WHERE  [source_type_id] = 'level_up' 
									  AND [coins] != 0
									      AND [LTV] = 0 
									AND [RegistrationCountry] != @test_country 
									  ) AS SQ 
						   ON MQ.[userid] = SQ.[userid] 
							  AND MQ.[timestamp] >= SQ.[timestamp] 
			WHERE  MQ.[timestamp] BETWEEN @start_date AND @end_date 
			   AND MQ.[UserId] != @cheater 
			   AND [RegistrationCountry] != @test_country 
			   AND [LTV] = 0 
			GROUP  BY MQ.[userid] 
					  , MQ.[timestamp]
					--   ,MQ.[arrived]
					  ,MQ.[sends] 
				,MQ.[acceleration]
			--ORDER  BY OD.[timestamp]  
        ) 


SELECT LVL 
       , Count(*)             AS Amount 
       , Count(DISTINCT U_ID) AS USERS 
	   , SUM (ACC)			  AS TOTAL_ACCELERATION
FROM   AIRPLANES_AND_LVL 
WHERE ACC != 0 
-- WHERE SENDS != 0 
GROUP  BY LVL 
ORDER  BY LVL

--SELECT   TOP (100)
--		U_ID
--		, TS
--		, LVL    
--	  ,MQ.[arrived]
--      ,MQ.[sends]
--      ,[open_slots]
--      ,[plane_id]
--      ,[slot1_quant]
--      ,[slot2_quant]
--      ,[slot3_quant]
--      ,[slot4_quant]
--      ,[slot5_quant]
--      ,[slot1_description]
--      ,[slot2_description]
--      ,[slot3_description]
--      ,[slot4_description]
--      ,[slot5_description]
--FROM   AIRPLANES_AND_LVL as MQ
--INNER JOIN  [dbo].[airport]  as SQ 
--ON MQ.TS = SQ.[timestamp]	
--ORDER BY [U_ID] desc, TS