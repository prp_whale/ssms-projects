declare @days int = 28;

declare @days_multi int = 1;
declare @st_per DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days*@days_multi, GETDATE())));
declare @end_per DATETIME  = DATEADD(ms, -3, CONVERT(datetime, CONVERT(date, GETDATE())));

declare @versa_limit int = 100;


WITH Versa
AS (
    SELECT registrationclientversion AS RegVer
    FROM  dbo.Users
    GROUP BY registrationclientversion
    HAVING Count(*) >=  @versa_limit
)



SELECT CONVERT(date, [TimeStamp]) AS 'Date', ClientVersion, loading_number, network_type , step_id, Platform , first_loading
    ,CASE   WHEN bundles_size = 0 THEN 1 ELSE 0  END AS 'BundleSize Zero'
    ,CASE   WHEN ltv > 0 THEN 1 ELSE 0 END AS 'IsPay'
    , Avg(time_since_startup) AS avg_time
   , Count(Distinct UserID) AS Loads

FROM system_analytics 
WHERE ClientVersion IN (SELECT * FROM Versa)
AND [Timestamp] BETWEEN @st_per AND @end_per
AND (time_since_startup IS  NULL OR time_since_startup < 1800)
GROUP BY CONVERT(date, [TimeStamp]) , ClientVersion, loading_number, network_type , step_id , Platform , first_loading
    , CASE   WHEN bundles_size = 0 THEN 1 ELSE 0  END 
    , CASE   WHEN ltv > 0 THEN 1 ELSE 0  END 

ORDER BY platform, CONVERT(date, [TimeStamp]), ClientVersion, loading_number, Loads DESC


