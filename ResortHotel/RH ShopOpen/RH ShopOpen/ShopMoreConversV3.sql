

--;WITH BOTH_SHOP_OPEN
--AS (
--	SELECT [UserId] AS ID 
--        FROM   [dbo].[PRP_ShopOpenAuto_March_May] 
--        GROUP  BY [UserId] 
--        HAVING Sum([is_auto]) < count(*) and Sum([is_auto]) != 0
-- )
 --SELECT COUNT (DISTINCT ID) FROM BOTH_SHOP_OPEN

;WITH SOLO_SHOP_OPENS 
AS (  
	SELECT [UserId]  AS ID
        FROM   [dbo].[PRP_ShopOpenAuto_March_May] 
        GROUP  BY [UserId] 
    --    HAVING Sum([is_auto]) = 0 -- no auto open
		HAVING Sum([is_auto]) = Count (*) -- only auto open
)
--SELECT COUNT (DISTINCT ID)  AS SHOP_OPEN  FROM SOLO_SHOP_OPENS
, SHOP_MORE
AS ( 
	SELECT [UserId] AS ID 
	FROM   [dbo].[PRP_ShopOpenMoreAuto_March_May] AS SOM 
		--   INNER JOIN BOTH_SHOP_OPEN AS SSO 
		   INNER JOIN SOLO_SHOP_OPENS AS SSO 
				   ON SSO.ID = SOM.[UserId] 
--	   AND [is_auto] = 1 
)

, SHOP_MORE_MULTI
AS ( 
	SELECT [UserId]  AS ID 
		   ,Count(*) AS Opens 
		FROM   [dbo].[PRP_ShopOpenMoreAuto_March_May] AS SOM 
		   INNER JOIN SHOP_MORE AS SM 
				   ON SM.ID = SOM.[UserId] 
	GROUP  BY [UserId] 
	HAVING Count(*) > 1 
) 

, SHOP_PURCHASE
AS ( 
	SELECT [UserId]  AS ID 
		   ,Count(*) AS Purchases 
	FROM   [dbo].[PRP_ShopPurchase_March_May] AS SP 
		   INNER JOIN SHOP_MORE_MULTI AS SMM 
				   ON SMM.ID = SP.[UserId] 
	WHERE  [full_shop] = 1 
	GROUP  BY [UserId] 
) 

--SELECT COUNT (DISTINCT ID) FROM SHOP_PURCHASE

SELECT COUNT (DISTINCT ID)  AS SHOP_OPEN 
,(SELECT COUNT (DISTINCT ID) FROM SHOP_MORE) AS MORE_OPENS
,(SELECT COUNT (DISTINCT ID) FROM SHOP_MORE_MULTI) AS MORE_OPENS2
,(SELECT COUNT (DISTINCT ID) FROM SHOP_PURCHASE) AS PURCHASE_IN_FULL
-- FROM BOTH_SHOP_OPEN
FROM SOLO_SHOP_OPENS

