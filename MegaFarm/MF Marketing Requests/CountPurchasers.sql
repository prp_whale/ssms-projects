/*
1.Average deposit count per depositor (lifetime).
Сколько в среднем депозитов, каждый депозитор делает за всю жизнь. Если не хватает когортного периода,  берем максимальный диапазон скажем  30-того дня.
Цель понять депозиторы делают один или не сколько депозитов за максимальный период?  Есди делают один мы понимаем чего нам ожидать от показателей в маркетинге.

1) берем всех депозиторов, кто пришел к нам раньше, не позже чем 30 дней назад
2) смотрим по каждому из них за его первыми 30 днями в игре
3) и строим гистограмму: сколько платежей они делают (по горизонтальной оси - число платежей, по вертикальной - число депозиторов, у которых столько платежей за их первые 30 дней)

*/

DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform int,
        @day_shift int;

SET @day_shift = 30
SET @startperiod = '2019-02-01 00:00:00.000';
SET @endperiod = DATEADD(dd, -@day_shift, DATEADD(dd,  DATEDIFF(dd,0,GETDATE()),0));
SET @platform = 1 ;

WITH
    PoolOfUsers
    AS
    (
        SELECT  UserId  AS U_ID
        , [Created] 
        FROM dbo.Users
        WHERE Created BETWEEN @startperiod AND @endperiod
        AND [RegistrationCountry] != 'UA'
          AND GameUserId NOT IN (103,	1240,	22,	1064,	0,	1002,	69,	482,	93,	671,	1050,	22,	1028,	647,	1004) -- testers
          AND [LTV] > 0
          AND [MediaSource] IS NOT  NULL
          AND [Platform] = @platform
      )

, UsersAndPur30D
 AS (
    SELECT [UserId]
    , Max([PaymentsCount]) AS Pay30Day
    , SUM (AmountInUSD) AS TotalRevenue
    FROM dbo.[EventsPurchase] as T
        INNER JOIN PoolOfUsers as POU ON t.[UserId] = POU.[U_ID]
    WHERE [Timestamp] BETWEEN POU.[Created] AND DATEADD(dd, @day_shift, POU.[Created])
   GROUP BY [UserId]
 )


SELECT Pay30Day AS 'CountPayment on Day 30'
, Count(UserId) as Users
, Avg(TotalRevenue) AS 'Avarege revenue for payments'
FROM UsersAndPur30D 
GROUP BY Pay30Day
ORDER BY Pay30Day

-- SELECT UserId from UsersAndPur30D WHERE Pay30Day = 38

-- SELEcT * FROM dbo.EventsPurchase WHERE Userid = 101282		
-- SELEcT * FROM dbo.Users WHERE Userid = 101282	
