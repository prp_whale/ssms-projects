declare @time_begin datetime = '2019-06-10 00:00:00';
declare @time_end   datetime = '2019-12-30 23:59:59';

 declare @TestDevices table(UserID int primary key); 

insert into @TestDevices values (6), (14), (23), (105), (239), (247), (256), (257), (333), (1331), (1366), (1367), (1368), (1375), (1381), (1382), (3487), (3737), (4244), (4253), (4255), (4265), (4266), (4270), (5517), (5906), (2), (3), (7), (8), (9), (12), (16), (17), (21), (22), (25), (26), (27), (51), (117), (249), (293), (598), (666), (670), (1190), (1192), (1193), (1337), (5568);

WITH ActiveUsersTableTemp as (
	SELECT [UserId]                    AS user_id 
		   ,Min([FirstDepositDate])    AS first_dep_date 
		   ,CONVERT(date, [Timestamp]) AS appopen_timestamp 
	FROM   [dbo].[EventsAppOpen] 
	WHERE  [Timestamp] BETWEEN @time_begin AND @time_end 
	GROUP  BY CONVERT(date, [Timestamp]) 
			  ,[UserId] 
)


,ActiveUsersTable as (
	SELECT appopen_timestamp      AS 
       _date 
       ,Count(DISTINCT user_id) 
        AS active_users 
       ,Count(DISTINCT Iif(first_dep_date <= appopen_timestamp, user_id, NULL)) 
        AS 
        active_dep 
FROM   ActiveUsersTableTemp 
       LEFT JOIN [Users] 
              ON [Users].[UserId] = user_id --AND [MediaSource] IS NULL 
GROUP  BY appopen_timestamp 
	) 
,BankLevel1 as (
		 SELECT CONVERT(DATE, [Timestamp]) AS date 
			   ,[ProductId]               AS Hot1 
			   ,Sum ([AmountInUSD])       AS AmountInUSD 
		FROM   [dbo].[EventsPurchase] 
		WHERE  eventspurchase.UserId NOT IN (SELECT UserID 
											 FROM   @TestDevices) 
		   AND timestamp BETWEEN @time_BEGIN AND @time_END 
		   AND ( productid LIKE '%bank_6%Price5' 
				  OR productid LIKE '%bank_7%Price10' ) 
		GROUP  BY CONVERT(DATE, [Timestamp]) , productid 
				  
)
,Banklevel2 as (
SELECT   CONVERT(date, [Timestamp]) as date
		,[ProductId] as Hot2
		
      ,sum ([AmountInUSD])  as AmountInUSD
	  FROM [dbo].[EventsPurchase] 
	   where eventspurchase.UserId NOT IN (SELECT UserID FROM @TestDevices) and timestamp BETWEEN @time_BEGIN AND @time_END and (productid like '%bank_8%Price10' or productid like '%bank_9%Price15')
	     group by productid, CONVERT(date, [Timestamp])  
)
,Banklevel3 as (
SELECT   CONVERT(date, [Timestamp]) as date
		,[ProductId] as Hot3
		
      ,sum ([AmountInUSD])  as AmountInUSD
	  FROM [dbo].[EventsPurchase] 
	   where eventspurchase.UserId NOT IN (SELECT UserID FROM @TestDevices) and timestamp BETWEEN @time_BEGIN AND @time_END and (productid like '%bank_10%Price10' or productid like '%bank_11%Price15')
	     group by productid, CONVERT(date, [Timestamp]) 
)
,Banklevel4 as (
SELECT   CONVERT(date, [Timestamp]) as date
		,[ProductId] as Hot4
		
      ,sum ([AmountInUSD])  as AmountInUSD
	  FROM [dbo].[EventsPurchase]
	   where eventspurchase.UserId NOT IN (SELECT UserID FROM @TestDevices) and timestamp BETWEEN @time_BEGIN AND @time_END and (productid like '%bank_12%Price15' or productid like '%bank_13%Price20')
	     group by productid, CONVERT(date, [Timestamp]) 
)	
,Banklevel5 as (
SELECT   CONVERT(date, [Timestamp]) as date
		,[ProductId] as hot5
		
      ,sum ([AmountInUSD])  as AmountInUSD
	  FROM [dbo].[EventsPurchase]
	   where eventspurchase.UserId NOT IN (SELECT UserID FROM @TestDevices) and timestamp BETWEEN @time_BEGIN AND @time_END and (productid like '%bank_13%Price15' or productid like '%bank_14%Price20')
	     group by productid, CONVERT(date, [Timestamp]) 
)			  
,Banklevel6 as (
SELECT   CONVERT(date, [Timestamp]) as date
		,[ProductId] as hot6
		
      ,sum ([AmountInUSD])  as AmountInUSD
	  FROM [dbo].[EventsPurchase] 
	   where eventspurchase.UserId NOT IN (SELECT UserID FROM @TestDevices) and timestamp BETWEEN @time_BEGIN AND @time_END and (productid like '%bank_15%Price20' or productid like '%bank_16%Price30')
	     group by productid, CONVERT(date, [Timestamp]) 
)	
,Banklevel7 as (
SELECT   CONVERT(date, [Timestamp]) as date
		,[ProductId] as hot7
		
      ,sum ([AmountInUSD])  as AmountInUSD
	  FROM [dbo].[EventsPurchase] 
	   where eventspurchase.UserId NOT IN (SELECT UserID FROM @TestDevices) and timestamp BETWEEN @time_BEGIN AND @time_END and (productid like '%bank_16%Price20' or productid like '%bank_17%Price30')
	     group by productid, CONVERT(date, [Timestamp]) 
)	
Select   ActiveUsersTable._date
 ,BankLevel1.hot1
       ,banklevel1.amountinusd
--	,BankLevel2.hot2
--	,banklevel2.amountinusd
--	,BankLevel3.hot3
--	,banklevel3.amountinusd
--	,BankLevel4.hot4
--	,banklevel4.amountinusd
--,BankLevel5.hot5
--	,banklevel5.amountinusd
--,BankLevel6.hot6
--	,banklevel6.amountinusd
--	,BankLevel7.hot7
  --,banklevel7.amountinusd
	 
From ActiveUsersTable ,Banklevel1 , banklevel2,banklevel3,banklevel4,banklevel5,banklevel6

order by ActiveUsersTable._date