SELECT [eventid], 
       [dbo].[eventsinfo].[name], 
       [dbo].[eventsparamsinfo].[name] AS TypeName, 
       [type], 
       [typenumber] 
FROM   [dbo].[eventsinfo] 
       LEFT JOIN [dbo].[eventsparamsinfo] 
              ON [dbo].[eventsinfo].[id] = [dbo].[eventsparamsinfo].[eventid] 
ORDER  BY id, 
          [type], 
          typenumber 


		  DECLARE @start_date Datetime = '2019-02-11 00:00:00.000'; 
DECLARE @end_date Datetime = '2019-02-24 23:59:59.999'; 
DECLARE @cheater Int = 7249; 
DECLARE @test_country Varchar(2) = 'UA' 
DECLARE @user_start_date Datetime = '2019-02-11 00:00:00.000'; 
DECLARE @user_end_date Datetime = '2019-02-11 23:59:59.999'; 

SELECT TOP (1000) [UserId] 
                  , [Timestamp] 
                  , [order_status] 
                  , [slot1_quant] 
                  , [slot2_quant] 
                  , [slot3_quant] 
                  , [slot4_quant] 
                  , [slot5_quant] 
                  , [slot6_quant] 
                  , [CustomerId] 
                  , [slot1_description] 
                  , [slot2_description] 
                  , [slot3_description] 
                  , [slot4_description] 
                  , [slot5_description] 
                  , [slot6_description] 
FROM   [dbo].[order_drone] AS OD 
       INNER JOIN (SELECT [UserId] AS ID 
                   FROM   [dbo].[users] 
                   WHERE  [RegistrationCountry] != @test_country 
                      AND [CreatedBackOfficeTime] BETWEEN 
                          @user_start_date AND @user_end_date) AS USERS 
               ON USERS.ID = OD.[UserId] 
WHERE  [Timestamp] BETWEEN @start_date AND @end_date 
   AND [UserId] != @cheater 
   AND [LTV] = 0 