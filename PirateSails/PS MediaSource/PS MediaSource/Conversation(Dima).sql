SELECT 
			 
             DATEPART(dd, created) as 'Day',
             DATEPART(mm, created) as 'Month',
             DATEPART(yy, created) as 'Year',
			 COUNT(DISTINCT UserId) as 'NewUsers',
			 count(iif(ltv1d>0,1,null))*1.0/count(*) [Conversion 1d],
			 count(iif(ltv2d>0,1,null))*1.0/count(*) [Conversion 2d],
			 count(iif(ltv7d>0,1,null))*1.0/count(*) [Conversion 7d],
			 count(iif(ltv14d>0,1,null))*1.0/count(*) [Conversion 14d],
			 count(iif(ltv28d>0,1,null))*1.0/count(*) [Conversion 28d]
		FROM users
	   WHERE Platform = @platform
	     AND created BETWEEN @startperiod AND @endperiod and mediasource is not null
		GROUP BY DATEPART(dd, created), DATEPART(mm, created), DATEPART(yy, created)