DECLARE @LastWeekLastDay DATETIME = '2019-03-31 23:59:59.993'; 
DECLARE @TwoWeeksBeforeEnd DATETIME = '2019-03-18 00:00:00.000'; 
DECLARE @ThreeDaysBeforeEnd DATETIME = '2019-03-28 23:59:59.993'; 

SET @LastWeekLastDay = Dateadd(MS, -5, Dateadd(WK, Datediff(WK, 0, Getdate()), 0)); 
SET @TwoWeeksBeforeEnd = Dateadd(WK, Datediff(WK, 0, Getdate()) - 2, 0); 
SET @ThreeDaysBeforeEnd = Dateadd(MS, -5, Dateadd(DD, 4, Dateadd(WK, Datediff(WK, 0, Getdate()) - 1, 0))); 
--SELECT @LastWeekLastDay AS LastWeekLastDay, @TwoWeeksBeforeEnd AS TwoWeeksBeforeEnd, @ThreeDaysBeforeEnd AS ThreeDaysBeforeEnd

DECLARE  @UsersTurorialStepSkipers TABLE (UserID INT PRIMARY KEY);
INSERT INTO @UsersTurorialStepSkipers VALUES (8690),	(19244),	(16677),	(21231),	(9314),	(8139),	(16913),	(12033),	(15092),	(8492),	(10685),	(16446),	(19346),	(10191),	(21119),	(10227),	(18345),	(21011),	(16466),	(21135),	(17762),	(8899),	(14980),	(8024),	(10634),	(12777),	(13799),	(16013),	(18049),	(18133),	(18350),	(18799),	(19144),	(20270),	(21659),	(10673),	(12443),	(12510),	(12715),	(12928),	(15141),	(17070),	(20984),	(21205),	(21270),	(7716),	(8911),	(8956),	(10616),	(11393),	(12410),	(12453),	(15595),	(16414),	(16845),	(17184),	(17692),	(17809),	(18243),	(18473),	(18646),	(18693),	(20279),	(21078),	(21203),	(7885),	(8216),	(8972),	(10687),	(10756),	(10783),	(12407),	(12621),	(17026),	(17676),	(17916),	(19703),	(21018),	(21166),	(10643),	(10650),	(10682),	(10742),	(10759),	(10770),	(11119),	(11385),	(12395),	(12436),	(12456),	(12472),	(12722),	(13466),	(13731),	(17277),	(17648),	(19795),	(21102),	(21197),	(7719),	(8486),	(8953),	(10644),	(10667),	(10736),	(10764),	(11419),	(11599),	(12416),	(12418),	(12432),	(12486),	(12505),	(12618),	(12700),	(12709),	(12719),	(12748),	(13487),	(13707),	(14787),	(14985),	(17010),	(17675),	(17681),	(18126),	(18357),	(18429),	(18883),	(19409),	(19580),	(19698),	(20121),	(20895),	(21200),	(21387),	(10207),	(10645),	(10659),	(10981),	(12575),	(13554),	(14231),	(16318),	(17061),	(17807),	(19300),	(20706),	(21045),	(7778),	(10205),	(10401),	(10755),	(12007),	(12306),	(12406),	(12440),	(12633),	(12757),	(12956),	(13438),	(13765),	(14440),	(14651),	(16489),	(17646),	(18301),	(18368),	(19704),	(21178);

DECLARE @test_country Varchar(2) = 'UA'; 

DECLARE @version Varchar(10) = '1.0.5'; 


--DECLARE @action Varchar(10) = 'Received'; 
--DECLARE @action Varchar(10) = 'Spend';
--DECLARE @action Varchar(20) = 'Transaction'; 
DECLARE @action Varchar(20) = 'TransactionOffer'; 



WITH POOL_OF_USERS 
AS ( 
		SELECT U.[UserId] AS ID 
		FROM   [dbo].[Users] AS U 
			   LEFT JOIN @UsersTurorialStepSkipers AS UTSS 
					  ON U.[UserId] = UTSS.UserID
		WHERE 
			UTSS.UserID IS NULL 
			AND  [Created] BETWEEN @TwoWeeksBeforeEnd AND @ThreeDaysBeforeEnd 
		    AND [RegistrationCountry] != @test_country 
			AND [RegistrationClientVersion] >= @version
		    AND [LTV] > 0
	) 

SELECT [ActionSource] 
       , Sum([CrystalAmount])      AS Crystals 
       , Count(*)                  AS Transactions 
       , Count (DISTINCT [UserId]) AS Users 
FROM   [dbo].[CrystalMovement] AS CM 
       INNER JOIN POOL_OF_USERS AS POU 
               ON POU.ID = CM.[UserId] 
WHERE  [Action] = @action 
 AND [TimeStamp]  BETWEEN @TwoWeeksBeforeEnd AND @LastWeekLastDay 
GROUP  BY [ActionSource] 
ORDER  BY [ActionSource] 

--SELECT [UserId]               AS U_ID 
--       , Sum([CrystalAmount]) AS Crystals 
--       , Count(*)             AS Transactions 
--FROM   [dbo].[CrystalMovement] AS CM 
--       INNER JOIN POOL_OF_USERS AS POU 
--               ON POU.ID = CM.[UserId] 
--WHERE  [Action] = @action 
--GROUP  BY [UserId] 
--ORDER  BY Crystals DESC 