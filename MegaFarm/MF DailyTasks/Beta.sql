SELEcT Convert(date,timestamp) as 'День'
, COUNT(Distinct UserId) AS 'Кількість користувачів' -- в день
, COUNT(*) AS 'Кількість подій' -- вдень
, COUNT(*)  / COUNT(Distinct UserId) AS 'Подій На Користувача' -- в день
FROM dbo.[daily_tasks_day_end]
--WHERE status_id	 = 1
GROUP BY Convert(date,timestamp)
ORDER BY Convert(date,timestamp) -- DESC


-- Product move 
SELEcT DISTINCT TriggerId   
FROM EventsProductMovement
WHERE Action = 'received'
AND TriggerSourceId = 'leaderboard' --'daily_event' --
 
-- daily task day + time
 SELECT
Convert(date, [Timestamp])
,Convert(time(0),[Timestamp])
,status_id	
--,level_id	
--,current_population	
,active_day	
,task_Id	
,action_type	
,action_target
FROM dbo.[daily_tasks]
WHERE UserID = 13632 --172231 --154660--
ORDER BY TIMESTAMP 

-- side task on choosen day
SELECT DISTINCT Convert(date, TIMESTAMP)
FROM side_tasks
WHERE Convert(date, DATEADD(d,1,[Timestamp])) = '2020-01-26'
ORDER BY Convert(date, TIMESTAMP)


declare @periodstart nvarchar(20) = '2020-01-20 00:00:00';
declare @periodend nvarchar (20) = '2020-01-20 23:59:59';
DECLARE @Hours  INT = 24;

SELEcT active_day, action_type
, COunt(Distinct (UserId)) AS U
, COunt (*) AS T
FROM dbo.[daily_tasks]
WHERE status_id = 1
AND [Timestamp] BETWEEN @periodstart AND @periodend
AND Platform = 1
AND [LTV] > 0
GROUP BY  active_day, action_type
ORDER BY active_day, T DESC