;WITH HealthPoint 
AS ( 
SELEcT UserID
, WorldId
, RoomNumber
, RunNumber
, HealthPoints AS Health
, LEAD (HealthPoints) OVER (PARTITION  BY UserID, WorldId, RunNumber ORDER BY RoomNumber) AS HealthAfter
, Healing
FROM RunRoomInfo

)
,  HealthScore 
AS ( 
    SELEcT UserID
, WorldId
, RoomNumber
, RunNumber
,Healing
, Health - HealthAfter - Healing AS HPLoss
, Health - HealthAfter   AS HealAndHPLoss
FROM HealthPoint
WHERE HealthAfter < 5000  AND Health < 5000 
AND HealthAfter IS NOT NULL
AND RoomNumber != 0
)

SELECT TOP (100) * 
FROM HealthScore
ORDER BY HealAndHPLoss DESC