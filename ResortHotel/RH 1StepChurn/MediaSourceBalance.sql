-- SELEcT [MediaSource]
-- , Count(*) AS users
--, SUM (LTV) AS Total_LTV
-- FROM PRP_Pool_Of_Users_Android 
-- --FROM PRP_Pool_Of_Deviators_Android
-- GROUP BY MediaSource
-- HAVING Count(*) > 10
-- ORDER BY MediaSource

--------------------
SELEcT
U.[MediaSource]
,U.[users]
,U0.[Users] AS '0-500'
,U500.[Users] AS '500-1000'
,U1000.[Users] AS '1000-1500'
,U1500.[Users] AS '1500-2000'
,U2000.[Users] AS '2000-2500'
,U2500.[Users] AS '2500+'
,U.[Total_LTV]
,U0.[Total_LTV] AS '0-500'
,U500.[Total_LTV] AS '500-1000'
,U1000.[Total_LTV] AS '1000-1500'
,U1500.[Total_LTV] AS '1500-2000'
,U2000.[Total_LTV] AS '2000-2500'
,U2500.[Total_LTV] AS '2500+'
FROM (
    SELEcT [MediaSource]
    , Count(*) AS users
, SUM (LTV) AS Total_LTV
    FROM PRP_Pool_Of_Users_Android 
    GROUP BY MediaSource
 ) AS U 
 LEFT JOIN 
 (
    SELEcT [MediaSource]
    , Count(*) AS users
, SUM (LTV) AS Total_LTV
    FROM PRP_Pool_Of_Users_Android 
    WHERE [RamSize] BETWEEN 0 AND 500
    GROUP BY MediaSource
 ) AS U0 ON U.[MediaSource] = U0.[MediaSource]  OR (U.[MediaSource] IS NULL AND U0.[MediaSource] IS NULL )

 LEFT JOIN 
 (
    SELEcT [MediaSource]
    , Count(*) AS users
, SUM (LTV) AS Total_LTV
    FROM PRP_Pool_Of_Users_Android 
    WHERE [RamSize] BETWEEN 500 AND 500 + 500
    GROUP BY MediaSource
 ) AS U500 ON U.[MediaSource] = U500.[MediaSource] OR (U.[MediaSource] IS NULL AND U500.[MediaSource] IS NULL )
 LEFT JOIN 
 (
    SELEcT [MediaSource]
    , Count(*) AS users
, SUM (LTV) AS Total_LTV
    FROM PRP_Pool_Of_Users_Android 
    WHERE [RamSize] BETWEEN 1000 AND 1000 + 500
    GROUP BY MediaSource
 ) AS U1000 ON U.[MediaSource] = U1000.[MediaSource]  OR (U.[MediaSource] IS NULL AND U1000.[MediaSource] IS NULL )

  LEFT JOIN 
 (
    SELEcT [MediaSource]
    , Count(*) AS users
, SUM (LTV) AS Total_LTV
    FROM PRP_Pool_Of_Users_Android 
    WHERE [RamSize] BETWEEN 1500 AND 1500 + 500
    GROUP BY MediaSource
 ) AS U1500 ON U.[MediaSource] = U1500.[MediaSource]  OR (U.[MediaSource] IS NULL AND U1500.[MediaSource] IS NULL )
 LEFT JOIN 
 (
    SELEcT [MediaSource]
    , Count(*) AS users
, SUM (LTV) AS Total_LTV
    FROM PRP_Pool_Of_Users_Android 
    WHERE [RamSize] BETWEEN 2000 AND 2000 + 500
    GROUP BY MediaSource
 ) AS U2000 ON U.[MediaSource] = U2000.[MediaSource] OR (U.[MediaSource] IS NULL AND U2000.[MediaSource] IS NULL )

 LEFT JOIN 
 (
    SELEcT [MediaSource]
    , Count(*) AS users
, SUM (LTV) AS Total_LTV
    FROM PRP_Pool_Of_Users_Android 
    WHERE [RamSize] > 2500 
    GROUP BY MediaSource
 ) AS U2500 ON U.[MediaSource] = U2500.[MediaSource] OR (U.[MediaSource] IS NULL AND U2500.[MediaSource] IS NULL )

 ORDER BY users desc