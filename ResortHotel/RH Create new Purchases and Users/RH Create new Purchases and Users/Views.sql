
SELECT DBO.EventsPurchase.* 
       ,DBO.EventsPurchase.AmountInUSD * DBO.PRP_Sales_Coefficients.Coeff AS   AmountInUSDCoeff 
FROM   DBO.PRP_Sales_Coefficients 
       RIGHT JOIN DBO.EventsPurchase 
               ON DBO.PRP_Sales_Coefficients.Amount = DBO.EventsPurchase.Amount 
                  AND DBO.PRP_Sales_Coefficients.Country = 
                      DBO.EventsPurchase.Country
				   

-------------------------------------------------------
-- EventsPurchaseCoeff
-------------------------------------------------------
SELECT Iif(	DBO.EventsPurchase.Currency = 'USD', 
	         	Iif( 
			         DBO.EventsPurchase.Timestamp >= DBO.PRP_Sales_Coefficients.Timestamp, 
			         DBO.EventsPurchase.AmountInUSD * DBO.PRP_Sales_Coefficients.Coeff, 
			         DBO.EventsPurchase.AmountInUSD
	         		), 
	         	DBO.EventsPurchase.AmountInUSD) AS  AmountInUSDCoeff 
				, DBO.EventsPurchase.* 
FROM   DBO.PRP_Sales_Coefficients 
       RIGHT JOIN DBO.EventsPurchase 
               ON DBO.PRP_Sales_Coefficients.Amount = DBO.EventsPurchase.Amount 
                  AND DBO.PRP_Sales_Coefficients.Country = 
                      DBO.EventsPurchase.Country 
				  AND DBO.PRP_Sales_Coefficients.Platform = 
                      DBO.EventsPurchase.Platform 
---------------------------------------------------------