 DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform int,
        @projectName NVARCHAR(20)

SET @startperiod = '2020-01-07 00:00:00.000';
SET @endperiod = '2020-01-07 23:59:59.997';
SET @projectName = 'Hunter'
--SET @platform = 2;

WITH PooOfUsers
AS 
(
    SELECT UserId  AS U_ID
    , InstallTime
    , af.AdvertisingId
    , af.MediaSource
    , af.Campaign
    , af.Platform
   -- , RegistrationClientVersion
    FROM [dbo].[AFEventsInstall] as af INNER JOIN dbo.Users as u 
        ON u.AdvertisingId = af.AdvertisingId AND u.MediaSource = af.MediaSource  AND u.Campaign = af.Campaign 
    WHERE projectname = @projectName
    AND InstallTime BETWEEN @startperiod  AND @endperiod
)
--, TT AS (
SELEcT AdvertisingId
, MediaSource
, Campaign
, Convert(date, InstallTime) AS IntsalledDate
, PaymentsCount
, AmountInUsd * 0.7 as 'Net' 
--, CONVERT(date, [Timestamp]) as 'PurchseDate'
, DATEDIFF(mi, InstallTime, Timestamp ) / 1440 AS 'DaySinceInstall'
, ClientVersion
, RegistrationClientVersion
, Platform
FROM EventsPurchase INNER JOIN PooOfUsers ON U_ID = UserId
ORDER BY IntsalledDate 
--)




-- SELECT DaySinceInstall
-- , Count(*) AS T
-- FROM TT 
-- WHERE PaymentsCount = 11
-- GROUP BY DaySinceInstall
-- ORDER BY DaySinceInstall