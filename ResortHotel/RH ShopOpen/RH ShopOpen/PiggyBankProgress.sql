--DECLARE @start_period DATETIME = '2019-03-09 00:00:00.000'; 
--DECLARE @end_period DATETIME = '2019-04-19 00:00:00.000'; 

DECLARE @start_period DATETIME = '2019-04-19 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-05-09 23:59:59.997'; 

--DECLARE @type Varchar(30) = 'craig_safe_1';
DECLARE @type Varchar(30) = 'craig_safe_2';
--DECLARE @type Varchar(30) = 'craig_safe_3';
--DECLARE @type Varchar(30) = 'craig_safe_4';

SELECT [progress]
 ,Count (DISTINCT [UserId]) AS Users 
FROM   [dbo].[ev_PiggyBankPurchase] AS PB
		INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS_20_PLUS_LVL]  AS AU
			ON AU.U_ID = PB.[UserId]
WHERE [TimeStamp] BETWEEN @start_period AND @end_period
AND [ClientVersion] >= '1.9.0'
AND [Result] = 'OK'
AND [type] = @type
GROUP BY [progress]
ORDER BY [progress]

SELECT TOP (100)* 
FROM   [dbo].[ev_PiggyBankPurchase]
WHERE [progress] = 525
AND [ClientVersion] >= '1.9.0' 
ORDER BY [Timestamp] 