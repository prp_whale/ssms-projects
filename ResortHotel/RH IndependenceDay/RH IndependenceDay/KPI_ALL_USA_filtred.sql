DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform varchar(4)
        --@regcountry varchar(4)
--        

SET @startperiod = '2019-06-17 00:00:00.000' -- ���� (START)
SET @endperiod = '2019-07-10 23:59:59.999' -- ���� (END)
set @platform = 1 -- 2 -- 3 --
--set @regcountry =  '%'--'us'--'gb' --'de' --'fr' --'us'

SELECT t1.Day as 'Day',
       t1.Month,
       t1.Year,
       t2.[DAU],
	   t1.[Number of Users],
	   t1.[AVGDuration],
	   t2.[AvgSessionsPerUser],
-- --      COUNT(t1.[UID]) as 'Number of Users',
----       (AVG(t1.[Dur])+0.0)/60 as 'AVGDuration',
       t10.NewUsers,
       (t10.RetentionD1+0.0) / t10.NewUsers * 100 as 'Retention D1',
       (t10.RetentionD3+0.0) / t10.NewUsers * 100 as 'Retention D3',
       (t10.RetentionD7+0.0) / t10.NewUsers * 100 as 'Retention D7',
       (t10.RetentionD14+0.0) / t10.NewUsers * 100 as 'Retention D14',
       (t10.RetentionD28+0.0) / t10.NewUsers * 100 as 'Retention D28',
	   t9.trans 'Trans',
	   t9.IAP+0.0 'IAP',
	   t9.PAYingUsers 'PAYingUsers',
	   t9.[Total New Paying Users],
	   t10.[Conversion 1d] 'Conversion 1d',
	   t10.[Conversion 2d] 'Conversion 2d',
	   t10.[Conversion 7d] 'Conversion 7d',
	   t10.[Conversion 14d] 'Conversion 14d',
	   t10.[Conversion 28d] 'Conversion 28d'

FROM    
(
      SELECT --1.0*count(*)/COUNT(DISTINCT UserId) as 'Sessions per day(AVG)',
			 COUNT(distinct userid) as 'Number of Users',
             1.0*(cast((sum(duration)+0.0) as bigint))/(60*COUNT(distinct userid)) as 'AVGDuration',
             DATEPART(dd, Timestamp) as 'Day',
             DATEPART(mm, Timestamp) as 'Month',
             DATEPART(yy, Timestamp) as 'Year'
      FROM EventsSessionFinished TT 
			   INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_USA_Users] AS POU 
					   ON TT.[UserId] = POU.U_ID 
      WHERE Platform = @platform
      AND Timestamp BETWEEN @startperiod AND @endperiod
	  and duration<2000000
      --and Country = @regcountry
      GROUP BY DATEPART(dd, Timestamp), DATEPART(mm, Timestamp), DATEPART(yy, Timestamp)
    ) t1
LEFT JOIN
        (
		SELECT
			 count(*)/(count(iif (firstloginthisday=1,1,null))+0.01) [AvgSessionsPerUser], 
			 COUNT(DISTINCT UserId) as 'DAU',
             DATEPART(dd, Timestamp) as 'Day',
             DATEPART(mm, Timestamp) as 'Month',
             DATEPART(yy, Timestamp) as 'Year'
            FROM EventsAppOpen TT 
			   INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_USA_Users] AS POU 
					   ON TT.[UserId] = POU.U_ID 
            WHERE Timestamp BETWEEN @startperiod AND @endperiod
            AND Platform = @platform
            --and Country = @regcountry
            GROUP BY DATEPART(dd, Timestamp), DATEPART(mm, Timestamp), DATEPART(yy, Timestamp)
            ) t2
ON (t1.Year = t2.Year AND t1.Month = t2.Month AND t1.Day = t2.Day)

LEFT JOIN
        (
			SELECT 
             DATEPART(dd, Timestamp) as 'Day',
             DATEPART(mm, Timestamp) as 'Month',
             DATEPART(yy, Timestamp) as 'Year',
			 COUNT(UserId) as 'Trans',
			 SUM(AmountInUSD) as 'IAP',
             COUNT(DISTINCT UserId) as 'PAYingUsers',
			 sum(cast([FirstDeposit] as int)) as 'Total New Paying Users'
		FROM EventsPurchase TT 
			   INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_USA_Users] AS POU 
					   ON TT.[UserId] = POU.U_ID 
	   WHERE Platform = @platform
	     AND Timestamp BETWEEN @startperiod AND @endperiod
		 --and Country = @regcountry
		GROUP BY DATEPART(dd, Timestamp), DATEPART(mm, Timestamp), DATEPART(yy, Timestamp)
            ) t9
ON (t1.Year = t9.Year AND t1.Month = t9.Month AND t1.Day = t9.Day)

LEFT JOIN
        (
			SELECT 
			 
             DATEPART(dd, created) as 'Day',
             DATEPART(mm, created) as 'Month',
             DATEPART(yy, created) as 'Year',
			 COUNT(DISTINCT UserId) as 'NewUsers',
			 sum(cast(Retention1D as int)) as 'RetentionD1',
			 sum(cast(Retention3D as int)) as 'RetentionD3',
			 sum(cast(Retention7D as int)) as 'RetentionD7',
			 sum(cast(Retention14D as int)) as 'RetentionD14',
			 sum(cast(Retention28D as int)) as 'RetentionD28',
			 count(iif(ltv1d>0,1,null))*1.0/count(*) [Conversion 1d],
			 count(iif(ltv2d>0,1,null))*1.0/count(*) [Conversion 2d],
			 count(iif(ltv7d>0,1,null))*1.0/count(*) [Conversion 7d],
			 count(iif(ltv14d>0,1,null))*1.0/count(*) [Conversion 14d],
			 count(iif(ltv28d>0,1,null))*1.0/count(*) [Conversion 28d]
		FROM users TT 
			   INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_USA_Users] AS POU 
					   ON TT.[UserId] = POU.U_ID 
	   WHERE Platform = @platform
	     AND created BETWEEN @startperiod AND @endperiod
		 --and [RegistrationCountry] = @regcountry
		GROUP BY DATEPART(dd, created), DATEPART(mm, created), DATEPART(yy, created)
--		ORDER BY Year, Month, Day
             )t10
ON (t1.Year = t10.Year AND t1.Month = t10.Month AND t1.Day = t10.Day)
ORDER BY t1.Year , t1.Month , t1.Day 