DECLARE @start_period DATETIME = '2019-06-01 00:00:00.000';
DECLARE @end_period DATETIME = '2019-06-14 23:59:59.997';


SET @start_period  = '2019-06-15 00:00:00.000';
SET @end_period  = '2019-06-28 23:59:59.997';

SET @start_period  = '2019-07-01 00:00:00.000';
SET @end_period   = '2019-07-14 23:59:59.997';

DECLARE @bottom_limit INT = 200;


WITH MEDIA_COUNTRY
AS ( 
    SELECT [MediaSource] 
            ,[RegistrationCountry] 
            ,Count(DISTINCT [UserId]) AS Users 
        FROM   DBO.[Users] 
        WHERE  [Created] BETWEEN @start_period AND @end_period 
        AND  [Platform] = 1
        GROUP  BY [MediaSource] 
                ,[RegistrationCountry] 
        HAVING Count(DISTINCT [UserId]) > @bottom_limit
)
, POOL_OF_USERS
AS (
    SELECT [UserId]
         , U.[MediaSource] 
         , U.[RegistrationCountry] 
      FROM   DBO.[Users] AS U 
        INNER JOIN  MEDIA_COUNTRY AS MC
            ON (U.[MediaSource] = MC.[MediaSource] OR (U.[MediaSource]is NULL and MC.[MediaSource] is NULL) )
            AND U.[RegistrationCountry] = MC.[RegistrationCountry] 
     WHERE  [Created] BETWEEN @start_period AND @end_period 
)



SELECT [MediaSource] 
            ,[RegistrationCountry] 
            ,Count(DISTINCT [UserId]) AS Registration
            , (SELECT Sum(cast([Retention1D] as int)) 
                FROM   [dbo].[Users] AS U 
                    INNER JOIN POOL_OF_USERS AS POU 
                            ON U.[UserId] = POU.[UserId] 
                WHERE  ( POU.[MediaSource] = P.[MediaSource] 
                        OR ( POU.[MediaSource] IS NULL 
                            AND P.[MediaSource] IS NULL ) ) 
                AND POU.[RegistrationCountry] = P.[RegistrationCountry] 
                 ) AS 'Ret Day1'
            , (SELECT Sum(cast([Retention7D] as int)) 
                FROM   [dbo].[Users] AS U 
                    INNER JOIN POOL_OF_USERS AS POU 
                            ON U.[UserId] = POU.[UserId] 
                WHERE  ( POU.[MediaSource] = P.[MediaSource] 
                        OR ( POU.[MediaSource] IS NULL 
                            AND P.[MediaSource] IS NULL ) ) 
                AND POU.[RegistrationCountry] = P.[RegistrationCountry] 
                 ) AS 'Ret Day7'            
             , (SELECT Sum(cast([Retention14D] as int)) 
                FROM   [dbo].[Users] AS U 
                    INNER JOIN POOL_OF_USERS AS POU 
                            ON U.[UserId] = POU.[UserId] 
                WHERE  ( POU.[MediaSource] = P.[MediaSource] 
                        OR ( POU.[MediaSource] IS NULL 
                            AND P.[MediaSource] IS NULL ) ) 
                AND POU.[RegistrationCountry] = P.[RegistrationCountry] 
                 ) AS 'Ret Day14'
             , (SELECT Sum(cast([Retention30D] as int)) 
                FROM   [dbo].[Users] AS U 
                    INNER JOIN POOL_OF_USERS AS POU 
                            ON U.[UserId] = POU.[UserId] 
                WHERE  ( POU.[MediaSource] = P.[MediaSource] 
                        OR ( POU.[MediaSource] IS NULL 
                            AND P.[MediaSource] IS NULL ) ) 
                AND POU.[RegistrationCountry] = P.[RegistrationCountry] 
                 ) AS 'Ret Day30'        
                 , (SELECT Count(DISTINCT U.[UserId])
                    FROM   [dbo].[Users] AS U 
                        INNER JOIN POOL_OF_USERS AS POU 
                                ON U.[UserId] = POU.[UserId] 
                    WHERE  ( POU.[MediaSource] = P.[MediaSource] 
                            OR ( POU.[MediaSource] IS NULL 
                                AND P.[MediaSource] IS NULL ) ) 
                    AND POU.[RegistrationCountry] = P.[RegistrationCountry] 
                    AND [LTV1D] > 0
                 ) AS 'C1 Day1'    
                , (SELECT Count(DISTINCT U.[UserId])
                    FROM   [dbo].[Users] AS U 
                        INNER JOIN POOL_OF_USERS AS POU 
                                ON U.[UserId] = POU.[UserId] 
                    WHERE  ( POU.[MediaSource] = P.[MediaSource] 
                            OR ( POU.[MediaSource] IS NULL 
                                AND P.[MediaSource] IS NULL ) ) 
                    AND POU.[RegistrationCountry] = P.[RegistrationCountry] 
                    AND [LTV7D] > 0
                 ) AS 'C1 Day7'  
                 , (SELECT Count(DISTINCT U.[UserId])
                    FROM   [dbo].[Users] AS U 
                        INNER JOIN POOL_OF_USERS AS POU 
                                ON U.[UserId] = POU.[UserId] 
                    WHERE  ( POU.[MediaSource] = P.[MediaSource] 
                            OR ( POU.[MediaSource] IS NULL 
                                AND P.[MediaSource] IS NULL ) ) 
                    AND POU.[RegistrationCountry] = P.[RegistrationCountry] 
                    AND [LTV14D] > 0
                 ) AS 'C1 Day14'            
                , (SELECT Count(DISTINCT U.[UserId])
                    FROM   [dbo].[Users] AS U 
                        INNER JOIN POOL_OF_USERS AS POU 
                                ON U.[UserId] = POU.[UserId] 
                    WHERE  ( POU.[MediaSource] = P.[MediaSource] 
                            OR ( POU.[MediaSource] IS NULL 
                                AND P.[MediaSource] IS NULL ) ) 
                    AND POU.[RegistrationCountry] = P.[RegistrationCountry] 
                    AND [LTV30D] > 0
                 ) AS 'C1 Day30'               
        FROM  POOL_OF_USERS AS P
        GROUP BY [MediaSource] 
                ,[RegistrationCountry] 
        ORDER BY [MediaSource] 
                ,[RegistrationCountry] 

        

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
