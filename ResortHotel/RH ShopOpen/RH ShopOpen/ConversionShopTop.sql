DECLARE @start_period DATETIME = '2019-03-09 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-05-09 23:59:59.997'; 


--SELECT [purchase_name] 
--       ,Count (DISTINCT [UserId]) AS Depositors 
--       ,Count (*)                 AS Amount 
--FROM   [dbo].[ev_ShopPurchase] AS PBP 
--       INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS] AS POAU 
--               ON POAU.U_ID = PBP.[UserId] 
--WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
--   AND [result] = 'OK' 
--   AND [new_depositor] = 1  
--GROUP  BY [purchase_name] 
--ORDER  BY Depositors  DESC



--SELECT [ProductId] 
--       ,Count (DISTINCT [UserId]) AS Depositors 
--       ,Count (*)                 AS Amount 
--FROM   [dbo].[EventsPurchase]  AS PBP 
--       INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS] AS POAU 
--               ON POAU.U_ID = PBP.[UserId] 
--WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
--   AND [PaymentsCount] = 1  
--GROUP  BY [ProductId] 
--ORDER  BY Depositors  DESC

SELECT Count (DISTINCT [UserId]) AS Depositors 
       ,Count (*)                 AS Amount 
FROM   [dbo].[EventsPurchase]  AS PBP 
       INNER JOIN [dbo].[PRP_Pool_Of_ACTIVE_USERS] AS POAU 
               ON POAU.U_ID = PBP.[UserId] 
WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
   AND [PaymentsCount] = 1  

  SELECT TOP (10) * 
  FROM   [dbo].[EventsPurchase]
  WHERE [ProductId] = 'craig_safe_1'