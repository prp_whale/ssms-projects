DECLARE @st_per DATETIME = '2019-01-01 00:00:00.000';
DECLARE @end_per DATETIME =  '2019-12-31 23:59:59.997';

DECLARE @days_tail INT = 90;

SET @end_per  = DATEADD(ms, -3 ,  DATEDIFF(dd, @days_tail, GETDATE()) ) ; 


WITH PoU
AS (
    SELECT DISTINCT UserId AS U_ID
    FROM dbo.Users 
    WHERE [Created] < = @end_per

)


, Purchasing 
AS (
    SELECT UserID
    , SUM([AmountInUsd]) AS [LTV]
    , DATEDIFF(d, [Created], MIN([Timestamp])) AS [ConversionDay]
    FROM [dbo].[EventsPurchase] JOIN  PoU ON UserID = U_ID
    WHERE [Timestamp] BETWEEN [Created] AND DATEADD(d, @days_tail, [Created] )
    GROUP BY UserID, [Created]

)
, Segments
AS (
    SELECT 
    CASE 
        WHEN LTV between 0.0001 and 7 THEN  '0 minnows'
        WHEN LTV between 7.0001 and 70 THEN  '1 dolphins'
        WHEN LTV between 70.0001 and 300 THEN  '2 grand dolphins'
        WHEN LTV > 300 THEN  '3 whales'
        ELSE 'err'
    END AS Segment
    , ConversionDay
    FROM Purchasing
)


SELECT Segment , ConversionDay
, Count(*) AS Users
FROM Segments
GROUP BY Segment , ConversionDay
ORDER BY Segment , ConversionDay


-- SELECT Segment
-- , AVG(ConversionDay)
-- FROM Segments
-- GROUP BY Segment

--SELECT TOP (100) * FROM Segments