declare @days int = 28;

declare @days_multi int = 1;
declare @st_per DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days*@days_multi, GETDATE())));
declare @end_per DATETIME  = DATEADD(ms, -3, CONVERT(datetime, CONVERT(date, GETDATE())));

declare @versa_limit int = 10;

WITH Versa
AS (
    SELECT registrationclientversion AS RegVer
    FROM  dbo.Users
   -- WHERE  created BETWEEN @st_per AND @end_per
    GROUP BY registrationclientversion
    HAVING Count(*) >=  @versa_limit
)

,  Lvl_Start as
( 
    
    SELECT UserId
    , LevelId AS lvl
    ,  CONVERT(date,Timestamp) as lvl_date
    , [Timestamp]
    ,  FIRST_VALUE (LevelId) OVER (PARTITION BY UserId, CONVERT(date,Timestamp) ORDER BY Timestamp DESC) as last_lvl
    , AddSteps
    , [ClientVersion]
    , CASE   WHEN IsDepositor > 0 THEN 'Yes'  ELSE 'No'  END AS 'IsPay'
    FROM EventsLevelAttempts
    WHERE [Timestamp] BETWEEN @st_per AND @end_per
)

, Lvl_Complete 
as (
    SELECT UserId AS U_ID
    , LevelId AS lvl_c
    ,  [Timestamp]
   -- ,  FIRST_VALUE (LevelId) OVER (PARTITION BY UserId, CONVERT(date,Timestamp) ORDER BY Timestamp DESC) as last_lvl_c
    FROM EventsLevelAttempts
    WHERE [Timestamp] BETWEEN @st_per AND @end_per
    AND Completed = '1'
)


,AppOpen AS (
    SELECT UserId AS U_ID
    , CONVERT(date,Timestamp) as first_date
    , LEAD (CONVERT(date,Timestamp),1,CONVERT(date,getdate())) OVER (PARTITION BY UserId ORDER BY CONVERT(date,Timestamp) ASC)  as next_date
    FROM EventsAppOpen
        WHERE [Timestamp] BETWEEN @st_per AND @end_per
        AND [ClientVersion] IN (SELECT * FROM Versa)
    GROUP BY UserId, CONVERT(date,Timestamp)
)

SELECT lvl_date   , ClientVersion , [IsPay] , lvl

, COUNT(Distinct UserId) AS Users
, COUNT(Distinct (iif (datediff(dd, lvl_date, next_date)>1, UserID, null))) AS Churn_1D
, COUNT(Distinct (iif (datediff(dd, lvl_date, next_date)>3, UserID, null))) AS Churn_3D
, COUNT(Distinct (iif (datediff(dd, lvl_date, next_date)>7, UserID, null))) AS Churn_7D
, COUNT(Distinct (iif (datediff(dd, lvl_date, next_date)>14, UserID, null))) AS Churn_14D
, COUNT(Distinct (iif (datediff(dd, lvl_date, next_date)>28, UserID, null))) AS Churn_28D
, min([AddSteps] ) AS MinSkips
, avg([AddSteps] ) AS AvGSkips
, max([AddSteps] ) AS MaxSkips

FROM Lvl_Start AS LS LEFT JOIN Lvl_Complete LC  ON LC.U_ID = LS.UserId AND LC.lvl_c = LS.lvl AND LC.[Timestamp] = LS.[Timestamp]
LEFT JOIN AppOpen AS AO on LS.UserID = AO.U_ID AND LS.lvl_date = AO.[first_date] AND LS.lvl  = LS.last_lvl 
GROUP BY lvl_date , ClientVersion , [IsPay] , lvl
ORDER BY lvl_date   , ClientVersion , [IsPay] , Convert(int, lvl)