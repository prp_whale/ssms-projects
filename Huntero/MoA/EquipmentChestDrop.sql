DECLARE @chestType NVARCHAR(40) = 'Special'
--SET @chestType = 'Legendary'

;WITH POOL_OF_USERS
AS (
 
    SELECT UserId AS U_ID
    --SELECT TOP (100000) UserId AS U_ID
    FROM dbo.Users
    WHERE [RegistrationClientVersion] IN ('0.0.105', '0.0.108', '0.0.124','0.0.177', '0.0.189')
  --  AND Platform = 2
    AND Created < '2020-02-16'
),
    Chest10Opens
    AS (
        SELEcT  UserId AS U_ID
        , Count(*) AS 'Opens'
        FROM EquipmentMovement 
            AS EM INNER JOIN POOL_OF_USERS ON U_ID = UserId
        WHERE TriggerSourceId = 'Chest'
        AND TriggerId = @chestType
        GROUP By  UserId
        HAVING Count(*) >= 10
    ),
    Proxy10Chest
    AS (
        SELECT USERID
        , [TIMESTAMP]
        , EquipmentValuable
          FROM EquipmentMovement 
            AS EM INNER JOIN Chest10Opens ON U_ID = UserId
        WHERE TriggerSourceId = 'Chest'
        AND TriggerId = @chestType
    ),
ChestTop10 
AS (
    SELECT  UserId
    , EquipmentValuable
    FROM (
        SELECT  UserId
        , EquipmentValuable
        ,ROW_NUMBER() over (Partition BY UserId ORDER BY [TIMESTAMP] ) AS RankOs
        FROM Proxy10Chest 
    ) rs WHERE RankOs <= 10
),

ChestTop10Structure 
AS (
    SELECT DISTINCT UserId 
    , (SELECT Count(EquipmentValuable) FROM ChestTop10  AS C
      WHERE EquipmentValuable = 'Uncommon' AND c.UserId = cc.UserID ) AS 'Uncommon'
          , (SELECT Count(EquipmentValuable) FROM ChestTop10  AS C
      WHERE EquipmentValuable = 'Rare' AND c.UserId = cc.UserID ) AS 'Rare'
          , (SELECT Count(EquipmentValuable) FROM ChestTop10  AS C
      WHERE EquipmentValuable = 'Epic' AND c.UserId = cc.UserID ) AS 'Epic'
    FROM  ChestTop10 AS CC
)

SELECT Epic, Rare, Uncommon
, Count(*) AS Users
FROM ChestTop10Structure
GROUP BY Epic, Rare, Uncommon
ORDER BY Epic DESC, Rare DESC, Uncommon DESC





-- SELECT EquipmentValuable
-- , Count(DISTINCT UserId) AS Users
-- , Count(*) AS Actions
-- FROM ChestTop10
--  GROUP By  EquipmentValuable
--  ORDER BY EquipmentValuable




-- SELECT EquipmentValuable
-- , COUNT(*) AS Opens
-- ORDER BY UserId

 

-- SELECT EquipmentValuable
-- , Count(DISTINCT UserId) AS Users
-- , Count(*) AS Actions
-- FROM EquipmentMovement AS EM INNER JOIN Chest10Opens ON U_ID = UserId
-- WHERE TriggerSourceId = 'Chest'
-- AND TriggerId = @chestType
-- GROUP By  EquipmentValuable
-- ORDER BY EquipmentValuable





-- SELECT DISTINCT  TriggerID
-- FROM EquipmentMovement
-- WHERE TriggerSourceId = 'Chest'

