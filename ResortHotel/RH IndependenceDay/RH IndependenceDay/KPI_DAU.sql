DECLARE @start_period DATETIME = '2019-07-04 00:00:00.000';  -- INDEPENDESE EVENT
DECLARE @end_period DATETIME = '2019-07-08 23:59:59.997'; 

DECLARE @platform INT = 2;
DECLARE @country VARCHAR(2) = 'US'


SET @start_period  =  DATEADD (WW, -2, @start_period); 
SET @end_period =  DATEADD (WW, -2, @end_period); 

--SELEcT @start_period, @end_period



SELECT t.[DATE] 
, t.USERS AS DAU
, t2.[DEPOSITORS]
, t2.[Transactions]
, t2.[IAP]
FROM (
		SELECT Cast([Timestamp] AS DATE) AS 'DATE' 
			   ,Count(DISTINCT [UserId]) AS USERS 
		FROM   [dbo].[EventsAppOpen] AS TT 
			   INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
					   ON TT.[UserId] = POU.U_ID 
		WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
		   AND [Platform] = @platform 
		GROUP  BY Cast([Timestamp] AS DATE) 
 ) AS T 
INNER JOIN ( 
		SELECT Cast([Timestamp] AS DATE) AS 'DATE' 
			   ,Count(DISTINCT [UserId]) AS DEPOSITORS
			   ,COUNT(*) as 'Transactions'
			   , SUM(AmountInUSD) as 'IAP'
		FROM   [dbo].[EventsPurchase] AS AO 
			   INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
					   ON AO.[UserId] = POU.U_ID 
		WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
		   AND [Platform] = @platform
		GROUP  BY Cast([Timestamp] AS DATE)
) AS T2 
ON T.[DATE] = T2.[DATE] 


ORDER  BY 'DATE' 