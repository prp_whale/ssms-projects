--до какого уровня в среднем первой главы игроки доходят с первой попытки. Со второй попытки. С третьей попытки и так до 10-й. По каждой главе.

declare @days int = 28

declare @st_per DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days, GETDATE())));
declare @end_per DATETIME  = DATEADD(ms, -3, CONVERT(datetime, CONVERT(date, GETDATE())));

declare @run_limit int = 20;
--declare @end_created DATETIME  = DATEADD(D, -1, @end_per);

WITH Versa
AS (
    SELECT registrationclientversion AS RegVer
    FROM  dbo.Users
    WHERE  created BETWEEN @st_per AND @end_per
    AND  created > '2020-04-01 00:00:00.000'
    GROUP BY registrationclientversion
    HAVING Count(*) > 1000
)

, PoU 
AS(
    SELECT UserId AS U_ID
    , CASE 
        WHEN GameUserID % 2 = 0 THEN 'A'
        WHEN GameUserID % 2 = 1 THEN 'B'
        ELSE 'ERROR' END AS 'Group'
    , CASE 
        WHEN ltv = 0 THEN 'ND'
        ELSE 'Depos' END AS 'IsPay'
    , registrationclientversion AS CreatedVersion
    FROM   dbo.Users 
    WHERE registrationclientversion IN ( SELECT * FROM Versa ) 
        AND created BETWEEN @st_per AND @end_per
        AND  created > '2020-04-01 00:00:00.000'
)

,SR -- start run 
AS (
    SELECT UserId, [Group] , [IsPay] , [CreatedVersion]
        , LocationId AS WorldId
        , RunNumber
        , [Timestamp]  AS katka_start
    FROM dbo.[StartRun] INNER JOIN POU ON UserId = U_ID
    AND [Timestamp] BETWEEN @st_per AND @end_per 
    AND  [Timestamp] > '2020-04-01 00:00:00.000'
    AND healthpoint < 9999 and attackpoint < 9999 
    AND Runnumber <= @run_limit
)

, FR -- finish run 
AS (
    SELECT FR.UserID , [Group] , [IsPay] , [CreatedVersion]
        , FR.WorldId 
        , FR.RunNumber
        , FR.RoomNumber AS end_room
        , [Timestamp] aS katka_end
    FROM dbo.[Finishrun] AS FR INNER JOIN SR ON FR.UserId = SR.UserId AND FR.WorldId = SR.WorldId AND FR.Runnumber = SR.Runnumber
    AND  [Timestamp] BETWEEN katka_start AND @end_per
    and IsLeft = 0 and   roomnumber!=0 and roomname!='-2' and runlevel<=11
    and FR.RoomNumber != '-1'
    AND FR.Runnumber <= @run_limit
) 
, FS  -- first succses 
AS (
    SELECT FR.UserID AS U_ID , [Group] AS _G  , [IsPay]  AS _IP , [CreatedVersion] AS _CV
        , FR.WorldId AS W_ID
        , FR.RunNumber AS R_N
        , [Timestamp] aS katka_end
    FROM dbo.[Finishrun] AS FR INNER JOIN SR ON FR.UserId = SR.UserId AND FR.WorldId = SR.WorldId AND FR.Runnumber = SR.Runnumber
    AND  [Timestamp] BETWEEN katka_start AND @end_per
    and IsLeft = 0 and   roomnumber!=0 and roomname!='-2' and runlevel<=11
    and FR.RoomNumber = '-1'
    AND FR.Runnumber <= @run_limit
)

SELECT  WorldId, RunNumber, [IsPay] , [Group]  , [CreatedVersion]

,  COUNT(DISTINCT UserId) AS 'Start Users'
,  AVG(end_room)         AS 'Avg end level'
,  COUNT(DISTINCT U_ID) AS 'Finishers'
FROM FR LEFT JOIN FS ON FR.RunNumber = FS.R_N AND FR.WorldId = FS.W_ID AND [IsPay] = _IP AND  [Group] = _G AND   [CreatedVersion] = _CV
GROUP BY WorldId, RunNumber, [IsPay] , [Group]  , [CreatedVersion]
ORDER BY WorldId, RunNumber, [IsPay] DESC, [Group], [CreatedVersion]







-- SELECT  WorldId, RunNumber, [IsPay] , [Group]  , [CreatedVersion]
-- ,  COUNT(DISTINCT UserId) AS 'Start Users'
-- , AVG(end_room)         AS 'Avg end level'
-- , (SELECT  COUNT(DISTINCT UserId) FROM FS WHERE  FS.IsPay = T.IsPay AND FS.[Group] = T.[Group] AND FS.WorldId = T.WorldId AND FS.Runnumber = T.Runnumber) AS 'Finishers'
-- FROM FR AS T
-- WHERE RunNumber BETWEEN 1 AND 20
-- GROUP BY WorldId, RunNumber, [IsPay] , [Group]  , [CreatedVersion]
-- ORDER BY WorldId, RunNumber, [IsPay] , [Group]  , [CreatedVersion]