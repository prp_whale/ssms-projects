DECLARE @start_period DATETIME = '2019-03-09 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-05-09 23:59:59.997'; 

DROP TABLE IF EXISTS PRP_SECOND_PURCHASERS;

SELECT EP.[UserId]
, EP.FIRST_PURCHASE  
, EP2.[ProductId] As SECOND_PURCHASE
INTO PRP_SECOND_PURCHASERS
FROM   [dbo].[PRP_FIRST_PURCHASERS]  AS EP 
       LEFT JOIN  [dbo].[EventsPurchase] AS EP2 
              ON EP.[UserId] = EP2.[UserId]
			  AND  EP2.[PaymentsCount] = 2
--WHERE --[TimeStamp] BETWEEN @start_period AND @end_period



SELECT Count (DISTINCT [UserId]) AS Users 
       ,Count (*)            AS Amount 
FROM   [dbo].[PRP_SECOND_PURCHASERS] 
WHERE SECOND_PURCHASE IS NULL

SELECT Count (DISTINCT [UserId]) AS Users 
       ,Count (*)            AS Amount 
FROM   [dbo].[PRP_FIRST_PURCHASERS]

--SELECT TOP (10) *  FROM  PRP_SECOND_PURCHASERS