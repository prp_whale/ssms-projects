declare @periodstart datetime = '2020-01-29 00:00:00';
declare @periodend datetime = '2020-01-29 23:59:59';
DECLARE @Hours  INT = 24;


SELECT Count(*) AS W
, Count(Distinct U.UserId) AS WU 
FROM  VideoAdStatus as V INNER JOIN dbo.Users AS U 
    ON V.UserId = U.UserId
WHERE U.Created BETWEEN @periodstart AND @periodend
AND V.[Timestamp] BETWEEN U.Created AND  dateadd(hh,@Hours,U.created)
AND AdStatus = 1 

-- SELECT Count(*) AS W
-- , Count(Distinct UserId) AS WU 
-- FROM  VideoAdStatus
-- WHERE Timestamp BETWEEN @periodstart AND @periodend
-- AND AdStatus = 0  AND Platform = 1 