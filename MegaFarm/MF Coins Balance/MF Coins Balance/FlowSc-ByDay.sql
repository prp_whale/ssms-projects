declare @start_date datetime	= '2019-02-11 00:00:00';
declare @end_date datetime		= '2019-02-24 23:59:59';
declare @cheater int			= 7249;
declare @depositor int			= 0;

--declare @day date = '2019-02-11';
--declare @day date = '2019-02-12';
--declare @day date = '2019-02-13';
--declare @day date = '2019-02-14';
--declare @day date = '2019-02-15';
--declare @day date = '2019-02-16';
--declare @day date = '2019-02-17';
declare @day date = '2019-02-18';
--declare @day date = '2019-02-19';
--declare @day date = '2019-02-20';
--declare @day date = '2019-02-21';
--declare @day date = '2019-02-22';
--declare @day date = '2019-02-23';
--declare @day date = '2019-02-24';

SELECT [level] as lvl
	,	avg([coins]) as COIN
	FROM [dbo].[balance]
	 WHERE   [RegistrationCountry] != 'UA'
		    AND  [timestamp] between @start_date and @end_date
			AND [is_depositor] = @depositor
			AND [UserId] != @cheater
			AND cast ([timestamp] as date) = @day
			
	GROUP BY [level]
	ORDER BY lvl