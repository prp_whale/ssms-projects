--[dbo].[ev_Tutorial] 
SELECT UserId 
       ,Timestamp 
       ,ClientTime 
       ,BackOfficeTime 
       ,ClientVersion
	   ,[Order]
	 --  ,SessionId not exist in AnalyticsEvent?
       ,String1 AS step_key 
FROM   dbo.Events 
WHERE  ( EventId = 304 ) 


--[dbo].[ev_InAppPurchase] 
SELECT UserId 
       ,Timestamp 
       ,ClientTime 
       ,BackOfficeTime 
       ,ClientVersion
	   ,[Order]
	   ,Double1 AS Price
		,Int1 AS CaptainLevel
		,Int2 AS MainBuildingLevel_v2
		,String1 AS HeroLevel
		,String2 AS TransactionId
		,String3 AS TriggerSourceId
		,String4 AS TriggerId
		,String5 AS ProductId
		,String6 AS MainBuildingLevel
FROM   dbo.Events 
WHERE  ( EventId = 217 ) 

--[dbo].[ev_InAppPurchase] 
SELECT UserId 
       ,Timestamp 
       ,ClientTime 
       ,BackOfficeTime 
       ,ClientVersion
	   ,[Order]
	   ,Double1 AS Price
		,Int1 AS CaptainLevel
		,Int2 AS MainBuildingLevel_v2
		,String1 AS HeroLevel
		,String2 AS TransactionId
		,String3 AS TriggerSourceId
		,String4 AS TriggerId
		,String5 AS ProductId
		,String6 AS MainBuildingLevel
FROM   dbo.Events 
WHERE  ( EventId = 26) 

--[dbo].[ev_MainResourcesBalance] 
SELECT UserId 
       ,Timestamp 
       ,ClientTime 
       ,BackOfficeTime 
       ,ClientVersion
	   ,[Order]
	  ,Int1 AS WoodBalance
		,Int2 AS IsDepositorClient
		,Int3 AS StoneBalance
		,Int4 AS MainBuildingLevel
		,Int5 AS WoodProduction
		,Int6 AS FoodConsumption
		,Int7 AS IronProduction
		,Int8 AS StoneProduction
		,Int9 AS GoldProduction
		,Int10 AS FoodProduction
		,Int11 AS IronBalance
		,Int12 AS GoldBalance
		,Int13 AS FoodBalance
		,Int14 AS CaptainLevel
		,Int15 AS AllianceId
		,Int16 AS Kingdom
		,Int17 AS KingdomId
		,String1 AS BalanceVersion
		,String2 AS FoodBonus
		,String3 AS IronBonus
		,String4 AS GoldBonus
		,String5 AS WoodBonus
		,String6 AS StoneBonus
		,String7 AS Alliance
FROM   dbo.Events 
WHERE  ( EventId = 26) 


--[dbo].[ev_StorylineProgress] 
SELECT UserId 
       ,Timestamp 
       ,ClientTime 
       ,BackOfficeTime 
       ,ClientVersion
	   ,[Order]
		,Int1 AS IsDepositorClient
		,Int2 AS MainBuildingLevel
		,Int3 AS CaptainLevel
		,Int4 AS ActIdInt
		,Int5 AS AllianceId
		,Int6 AS Kingdom
		,Int7 AS QuestIdInt
		,Int8 AS KingdomId
		,String1 AS BalanceVersion
		,String2 AS QuestName
		,String3 AS Alliance
		,String4 AS ActId
		FROM   dbo.Events 
WHERE  ( EventId = 18) 