
--------------------------------------------------------------------------------------------------------------
DECLARE @act INT = 10;
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------
DECLARE @sql_0 VARCHAR (max);
DECLARE @sql_1 VARCHAR (max);
DECLARE @sql_2 VARCHAR (max);
DECLARE @sql_3 VARCHAR (max);
DECLARE @sql_4 VARCHAR (max);
--------------------------------------------------------------------------------------------------------------
SET @sql_0 = 'DROP TABLE IF EXISTS PRP_LifeTime_StoryLine_Act_'+Cast( @act AS varchar)+';'

SET @sql_1  = '
SELECT  U.[UserId] , 
                MAX(LTV)                            AS LTV , 
                DATEDIFF (ss, created, [LastLogin]) AS LT 
        INTO PRP_LifeTime_StoryLine_Act_'+Cast( @act AS varchar)+'
       FROM   dbo.[Users]  AS U  INNER JOIN  PRP_Pool_StoryLine_Leave_On_Act_'+Cast( @act AS varchar)+'  AS POU ON POU.[U_ID] = U.[UserId]
       GROUP BY U.[UserId] , 
                created, 
                [LastLogin] 
'
SET @sql_2 = 'SELECT COUNT(*) FROM PRP_LifeTime_StoryLine_Act_'+Cast( @act AS varchar)
SET @sql_3 = 'SELECT DISTINCT Percentile_cont(0.25) within GROUP (ORDER BY LT) OVER () AS Q_1
                ,  Percentile_cont(0.5) within GROUP (ORDER BY LT) OVER () AS Mediane
                ,  Percentile_cont(0.75) within GROUP (ORDER BY LT) OVER () AS Q_3 
                FROM PRP_LifeTime_StoryLine_Act_'+Cast( @act AS varchar)+' ;'
SET @sql_4 = 'SELECT L.Users as Leave
                , U.Users
                , L.Users*1.0/U.Users AS Churn
                , D.Users*1.0/U.Users AS PayingShare
                FROM 
                (SELECT Count(*) AS Users FROM PRP_LifeTime_StoryLine_Act_'+Cast( @act AS varchar)+' WHERE LT = 0) AS L
                , (SELECT Count(*) AS Users FROM PRP_LifeTime_StoryLine_Act_'+Cast( @act AS varchar)+' ) AS U
                , (SELECT Count(*) AS Users FROM PRP_LifeTime_StoryLine_Act_'+Cast( @act AS varchar)+' WHERE LTV > 0) AS D
                ;'
--------------------------------------------------------------------------------------------------------------
-- EXEC (@sql_0)
-- EXEC (@sql_1)
-- EXEC (@sql_2)
-- EXEC (@sql_3)
EXEC (@sql_4)
--------------------------------------------------------------------------------------------------------------

-- SELECT L.Users as Leave
-- , U.Users
-- , L.Users*1.0/U.Users AS Churn
-- FROM 
-- (SELECT Count(*) AS Users FROM PRP_LifeTime_StoryLine_Act_10 WHERE LT = 0) AS L
-- , (SELECT Count(*) AS Users FROM PRP_LifeTime_StoryLine_Act_10  ) AS U



-- SELECT DISTINCT Percentile_cont(0.25) within GROUP (ORDER BY LT) OVER () AS Q_1
-- ,  Percentile_cont(0.5) within GROUP (ORDER BY LT) OVER () AS Mediane
-- ,  Percentile_cont(0.75) within GROUP (ORDER BY LT) OVER () AS Q_3 
--  FROM PRP_LifeTime_StoryLine_Act_10 ;


-- SELECT  U_ID 
-- , DATEDIFF(ss, Min(Created) , Max ([Timestamp])) AS 'Lifetime'
-- INTO PRP_LifeTime_StoryLine_Act_10
-- FROM [dbo].[Events] AS E INNER JOIN  PRP_Pool_StoryLine_Leave_On_Act_10 AS POU ON POU.[U_ID] = E.[UserId]
-- GROUP BY U_ID

-- SELEcT   AVg (CAST(Lifetime AS BIGINT)) /3600
-- FROM PRP_LifeTime_StoryLine_Act_10

-- SELECT TOP (100) *
-- FROM dbo.Users
-- WHERE [LTV] > 500
-- ORder by LastLogin desc

-- select 	
--         distinct percentile_cont(0.5) within group (order by a.Lifetime) over ()/3600 as Mediane  --partition by
-- from PRP_LifeTime_StoryLine_Act_10 as A


-- SELECT 
-- DISTINCT Percentile_cont(0.5) within GROUP (ORDER BY LT) OVER () AS Mediane --partition by 
-- FROM PRP_LifeTime_StoryLine_Act_10 

-- SELEcT Avg(sq.LT) FROM PRP_LifeTime_StoryLine_Act_10 as sq 

-- SELECT  
-- (SELECT Count(*) FROM PRP_LifeTime_StoryLine_Act_10 WHERE LT = 0)*1.0 / Count(*) AS Churn
-- FROM PRP_LifeTime_StoryLine_Act_10 