SELECT TOP (100) U.*
FROM  dbo.[Users] AS U
     INNER JOIN  [dbo].[AFPartnersDailyReport] AS AF
        ON U.[MediaSource] = AF.[MediaSource] AND U.[Channel] = AF.[Channel] AND  U.[Campaign] =  AF.[Campaign]  AND U.[AdSet] =  AF.[AdSet] AND U.[Ad] =  AF.[aD] --AND U.[AdId] =  AF.[AfAdsetId]
        --AND U.[MediaSource] = 'Facebook Ads'
        AND  AF.[projectname] = 'Pirates' 
    --     SELECT MediaSource,  Channel, Campaign
    --     ,Count(DISTINCT UserId) AS U 
    --    , Count (*) AS A
    --    FROM  dbo.[Users] AS U
    --    GROUP BY MediaSource,  Channel, Campaign
    --     ORDER BY U DESC

        -- SELECT DISTINCT AdId 
        --     FROM  dbo.[Users] AS U
        --     WHERE  MediaSource = 'Facebook Ads' AND Channel = 'AudienceNetwork' AND Campaign = '19_whaleapp_pirate_sails_android_s2'

        SELECT MediaSource , Channel, Campaign, AdSet, Ad
        , Count(UserId) AS Users
        , Sum([]) AS Spend
        , Sum ([LTV]) AS   
        GROUP BY MediaSource , Channel, Campaign, AdSet, Ad
        HAVING Count(UserId) > 5