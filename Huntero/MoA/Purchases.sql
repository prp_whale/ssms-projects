
SELECT DISTINCT ProductID
  , Count(Distinct UserId) AS Users
  , Count(*) AS Transactions
FROM [dbo].[EventsPurchase]
WHERE ClientVersion IN ('1.0.231') -- 1.0.182 1.0.180 1.0.184 -- 1.0.244 1.0.243 1.0.231

GROUP BY ProductID
ORDER BY ProductID