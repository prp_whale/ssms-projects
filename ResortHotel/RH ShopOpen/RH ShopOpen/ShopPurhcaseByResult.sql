DECLARE @start_period DATETIME = '2019-03-09 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-05-09 23:59:59.997'; 
  
--SELECT [result] 
--       ,Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)                 AS Amt 
--FROM   [dbo].[ev_ShopPurchase] AS SP 
--       INNER JOIN PRP_Pool_Of_USERS AS POU 
--               ON SP.[UserId] = POU.U_ID 
--WHERE  [timestamp] BETWEEN @start_period AND @end_period 
--GROUP  BY [result] 
--ORDER  BY [Users] 

--SELECT  Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)                 AS Amt 
--FROM   [dbo].[ev_ShopPurchase] AS SP 
--       INNER JOIN PRP_Pool_Of_USERS AS POU 
--               ON SP.[UserId] = POU.U_ID 
--WHERE  [timestamp] BETWEEN @start_period AND @end_period 



SELECT [result] 
       ,Count (DISTINCT SP.[UserId]) AS Users 
       ,Count (*)                 AS Amt 
FROM   [dbo].[ev_ShopPurchase] AS SP 
       INNER JOIN ( SELECT DISTINCT [UserId] FROM [dbo].[PRP_ShopPurchase_March_May]) AS SSS 
               ON SP.[UserId] = SSS.[UserId]
WHERE  [timestamp] BETWEEN @start_period AND @end_period 
GROUP  BY [result] 
ORDER  BY [result] 

--SELECT Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)                 AS Amt 
--	   FROM [dbo].[PRP_ShopPurchase_March_May]