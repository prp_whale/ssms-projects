
-- -- OLD SYSTEM
-- WITH INVATIORS 
-- AS (
-- 	SELECT DISTINCT [UserId]  AS U_ID
-- 	FROM   [dbo].PRP_Terminal_Inviters_MORE_1_6_0 AS I 
-- 		   INNER JOIN [dbo].[Users] AS U 
-- 				   ON U.[UserID] = I.U_ID 
-- 				   AND [LTV] = 0
-- )

-- OLD SYSTEM
-- SELECT cast ([PaymentsCount] as bit) as IS_DEPO
-- 	  ,Count (DISTINCT FRINEDS.[UserId])  AS Users
-- FROM  [dbo].[ev_InvitationFriends]  AS FRINEDS 
-- 		INNER JOIN INVATIORS AS I 
-- 			ON I.U_ID =  FRINEDS.[UserIdInvitationSender]
--         INNER JOIN [dbo].[Users] AS U 
--                ON U.[UserID] = FRINEDS.[UserId]
-- GROUP BY cast ([PaymentsCount] as bit)

-- OLD SYSTEM
-- SELECT cast ([PaymentsCount] as bit) as IS_DEPO
-- 	  ,Count (DISTINCT I.U_ID)  AS Users
-- FROM  [dbo].PRP_Terminal_Inviters_MORE_1_6_0 AS I 
-- 	 INNER JOIN [dbo].[Users] AS U 
--               ON U.[UserID] = I.U_ID
-- GROUP BY cast ([PaymentsCount] as bit)



-- NEW  SYSTEM
WITH INVATIORS 
AS (
	SELECT DISTINCT I.[U_INVITE]  AS U_INV
	FROM   [dbo].PRP_Pool_Of_Real_Invatiors AS I 
		   INNER JOIN [dbo].[Users] AS U 
				   ON U.[UserID] = I.U_ID 
				   AND [LTV]  > 0
)
-- NEW  SYSTEM
SELECT cast ([PaymentsCount] as bit) as IS_DEPO
	  ,Count (DISTINCT FRINEDS.[UserId])  AS Users
FROM  [dbo].[ev_InvitationFriends]  AS FRINEDS 
		INNER JOIN INVATIORS AS I 
			ON I.[U_INV] =  FRINEDS.[UserIdInvitationSender]
        INNER JOIN [dbo].[Users] AS U 
               ON U.[UserID] = FRINEDS.[UserId]
WHERE FRINEDS.[ClientVersion] BETWEEN '1.14.2' AND '1.15.0'
--	WHERE ( (FRINEDS.[ClientVersion] BETWEEN '1.0.0' AND  '1.1.99') OR (FRINEDS.[ClientVersion] BETWEEN '1.2.0' AND  '1.5.99')) -- LESS_1_6_0
--	WHERE (FRINEDS.[ClientVersion] >= '1.6.0' OR (FRINEDS.[ClientVersion] BETWEEN '1.11.0' AND  '1.14.01')) -- MORE_1_6_0

GROUP BY cast ([PaymentsCount] as bit)



-- -- NEW  SYSTEM
-- SELECT cast ([PaymentsCount] as bit) as IS_DEPO
-- 	  ,Count (DISTINCT I.U_ID)  AS Users
-- FROM  [dbo].PRP_Pool_Of_Real_Invatiors AS I 
-- 	 INNER JOIN [dbo].[Users] AS U 
--               ON U.[UserID] = I.U_ID
-- 	  INNER JOIN [dbo].[ev_InvitationFriends]  AS FRINEDS ON I.[U_INVITE] =  FRINEDS.[UserIdInvitationSender]
-- --WHERE FRINEDS.[ClientVersion] BETWEEN '1.14.2' AND '1.15.0'
-- --	WHERE ( (FRINEDS.[ClientVersion] BETWEEN '1.0.0' AND  '1.1.99') OR (FRINEDS.[ClientVersion] BETWEEN '1.2.0' AND  '1.5.99')) -- LESS_1_6_0
-- --	WHERE (FRINEDS.[ClientVersion] >= '1.6.0' OR (FRINEDS.[ClientVersion] BETWEEN '1.11.0' AND  '1.14.01')) -- MORE_1_6_0
-- GROUP BY cast ([PaymentsCount] as bit)