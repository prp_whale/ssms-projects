DECLARE @startperiod datetime,
        @platform varchar(4)    
SET @startperiod = '2018-11-01 00:00:00.000' -- ���� (START)
set @platform = 1 --1--2--3

select
'ALL',
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod    AND @startperiod+7  AND Platform = @platform),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+7  AND @startperiod+14 AND Platform = @platform),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+14 AND @startperiod+21 AND Platform = @platform),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+21 AND @startperiod+28 AND Platform = @platform),
 ' ',
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+28 AND @startperiod+35 AND Platform = @platform),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+35 AND @startperiod+42 AND Platform = @platform),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+42 AND @startperiod+49 AND Platform = @platform),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+49 AND @startperiod+56 AND Platform = @platform),
' ',
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+56 AND @startperiod+63 AND Platform = @platform),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+63 AND @startperiod+70 AND Platform = @platform),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+70 AND @startperiod+77 AND Platform = @platform),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+77 AND @startperiod+84 AND Platform = @platform)

union
select
'US',
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod    AND @startperiod+7  AND Platform = @platform and [Country]='us'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+7  AND @startperiod+14 AND Platform = @platform and [Country]='us'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+14 AND @startperiod+21 AND Platform = @platform and [Country]='us'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+21 AND @startperiod+28 AND Platform = @platform and [Country]='us'),
 ' ',
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+28 AND @startperiod+35 AND Platform = @platform and [Country]='us'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+35 AND @startperiod+42 AND Platform = @platform and [Country]='us'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+42 AND @startperiod+49 AND Platform = @platform and [Country]='us'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+49 AND @startperiod+56 AND Platform = @platform and [Country]='us'),
' ',
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+56 AND @startperiod+63 AND Platform = @platform and [Country]='us'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+63 AND @startperiod+70 AND Platform = @platform and [Country]='us'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+70 AND @startperiod+77 AND Platform = @platform and [Country]='us'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+77 AND @startperiod+84 AND Platform = @platform and [Country]='us')

union
select
'GB',
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod    AND @startperiod+7  AND Platform = @platform and [Country]='gb'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+7  AND @startperiod+14 AND Platform = @platform and [Country]='gb'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+14 AND @startperiod+21 AND Platform = @platform and [Country]='gb'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+21 AND @startperiod+28 AND Platform = @platform and [Country]='gb'),
 ' ',
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+28 AND @startperiod+35 AND Platform = @platform and [Country]='gb'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+35 AND @startperiod+42 AND Platform = @platform and [Country]='gb'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+42 AND @startperiod+49 AND Platform = @platform and [Country]='gb'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+49 AND @startperiod+56 AND Platform = @platform and [Country]='gb'),
' ',
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+56 AND @startperiod+63 AND Platform = @platform and [Country]='gb'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+63 AND @startperiod+70 AND Platform = @platform and [Country]='gb'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+70 AND @startperiod+77 AND Platform = @platform and [Country]='gb'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+77 AND @startperiod+84 AND Platform = @platform and [Country]='gb')

union
select
'DE',
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod    AND @startperiod+7  AND Platform = @platform and [Country]='de'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+7  AND @startperiod+14 AND Platform = @platform and [Country]='de'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+14 AND @startperiod+21 AND Platform = @platform and [Country]='de'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+21 AND @startperiod+28 AND Platform = @platform and [Country]='de'),
 ' ',
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+28 AND @startperiod+35 AND Platform = @platform and [Country]='de'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+35 AND @startperiod+42 AND Platform = @platform and [Country]='de'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+42 AND @startperiod+49 AND Platform = @platform and [Country]='de'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+49 AND @startperiod+56 AND Platform = @platform and [Country]='de'),
' ',
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+56 AND @startperiod+63 AND Platform = @platform and [Country]='de'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+63 AND @startperiod+70 AND Platform = @platform and [Country]='de'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+70 AND @startperiod+77 AND Platform = @platform and [Country]='de'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+77 AND @startperiod+84 AND Platform = @platform and [Country]='de')

union
select
'FR',
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod    AND @startperiod+7  AND Platform = @platform and [Country]='fr'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+7  AND @startperiod+14 AND Platform = @platform and [Country]='fr'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+14 AND @startperiod+21 AND Platform = @platform and [Country]='fr'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+21 AND @startperiod+28 AND Platform = @platform and [Country]='fr'),
 ' ',
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+28 AND @startperiod+35 AND Platform = @platform and [Country]='fr'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+35 AND @startperiod+42 AND Platform = @platform and [Country]='fr'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+42 AND @startperiod+49 AND Platform = @platform and [Country]='fr'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+49 AND @startperiod+56 AND Platform = @platform and [Country]='fr'),
' ',
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+56 AND @startperiod+63 AND Platform = @platform and [Country]='fr'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+63 AND @startperiod+70 AND Platform = @platform and [Country]='fr'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+70 AND @startperiod+77 AND Platform = @platform and [Country]='fr'),
(SELECT COUNT(DISTINCT UserId) as 'Total Paying Users' FROM EventsPurchase WHERE Timestamp BETWEEN @startperiod+77 AND @startperiod+84 AND Platform = @platform and [Country]='fr')