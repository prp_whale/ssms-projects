 DECLARE @start_created datetime,
        @end_created datetime, 
        @startperiod datetime,
        @endperiod datetime,
        @platform int,
        @days_ago int ,
        @mediaSource NVARCHAR(20),
        @projectName NVARCHAR(20)

SET @days_ago = 1
SET @startperiod = '2020-01-06 00:00:00.000';
SET @endperiod = DATEADD(ms,   -3, DATEADD(dd,  DATEDIFF(dd,0,GETDATE()),-@days_ago));



SET @start_created  = '2020-01-01 00:00:00.000'; -- DONT CHANGE
SET @end_created = '2020-04-01 00:00:00.000'; -- fisrt day of month
SET @end_created = DATEADD(ms,   -3, @end_created); -- day before




SET @mediaSource =  'unityads_int';
SET @projectName = 'Hunter';



WITH PoolOFUsers
AS (

    SELECT UserId  AS U_ID
        , [Created] AS InstallTime
        , AdvertisingId AS Adv_ID
        , Platform
        , ISNULL([MediaSource], 'Organic') AS MediaSource
        , ISNULL([Ad], 'None') AS Ad
        FROM dbo.[Users] as u  
        WHERE [Created] BETWEEN @start_created AND @end_created
        AND MediaSource = @mediaSource
)


, Purchasers AS (
    SELEcT 
    Convert(date, InstallTime) AS IntsalledDate
    , MediaSource
    , Ad
    , PoU.Platform
    , DATEDIFF(dd, InstallTime, Timestamp ) AS DayOfLife
    , Sum(PaymentsCount) AS TotalPayments
    , Sum(AmountInUsd )* 0.7 AS IAPDailyNetRevenue

    FROM EventsPurchase AS P 
        INNER JOIN PoolOFUsers AS PoU ON PoU.U_ID = P.UserId AND PoU.Platform = P.Platform
    WHERE DATEDIFF(dd, InstallTime, Timestamp ) >= 0
    AND [Timestamp] BETWEEN @startperiod AND @endperiod
    AND MediaSource = @mediaSource
    GROUP BY PoU.Platform,  Convert(date, InstallTime), MediaSource, Ad , DATEDIFF(dd, InstallTime, Timestamp ) 
)

,Aders
AS 
(
    SELECT     
     Convert(date, InstallTime) AS IntsalledDate   
    , MediaSource
    , Ad
    , PoU.Platform
    , DATEDIFF(dd, InstallTime, ad.Date ) AS DayOfLife
    , Sum (Revenue) AdDailyNetRevenue 
    FROM [dbo].[ISUserAdRevenue] as  ad
        INNER JOIN PoolOFUsers AS PoU  ON PoU.Adv_ID = ad.AdvertisingId AND PoU.Platform = ad.Platform  AND  ad.ProjectName = @projectName
    WHERE DATEDIFF(dd, InstallTime, ad.Date ) >= 0
        AND ad.Date BETWEEN @startperiod AND @endperiod
        AND MediaSource = @mediaSource
    GROUP BY PoU.Platform,  Convert(date, InstallTime), MediaSource, Ad , DATEDIFF(dd, InstallTime, ad.Date ) 
)



SELECT ISNULL(a.IntsalledDate, p.IntsalledDate) AS 'IntsalledDate'
, ISNULL(a.MediaSource, p.MediaSource) AS 'MediaSource'
, ISNULL(a.Ad, p.Ad) + '   [' + ISNULL(a.MediaSource, p.MediaSource)   + '][Platform: ' +  ISNULL(a.Platform, p.Platform)  +']'   AS 'Ad' 
--, ISNULL(a.Ad, p.Ad) AS 'Ad'
, ISNULL(a.Platform, p.Platform) AS 'Platform' 
, ISNULL(a.DayOfLife, p.DayOfLife) AS 'DayOfLife'
, ISNULL(p.IAPDailyNetRevenue, 0) AS 'IAPDailyNetRevenue' 
, ISNULL(a.AdDailyNetRevenue, 0) AS 'AdDailyNetRevenue'
, Convert(date,@endperiod) AS 'LastDataUpdate'

FROM Purchasers AS p FULL OUTER JOIN Aders AS a
    ON p.IntsalledDate = a.IntsalledDate AND 
        p.MediaSource = a.MediaSource AND 
        p.Ad = a.Ad AND 
        p.Platform = a.Platform AND
        p.DayOfLife = a.DayOfLife
ORDER BY IntsalledDate,  Platform, MediaSource, Ad, DayOfLife


