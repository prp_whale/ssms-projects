DECLARE @start_date DATETIME = '2019-01-01 00:00:00.000'; 

--SELECT Cast([Created] AS DATE)     AS BORN 
--       , Count (DISTINCT [UserId]) AS USERS 
--FROM   [dbo].[Users] 
--WHERE  [Created] >= @start_date 
--GROUP  BY Cast([Created] AS DATE) 
--ORDER  BY BORN 


SELECT Cast([Timestamp] AS DATE)   AS 'OPEN' 
       , Count (DISTINCT [UserId]) AS USERS 
FROM   [dbo].[EventsAppOpen]
WHERE  [Timestamp] >= @start_date 
GROUP  BY Cast([Timestamp] AS DATE) 
ORDER  BY 'OPEN'  