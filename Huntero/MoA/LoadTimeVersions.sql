SELECT ClientVersion
, Count(Distinct UserID) AS U
FROM dbo.LoadingTime
WHERE Platform = 2
GROUP BY ClientVersion
HAVING Count(Distinct UserID) > 100

ORDER BY ClientVersion