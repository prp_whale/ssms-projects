DECLARE @DateFrom datetime = '2019-03-01 00:00:00'; --начало анализированного периода
DECLARE @DateTo datetime = '2019-09-16 23:59:59'; -- конец анализированного периода

SELECT --u.[RegistrationClientVersion] as ClientVersion
		CONVERT(date, u.[Created]) AS 'DAY'

		,COUNT(DISTINCT u.[UserId]) as registration
		,COUNT(DISTINCT IIF(a.[Completed] = 1 AND [LevelId] = 1, a.[UserId], null)) as level_1_pass
		,COUNT(DISTINCT IIF(a.[Completed] = 1 AND [LevelId] = 2, a.[UserId], null)) as level_2_pass
		,COUNT(DISTINCT IIF(a.[Completed] = 1 AND [LevelId] = 3, a.[UserId], null)) as level_3_pass
		,COUNT(DISTINCT IIF(a.[Completed] = 1 AND [LevelId] = 4, a.[UserId], null)) as level_4_pass
		,COUNT(DISTINCT IIF(a.[Completed] = 1 AND [LevelId] = 5, a.[UserId], null)) as level_5_pass

		,(COUNT(DISTINCT IIF(a.[Completed] = 1 AND [LevelId] = 1, a.[UserId], null)) + 0.0) / COUNT(DISTINCT u.[UserId]) as level_1_pass
		,(COUNT(DISTINCT IIF(a.[Completed] = 1 AND [LevelId] = 2, a.[UserId], null)) + 0.0) / COUNT(DISTINCT u.[UserId]) as level_2_pass
		,(COUNT(DISTINCT IIF(a.[Completed] = 1 AND [LevelId] = 3, a.[UserId], null)) + 0.0) / COUNT(DISTINCT u.[UserId]) as level_3_pass
		,(COUNT(DISTINCT IIF(a.[Completed] = 1 AND [LevelId] = 4, a.[UserId], null)) + 0.0) / COUNT(DISTINCT u.[UserId]) as level_4_pass
		,(COUNT(DISTINCT IIF(a.[Completed] = 1 AND [LevelId] = 5, a.[UserId], null)) + 0.0) / COUNT(DISTINCT u.[UserId]) as level_5_pass

FROM [dbo].[Users] u 

LEFT JOIN [dbo].[EventsLevelAttempts] a ON a.[UserId] = u.[UserId] AND a.[Timestamp] BETWEEN @DateFrom AND @DateTo

WHERE u.[Created] BETWEEN @DateFrom AND @DateTo 
AND [MediaSource] IS NULL 
--AND u.[RegistrationClientVersion] IN ('1.4.7', '1.4.8', '1.4.9', '1.4.10', '1.4.11', '1.4.12', '1.4.13')
AND u.[Platform] = 1
--AND u.[RegistrationCountry] IN ('GB', 'DE', 'FR', 'US') 
	--AND u.[RegistrationCountry] IN ('US') 

GROUP BY CONVERT(date, u.[Created])
ORDER BY CONVERT(date, u.[Created])

--GROUP BY u.[RegistrationClientVersion]
--ORDER BY u.[RegistrationClientVersion]
