DECLARE @start_date Datetime = '2019-02-11 00:00:00.000'; 
DECLARE @end_date Datetime = '2019-02-24 23:59:59.999'; 
DECLARE @user_id Int = 4311; 


SELECT [UserId] 
       , [sends] 
       , [arrived] 
       , [acceleration] 
       , [open_slots] 
       , [plane_id] 
       , [slot1_quant] 
       , [slot2_quant] 
       , [slot3_quant] 
       , [slot4_quant] 
       , [slot5_quant] 
       , [slot1_description] 
       , [slot2_description] 
       , [slot3_description] 
       , [slot4_description] 
       , [slot5_description] 
       , [Timestamp] 
       , [RegistrationCountry] 
       , [SessionId] 
       , [EventId] 
       , [ClientTime] 
       , [BackOfficeTime] 
       , [Order] 
       , [ClientVersion] 
       , [IsOffline] 
       , [Platform] 
       , [Created] 
       , [LTV] 
       , [Retention1D] 
       , [Retention2D] 
       , [Retention7D] 
       , [FirstDepositDate] 
       , [LTV7D] 
       , [LTV2D] 
       , [LTV1D] 
       , [LTV30D] 
FROM   [dbo].[airport] 
WHERE  [Timestamp] BETWEEN @start_date AND @end_date 
   AND [UserId] = @user_id 
ORDER  BY [Timestamp] 
