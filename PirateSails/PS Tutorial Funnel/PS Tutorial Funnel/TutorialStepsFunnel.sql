
DECLARE @pool_size INT;
SET @pool_size = (SELECT  Count(*)  FROM  PRP_Tutorial_Step_Seconds) ;

SELECT  @pool_size AS Pool_Users
        , S1.Leavers AS step_1_leave
        , S2.Leavers - S1.Leavers AS step_2_leave
        , S3.Leavers - S2.Leavers AS step_3_leave
        , S4.Leavers - S3.Leavers AS step_4_leave
        , S5.Leavers - S4.Leavers AS step_5_leave
        , S6.Leavers - S5.Leavers AS step_6_leave
        , S7.Leavers - S6.Leavers AS step_7_leave
        , S8.Leavers - S7.Leavers AS step_8_leave
        , S9.Leavers - S8.Leavers AS step_9_leave
        , S10.Leavers - S9.Leavers AS step_10_leave

FROM (SELECT Count(*) AS Leavers  FROM PRP_Tutorial_Step_Seconds  WHERE sec_step_1 IS NULL) AS S1
,    (SELECT Count(*) AS Leavers  FROM PRP_Tutorial_Step_Seconds  WHERE sec_step_2 IS NULL) AS S2
,    (SELECT Count(*) AS Leavers  FROM PRP_Tutorial_Step_Seconds  WHERE sec_step_3 IS NULL) AS S3
,    (SELECT Count(*) AS Leavers  FROM PRP_Tutorial_Step_Seconds  WHERE sec_step_4 IS NULL) AS S4
,    (SELECT Count(*) AS Leavers  FROM PRP_Tutorial_Step_Seconds  WHERE sec_step_5 IS NULL) AS S5
,    (SELECT Count(*) AS Leavers  FROM PRP_Tutorial_Step_Seconds  WHERE sec_step_6 IS NULL) AS S6
,    (SELECT Count(*) AS Leavers  FROM PRP_Tutorial_Step_Seconds  WHERE sec_step_7 IS NULL) AS S7
,    (SELECT Count(*) AS Leavers  FROM PRP_Tutorial_Step_Seconds  WHERE sec_step_8 IS NULL) AS S8
,    (SELECT Count(*) AS Leavers  FROM PRP_Tutorial_Step_Seconds  WHERE sec_step_9 IS NULL) AS S9
,    (SELECT Count(*) AS Leavers  FROM PRP_Tutorial_Step_Seconds  WHERE sec_step_10 IS NULL) AS S10

