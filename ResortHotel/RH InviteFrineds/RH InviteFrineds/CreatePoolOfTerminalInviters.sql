DECLARE @start_period DATETIME = '2018-12-01 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-06-01 23:59:59.997'; 

-- SELECT  Count (Distinct[UserIdInvitationSender])	AS U_ID
--        ,Count (*)                AS Invitations  
-- FROM  [dbo].[ev_InvitationFriends]  AS IA
-- WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
--   AND ( ( [ClientVersion] BETWEEN '1.0.0' AND '1.1.99' )  OR ( [ClientVersion] BETWEEN '1.2.0' AND '1.5.99' ) ) -- LESS_1_6_0 



DROP TABLE IF EXISTS PRP_Terminal_Inviters_LESS_1_6_0;

SELECT [UserIdInvitationSender]	AS U_ID
       ,Count (*)                AS Invitations  
INTO PRP_Terminal_Inviters_LESS_1_6_0
FROM  [dbo].[ev_InvitationFriends]  AS IA
	INNER JOIN PRP_Pool_Of_Active_Users_6_Month_LESS_1_6_0 AS POU
		ON	IA.[UserIdInvitationSender] = POU.U_ID
WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
   AND ( ( [ClientVersion] BETWEEN '1.0.0' AND '1.1.99' )  OR ( [ClientVersion] BETWEEN '1.2.0' AND '1.5.99' ) ) -- LESS_1_6_0 
GROUP  BY [UserIdInvitationSender] 

SELECT Count (DISTINCT U_ID) AS Users 
       ,Count (*)            AS Amount 
FROM   PRP_Terminal_Inviters_LESS_1_6_0


------------------------------------------------------------------------------------

--DROP TABLE IF EXISTS PRP_Terminal_Inviters_MORE_1_6_0;

--SELECT [UserIdInvitationSender]	AS U_ID
--       ,Count (*)                AS Invitations  
--INTO PRP_Terminal_Inviters_MORE_1_6_0
--FROM  [dbo].[ev_InvitationFriends]  AS IA
--	INNER JOIN [dbo].[PRP_Pool_Of_Active_Users_6_Month_MORE_1_6_0] AS POU
--		ON	IA.[UserIdInvitationSender] = POU.U_ID
--WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
--   AND ([ClientVersion] >= '1.6.0' OR ([ClientVersion] BETWEEN '1.11.0' AND  '1.19.99')) -- MORE_1_6_0
--GROUP  BY [UserIdInvitationSender] 

--SELECT Count (DISTINCT U_ID) AS Users 
--       ,Count (*)                 AS Amount 
--FROM   PRP_Terminal_Inviters_MORE_1_6_0


--SELECT avg (Invitations*1.0 )
--, Count  (Invitations)
--from PRP_Terminal_Inviters_MORE_1_6_0

