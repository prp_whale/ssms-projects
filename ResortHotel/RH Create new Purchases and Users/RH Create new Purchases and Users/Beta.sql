SELEcT top (100) * 
FROM   [dbo].[EventsPurchase] 
ORDER  BY [Timestamp] DESC 


DECLARE @start_period DATETIME = '2019-07-01 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-07-30 23:59:59.997'; 

SELECT   [Amount] , 
         Count (*) AS TRANSTACTIONS 
FROM     [dbo].[EventsPurchase] 
WHERE    [Timestamp] BETWEEN @start_period AND      @end_period
AND [Country] = 'RU'
GROUP BY [Amount] 
ORDER BY TRANSTACTIONS DESC
 

-------------------------------------------------------------------------------
SELECT TOP (100) [Amount] 
                 ,[AmountInUSD] 
FROM   [dbo].[EventsPurchase] 
WHERE  [Country] = 'RU' 
ORDER  BY [Timestamp] DESC 


SELECT TOP (100) [Amount] 
                 ,[AmountInUSD] 
                 ,[AmountInUSDCoeff] 
FROM   [dbo].[EventsPurchaseCoeff] 
WHERE  [Country] = 'RU' 
ORDER  BY [Timestamp] DESC 
------------------------------------------------------------------------------
------------------------------------------------------------------------------
SELECT TOP (10000) cast( [Timestamp] as date)
				, [Amount] 
				, [Currency]
                 ,[AmountInUSD] 
				    ,[AmountInUSDCoeff] 
FROM   [dbo].[EventsPurchaseCoeff] 
WHERE  [Country] = 'PL' 
--AND [Currency] = 'USD'
AND [Timestamp] Between '2019-04-01' AND '2019-07-31'
ORDER  BY [Timestamp] DESC 
------------------------------------------------------------------------------