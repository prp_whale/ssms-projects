DECLARE @startperiod datetime,
        @endperiod datetime,
	    @platform int;
        
    SET @startperiod = '2019-08-15 00:00:00.000';
    SET @endperiod = '2019-11-07 23:59:59.999';
    SET @platform =1 ;



WITH PoolOfUsers
AS (
     SELECT UserId as U_ID
     FROM dbo.[Users] 
     --WHERE  [RegistrationCountry] != 'UA'
     WHERE  [RegistrationCountry] = 'NL'
     AND [platform] = @platform
     --AND [RegistrationCountry] IN ('US', 'GB', 'DE','FR', 'JP')
   )

SELECT t.U AS Active_U
, t2.U  AS Purchase_U
, t3.U  AS Golden_U
, t2.TR  AS Trans
, t3.TR  AS Golden_Trans
, t2.G  AS Purchase_Gross
, t3.G  AS Golden_Gross
, t2.U*1.0 / t.U AS 'P/A'
, t3.U*1.0 / t.U AS 'G/A'
, t3.U*1.0 / t2.U AS 'G/P'
, t2.G*1.0 /t2.tr  AS 'Avg Check'
, t3.G*1.0 /t3.tr  AS 'Gold Avg Check'
, t2.G*1.0 /t2.U  AS 'ARPPU'
, t3.G*1.0 /t3.U  AS 'Gold ARPPU'
FROM 
(
Select '1' as link 
 ,Count(Distinct UserId) AS U
FROM [dbo].[EventsAppOpen] AS T 
        INNER JOIN PoolOfUsers AS POU ON T.[UserId] = POU.[U_ID]
WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
) AS t 
INNER JOIN 
(
    Select '1' as link 
    ,Count(Distinct UserId) AS U
     ,Count(*) AS TR
    ,Sum(AmountInUsd) AS G
    FROM [dbo].[EventsPurchase] AS T 
        INNER JOIN PoolOfUsers AS POU ON T.[UserId] = POU.[U_ID]
    WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
)
AS t2 ON t.link = t2.link 
INNER JOIN 
(
    Select '1' as link 
    ,Count(Distinct UserId) AS U
      ,Count(*) AS TR
      ,Sum(AmountInUsd) AS G
    FROM [dbo].[EventsPurchase] AS T 
        INNER JOIN PoolOfUsers AS POU ON T.[UserId] = POU.[U_ID]
    WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
    AND ProductId LIKE 'golden_ors_%'
)
AS t3 ON t.link = t3.link 

-- SELEcT Country
--     ,Count(Distinct UserId) AS U
--         ,Count( *) AS A
--             FROM [dbo].[EventsPurchase] 
--     WHERE  ProductId LIKE 'golden_ors_%'
--     GROUP BY Country
--     ORDER BY A DESC