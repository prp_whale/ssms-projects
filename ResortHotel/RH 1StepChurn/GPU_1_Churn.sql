DECLARE @start_registration DATETIME = '2019-03-01 00:00:00.000';
DECLARE @end_registration DATETIME = '2019-08-01 23:59:59.997';

SELECT L.* 
--,All_Users
,L1.[Leavers] AS leave_month_1
,L2.[Leavers] AS leave_month_2
,L3.[Leavers] AS leave_month_3
,L4.[Leavers] AS leave_month_4
,L5.[Leavers] AS leave_month_5
,L6.[Leavers] AS leave_month_6
,[All_Leavers]*1.0 / [All_Users] AS churn
,L1.[Leavers]*1.0 / U1.[Users] AS churn_month_1
,L2.[Leavers]*1.0 / U2.[Users] AS churn_month_2
,L3.[Leavers]*1.0 / U3.[Users] AS churn_month_3
,L4.[Leavers]*1.0 / U4.[Users] AS churn_month_4
,L5.[Leavers]*1.0 / U5.[Users] AS churn_month_5
,L6.[Leavers]*1.0 / U6.[Users] AS churn_month_6
FROM
(SELECT [GPUType]
, Count(*) AS All_Leavers
FROM PRP_Pool_Of_Deviators_Android
GROUP BY [GPUType]
 ) AS L
INNER JOIN 
(SELECT [GPUType]
, Count(*) AS All_Users
FROM PRP_Pool_Of_Users_Android
GROUP BY [GPUType]
HAVING Count(*) > 5) AS U
ON U.[GPUType] =  L.[GPUType]
