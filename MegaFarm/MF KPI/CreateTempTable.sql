DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform varchar(4)
--,@regcountry varchar(4)

-- дата (START)
SET @startperiod = '2019-01-28 00:00:00.000'
-- дата (END)
--SET @endperiod = '2019-11-03 23:59:59.999'
SET @endperiod = DATEADD(ms, -3, DATEADD(wk,  DATEDIFF(wk,0,GETDATE()),0))
--SELECT @endperiod
SET @platform = 1 -- 2 -- 3 --
--set @regcountry =  'fr'--'us'--'gb' --'de' --'fr' --'jp'
If(OBJECT_ID('tempdb..#temp_dates') Is Null)
Begin
    CREATE TABLE #temp_dates ( [date] DATE PRIMARY KEY );
    INSERT #temp_dates([date]) SELECT d FROM ( 
        SELECT d = DATEADD(DAY, rn - 1, @startperiod) FROM  (
                SELECT TOP (DATEDIFF(DAY, @startperiod, @endperiod)) rn = ROW_NUMBER() OVER (ORDER BY s1.[object_id])
                FROM sys.all_objects AS s1 CROSS JOIN sys.all_objects AS s2
                ORDER BY s1.[object_id] ) AS x ) AS y;
End

SELEcT * FROM #temp_dates

