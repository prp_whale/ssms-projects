declare @start_date datetime	= '2019-02-11 00:00:00';
declare @end_date datetime		= '2019-02-24 23:59:59';
declare @cheater int			= 7249

	--declare @source varchar(25)		= 'complete_building'
	--declare @source varchar(25)		= 'daily_bonus'
	--declare @source varchar(25)		= 'get_achievement'
	--declare @source varchar(25)		= 'gift'
	declare @source varchar(25)		= 'iap'
	--declare @source varchar(25)		= 'level_up'
	--declare @source varchar(25)		= 'reward_leaderboard'
	--declare @source varchar(25)		= 'reward_quest'
	--declare @source varchar(25)		= 'reward_video'
	--declare @source varchar(25)		= 'reward_wheel_fortune'

SELECT	[Level_id] as lvl 
		, SUM	([cash]) as CASH
		, COUNT (DISTINCT [UserId]) as USERS_ON_LVL
		, COUNT (*) as TRANSACTIONS
	  FROM [dbo].[currency_received]
  WHERE   [RegistrationCountry] <> 'UA'
   AND [UserID] != @cheater
   AND [timestamp] between @start_date and @end_date
   AND [source_type_id] != @source
   AND [cash] != 0
  GROUP BY [Level_id]
  ORDER BY lvl 