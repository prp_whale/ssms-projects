DECLARE @DateFrom datetime = '2019-03-01 00:00:00'; --начало анализированного периода
DECLARE @DateTo datetime = '2019-09-16 23:59:59'; -- конец анализированного периода

SELECT [RegistrationClientVersion] as ClientVersion
		,COUNT(*)
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 1, [LTV1D], null)) as 'Conversion_Day1'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 3, [LTV3D], null)) as 'Conversion_Day3'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 7, [LTV7D], null)) as 'Conversion_Day7'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 14, [LTV14D], null)) as 'Conversion_Dy14'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 30, [LTV30D], null)) as 'Conversion_Day30'


FROM [dbo].[Users] WHERE [Created] BETWEEN @DateFrom AND @DateTo
--AND [RegistrationCountry] IN ('GB', 'DE', 'FR') 
 --AND [RegistrationCountry] IN ('US') 
 
 AND [Platform] = 1

 AND [RegistrationClientVersion] IN ('1.4.7', '1.4.8', '1.4.9', '1.4.10', '1.4.11', '1.4.12', '1.4.13') AND [MediaSource] IS NULL

-- AND [RamSize] >= 1100

GROUP BY [RegistrationClientVersion]
ORDER BY [RegistrationClientVersion]