USE PIRATES 

DECLARE @PERIOD_BEGIN NVARCHAR(20) = '2019-04-11 00:00:00'; --начало анализированного периода 
DECLARE @PERIOD_END NVARCHAR(20) = '2019-04-11 23:59:59'; -- конец анализированного периода 
DECLARE @TestDevices TABLE  ( UserID INT PRIMARY KEY ); 

INSERT INTO @TestDevices VALUES  (6), (14), (23), (105), (239), (247), (256), (257), (333), (1331), (1366), (1367), (1368), (1375), (1381), (1382), (3487), (3737), (4244), (4253), (4255), (4265), (4266), (4270), (5517), (5906), (2), (3), (7), (8), (9), (12), (16), (17), (21), (22), (25), (26), (27), (51), (117), (249), (293), (598), (666), (670), (1190), (1192), (1193), (1337), (5568);
    

SELECT int5                            AS IsFreeOpen 
       , int4                          AS ChestLvl 
       , Count(DISTINCT events.userid) AS users 
FROM   events 
       JOIN users 
         ON users.userid = events.userid 
WHERE  eventid = 15 
   AND events.UserId NOT IN (SELECT UserID 
                             FROM   @TestDevices) 
   AND ( timestamp BETWEEN @PERIOD_BEGIN AND @PERIOD_END 
         AND created BETWEEN '2019-04-06 00:00:00' AND '2019-04-06 23:59:59' ) 
GROUP  BY int5 
          , int4 
ORDER  BY int4 