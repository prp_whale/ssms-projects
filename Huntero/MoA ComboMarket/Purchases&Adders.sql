 DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform int,
        @days_ago int ,
        @projectName NVARCHAR(20)

SET @days_ago = 6
SET @startperiod = '2020-02-06 00:00:00.000';

SET @endperiod = '2020-02-06 23:59:59.997';
--SET @endperiod = DATEADD(dd,  DATEDIFF(dd,0,GETDATE()),-@days_ago);
SET @projectName = 'Hunter';

--SET @platform = 2;



WITH PoolOFUsers
AS (

 SELECT 
    Convert(date, install_time) AS IntsalledDate
    , [platform]
    , ISNULL([media_source], 'Organic') AS MediaSource
    , ISNULL([campaign], 'None') AS Campaign
    , Count([advertising_id]) AS Users
    FROM [dbo].[AFEvents]
   -- WHERE  [ProjectName] = @projectName 
   -- AND [event_name]  = 'install'
    WHERE [install_time] BETWEEN @startperiod AND @endperiod
   
     
    GROUP BY Convert(date, install_time) , [platform] ,[media_source], [campaign], [advertising_id]
--  SELECT u.UserId  AS U_ID
--     , InstallTime
--     , af.AdvertisingId AS Adv_ID
--     , af.Platform
--     , ISNULL(af.MediaSource, 'Organic') AS MediaSource
--     , ISNULL(af.Campaign, 'None') AS Campaign
--     FROM dbo.[Users] as u  
--     LEFT JOIN [dbo].[AFEventsInstall] as af 
--         ON u.AdvertisingId = af.AdvertisingId AND ISNULL(u.MediaSource, 'Organic') = ISNULL(af.MediaSource, 'Organic')  
--                 AND ISNULL(u.Campaign, 'None') = ISNULL(af.Campaign, 'None')  AND  af.ProjectName = @projectName
--     WHERE InstallTime BETWEEN @startperiod AND @endperiod
)


-- SELEcT Adv_ID  
-- FROM PoolOFUsers
-- GROUP BY Adv_ID
-- HAVING Count(*) > 1

SELEcT TOP(100) * FROM PoolOfUsers
-- SELEcT * 
-- FROM PoolOfUsers
-- WHERE Adv_ID IN ('3a546398-b27e-453a-b0c3-67d762e012bd', 'c0022cd3-6b1e-4a8c-8b7f-b8ad0f83cbe6')



-- , Purchasers AS (
--     SELEcT 
--     Convert(date, InstallTime) AS IntsalledDate
--     , MediaSource
--     , Campaign
--     , PoU.Platform
--     , DATEDIFF(dd, InstallTime, Timestamp ) AS DayOfLife
--     , Sum(PaymentsCount) AS TotalPayments
--     , Sum(AmountInUsd )* 0.7 AS IAPDailyNetRevenue

--     FROM EventsPurchase AS P 
--         INNER JOIN PoolOFUsers AS PoU ON PoU.U_ID = P.UserId AND PoU.Platform = P.Platform
--      WHERE DATEDIFF(dd, InstallTime, Timestamp ) >= 0
--     GROUP BY PoU.Platform,  Convert(date, InstallTime), MediaSource, Campaign , DATEDIFF(dd, InstallTime, Timestamp ) 
-- )

-- ,Aders
-- AS 
-- (
--     SELECT     
--      Convert(date, InstallTime) AS IntsalledDate   
--     , MediaSource
--     , Campaign
--     , PoU.Platform
--     , DATEDIFF(dd, InstallTime, ad.Date ) AS DayOfLife
--     , Sum (Revenue) AdDailyNetRevenue 
--     FROM [dbo].[ISUserAdRevenue] as  ad
--         INNER JOIN PoolOFUsers AS PoU  ON PoU.Adv_ID = ad.AdvertisingId AND PoU.Platform = ad.Platform  AND  ad.ProjectName = @projectName
--     WHERE DATEDIFF(dd, InstallTime, ad.Date ) >= 0
--     GROUP BY PoU.Platform,  Convert(date, InstallTime), MediaSource, Campaign , DATEDIFF(dd, InstallTime, ad.Date ) 
-- )



-- SELECT ISNULL(a.IntsalledDate, p.IntsalledDate) AS 'IntsalledDate'
-- , ISNULL(a.MediaSource, p.MediaSource) AS 'MediaSource'
-- , ISNULL(a.MediaSource, p.MediaSource) + '|' + ISNULL(a.Campaign, p.Campaign) AS 'Campaign'
-- --, ISNULL(a.Campaign, p.Campaign) AS 'Campaign'
-- , ISNULL(a.Platform, p.Platform) AS 'Platform' 
-- , ISNULL(a.DayOfLife, p.DayOfLife) AS 'DayOfLife'
-- , ISNULL(p.IAPDailyNetRevenue, 0) AS 'IAPDailyNetRevenue' 
-- , ISNULL(a.AdDailyNetRevenue, 0) AS 'AdDailyNetRevenue'
-- FROM Purchasers AS p FULL OUTER JOIN Aders AS a
--     ON p.IntsalledDate = a.IntsalledDate AND 
--         p.MediaSource = a.MediaSource AND 
--         p.Campaign = a.Campaign AND 
--         p.Platform = a.Platform AND
--         p.DayOfLife = a.DayOfLife
-- ORDER BY Platform, IntsalledDate, MediaSource, Campaign, DayOfLife


