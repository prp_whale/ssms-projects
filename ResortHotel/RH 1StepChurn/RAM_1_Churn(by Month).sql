DECLARE @start_registration DATETIME = '2019-07-01 00:00:00.000';
--DECLARE @end_registration DATETIME = '2019-08-01 23:59:59.997';

SELECT L.* 
,All_Users
,L1.[Leavers] AS leave_month_1
,L2.[Leavers] AS leave_month_2
,L3.[Leavers] AS leave_month_3
-- ,L4.[Leavers] AS leave_month_4
-- ,L5.[Leavers] AS leave_month_5
-- ,L6.[Leavers] AS leave_month_6
,[All_Leavers]*1.0 / [All_Users] AS churn
,L1.[Leavers]*1.0 / U1.[Users] AS churn_month_1
,L2.[Leavers]*1.0 / U2.[Users] AS churn_month_2
,L3.[Leavers]*1.0 / U3.[Users] AS churn_month_3
-- ,L4.[Leavers]*1.0 / U4.[Users] AS churn_month_4
-- ,L5.[Leavers]*1.0 / U5.[Users] AS churn_month_5
-- ,L6.[Leavers]*1.0 / U6.[Users] AS churn_month_6
FROM
(SELECT [RamSize]/10*10 AS RamSize
, Count(*) AS All_Leavers
FROM PRP_Pool_Of_Deviators_Android
GROUP BY [RamSize]/10*10
 ) AS L
INNER JOIN 
(SELECT [RamSize]/10*10 AS RamSize
, Count(*) AS All_Users
FROM PRP_Pool_Of_Users_Android
GROUP BY [RamSize]/10*10
HAVING Count(*) > 1) AS U
ON U.[RamSize] =  L.[RamSize]

LEFT JOIN
(SELECT [RamSize]/10*10 AS RamSize
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN @start_registration AND DATEADD(mm,1,@start_registration)
GROUP BY [RamSize]/10*10
 ) AS L1 ON L.[RamSize] = L1.[RamSize]
LEFT JOIN 
(SELECT [RamSize]/10*10 AS RamSize
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN @start_registration AND DATEADD(mm,1,@start_registration)
GROUP BY [RamSize]/10*10
) AS U1
ON L.[RamSize] = U1.[RamSize] 

LEFT JOIN
(SELECT [RamSize]/10*10 AS RamSize
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,1,@start_registration) AND DATEADD(mm,2,@start_registration)
GROUP BY [RamSize]/10*10
 ) AS L2 ON L.[RamSize] = L2.[RamSize]
LEFT JOIN 
(SELECT [RamSize]/10*10 AS RamSize
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,1,@start_registration) AND DATEADD(mm,2,@start_registration)
GROUP BY [RamSize]/10*10
) AS U2
ON L.[RamSize] = U2.[RamSize] 

LEFT JOIN
(SELECT [RamSize]/10*10 AS RamSize
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,2,@start_registration) AND DATEADD(mm,3,@start_registration)
GROUP BY [RamSize]/10*10
 ) AS L3 ON L.[RamSize] = L3.[RamSize]
LEFT JOIN 
(SELECT [RamSize]/10*10 AS RamSize
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,2,@start_registration) AND DATEADD(mm,3,@start_registration)
GROUP BY [RamSize]/10*10
) AS U3
ON L.[RamSize] = U3.[RamSize] 

LEFT JOIN
(SELECT [RamSize]/10*10 AS RamSize
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,3,@start_registration) AND DATEADD(mm,4,@start_registration)
GROUP BY [RamSize]/10*10
 ) AS L4 ON L.[RamSize] = L4.[RamSize]
LEFT JOIN 
(SELECT [RamSize]/10*10 AS RamSize
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,3,@start_registration) AND DATEADD(mm,4,@start_registration)
GROUP BY [RamSize]/10*10
) AS U4
ON L.[RamSize] = U4.[RamSize] 

LEFT JOIN
(SELECT [RamSize]/10*10 AS RamSize
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,4,@start_registration) AND DATEADD(mm,5,@start_registration)
GROUP BY [RamSize]/10*10
 ) AS L5 ON L.[RamSize] = L5.[RamSize]
LEFT JOIN 
(SELECT [RamSize]/10*10 AS RamSize
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,4,@start_registration) AND DATEADD(mm,5,@start_registration)
GROUP BY [RamSize]/10*10
) AS U5
ON L.[RamSize] = U5.[RamSize] 

LEFT JOIN
(SELECT [RamSize]/10*10 AS RamSize
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,5,@start_registration) AND DATEADD(mm,6,@start_registration)
GROUP BY [RamSize]/10*10
 ) AS L6 ON L.[RamSize] = L6.[RamSize]
LEFT JOIN 
(SELECT [RamSize]/10*10 AS RamSize
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,5,@start_registration) AND DATEADD(mm,6,@start_registration)
GROUP BY [RamSize]/10*10
) AS U6
ON L.[RamSize] = U6.[RamSize] 
ORDER BY All_Leavers DESC
