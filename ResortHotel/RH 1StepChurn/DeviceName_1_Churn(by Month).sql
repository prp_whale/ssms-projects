DECLARE @start_registration DATETIME = '2019-03-01 00:00:00.000';
DECLARE @end_registration DATETIME = '2019-08-01 23:59:59.997';

SELECT L.* 
,All_Users
,L1.[Leavers] AS leave_month_1
,L2.[Leavers] AS leave_month_2
,L3.[Leavers] AS leave_month_3
,L4.[Leavers] AS leave_month_4
,L5.[Leavers] AS leave_month_5
,L6.[Leavers] AS leave_month_6
,L7.[Leavers] AS leave_month_7
,[All_Leavers]*1.0 / [All_Users] AS churn
,L1.[Leavers]*1.0 / U1.[Users] AS churn_month_1
,L2.[Leavers]*1.0 / U2.[Users] AS churn_month_2
,L3.[Leavers]*1.0 / U3.[Users] AS churn_month_3
,L4.[Leavers]*1.0 / U4.[Users] AS churn_month_4
,L5.[Leavers]*1.0 / U5.[Users] AS churn_month_5
,L6.[Leavers]*1.0 / U6.[Users] AS churn_month_6
,L7.[Leavers]*1.0 / U7.[Users] AS churn_month_7
FROM
(SELECT [DeviceName]
, Count(*) AS All_Leavers
FROM PRP_Pool_Of_Deviators_Android
GROUP BY [DeviceName]
 ) AS L
INNER JOIN 
(SELECT [DeviceName]
, Count(*) AS All_Users
FROM PRP_Pool_Of_Users_Android
GROUP BY [DeviceName]
HAVING Count(*) > 100) AS U
ON U.[DeviceName] =  L.[DeviceName]

LEFT JOIN
(SELECT [DeviceName]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN @start_registration AND DATEADD(mm,1,@start_registration)
GROUP BY [DeviceName]
 ) AS L1 ON L.[DeviceName] = L1.[DeviceName]
LEFT JOIN 
(SELECT [DeviceName]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN @start_registration AND DATEADD(mm,1,@start_registration)
GROUP BY [DeviceName]
) AS U1
ON L.[DeviceName] = U1.[DeviceName] 

LEFT JOIN
(SELECT [DeviceName]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,1,@start_registration) AND DATEADD(mm,2,@start_registration)
GROUP BY [DeviceName]
 ) AS L2 ON L.[DeviceName] = L2.[DeviceName]
LEFT JOIN 
(SELECT [DeviceName]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,1,@start_registration) AND DATEADD(mm,2,@start_registration)
GROUP BY [DeviceName]
) AS U2
ON L.[DeviceName] = U2.[DeviceName] 

LEFT JOIN
(SELECT [DeviceName]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,2,@start_registration) AND DATEADD(mm,3,@start_registration)
GROUP BY [DeviceName]
 ) AS L3 ON L.[DeviceName] = L3.[DeviceName]
LEFT JOIN 
(SELECT [DeviceName]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,2,@start_registration) AND DATEADD(mm,3,@start_registration)
GROUP BY [DeviceName]
) AS U3
ON L.[DeviceName] = U3.[DeviceName] 

LEFT JOIN
(SELECT [DeviceName]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,3,@start_registration) AND DATEADD(mm,4,@start_registration)
GROUP BY [DeviceName]
 ) AS L4 ON L.[DeviceName] = L4.[DeviceName]
LEFT JOIN 
(SELECT [DeviceName]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,3,@start_registration) AND DATEADD(mm,4,@start_registration)
GROUP BY [DeviceName]
) AS U4
ON L.[DeviceName] = U4.[DeviceName] 

LEFT JOIN
(SELECT [DeviceName]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,4,@start_registration) AND DATEADD(mm,5,@start_registration)
GROUP BY [DeviceName]
 ) AS L5 ON L.[DeviceName] = L5.[DeviceName]
LEFT JOIN 
(SELECT [DeviceName]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,4,@start_registration) AND DATEADD(mm,5,@start_registration)
GROUP BY [DeviceName]
) AS U5
ON L.[DeviceName] = U5.[DeviceName] 

LEFT JOIN
(SELECT [DeviceName]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,5,@start_registration) AND DATEADD(mm,6,@start_registration)
GROUP BY [DeviceName]
 ) AS L6 ON L.[DeviceName] = L6.[DeviceName]
LEFT JOIN 
(SELECT [DeviceName]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,5,@start_registration) AND DATEADD(mm,6,@start_registration)
GROUP BY [DeviceName]
) AS U6
ON L.[DeviceName] = U6.[DeviceName] 

LEFT JOIN
(SELECT [DeviceName]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,7-1,@start_registration) AND DATEADD(mm,7,@start_registration)
GROUP BY [DeviceName]
 ) AS L7 ON L.[DeviceName] = L7.[DeviceName]
LEFT JOIN 
(SELECT [DeviceName]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,7-1,@start_registration) AND DATEADD(mm,7,@start_registration)
GROUP BY [DeviceName]
) AS U7
ON L.[DeviceName] = U7.[DeviceName] 
ORDER BY All_Leavers DESC
