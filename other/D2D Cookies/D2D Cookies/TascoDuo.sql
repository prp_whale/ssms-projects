WITH SOURSES
AS (
	SELECT I.source                      AS 'source' 
	--		,Count (DISTINCT I.player_id) AS Users 
	--		,Sum (P.amount) - Sum (P.tax) AS Net 
	FROM   PRP_D2D_Payments AS P 
			RIGHT JOIN PRP_D2D_Installs AS I 
					ON P.player_id = I.player_id 
	GROUP  BY I.source 
	HAVING Count (DISTINCT I.player_id) > 50 
			OR Sum (P.amount) - Sum (P.tax) > 1000 
)

SELECT I.source                      AS 'source' 
       ,Count (DISTINCT I.player_id) AS Users 
       ,Sum (P.amount) - Sum (P.tax) AS Net 
FROM   PRP_D2D_Payments AS P 
       INNER JOIN (SELECT DISTINCT player_id 
                   FROM   PRP_D2D_Visits 
                   WHERE  date_index > Dateadd(WW, -2, Getdate())) AS V 
               ON P.player_id = V.player_id 
       INNER JOIN PRP_D2D_Installs AS I 
               ON P.player_id = I.player_id 
       INNER JOIN SOURSES AS S 
               ON S.source = I.source 
GROUP  BY I.source 
ORDER  BY I.source 

