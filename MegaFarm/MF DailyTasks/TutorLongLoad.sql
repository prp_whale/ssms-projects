SELEcT Top ( 100) * 
FROM dbo.Tutorial 

SELECT step_id
, count (DISTINCT UserID) AS Users
, count (*) AS Trans 
FROM dbo.Tutorial 
GROUP BY step_id
ORDER BY step_id

SELECT tutorial_step_id
, count (DISTINCT UserID) AS Users
, count (*) AS Trans 
FROM dbo.tutor_seq 
GROUP BY tutorial_step_id
ORDER BY tutorial_step_id

SELECT TOP (100) UserID
, Count(*) AS F
FROM dbo.tutorial
GROUP BY UserId
ORDER BY F DESC

SELEcT 
    distinct  PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY LTV30D) OVER() as median_cont
From AutumnUsers 

SELECT 
    DATEDIFF(mi,TIMESTAMP, LEAD(Timestamp) OVER(PARTITION BY UserId ORDER BY Level_id) ) AS 'MinuteToLvl'
FROM dbo.tutorial


SELECT TOP(200) UserId, step_id
, LEAD(step_id, 1) OVER(PARTITION BY UserId ORDER BY Timestamp)  AS "NextStep"
, DATEDIFF(ss,TIMESTAMP, LEAD(Timestamp) OVER(PARTITION BY UserId ORDER BY Timestamp) ) AS 'SecondsTimeStamp'
, DATEDIFF(ss,ClientTime, LEAD(ClientTime) OVER(PARTITION BY UserId ORDER BY Timestamp) ) AS 'SecondsClientV'
FROM dbo.Tutorial
WHERE UserId = '116976'
--AND  step_id bEtween '150' AND '160'

ORDER BY [Timestamp]