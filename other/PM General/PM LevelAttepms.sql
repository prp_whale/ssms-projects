DECLARE @cv NVARCHAR(10) = '1.17';

SELECT 
LevelId
, Count(*) AS 'ALL'
, (SELECT Count(*) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND Completed = 1 AND ClientVersion IN ('1.16', '1.17') ) AS Sucsess
, (SELECT Count(*) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND CompletedType = 0 AND ClientVersion IN ('1.16', '1.17') ) AS Lose
, (SELECT Count(*) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND CompletedType = 2 AND ClientVersion IN ('1.16', '1.17')  ) AS LeftMenu
, (SELECT Count(*) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND CompletedType = 3 AND ClientVersion IN ('1.16', '1.17') ) AS LeftGame
, (SELECT Count(*) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND AddSteps > 0  AND ClientVersion IN ('1.16', '1.17') ) AS SpendCoinsTimes
--,  1.0*Sum(CoinsSpent)/Count(*) AS SpendCoins
FROM EventsLevelAttempts AS ela
WHERE ClientVersion IN ('1.16', '1.17') -- 17
AND LevelId <= 30
GROUP BY LevelId
ORDER BY LevelID


-- SELECT 
-- LevelId
-- , Count(distinct UserID) AS 'ALL'
-- , (SELECT Count(distinct UserID) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND Completed = 1 AND ClientVersion IN ('1.16', '1.17') ) AS Sucsess
-- , (SELECT Count(distinct UserID) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND CompletedType = 0 AND ClientVersion IN ('1.16', '1.17') ) AS Lose
-- , (SELECT Count(distinct UserID) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND CompletedType = 2 AND ClientVersion IN ('1.16', '1.17')  ) AS LeftMenu
-- , (SELECT Count(distinct UserID) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND CompletedType = 3 AND ClientVersion IN ('1.16', '1.17') ) AS LeftGame
-- , (SELECT Count(distinct UserID) FROM EventsLevelAttempts WHERE LevelId = ela.LevelId AND AddSteps > 0  AND ClientVersion IN ('1.16', '1.17') ) AS SpendCoinsTimes
-- --,  1.0*Sum(CoinsSpent)/Count(distinct UserID) AS SpendCoins
-- FROM EventsLevelAttempts AS ela
-- WHERE ClientVersion IN ('1.16', '1.17') -- 17
-- AND LevelId <= 30
-- GROUP BY LevelId
-- ORDER BY LevelID
