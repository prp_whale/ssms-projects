CREATE FUNCTION dbo.Test_Funk(@MediaSource as nvarchar(30))
RETURNS nvarchar(100)
AS  BEGIN 
    SELECT Top(1) UserID 
    FROM dbo.[Users] 
    WHERE MediaSource = @MediaSource 
    ORDER BY TIMESTAMP Desc

    RETURN UserId
END 

DECLARE @startperiod datetime  = '2019-06-17 00:00:00.000'
SELECT @startperiod+7 