DECLARE @start_period DATETIME = '2019-07-04 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-07-08 23:59:59.997'; 

--SET @start_period  =  DATEADD (WW, -1, @start_period); 
--SET @end_period =  DATEADD (WW, -1, @end_period); 

--SELECT [LevelId]
--, Count (DISTINCT [UserId]) AS Users 
--, Sum ([Gross]) AS Gross
-- FROM [dbo].[EventsLevelAttempts] AS ELA
--	INNER JOIN  [dbo].PRP_TOTAL_Pool_Of_Active_Users AS POU 
----		INNER JOIN  [dbo].PRP_TOTAL_Pool_Of_Active_USA_Users AS POU 
--		 ON ELA.[UserId] = POU.U_ID 
--WHERE [Timestamp] BETWEEN @start_period AND @end_period
--AND [TypeId] = 8 
--GROUP BY [LevelId]
--ORDER BY [LevelId]

--SELECT TOP (100) * 
--FROM [dbo].[EventsLevelAttempts] AS ELA
--	INNER JOIN  [dbo].PRP_TOTAL_Pool_Of_Active_USA_Users AS POU 
--		 ON ELA.[UserId] = POU.U_ID 
--WHERE [TypeId] = 8 
--ORDER BY [Timestamp] DESC


--SELECT cast ([Timestamp] as Date) as 'DAY'
--, Count (DISTINCT [UserId]) AS Users 
--	FROM [dbo].[EventsLevelAttempts] AS ELA
--	INNER JOIN  [dbo].PRP_TOTAL_Pool_Of_Active_USA_Users AS POU 
--		 ON ELA.[UserId] = POU.U_ID 
--		 WHERE [Timestamp] BETWEEN @start_period AND @end_period
--		 AND [TypeId] = 8 
--GROUP BY cast ([Timestamp] as Date)
--ORDER BY cast ([Timestamp] as Date)


SELECT  count (distinct [UserId]) as Users
FROM [dbo].[EventsAppOpen] as AO
	INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
--	INNER JOIN  [dbo].PRP_TOTAL_Pool_Of_Active_USA_Users AS POU 
		ON AO.[UserId] = POU.U_ID
WHERE [Timestamp] BETWEEN @start_period AND @end_period
AND [ClientVersion] BETWEEN '1.14.0' AND  '1.19.99' 
--AND [ClientVersion] = '1.14.0'