	  DECLARE @start_date Datetime = '2019-02-25 00:00:00.000'; 
DECLARE @end_date Datetime = '2019-03-06 23:59:59.999'; 
DECLARE @cheater Int = 7249; 
DECLARE @test_country Varchar(2) = 'UA' 
DECLARE @source Varchar(25) = 'level_up' 

	  SELECT * 
FROM   [dbo].[currency_received]  AS U 
       INNER JOIN (
	   SELECT [UserId]    AS U_ID 
                          , Count (*) AS TRANSACTIONS 
                   FROM   [dbo].[currency_received] 
                   WHERE  [RegistrationCountry] != @test_country 
                      AND [RegistrationCountry] != 'RU' 
                      AND [UserID] != @cheater 
                      AND [timestamp] BETWEEN @start_date AND @end_date 
                      AND [source_type_id] = @source 
                      AND [coins] != 0 
                      AND [level_id] = 4 
                   GROUP  BY [UserId] 
                   HAVING Count (*) > 1 
                  --ORDER BY TRANSACTIONS desc  
                  ) AS SQ 
               ON SQ.U_ID = U.[UserId] 
			   WHERE  [source_type_id] = @source
			    AND [coins] != 0 
			   ORDER BY [UserId],[Timestamp],[level_id]