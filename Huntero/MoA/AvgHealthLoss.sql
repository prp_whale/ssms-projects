declare @periodstart nvarchar(20) = '2020-01-04 00:00:00';
declare @periodend nvarchar (20) = '2020-01-13 23:59:59';
--SET  @periodend = DATEADD(hh, -6, SYSDATETIME())

DECLARE @WorldID  int = 1 -- 1, 2, 3, 4
DECLARE @ClientVer nvarchar (20) = '0.0.79'

;WITH HealthPoint
    AS
    (
        SELECT rri.[UserID]
            , WorldId
            , RoomNumber
            , healing
            , HealthPoints AS Health
            , registrationclientversion AS version
            , timestamp
            , roomname
            , LEAD (HealthPoints) OVER (PARTITION  BY rri.UserID, WorldId, RunNumber ORDER BY RoomNumber) AS HealthAfter
        FROM RunRoomInfo AS rri
        JOIN users  
         ON users.userid = rri.userid 
WHERE  worldid = @WorldID 
   AND registrationclientversion = @ClientVer 
   AND timestamp BETWEEN @periodstart AND @periodend 

    )
,HealthScore 
AS
( 
    SELECT UserID
        , RoomNumber
        , health
        , Health - healthafter AS HpLoss
    FROM HealthPoint
    WHERE  HealthAfter IS NOT NULL 
)


SELECT roomnumber
    , count(DISTINCT userid) Users
    , count (userid) Attempts
    , avg(health) AvgHp
    , -1*avg(hploss) AvgHPLoss
FROM HealthScore
WHERE hploss BETWEEN -500 AND 5000 
    AND roomnumber != 0  AND health < 9999
GROUP BY roomnumber
ORDER BY roomnumber
