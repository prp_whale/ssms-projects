--DROP TABLE IF EXISTS PRP_OpenToPurchase_March_May;
--DROP TABLE IF EXISTS PRP_AutoOpenToPurchase_March_May;

;WITH FIRST_DEPOSITORS -- ������ ������� �������� �������������
 AS(
		SELECT [UserId] 
			,[is_auto]
		   ,Min ([Timestamp]) AS FistDepositTime 
		FROM   [dbo].[PRP_ShopPurchase_March_May] 
		WHERE  [new_depositor] = 1 
			   --    AND [Platform] = 1 
		--	   AND [is_auto] = 1 
		GROUP  BY [UserId] ,[is_auto]
	)

, SHOP_OPEN_ALL AS 
	( 
	SELECT SO.[UserId] 
			,FD.[is_auto]
		   ,Count (*)               AS opens
	FROM   [dbo].[ev_ShopOpen] AS SO 
		   INNER JOIN FIRST_DEPOSITORS AS FD 
				   ON FD.[UserId] = SO.[UserId] 
					  AND SO.[Timestamp] <= FD.FISTDEPOSITTIME 
--	WHERE  SO.[is_auto] = 0 
	WHERE  SO.[is_auto] = 1 
	GROUP  BY SO.[UserId] ,FD.[is_auto]
	 )



  SELECT *
 -- INTO PRP_OpenToPurchase_March_May
   INTO PRP_AutoOpenToPurchase_March_May
  FROM SHOP_OPEN_ALL

--SELECT Count (*) FROM PRP_OpenToPurchase_March_May
--SELECT Count (*) FROM PRP_AutoOpenToPurchase_March_May


  ---------------------------------------------------------------------------------------
--DROP TABLE IF EXISTS PRP_OpensToPurchase_March_May_Full;
--SELECT AO.[UserId] 
--       ,AO.[is_auto] 
--       ,O.[opens] AS OPENS
--       ,AO.[opens] AS OPENS_AUTO
--INTO   PRP_OpensToPurchase_March_May_Full 
--FROM   PRP_OpenToPurchase_March_May AS O 
--       FULL JOIN PRP_AutoOpenToPurchase_March_May AS AO 
--               ON O.[UserId] = AO.[UserId] 


