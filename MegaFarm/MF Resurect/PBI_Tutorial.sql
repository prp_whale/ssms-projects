/*Туториал
В данной таблице интересуют статистика по прохождения туториалов. Только туториальных! Это значит что имеющиеся шаги в туторе по загрузке (логина, бандлов, сцены) - сделать единичными и неповторяющимися при перезаходе в игру

При построении графика нужно учитывать следующие параметры:
кол-во пользователей начавших туториал (каждый шаг)
кол-во пользователей завершивших туториал (каждый шаг)
возможность выбирать версии и временные промежутки
*/

declare @days int = 28;
​declare @days_multi int = 2;
declare @st_per DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days*@days_multi, GETDATE())));
declare @end_per DATETIME  = DATEADD(ms, -3, CONVERT(datetime, CONVERT(date, GETDATE())));
​
declare @versa_limit int = 100;


WITH Versa
AS (
    SELECT registrationclientversion AS RegVer
    FROM  dbo.Users
    WHERE  created BETWEEN @st_per AND @end_per
    GROUP BY registrationclientversion
    HAVING Count(*) >=  @versa_limit
)


SELECT CONVERT(date, [TimeStamp]) AS 'Date',  ClientVersion, step_id, step_name
, Count(Distinct UserID) AS Userss
FROM Tutorial 
WHERE ClientVersion IN (SELECT * FROM Versa)
AND [Timestamp] BETWEEN @st_per AND @end_per
AnD step_Id BETWEEN 130 AND 500
GROUP BY CONVERT(date, [TimeStamp]) ,  ClientVersion, step_id, step_name
ORDER BY CONVERT(date, [TimeStamp]) ,  ClientVersion, step_id 