DECLARE @startperiod datetime = '2020-02-02 00:00:00.000';
DECLARE   @endperiod datetime = '2020-02-02 23:59:59.997';

SELECT step_id
, step_name
, count (DISTINCT UserID) AS Users
, count (*) AS Trans 
, count (*) /  count (DISTINCT UserID) AS 'Trans by Users'
FROM dbo.Tutorial 
WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
GROUP BY step_id , step_name
ORDER BY step_id