DECLARE @start_period DATETIME = '2019-06-01 00:00:00.000';
DECLARE @end_period DATETIME = '2019-06-14 23:59:59.997';

SET @start_period  = '2019-06-15 00:00:00.000';
SET @end_period  = '2019-06-28 23:59:59.997';

SET @start_period  = '2019-07-01 00:00:00.000';
SET @end_period   = '2019-07-14 23:59:59.997';

SET @start_period  = '2019-03-01 00:00:00.000';
SET @end_period   = '2019-08-01 23:59:59.997';

DECLARE  @UsersTurorialStepSkipers TABLE (UserID INT PRIMARY KEY);
INSERT INTO @UsersTurorialStepSkipers VALUES (8690),	(19244),	(16677),	(21231),	(9314),	(8139),	(16913),	(12033),	(15092),	(8492),	(10685),	(16446),	(19346),	(10191),	(21119),	(10227),	(18345),	(21011),	(16466),	(21135),	(17762),	(8899),	(14980),	(8024),	(10634),	(12777),	(13799),	(16013),	(18049),	(18133),	(18350),	(18799),	(19144),	(20270),	(21659),	(10673),	(12443),	(12510),	(12715),	(12928),	(15141),	(17070),	(20984),	(21205),	(21270),	(7716),	(8911),	(8956),	(10616),	(11393),	(12410),	(12453),	(15595),	(16414),	(16845),	(17184),	(17692),	(17809),	(18243),	(18473),	(18646),	(18693),	(20279),	(21078),	(21203),	(7885),	(8216),	(8972),	(10687),	(10756),	(10783),	(12407),	(12621),	(17026),	(17676),	(17916),	(19703),	(21018),	(21166),	(10643),	(10650),	(10682),	(10742),	(10759),	(10770),	(11119),	(11385),	(12395),	(12436),	(12456),	(12472),	(12722),	(13466),	(13731),	(17277),	(17648),	(19795),	(21102),	(21197),	(7719),	(8486),	(8953),	(10644),	(10667),	(10736),	(10764),	(11419),	(11599),	(12416),	(12418),	(12432),	(12486),	(12505),	(12618),	(12700),	(12709),	(12719),	(12748),	(13487),	(13707),	(14787),	(14985),	(17010),	(17675),	(17681),	(18126),	(18357),	(18429),	(18883),	(19409),	(19580),	(19698),	(20121),	(20895),	(21200),	(21387),	(10207),	(10645),	(10659),	(10981),	(12575),	(13554),	(14231),	(16318),	(17061),	(17807),	(19300),	(20706),	(21045),	(7778),	(10205),	(10401),	(10755),	(12007),	(12306),	(12406),	(12440),	(12633),	(12757),	(12956),	(13438),	(13765),	(14440),	(14651),	(16489),	(17646),	(18301),	(18368),	(19704),	(21178);

DECLARE @completed_chapter Varchar(20) = '1_window_quests_open';
DECLARE @not_completed_chapter Varchar(20) = '1_2_avaliable';

--DECLARE @completed_chapter Varchar(20) = '1_2_avaliable';
--DECLARE @not_completed_chapter Varchar(20) = '2_3_avaliable';

SET @completed_chapter  		= '2_3_avaliable';
SET @not_completed_chapter  	= '3_4_avaliable';

--DECLARE @completed_chapter Varchar(20) = '3_4_avaliable';
--DECLARE @not_completed_chapter Varchar(20) = '4_5_avaliable';


--DECLARE @completed_chapter Varchar(20) = '4_5_avaliable';
--DECLARE @not_completed_chapter Varchar(20) = '5_6_avaliable';

--DECLARE @completed_chapter Varchar(20) = '5_6_avaliable';
--DECLARE @not_completed_chapter Varchar(20) = '6_7_avaliable';

--DECLARE @completed_chapter Varchar(20) = '6_7_avaliable';
--DECLARE @not_completed_chapter Varchar(20) = '7_8_avaliable';


SET @completed_chapter  		= '7_8_avaliable';
SET @not_completed_chapter  	= '8_9_avaliable';

--DECLARE @completed_chapter Varchar(20) = '8_9_avaliable';
--DECLARE @not_completed_chapter Varchar(20) = '9_10_avaliable';

--DECLARE @completed_chapter Varchar(20) = '9_10_avaliable';
--DECLARE @not_completed_chapter Varchar(20) = '10_tutorial_end';

--DECLARE @completed_chapter Varchar(25) = '8_sea_map_open';
--DECLARE @not_completed_chapter Varchar(25) = '8_window_village_open';

--DECLARE @completed_chapter Varchar(25) = '1_window_buildings_list_open';
--DECLARE @not_completed_chapter Varchar(25) = '1_window_Farm_build_open';

-- DECLARE @completed_chapter Varchar(30) = '8_island_open';
-- DECLARE @not_completed_chapter Varchar(30) = '9_Hospital_0_improvement_start';

WITH POOL_OF_USERS 
AS ( 
		SELECT U.[UserId] AS ID 
		FROM   [dbo].[Users] AS U 
			   LEFT JOIN @UsersTurorialStepSkipers AS UTSS 
					  ON U.[UserId] = UTSS.UserID
		WHERE 
			UTSS.UserID IS NULL AND
		    U.[Created] BETWEEN @start_period AND @end_period 
	) 

, USERS_STEPS
AS (
	SELECT T.[UserId]  AS U_ID 
				,[step_key] AS T_STEP 
	FROM   [dbo].[Tutorial] AS T 
				INNER JOIN POOL_OF_USERS AS POU 
								ON T.[UserId] = POU.ID 
	)

, CERTAIN_CHAPTER
AS (
	SELECT U1.U_ID 
				,U1.T_STEP 
	FROM   USERS_STEPS AS U1 
				LEFT JOIN (SELECT U_ID 
										FROM   USERS_STEPS 
										WHERE  T_STEP = @not_completed_chapter) AS U2 
								ON U1.U_ID = U2.U_ID 
	WHERE  U2.U_ID IS NULL 
		AND U1.T_STEP = @completed_chapter 
	
)

-- SELECT DISTINCT U_ID FROM CERTAIN_CHAPTER
-- ORDER BY U_ID

-- SELECT Count (distinct U_ID ) AS USERS
-- , Count(*) AS EVENTSS
-- FROM CERTAIN_CHAPTER


SELECT US.T_STEP
	, Count(DISTINCT US.U_ID) AS USERS
FROM USERS_STEPS AS US 
	INNER JOIN CERTAIN_CHAPTER AS CC ON US.U_ID = CC.U_ID
--WHERE US.T_STEP LIKE '1\_%' escape '\'
--WHERE US.T_STEP LIKE '3\_%' escape '\' OR US.T_STEP = '2_3_avaliable'
WHERE US.T_STEP LIKE '8\_%' escape '\' OR US.T_STEP = '7_8_avaliable'
GROUP BY US.T_STEP 
ORDER BY USERS DESC