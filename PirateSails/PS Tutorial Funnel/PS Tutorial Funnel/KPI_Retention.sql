DECLARE @start_period DATETIME = '2019-06-01 00:00:00.000';
DECLARE @end_period DATETIME = '2019-06-14 23:59:59.997';

--SET @start_period  = '2019-06-15 00:00:00.000';
--SET @end_period  = '2019-06-28 23:59:59.997';

SET @start_period  = '2019-07-01 00:00:00.000';
SET @end_period   = '2019-07-14 23:59:59.997';

-- SET @start_period  = '2019-07-14 00:00:00.000';
-- SET @end_period   = '2019-07-28 23:59:59.997';

SET @start_period  = '2019-08-01 00:00:00.000';
SET @end_period   = '2019-08-14 23:59:59.997';


SELECT Count (DISTINCT [UserId]) AS 'Registration'
,Sum(Cast([Retention1D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R1'
,Sum(Cast([Retention2D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R2'
,Sum(Cast([Retention3D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R3'
,Sum(Cast([Retention4D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R4'
,Sum(Cast([Retention5D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R5'
,Sum(Cast([Retention6D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R6'
,Sum(Cast([Retention7D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R7'
,Sum(Cast([Retention8D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R8'
,Sum(Cast([Retention9D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R9'
,Sum(Cast([Retention10D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R10'
,Sum(Cast([Retention11D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R11'
,Sum(Cast([Retention12D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R12'
,Sum(Cast([Retention13D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R13'
,Sum(Cast([Retention14D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R14'
,Sum(Cast([Retention15D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R15'
,Sum(Cast([Retention16D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R16'
,Sum(Cast([Retention17D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R17'
,Sum(Cast([Retention18D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R18'
,Sum(Cast([Retention19D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R19'
,Sum(Cast([Retention20D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R20'
,Sum(Cast([Retention21D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R21'
,Sum(Cast([Retention22D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R22'
,Sum(Cast([Retention23D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R23'
,Sum(Cast([Retention24D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R24'
,Sum(Cast([Retention25D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R25'
,Sum(Cast([Retention26D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R26'
,Sum(Cast([Retention27D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R27'
,Sum(Cast([Retention28D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R28'
,Sum(Cast([Retention29D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R29'
,Sum(Cast([Retention30D] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R30'
,Sum(Cast([Retention2M] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R2M'
,Sum(Cast([Retention4M] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R4M'
,Sum(Cast([Retention6M] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R6M'
,Sum(Cast([Retention1Y] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R1Y'
,Sum(Cast([Retention2Y] AS INT))*1.0 / Count (DISTINCT [UserId]) AS 'R2Y'
FROM [dbo].[Users] 
WHERE  [Created] BETWEEN @start_period AND @end_period 
AND [Platform] = 1