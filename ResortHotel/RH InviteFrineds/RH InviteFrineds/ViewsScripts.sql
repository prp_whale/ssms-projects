--[dbo].[ev_friendies] 
SELECT UserId 
       ,Timestamp 
       ,ClientTime 
       ,BackOfficeTime 
       ,ClientVersion 
       ,Int1    AS amount 
       ,String1 AS action_id 
       ,String2 AS spent_source 
	   ,String3 AS spent_source_id 
FROM    dbo.Events  
WHERE  ( EventId = 77 ) 

--[dbo].[ev_InvitationFriends] 
SELECT UserId 
       ,Timestamp 
       ,ClientTime 
       ,BackOfficeTime 
       ,ClientVersion 
       ,String1 AS UserIdInvitationSender 
FROM    dbo.Events  
WHERE  ( EventId = 76 ) 

--[dbo].[ev_InvitationAction] 
SELECT UserId 
       ,Timestamp 
       ,ClientTime 
       ,BackOfficeTime 
       ,ClientVersion 
       ,Int1 AS ActionId 
FROM    dbo.Events  
WHERE  ( EventId = 75 ) 

--[dbo].[ev_FriendsAction] 
SELECT UserId 
       ,Timestamp 
       ,ClientTime 
       ,BackOfficeTime 
       ,ClientVersion 
        ,Int1 AS IsDepositor
        ,Int2 AS FriendLevelId
        ,Int3 AS Balance1
        ,Int4 AS Balance2
        ,Int5 AS BundleVer
        ,Int6 AS ActiveWindow
        ,Int7 AS LevelId
        ,Int8 AS Balance3
        ,String1 AS FriendUserId
        ,String2 AS ActionId
FROM    dbo.Events  
WHERE  ( EventId = 125 ) 

--[dbo].[ev_FriendsWindow] 
SELECT UserId 
       ,Timestamp 
       ,ClientTime 
       ,BackOfficeTime 
       ,ClientVersion 
       ,Int1 AS BundleVer
        ,Int2 AS ActiveWindow
        ,Int3 AS IsDepositor
        ,Int4 AS CurrentFriendsAmount
        ,Int5 AS LevelId
        ,String1 AS ActionId
FROM    dbo.Events  
WHERE  ( EventId = 123 ) 