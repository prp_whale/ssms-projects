DECLARE @start_period DATETIME = '2019-03-09 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-05-09 23:59:59.997'; 

--DECLARE @first_offer varchar (40) = 'Happy_Holidays_Offer_Pack_4_99';	DECLARE @second_offer varchar (40) = 'Happy_Holidays_Offer_Pack_7_99';
--DECLARE @first_offer varchar (40) = 'Happy_Holidays_Offer_Pack_19_99';	DECLARE @second_offer varchar (40) = 'Happy_Holidays_Offer_Pack_31_99';
--DECLARE @first_offer varchar (40) = 'Happy_Holidays_Offer_Pack_9_99';	DECLARE @second_offer varchar (40) = 'Happy_Holidays_Offer_Pack_15_99';
--DECLARE @first_offer varchar (40) = 'Happy_Holidays_Offer_Pack_2_99';	DECLARE @second_offer varchar (40) = 'Happy_Holidays_Offer_Pack_4_99';
DECLARE @first_offer varchar (40) = 'Happy_Holidays_Offer_Pack_1_99';	DECLARE @second_offer varchar (40) = 'Happy_Holidays_Offer_Pack_2_99';




--SELECT [segment_1]  , [segment_3]
--       ,Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)                 AS Amt 
--	 --  ,Count ( DISTINCT [purchase_name])
--FROM   [dbo].[ev_OfferPurchase]   AS OSU 
--       INNER JOIN PRP_Pool_Of_USERS AS POU 
--               ON OSU.[UserId] = POU.U_ID 
--WHERE  [timestamp] BETWEEN @start_period AND @end_period 
--	AND [result] = 'OK'
--	AND  [offers_showed_1] = @first_offer 
--	AND [offers_showed_2] = @second_offer
--	AND [purchase_name] = @first_offer
----	AND [purchase_name] = @second_offer
--GROUP  BY [segment_1]  , [segment_3]
--ORDER  BY [segment_1]  , [segment_3]


SELECT Count (DISTINCT [UserId]) AS Users 
       ,Count (*)                 AS Amt 
FROM   [dbo].[ev_OfferPurchase]   AS OSU 
       INNER JOIN PRP_Pool_Of_USERS AS POU 
               ON OSU.[UserId] = POU.U_ID 
WHERE  [timestamp] BETWEEN @start_period AND @end_period 
	AND [result] = 'OK'
	AND  [offers_showed_1] = @first_offer 
	AND [offers_showed_2] = @second_offer
	--AND ([purchase_name] = @first_offer OR [purchase_name] = @second_offer)
	--AND [purchase_name] = @first_offer
	AND [purchase_name] = @second_offer



--SELECT [purchase_name]
--	  ,Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)                 AS Amt 
--	   FROM   [dbo].[ev_OfferPurchase]   AS OSU 
--       INNER JOIN PRP_Pool_Of_USERS AS POU 
--               ON OSU.[UserId] = POU.U_ID 
--WHERE  [timestamp] BETWEEN @start_period AND @end_period 
--	AND  [offers_showed_1] = @first_offer 
--	AND [offers_showed_2] = @second_offer
--	AND [segment_1] = 1005
--	AND [segment_2] = 1117
--	AND [segment_3] = 1321
--GROUP  BY [purchase_name]