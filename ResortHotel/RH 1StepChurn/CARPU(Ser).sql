DECLARE @DateFrom datetime = '2019-03-01 00:00:00'; --������ ���������������� �������
DECLARE @DateTo datetime = '2019-09-16 23:59:59'; -- ����� ���������������� �������

SELECT [RegistrationClientVersion] as ClientVersion
		,COUNT(*)
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 1, IIF([LTV1D] > 0, 1.0, 0.0), null)) as 'RetentionDay1'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 3, IIF([LTV3D] > 0, 1.0, 0.0), null)) as 'RetentionDay3'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 7, IIF([LTV7D] > 0, 1.0, 0.0), null)) as 'RetentionDay7'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 14, IIF([LTV14D] > 0, 1.0, 0.0), null)) as 'RetentionDay14'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 30, IIF([LTV30D] > 0, 1.0, 0.0), null)) as 'RetentionDay30'


FROM [dbo].[Users] WHERE [Created] BETWEEN @DateFrom AND @DateTo 
AND [RegistrationCountry] IN ('GB', 'DE', 'FR') 
--AND [RegistrationCountry] IN ('US') 

AND [RegistrationClientVersion] IN ('1.4.7', '1.4.8', '1.4.9', '1.4.10', '1.4.11', '1.4.12', '1.4.13') AND [MediaSource] IS NULL

GROUP BY [RegistrationClientVersion]
ORDER BY [RegistrationClientVersion]

