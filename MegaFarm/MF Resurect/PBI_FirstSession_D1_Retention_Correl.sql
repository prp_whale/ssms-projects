declare @days int = 28;

declare @days_multi int = 1;
declare @st_per DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days*@days_multi, GETDATE())));
declare @end_per DATETIME  = DATEADD(ms, -3, CONVERT(datetime, CONVERT(date, GETDATE())));

WITH POU AS (
    SELECT UserID AS U_ID
    , Created
    FROM dbo.Users
    WHERE Created BETWEEN @st_per AND @end_per
)

, FIRST_Session_ID
AS (


    SELECT * FROM (
    SELECT UserID AS U_ID, SessionId AS S_ID 
        , [TIMESTAMP] AS ss
        , ROW_NUMBER() OVER (PARTITION BY UserID ORDER BY [TIMESTAMP] ASC) AS RN
        FROM Events JOIN PoU ON UserID = U_ID
        WHERE [Timestamp] BETWEEN Created AND DATEADD(hh, 24, Created)
    ) AS sq
    WHERE RN = 1 
)

, FS_Length
AS (
    SELECT UserID, Convert(date, MIN([Timestamp])) AS InstallDay
    , DATEDIFF(ss, MIN([Timestamp]), MAX([Timestamp])) AS SessionLength
    , ClientVersion
    FROM Events JOIN FIRST_Session_ID ON UserID = U_ID AND SessionId = S_ID
    GROUP BY UserId, SessionId, ClientVersion

)
, D1_Retention 
AS (
    SELECT UserID 
    , Convert(Date, MIN(ss)) AS InstallDay
    FROM EventsAppOpen JOIN FIRST_Session_ID 
        ON UserID = U_ID AND [Timestamp] 
        BETWEEN DATEADD(hh,24, ss) AND DATEADD(hh,48, ss) 
    GROUP BY UserId  
)

, D1_Roll_Retention 
AS (
    SELECT UserID 
    , Convert(Date, MIN(ss)) AS InstallDay
    FROM EventsAppOpen JOIN FIRST_Session_ID 
        ON UserID = U_ID AND [Timestamp] 
        >= DATEADD(hh,24, ss) 
    GROUP BY UserId  
)


SELECT SessionLength , FS.InstallDay, ClientVersion 
, Count(FS.UserID) AS Installs
, COUNT(D1.UserId) AS D1_Roll
, COUNT(D2.UserId) AS D1_Ret
FROM FS_Length AS FS 
    LEFT JOIN D1_Roll_Retention AS D1 
    ON FS.UserId = D1.UserId AND FS.InstallDay = D1.InstallDay 
    LEFT JOIN D1_Retention AS D2
    ON FS.UserId = D2.UserId AND FS.InstallDay = D2.InstallDay 

GROUP BY SessionLength  , FS.InstallDay , ClientVersion
ORDER BY InstallDay, SessionLength

