declare @start_date datetime	= '2019-02-11 00:00:00';
declare @end_date datetime		= '2019-02-24 23:59:59';

SELECT  [level_id] as lvl
		,sum([cash]) as TOTAL_CASH_SPENT
	--	, SUM	([coins]) as TOTAL_COINS_SPENT
		, COUNT (DISTINCT [UserID]) as USER_ON_LVL
		, COUNT (*) as TRANSACTIONS
  FROM [dbo].[currency_spent]
  WHERE   [RegistrationCountry] != 'UA'
  AND [timestamp] between @start_date and @end_date
  --AND [coins] != 0
  AND [cash] != 0
  GROUP BY [level_id]
  ORDER BY lvl