DECLARE @start_period DATETIME = '2019-09-01 00:00:00.000';
DECLARE @end_period DATETIME = '2019-09-08 23:59:59.997';

-- SET @start_period  = '2019-06-15 00:00:00.000';
-- SET @end_period  = '2019-06-28 23:59:59.997';

-- SET @start_period  = '2019-07-01 00:00:00.000';
-- SET @end_period   = '2019-07-14 23:59:59.997';

DECLARE @ms1 AS NVARCHAR(30) = 'unityads_int';
DECLARE @ms2 AS NVARCHAR(30) = 'NULL';
DECLARE @ms3 AS NVARCHAR(30) = 'Facebook Ads';
DECLARE @ms4 AS NVARCHAR(30) = 'gurutraff_int';
DECLARE @ms5 AS NVARCHAR(30) = 'googleadwords_int';
DECLARE @ms6 AS NVARCHAR(30) = 'ironsource_int';


SELECT cast([Created] as Date) AS 'Day'
, (Select Count(UserId) FROM   dbo.[Users]  AS U WHERE  cast(U.[Created] as Date) =  cast(US.[Created] as Date) AND [MediaSource] = @ms1 ) AS 'unityads_int'
, (Select Count(UserId) FROM   dbo.[Users]  AS U WHERE  cast(U.[Created] as Date) =  cast(US.[Created] as Date) AND [MediaSource] IS NULL ) AS 'organic'
, (Select Count(UserId) FROM   dbo.[Users]  AS U WHERE  cast(U.[Created] as Date) =  cast(US.[Created] as Date) AND [MediaSource] = @ms3 ) AS 'Facebook Ads'
, (Select Count(UserId) FROM   dbo.[Users]  AS U WHERE  cast(U.[Created] as Date) =  cast(US.[Created] as Date) AND [MediaSource] = @ms4 ) AS 'gurutraff_int'
, (Select Count(UserId) FROM   dbo.[Users]  AS U WHERE  cast(U.[Created] as Date) =  cast(US.[Created] as Date) AND [MediaSource] = @ms5 ) AS 'googleadwords_int'
, (Select Count(UserId) FROM   dbo.[Users]  AS U WHERE  cast(U.[Created] as Date) =  cast(US.[Created] as Date) AND [MediaSource] = @ms6 ) AS 'ironsource_int'
FROM   DBO.[Users] AS US 
WHERE [Created] BETWEEN @start_period AND @end_period
GROUP BY cast([Created] as Date)
ORDER BY cast([Created] as Date)





