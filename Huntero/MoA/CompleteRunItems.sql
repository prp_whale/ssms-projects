WITH CTE1 
AS (
    SELECT UserId as U_ID
    , Min([Timestamp]) AS ts
     FROM [dbo].[EquipmentMovement] 
     where triggersourceid='drop' 
     and triggerid  = '6_30' --  (,'2_30','3_50','4_30','5_50','6_30','7_60','8_30') 
     and timestamp between '2020-03-08 00:00:00.000' and '2020-03-22 23:59:59.997'
     GROUP BY UserId
)

SELECT 
triggerid
--,HeroSlotCategoryId
--,equipmentid
,count(distinct userid) as users
,count(userid) as items
,count( userid)*1.0 / count(distinct userid)  AS PP

FROM [dbo].[EquipmentMovement] JOIN CTE1 ON UserId = U_ID AND Timestamp between  TS AND dateadd(ss,3, ts)
where triggersourceid='drop' and triggerid in ('1_50','2_30','3_50','4_30','5_50','6_30','7_60','8_30') 
and timestamp between '2020-03-08 00:00:00.000' and '2020-03-22 23:59:59.997'
group by triggerid--,heroslotcategoryid,equipmentid
order by triggerid--,heroslotcategoryid,equipmentid