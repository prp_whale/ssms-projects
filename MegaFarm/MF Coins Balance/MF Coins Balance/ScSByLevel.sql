DECLARE @start_period DATETIME = '2019-07-01 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-08-01 23:59:59.997'; 

SELECT  [source_type_id] as SOURCE
		, SUM	([coins]) as COINS
		, COUNT (DISTINCT [UserId]) as USERS_ON_LVL
		, COUNT (*) as TRANSACTIONS
  FROM [dbo].[currency_spent]
  WHERE   [RegistrationCountry] != 'UA'
   AND [timestamp] between @start_period and @end_period
   AND [coins] != 0
   -- 'AND [Level_id] = 21
 -- AND [Level_id] BETWEEN 1 AND 3
  GROUP BY [source_type_id] 
  --ORDER BY  TOTAL_COINS DESC
  ORDER BY SOURCE