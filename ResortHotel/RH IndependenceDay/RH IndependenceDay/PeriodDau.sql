DECLARE @start_period DATETIME = '2019-06-01 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-07-08 23:59:59.997'; 

SELECT Cast ([Timestamp] AS DATE) AS 'DAY' 
       ,Count (DISTINCT [UserId]) AS Users 
       ,Count (*)                 AS Amount 
FROM [dbo].[EventsAppOpen] as AO 
       INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
               ON AO.[UserId] = POU.U_ID 
WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
       --AND [ClientVersion] BETWEEN '1.14.0' AND  '1.19.99' 
GROUP  BY Cast ([Timestamp] AS DATE) 
ORDER  BY Cast ([Timestamp] AS DATE) 


--SELECT  count (distinct [UserId]) as Users
--FROM [dbo].[EventsAppOpen] as AO
--	INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU
--		ON AO.[UserId] = POU.U_ID
--WHERE [Timestamp] BETWEEN @start_period AND @end_period
--AND [ClientVersion] BETWEEN '1.14.0' AND  '1.19.99'