DECLARE @start_period DATETIME = '2019-03-09 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-05-09 23:59:59.997'; 
  
--SELECT [offers_showed_1] 
--       ,[offers_showed_2] 
--    --   ,Min ([segment_2]) AS mi 
--    --   ,Max ([segment_2]) AS ma 
--	   , count (distinct [UserId]) AS Users
--	   , count (*) as Amt
--FROM   [dbo].[ev_OfferShowUp] AS OSU
--INNER JOIN PRP_Pool_Of_USERS AS POU 
--               ON OSU.[UserId] = POU.U_ID 
--WHERE  [timestamp] BETWEEN @start_period AND @end_period 
--GROUP BY [offers_showed_1] , [offers_showed_2]
--ORDER BY [offers_showed_1] , [offers_showed_2]
--HAVING Min ([segment_3])> 0



SELECT [offers_showed_1] 
       ,[offers_showed_2] 
       ,Count (DISTINCT [UserId]) AS Users 
       ,Count (*)                 AS Amt 
FROM   [dbo].[ev_OfferPurchase]   AS OSU 
       INNER JOIN PRP_Pool_Of_USERS AS POU 
               ON OSU.[UserId] = POU.U_ID 
WHERE  [timestamp] BETWEEN @start_period AND @end_period 
		AND [result] = 'OK'
		AND ([purchase_name] = [offers_showed_1] OR [purchase_name] = [offers_showed_2])
GROUP  BY [offers_showed_1] ,[offers_showed_2]
ORDER  BY [offers_showed_1] ,[offers_showed_2]  
  



--SELECT [segment_3] 
--       ,[segment_1] 
--	   , MIN ([offers_showed_1] )
--	    , MAX ([offers_showed_1] )
--       ,Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)                 AS Amt 
--FROM   [dbo].[ev_OfferShowUp] AS OSU 
--       INNER JOIN PRP_Pool_Of_USERS AS POU 
--               ON OSU.[UserId] = POU.U_ID 
--WHERE  [timestamp] BETWEEN @start_period AND @end_period 
--GROUP  BY [segment_3] 
--          ,[segment_1] 
--ORDER  BY Amt desc


--SELECT count (distinct [UserId]) AS Users
--	   , count (*) as Amt
--FROM   [dbo].[ev_OfferShowUp] AS OSU
--INNER JOIN PRP_Pool_Of_USERS AS POU 
--               ON OSU.[UserId] = POU.U_ID 
--WHERE  [timestamp] BETWEEN @start_period AND @end_period 


SELECT count (distinct [UserId]) AS Users
	   , count (*) as Amt
FROM   [dbo].[ev_OfferPurchase]   AS OSU 
INNER JOIN PRP_Pool_Of_USERS AS POU 
               ON OSU.[UserId] = POU.U_ID 
WHERE  [timestamp] BETWEEN @start_period AND @end_period 
	AND [result] = 'OK'
	AND ([purchase_name] = [offers_showed_1] OR [purchase_name] = [offers_showed_2])