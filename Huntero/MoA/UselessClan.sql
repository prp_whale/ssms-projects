DECLARE @st_per DATETIME = '2020-04-01 00:00:00.000';
DECLARE @end_per DATETIME =  '2020-04-07 23:59:59.997'; --22

WITH PoU
AS (
    SELECT DISTINCT UserId AS U_ID
    FROM dbo.Users 
    WHERE RegistrationClientVersion IN ('1.0.217','1.0.302')
)
, Complete_W1 
AS (
    SELECT DISTINCT UserID 
    FROM FinishRun  INNER JOIN PoU ON USERID = U_ID
    AND RoomNumber = '-1'
    AND WorldId = '1'
)

, AnyClanAction 
AS (
    SELECT UserID AS U_ID
    , Max([Timestamp]) AS ts
    FROM ClanAction INNER JOIN PoU ON USERID = U_ID
    GROUP BY UserId
 --   WHERE [Timestamp]  
)
, CurrenInCLan
AS (
    SELECT DISTINCT UserID
     FROM ClanAction INNER JOIN AnyClanAction ON UserId = U_ID AND [Timestamp] = ts
     AND [Status] != 2
 )



SELECT Count(*) AS Users
, (SELECT Count(*) FROM Complete_W1 ) AS Complete_W1
, (SELECT Count(*) FROM ClanAction ) AS AnyClanAction
, (SELECT Count(*) FROM CurrenInCLan ) AS CurrenInCLan
FROM PoU

--SELECT Count(DISTINCT UserID) FROM ClanAction