SELECT [EventId],[dbo].[EventsInfo].[Name], 
       [dbo].[EventsParamsInfo].[Name] AS TypeName, 
       [Type],[TypeNumber] 
FROM   [dbo].[EventsInfo] 
       LEFT JOIN [dbo].[EventsParamsInfo] 
              ON [dbo].[EventsInfo].[Id] = [dbo].[EventsParamsInfo].[EventId] 
ORDER BY id,[type],typenumber 
-- [name], [type], typenumber

-- select TOP (100) *
-- from [events]
-- where UserID = 53378
-- ​ORDER BY TIMESTAMP DESC
