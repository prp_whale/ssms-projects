DECLARE @st_per DATETIME = '2020-03-08 00:00:00.000';
DECLARE @end_per DATETIME =  '2020-03-22 23:59:59.997'; --22

WITH CTE1 
AS (
    SELECT UserId
        , WorldId AS LocationId
        , RunNumber
        , [Timestamp] aS katka_end
    FROM dbo.[Finishrun] 
    WHERE RoomNumber = -1
    AND [Timestamp] BETWEEN @st_per AND @end_per
    and IsLeft = 0 and   roomnumber!=0 and roomname!='-2' and runlevel<=11
),

CTE2 
AS (
    SELECT C.UserId
        , C.LocationId
        , C.RunNumber
        , SR.[Timestamp]  AS katka_start
    FROM dbo.[StartRun] AS SR INNER JOIN CTE1 AS C  ON SR.UserId = C.UserId AND SR.LocationId = C.LocationId AND SR.Runnumber = C.Runnumber 
    WHERE SR.[Timestamp] BETWEEN @st_per AND @end_per 

 
), 

CTE3
AS (
    SELECT C1.UserId
    , C1.LocationId AS 'WorldId'
    , C1.RunNumber 
    , Sum(ProductAmount) AS 'GmePerKatka'
    , Count(*) AS C
      FROM dbo.EventsProductMovement AS EPM
        INNER JOIN CTE1 AS C1 ON EPM.UserID = C1.UserId 
        INNER JOIN CTE2 AS C2 ON C2.UserId = C1.UserId AND C2.LocationId = C1.LocationId AND C2.Runnumber = C1.Runnumber 
        WHERE Action = 'temp_received'
        AND  TriggerSourceId = 'ads_wheel'
        AND  ProductType = 'Gems'
        and timestamp BETWEEN katka_start AND katka_end
        AND ProductAmount < 200
    GROUP BY C1.UserId, C1.LocationId, C1.RunNumber
    HAVING Sum(ProductAmount) BETWEEN 0 AND 500 
)

SELECT WorldId
, Sum(GmePerKatka) AS 'Gems'
, Count( UserId) AS 'Katka'
, Count(DISTINCT UserId) AS 'Users'
, 1.0*Sum(GmePerKatka) / Count( UserId) AS 'Avg'
FROM CTE3
GROUP BY WorldId
ORDER BY WorldId

-- SELECT SR.*
-- , katka_end
-- , DATEDIFF(ss,katka_start,katka_end ) AS 'TIME'
-- FROM CTE2 AS SR 
-- INNER JOIN CTE1 AS C  ON SR.UserId = C.UserId AND SR.LocationId = C.LocationId AND SR.Runnumber = C.Runnumber 
-- WHERE SR.UserId = '707125' 
-- ORDER BY C.LocationId, C.Runnumber


