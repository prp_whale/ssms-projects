DECLARE @start_period DATETIME = '2019-03-10 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-04-10 23:59:59.997'; 


DECLARE @test_country Varchar(2) = 'UA'; 

DECLARE @media_source Varchar(20)  = 'unityads_int';
--DECLARE @media_source Varchar(20)  = 'googleadwords_int';
--DECLARE @media_source Varchar(20)  = 'gurutraff_int';
--DECLARE @media_source Varchar(20)  = 'ironsource_int';

--DECLARE @media_source Varchar(20)  = 'NULL';

WITH POOL_OF_USERS 
AS ( 
		SELECT *
		FROM   [dbo].[Users] AS U 
		WHERE 
		    [Created] BETWEEN @start_period AND @end_period 
		    AND [RegistrationCountry] != @test_country 
			AND [MediaSource] = @media_source
			
			--AND [MediaSource] IS NULL
		  -- AND [LTV] = 0
	) 


SELECT Datepart(DD, created)                              AS 'Day' 
       , Datepart(MM, created)                            AS 'Month' 
       , Datepart(YY, created)                            AS 'Year' 
       , Count(DISTINCT UserId)                           AS 'NewUsers' 
       , Count(Iif(ltv1d > 0, 1, NULL)) * 1.0 / Count(*)  AS [Conversion 1d] 
       , Count(Iif(ltv2d > 0, 1, NULL)) * 1.0 / Count(*)  AS[Conversion 2d] 
       , Count(Iif(ltv7d > 0, 1, NULL)) * 1.0 / Count(*)  AS[Conversion 7d] 
       , Count(Iif(ltv14d > 0, 1, NULL)) * 1.0 / Count(*) AS[Conversion 14d] 
       , Count(Iif(ltv28d > 0, 1, NULL)) * 1.0 / Count(*) AS[Conversion 28d] 
FROM   POOL_OF_USERS 
GROUP  BY Datepart(DD, created) 
          , Datepart(MM, created) 
          , Datepart(YY, created) 

ORDER  BY 'Year' , 'Month', 'Day' 
       