declare @start_date datetime	= '2019-02-11 00:00:00';
declare @end_date datetime		= '2019-02-24 23:59:59';
declare @cheaters int			= 7249

SELECT  [source_type_id] as SOURCE
		, SUM	([cash]) as CASH
		, COUNT (DISTINCT [UserId]) as USERS_ON_LVL
		, COUNT (*) as TRANSACTIONS
  FROM [dbo].[currency_received]
  WHERE   [RegistrationCountry] <> 'UA'
   AND [timestamp] between @start_date and @end_date
   AND [cash] != 0
   AND [UserID] != @cheaters
  GROUP BY [source_type_id]
  ORDER BY SOURCE