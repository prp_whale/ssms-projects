SELECT TOP (100) [UserId] 
                 , Max ([friends]) AS FRIEND_COUNT 
FROM   [dbo].[balance] 
GROUP  BY [UserId] 
ORDER  BY FRIEND_COUNT DESC

SELECT  *
FROM   [dbo].[balance] 
WHERE [UserId] = 4320
ORDER BY [friends], [Timestamp]