/****** ������ ��� ������� SelectTopNRows �� ����� SSMS  ******/
USE PIRATES 

DECLARE @PERIOD_BEGIN NVARCHAR(20) = '2019-04-26 00:00:00'; --������ ���������������� ������� 
DECLARE @PERIOD_END NVARCHAR(20) = '2019-05-01 23:59:59'; -- ����� ���������������� ������� 
DECLARE @TestDevices TABLE (UserID INT PRIMARY KEY); 

insert into @TestDevices values (6), (14), (23), (105), (239), (247), (256), (257), (333), (1331), (1366), (1367), (1368), (1375), (1381), (1382), (3487), (3737), (4244), (4253), (4255), (4265), (4266), (4270), (5517), (5906), (2), (3), (7), (8), (9), (12), (16), (17), (21), (22), (25), (26), (27), (51), (117), (249), (293), (598), (666), (670), (1190), (1192), (1193), (1337), (5568);

SELECT [ProductId] 
       , Count(DISTINCT eventspurchase.UserId) AS Users 
       , Sum ([AmountInUSD])                   AS AmountInUSD 
       , Count (*)                             AS PaymentsCount 
FROM   [dbo].[EventsPurchase] 
       JOIN users 
         ON users.userid = eventspurchase.userid 
WHERE  eventspurchase.UserId NOT IN (SELECT UserID 
                                     FROM   @TestDevices) 
   AND timestamp BETWEEN @PERIOD_BEGIN AND @PERIOD_END 
   AND productid LIKE '%special%' 
--and eventspurchase.created BETWEEN '2019-04-01 00:00:00' AND '2019-04-21 23:59:59') and registrationcountry='ru' 
GROUP  BY productid 
ORDER  BY users DESC 