--------------------------------------------------
-- STEP 1 BELOW
-------------------------------------------------

DECLARE @start_period DATETIME = '2019-03-01 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-08-01 23:59:59.997'; 

DROP TABLE IF EXISTS PRP_Pool_Of_Users;

SELECT DISTINCT [userid] 
INTO   PRP_Pool_Of_Users 
FROM   [dbo].[users] 
WHERE  [created] BETWEEN @start_period AND @end_period 
       AND [registrationcountry] != 'UA' 

-----------------------------------------------------
-- STEP 2  look into Create_Pool_TutorialDuplicators
-----------------------------------------------------
--------------------------------------------------
-- STEP 3 BELOW
-------------------------------------------------

DROP TABLE IF EXISTS PRP_Pool_Of_NoDuplicators;

SELECT DISTINCT POU.[userid] 
INTO   PRP_Pool_Of_NoDuplicators 
FROM   [dbo].[PRP_Pool_Of_Users] AS POU LEFT JOIN PRP_Pool_TutorialDuplicators AS TD 
        ON POU.[UserId] = TD.[UserId]
WHERE  TD.[UserId] IS NULL

------------------------------------------
SELECT Count (*) AS U FROM PRP_Pool_Of_Users
SELECT Count (*) AS U FROM PRP_Pool_Of_NoDuplicators