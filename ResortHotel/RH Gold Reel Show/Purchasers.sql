DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform int,
    @country  VARCHAR(2);
        
    SET @startperiod = '2019-08-15 00:00:00.000'
    SET @endperiod = '2019-11-07 23:59:59.999'
    SET @platform = 1 
    SET @country = 'US'

    
SELECT 
Country
,ProductId
, Count (DISTINCT UserId) AS Users
, Count (*) AS Trans
FROM [dbo].[EventsPurchase]
WHERE ProductId LIKE 'golden_ors_%'
AND Country IN (
'US',
'DE',
'GB',
'JP',
'RU',
'IT',
'KR',
'CA',
'NL',
'FR'
)
--AND Platform = @platform
GROUP BY Country,  ProductId
ORDER BY Country,  ProductId --DESC


-- SELECT TOP (100) * FROM [dbo].[EventsPurchase] WHERE ProductId LIKE 'golden_ors_%' 
-- AND UserID = 5291399	
-- ORDER BY TIMESTAMP desc