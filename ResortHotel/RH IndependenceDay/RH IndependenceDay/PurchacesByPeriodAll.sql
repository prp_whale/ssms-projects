DECLARE @start_period DATETIME = '2019-07-04 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-07-08 23:59:59.997'; 

SET @start_period  =  DATEADD (WW, -2, @start_period); 
SET @end_period =  DATEADD (WW, -2, @end_period)

--SELECT Cast ([Timestamp] AS DATE) AS 'DAY' 
--       ,Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)                 AS Amount 
--FROM [dbo].[EventsPurchase] as AO 
--       INNER JOIN [dbo].[PRP_TOTAL_Pool_Of_Active_Users] AS POU 
--               ON AO.[UserId] = POU.U_ID 
--WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
--       --AND [ClientVersion] BETWEEN '1.14.0' AND  '1.19.99' 
--     --  AND [result] = 'OK'
--	--   AND ( [ProductId] NOT LIKE '%0_99' )
--GROUP  BY Cast ([Timestamp] AS DATE) 
--ORDER  BY Cast ([Timestamp] AS DATE) 


--SELECT [ProductId]          AS Product 
--       ,Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)                 AS Amount 
--FROM   [dbo].[EventsPurchase]
--WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
--   AND [ClientVersion] BETWEEN '1.14.0' AND '1.19.99' 
--GROUP  BY [ProductId]
--ORDER  BY [ProductId]


SELECT [Country] 
       ,Sum ([AmountInUSD])       AS Gross 
       ,Count (DISTINCT [UserId]) Users 
       ,Count (*)                 AS Amount 
FROM   [dbo].[EventsPurchase] 
WHERE  [Timestamp] BETWEEN @start_period AND @end_period 
GROUP  BY [Country] 
ORDER  BY Gross DESC 

