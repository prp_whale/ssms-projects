
--WITH SECOND_PURCHESERS
--AS (
-- SELECT DISTINCT [UserId]
-- , Count(*) as A
-- FROM   [dbo].[PRP_ShopPurchase_March_May] 
-- GROUP  BY [UserId]
-- HAVING Count(*) > 1 
--)

--SELECT COUNT (*) FROM SECOND_PURCHESERS


SELECT [purchase_name]            AS Product 
       ,Count (DISTINCT [UserId]) AS Users 
       ,Count (*)                 AS Amount 
FROM   [dbo].[PRP_ShopPurchase_March_May] 
 --AS SP
 --INNER JOIN SECOND_PURCHESERS AS SS ON  SP.[UserId] = SS.[UserId]
WHERE  [is_auto] = 1 
  -- AND [is_auto] = 1 
   AND [new_depositor] = 0 
GROUP  BY [purchase_name] 
ORDER  BY Amount DESC 


--SELECT COunt (DISTINCT [UserId])
--FROM   [dbo].[PRP_ShopPurchase_March_May] 
--WHERE  [full_shop] = 1 
--  AND [new_depositor] = 1 

SELECT [is_auto]            AS AutoOpen
		,[full_shop]            AS FullShop
       ,Count (DISTINCT [UserId]) AS Users 
       ,Count (*)                 AS Amount 
FROM   [dbo].[PRP_ShopPurchase_March_May] 
WHERE  [new_depositor] = 1
GROUP  BY [is_auto] ,
ORDER  BY Amount DESC 
 

 
--SELECT Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)                 AS Amount 
	  
--FROM   [dbo].[PRP_ShopPurchase_March_May] 
-- WHERE  [new_depositor] = 1

