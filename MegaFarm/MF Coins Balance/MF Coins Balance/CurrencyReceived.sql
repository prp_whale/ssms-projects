declare @start_date datetime	= '2019-02-11 00:00:00';
declare @end_date datetime		= '2019-02-24 23:59:59';

SELECT  [source_type_id] as source_type
		,sum([cash]) as TOTAL_CASH
		,sum([coins]) as TOTAL_COINS
  FROM [dbo].[currency_received]
  WHERE   [RegistrationCountry] != 'UA' 
 -- AND [coins] != 0
  AND [timestamp] between @start_date and @end_date
  GROUP BY [source_type_id]
  ORDER BY TOTAL_CASH DESC , TOTAL_COINS DESC
  --ORDER BY source_type