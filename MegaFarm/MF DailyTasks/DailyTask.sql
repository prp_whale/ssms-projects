declare @periodstart nvarchar(20) = '2020-01-20 00:00:00';
declare @periodend nvarchar (20) = '2020-01-20 23:59:59';
DECLARE @Hours  INT = 24;

SELEcT active_day, action_type
, COunt(Distinct (UserId)) AS U
, COunt (*) AS T
FROM dbo.[daily_tasks]
WHERE status_id = 1
AND [Timestamp] BETWEEN @periodstart AND @periodend
AND Platform = 1
AND [LTV] > 0
GROUP BY  active_day, action_type
ORDER BY active_day, T DESC