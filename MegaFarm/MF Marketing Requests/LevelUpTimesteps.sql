DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform int;

SET @startperiod = '2019-09-01 00:00:00.000';
--SET @startperiod = '2019-02-01 00:00:00.000';
SET @endperiod = '2019-11-01 23:59:59.999';
SET @platform = 1 ;


WITH
    AutumnUsers
    AS
    (
        SELECT UserId  AS U_ID
        FROM dbo.Users
        WHERE Created BETWEEN @startperiod AND @endperiod
           AND [RegistrationCountry] != 'UA'
          AND GameUserId NOT IN (103,	1240,	22,	1064,	0,	1002,	69,	482,	93,	671,	1050,	22,	1028,	647,	1004) -- testers
          AND [LTV] = 0
          AND [MediaSource] IS NOT  NULL
          AND [Platform] = @platform )


, Filtred_U AS (
    SELECT UserId 
    , Level_id
    , MAX(TIMESTAMP) AS Timestamp
    FROM [dbo].[currency_received]  
    AS CT INNER JOIN AutumnUsers AS POU ON CT.[UserId]  = POU.[U_ID]
    WHERE source_type_id = 'level_up'
    GROUP BY UserID, Level_Id
    --ORDER BY UserID, Level_Id 
)

, Level_Steps
AS (
    SELECT  UserId
    , Level_id+1 AS nLevelID
    --,  TIMESTAMP
    -- , LEAD(Timestamp) OVER(PARTITION BY UserId ORDER BY Level_id) as 'next'
    , DATEDIFF(mi,TIMESTAMP, LEAD(Timestamp) OVER(PARTITION BY UserId ORDER BY Level_id) ) AS 'MinuteToLvl'
    from Filtred_U
)

-- SELECT TOP (100) * 
-- FROM Level_Steps
-- WHERE UserId = 46088	
-- ORDER BY UserId , nLevelID

SELECT Distinct nLevelID
, Count(*)  OVER(PARTITION by nLevelID) as Users
, PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY MinuteToLvl) OVER(PARTITION by nLevelID) as Q1_minutes
, PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY MinuteToLvl) OVER(PARTITION by nLevelID) as median_minutes
, PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY MinuteToLvl) OVER(PARTITION by nLevelID) as Q3_minutes
FROM Level_Steps 
ORDER BY nLevelID

-- SELECT nLevelID
-- , Count(*) AS u
-- FROM Level_Steps 
-- GROUP BY nLevelID 
-- ORDER BY nLevelID