DECLARE @DateFrom datetime = '2019-08-01 00:00:00'; 
DECLARE @DateTo datetime = '2019-08-14 23:59:59';

DECLARE @start_period DATETIME = '2019-07-01 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-10-01 23:59:59.997';  -- конец анализированного периода

WITH AttemptsTable as (
	SELECT [UserId] as user_id
			,MAX([LevelId]) as max_level_id

	FROM [dbo].[EventsLevelAttempts]
	WHERE [Timestamp] BETWEEN @DateFrom AND @DateTo AND [TypeId] = 1

	GROUP BY [UserId]
)

SELECT [RegistrationClientVersion] as ClientVersion
		,COUNT(*)
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 1, IIF([Retention1D] = 1, 1.0, 0.0), null)) as 'RetentionDay1'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 3, IIF([Retention3D] = 1, 1.0, 0.0), null)) as 'RetentionDay3'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 7, IIF([Retention7D] = 1, 1.0, 0.0), null)) as 'RetentionDay7'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 14, IIF([Retention14D] = 1, 1.0, 0.0), null)) as 'RetentionDay14'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 30, IIF([Retention30D] = 1, 1.0, 0.0), null)) as 'RetentionDay30'


FROM [dbo].[Users] --JOIN AttemptsTable ON [UserId] = user_id

WHERE [Created] BETWEEN @DateFrom AND @DateTo 
--AND [RegistrationCountry] IN ('GB', 'DE', 'FR') 
--AND [RegistrationCountry] IN ('US') 

AND [Platform] = 1

--AND [RegistrationClientVersion] IN ('1.4.7', '1.4.8', '1.4.9', '1.4.10', '1.4.11', '1.4.12', '1.4.13')
AND [MediaSource] IS NULL

--AND [RamSize] >= 1100

GROUP BY [RegistrationClientVersion]
ORDER BY [RegistrationClientVersion]
