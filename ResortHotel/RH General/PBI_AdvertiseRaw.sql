DECLARE @st_per datetime,
        @end_per datetime

DECLARE @days_ago_st INT = 90;
DECLARE @days_ago_end INT = 0; -- 0 = yesterday

SET @st_per  =  DATEDIFF(dd, @days_ago_st, GETDATE())  ; 
SET @end_per  = DATEADD(ms, -3 ,  DATEDIFF(dd, @days_ago_end, GETDATE()) ) ; 

WITH PoU
AS (
    SELECT AdvertisingID AS Adv_ID
    , MAX(UserId) AS U_ID
    FROM dbo.Users
    WHERE Created BETWEEN @st_per AND @end_per
    GROUP BY AdvertisingID
)


SELECT [Date]
     -- ,[ProjectName]
      ,case when [Platform]=1 then 'Android'
        when [Platform]=2 then 'iOS'
        else '-' end as OS
   --   ,[AdUnit]
      ,[AdvertisingID]
      , [U_ID] AS BOUserID
   --   ,[AdvertisingIDType]
   --   ,[UserID]
   --   ,[Segment]
      ,[Placement]
      ,[AdNetwork]
      ,[Impressions]
      ,[Revenue]
     -- ,[ABTesting]
  FROM [dbo].[ISUserAdRevenue] LEFT JOIN PoU ON AdvertisingID = Adv_ID
WHERE [Date] BETWEEN @st_per AND @end_per
AND [ProjectName] = DB_NAME()

