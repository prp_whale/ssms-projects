----------------------------------------------------------------------------
-----------------  AFPartnersDailyReport -----------------------------------
----------------------------------------------------------------------------
SELECT TOP (1000) [ProjectName]
      ,[Platform]
        ,[Date]
        ,[Agency]
        ,[MediaSource]
        ,[Channel]
        ,[Campaign]
        ,[AfCId]
--      ,[AfAdset]             -- error
        ,[AfAdsetId]
--      ,[AfAd]                -- error
--	    ,[AfAdId]              -- error
        ,[Impressions]
        ,[Clicks]
        ,[CTR]
        ,[Installs]
        ,[CRClickToInstall]
        ,[TotalCost]
        ,[AverageCPI]
--      ,[AfSiteId]             -- error
        ,[DAU]
        ,[GEO]
  FROM [dbo].[AFPartnersDailyReport]


----------------------------------------------------------------------------
-----------------  AFEventsPurchase  ---------------------------------------
----------------------------------------------------------------------------
SELECT TOP (1000) [ProjectName]
      ,[Platform]
      ,[CountryCode]
      ,[AttributedTouchTime]
      ,[InstallTime]
      ,[EventTime]
      ,[AdvertisingId]
      ,[AppID]
      ,[MediaSource]
      ,[Agency]
      ,[Channel]
      ,[Campaign]
      ,[Adset]
      ,[Ad]
      ,[SiteID]
      ,[EventValue]
      ,[EventRevenue]
      ,[EventRevenueCurrency]
      ,[EventRevenueUSD]
  FROM [dbo].[AFEventsPurchase]


----------------------------------------------------------------------------
---------------     AFEventsInstall  ---------------------------------------
----------------------------------------------------------------------------
SELECT TOP (1000) [ProjectName]
      ,[Platform]
      ,[CountryCode]
      ,[AttributedTouchTime]
      ,[InstallTime]
      ,[EventTime]
      ,[AdvertisingId]
      ,[AppID]
      ,[MediaSource]
      ,[Agency]
      ,[Channel]
      ,[Campaign]
      ,[Adset]
      ,[Ad]
      ,[SiteID]
      ,[AdID]
  FROM [dbo].[AFEventsInstall]