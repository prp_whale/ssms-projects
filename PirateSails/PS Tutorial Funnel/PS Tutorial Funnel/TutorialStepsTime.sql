DECLARE @pp FLOAT 
DECLARE @pp_size INT;

SET @pp = .05
--SET @pp_size = (SELECT @pp * Count(*)  FROM  PRP_Tutorial_Step_Seconds) ;
SET @pp_size = (SELECT @pp * Count(*)  FROM  PRP_Tutorial_Step_Seconds WHERE sec_step_10 IS NOT NULL) ;
SELECT @pp*2 AS Trimed 
        , @pp_size * 2  AS TrimSize
        , S1.TrimmedMeanP AS step_1_mean
        , S2.TrimmedMeanP AS step_2_mean
        , S3.TrimmedMeanP AS step_3_mean
        , S4.TrimmedMeanP AS step_4_mean
        , S5.TrimmedMeanP AS step_5_mean
        , S6.TrimmedMeanP AS step_6_mean
        , S7.TrimmedMeanP AS step_7_mean
        , S8.TrimmedMeanP AS step_8_mean
        , S9.TrimmedMeanP AS step_9_mean
        , S10.TrimmedMeanP AS step_10_mean

FROM (
        SELECT Avg(sec_step_1 * 1.0) AS TrimmedMeanP 
FROM PRP_Tutorial_Step_Seconds AS TSS
WHERE  (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds T
        WHERE  T.sec_step_1 <= TSS.sec_step_1) >= @pp_size
AND    (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds TT
        WHERE  TT.sec_step_1 >= TSS.sec_step_1) >= @pp_size
        ) AS S1
,        (SELECT Avg(sec_step_2 * 1.0) AS TrimmedMeanP 
FROM PRP_Tutorial_Step_Seconds AS TSS
WHERE  (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds T
        WHERE  T.sec_step_2 <= TSS.sec_step_2) >= @pp_size
AND    (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds TT
         WHERE  TT.sec_step_2 >= TSS.sec_step_2) >= @pp_size
         AND sec_step_1 IS NOT NULL) AS S2
,        (SELECT Avg(sec_step_3 * 1.0) AS TrimmedMeanP 
FROM PRP_Tutorial_Step_Seconds AS TSS
WHERE  (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds T
        WHERE  T.sec_step_3 <= TSS.sec_step_3) >= @pp_size
AND    (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds TT
         WHERE  TT.sec_step_3 >= TSS.sec_step_3) >= @pp_size
         AND sec_step_2 IS NOT NULL) AS S3
,        (SELECT Avg(sec_step_4 * 1.0) AS TrimmedMeanP 
FROM PRP_Tutorial_Step_Seconds AS TSS
WHERE  (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds T
        WHERE  T.sec_step_4 <= TSS.sec_step_4) >= @pp_size
AND    (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds TT
         WHERE  TT.sec_step_4 >= TSS.sec_step_4) >= @pp_size
         AND sec_step_3 IS NOT NULL) AS S4
,        (SELECT Avg(sec_step_5 * 1.0) AS TrimmedMeanP 
FROM PRP_Tutorial_Step_Seconds AS TSS
WHERE  (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds T
        WHERE  T.sec_step_5 <= TSS.sec_step_5) >= @pp_size
AND    (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds TT
         WHERE  TT.sec_step_5 >= TSS.sec_step_5) >= @pp_size
         AND sec_step_4 IS NOT NULL) AS S5
,        (SELECT Avg(sec_step_6 * 1.0) AS TrimmedMeanP 
FROM PRP_Tutorial_Step_Seconds AS TSS
WHERE  (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds T
        WHERE  T.sec_step_6 <= TSS.sec_step_6) >= @pp_size
AND    (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds TT
         WHERE  TT.sec_step_6 >= TSS.sec_step_6) >= @pp_size
         AND sec_step_5 IS NOT NULL) AS S6
,        (SELECT Avg(sec_step_7 * 1.0) AS TrimmedMeanP 
FROM PRP_Tutorial_Step_Seconds AS TSS
WHERE  (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds T
        WHERE  T.sec_step_7 <= TSS.sec_step_7) >= @pp_size
AND    (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds TT
         WHERE  TT.sec_step_7 >= TSS.sec_step_7) >= @pp_size
         AND sec_step_6 IS NOT NULL) AS S7
,        (SELECT Avg(sec_step_8 * 1.0) AS TrimmedMeanP 
FROM PRP_Tutorial_Step_Seconds AS TSS
WHERE  (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds T
        WHERE  T.sec_step_8 <= TSS.sec_step_8) >= @pp_size
AND    (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds TT
         WHERE  TT.sec_step_8 >= TSS.sec_step_8) >= @pp_size
         AND sec_step_7 IS NOT NULL) AS S8
,        (SELECT Avg(sec_step_9 * 1.0) AS TrimmedMeanP 
FROM PRP_Tutorial_Step_Seconds AS TSS
WHERE  (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds T
        WHERE  T.sec_step_9 <= TSS.sec_step_9) >= @pp_size
AND    (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds TT
         WHERE  TT.sec_step_9 >= TSS.sec_step_9) >= @pp_size
         AND sec_step_8 IS NOT NULL) AS S9
,        (SELECT Avg(sec_step_10 * 1.0) AS TrimmedMeanP 
FROM PRP_Tutorial_Step_Seconds AS TSS
WHERE  (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds T
        WHERE  T.sec_step_10 <= TSS.sec_step_10) >= @pp_size
AND    (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds TT
         WHERE  TT.sec_step_10 >= TSS.sec_step_10) >= @pp_size
         AND sec_step_9 IS NOT NULL) AS S10