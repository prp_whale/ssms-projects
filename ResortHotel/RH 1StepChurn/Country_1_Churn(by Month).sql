DECLARE @start_registration DATETIME = '2019-03-01 00:00:00.000';
DECLARE @end_registration DATETIME = '2019-08-01 23:59:59.997';


--SELEcT DATEADD(mm,6,@start_registration)

SELECT L.* 
--,All_Users
,L1.[Leavers] AS leave_month_1
,L2.[Leavers] AS leave_month_2
,L3.[Leavers] AS leave_month_3
,L4.[Leavers] AS leave_month_4
,L5.[Leavers] AS leave_month_5
,L6.[Leavers] AS leave_month_6
,[All_Leavers]*1.0 / [All_Users] AS churn
,L1.[Leavers]*1.0 / U1.[Users] AS churn_month_1
,L2.[Leavers]*1.0 / U2.[Users] AS churn_month_2
,L3.[Leavers]*1.0 / U3.[Users] AS churn_month_3
,L4.[Leavers]*1.0 / U4.[Users] AS churn_month_4
,L5.[Leavers]*1.0 / U5.[Users] AS churn_month_5
,L6.[Leavers]*1.0 / U6.[Users] AS churn_month_6
FROM
(SELECT [RegistrationCountry]
, Count(*) AS All_Leavers
FROM PRP_Pool_Of_Deviators_Android
GROUP BY [RegistrationCountry]
 ) AS L
INNER JOIN 
(SELECT [RegistrationCountry]
, Count(*) AS All_Users
FROM PRP_Pool_Of_Users_Android
GROUP BY [RegistrationCountry]
HAVING Count(*) > 5) AS U
ON U.[RegistrationCountry] =  L.[RegistrationCountry]

LEFT JOIN
(SELECT [RegistrationCountry]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN @start_registration AND DATEADD(mm,1,@start_registration)
GROUP BY [RegistrationCountry]
 ) AS L1 ON L.[RegistrationCountry] = L1.[RegistrationCountry]
LEFT JOIN 
(SELECT [RegistrationCountry]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN @start_registration AND DATEADD(mm,1,@start_registration)
GROUP BY [RegistrationCountry]
) AS U1
ON L.[RegistrationCountry] = U1.[RegistrationCountry] 

LEFT JOIN
(SELECT [RegistrationCountry]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,1,@start_registration) AND DATEADD(mm,2,@start_registration)
GROUP BY [RegistrationCountry]
 ) AS L2 ON L.[RegistrationCountry] = L2.[RegistrationCountry]
LEFT JOIN 
(SELECT [RegistrationCountry]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,1,@start_registration) AND DATEADD(mm,2,@start_registration)
GROUP BY [RegistrationCountry]
) AS U2
ON L.[RegistrationCountry] = U2.[RegistrationCountry] 

LEFT JOIN
(SELECT [RegistrationCountry]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,2,@start_registration) AND DATEADD(mm,3,@start_registration)
GROUP BY [RegistrationCountry]
 ) AS L3 ON L.[RegistrationCountry] = L3.[RegistrationCountry]
LEFT JOIN 
(SELECT [RegistrationCountry]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,2,@start_registration) AND DATEADD(mm,3,@start_registration)
GROUP BY [RegistrationCountry]
) AS U3
ON L.[RegistrationCountry] = U3.[RegistrationCountry] 

LEFT JOIN
(SELECT [RegistrationCountry]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,3,@start_registration) AND DATEADD(mm,4,@start_registration)
GROUP BY [RegistrationCountry]
 ) AS L4 ON L.[RegistrationCountry] = L4.[RegistrationCountry]
LEFT JOIN 
(SELECT [RegistrationCountry]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,3,@start_registration) AND DATEADD(mm,4,@start_registration)
GROUP BY [RegistrationCountry]
) AS U4
ON L.[RegistrationCountry] = U4.[RegistrationCountry] 

LEFT JOIN
(SELECT [RegistrationCountry]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,4,@start_registration) AND DATEADD(mm,5,@start_registration)
GROUP BY [RegistrationCountry]
 ) AS L5 ON L.[RegistrationCountry] = L5.[RegistrationCountry]
LEFT JOIN 
(SELECT [RegistrationCountry]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,4,@start_registration) AND DATEADD(mm,5,@start_registration)
GROUP BY [RegistrationCountry]
) AS U5
ON L.[RegistrationCountry] = U5.[RegistrationCountry] 

LEFT JOIN
(SELECT [RegistrationCountry]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,5,@start_registration) AND DATEADD(mm,6,@start_registration)
GROUP BY [RegistrationCountry]
 ) AS L6 ON L.[RegistrationCountry] = L6.[RegistrationCountry]
LEFT JOIN 
(SELECT [RegistrationCountry]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,5,@start_registration) AND DATEADD(mm,6,@start_registration)
GROUP BY [RegistrationCountry]
) AS U6
ON L.[RegistrationCountry] = U6.[RegistrationCountry] 
ORDER BY All_Leavers DESC
