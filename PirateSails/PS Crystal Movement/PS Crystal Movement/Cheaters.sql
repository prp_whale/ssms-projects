
--WITH CHANGERS 
--AS (
--	SELECT U.[UserId] AS ID
--	FROM [dbo].[Users] as U
--	INNER JOIN  [dbo].[EventsAppOpen] AS EP
--	ON U.[UserId] = EP.[UserId]
--	AND [RegistrationCountry] != [Country]
--)

--SELECT Count (DISTINCT ID) AS U 
--       ,Count(*)           AS A 
--FROM   CHANGERS 

--WITH APP_OPEN 
--AS (
--	SELECT U.[UserId] AS ID
--	FROM [dbo].[EventsAppOpen]  as U
--	INNER JOIN  [dbo].[EventsAppOpen] AS EP
--	ON U.[UserId] = EP.[UserId]
--	AND U.[Country] != EP.[Country]
--)

--SELECT Count (DISTINCT ID) AS U 
--       ,Count(*)           AS A 
--FROM   APP_OPEN 


WITH APP_OPEN_IP 
AS (
	SELECT U.[UserId] AS ID
	FROM [dbo].[Users] as U
	INNER JOIN  [dbo].[Users] AS EP
	ON U.[UserId] = EP.[UserId]
	AND U.[Created] != EP.[LastLogin]
)

SELECT Count (DISTINCT ID) AS U 
       ,Count(*)           AS A 
FROM   APP_OPEN_IP 



--SELECT [Currency]
-- ,Count (DISTINCT [UserID] ) AS U 
--       ,Count(*)       AS A
--FROM [dbo].[EventsPurchase]   
--GROUP BY [Currency]



