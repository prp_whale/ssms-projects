DROP TABLE IF EXISTS PRP_D2D_Installs;
DROP TABLE IF EXISTS PRP_D2D_Visits;
DROP TABLE IF EXISTS PRP_D2D_Payments;


CREATE TABLE PRP_D2D_Installs (date_index timestamp, player_id varchar(255), source varchar(2000))
CREATE TABLE PRP_D2D_Visits (date_index timestamp, player_id varchar(255))
CREATE TABLE PRP_D2D_Payments (date_index timestamp, player_id varchar(255), package_name varchar(255), amount decimal(19,4) , tax decimal(19,4))

