DECLARE @start_period DATETIME = '2020-01-16 00:00:00.000'; 
DECLARE @end_period DATETIME = '2020-03-05 23:59:59.997'; 

WITH POOL_OF_USERS
AS (
   SELECT UserId AS U_ID FROM dbo.Users WHERE UserId = 180675
    -- SELECT TOP (10) UserId AS U_ID
    -- FROM dbo.Users
    -- WHERE [RegistrationClientVersion] IN ('0.0.105', '0.0.108')
)

 ,STEPS_result AS (
    SELECT UserId 
    , result
    , LEAD (result) OVER (PARTITION  BY UserId ORDER BY [Timestamp]) AS NextResult
    , DATEDIFF(ms, [Timestamp], LEAD (Timestamp) OVER (PARTITION  BY UserId ORDER BY [Timestamp])) AS MiliSec
    FROM [dbo].[shop_purchase] INNER JOIN POOL_OF_USERS ON U_ID = UserId 
    WHERE [Timestamp] BETWEEN  @start_period AND @end_period
    AND UserId = 180675
 )

--SELECT Count(*) FROM  STEPS_result 

SELECT * FROM  STEPS_result  

