 DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform int

SET @startperiod = '2019-08-14 00:00:00.000';
SET @endperiod = '2019-11-14 23:59:59.999';
SET @platform = 2;

SELECT Convert(date, Timestamp)
, Count (DISTINCT UserID) AS Users
--, Count (*) AS Uber
--FROM  dbo.[JoinTeam] 
FROM  dbo.[CreateTeam] 
WHERE TIMESTAMP BETWEEN @startperiod AND  @endperiod
AND Platform = @platform
GROUP BY Convert(date, Timestamp)
ORDER BY Convert(date, Timestamp)
 