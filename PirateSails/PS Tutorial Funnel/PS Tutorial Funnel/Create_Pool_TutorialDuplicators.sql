-- DROP PROCEDURE IF EXISTS INSERTer;
-- CREATE PROCEDURE INSERTer @step_id VARCHAR(40)
-- AS
--     INSERT INTO dbo.PRP_Pool_TutorialDuplicators ([UserId])
--     SELEcT POU.[UserId]
--     FROM dbo.[ev_Tutorial] AS T 
--         INNER JOIN dbo.PRP_Pool_Of_Users  AS POU ON  POU.[UserId] = T.[UserId]
--     WHERE [step_key] = @step_id 
--     GROUP BY POU.[UserId]
--     HAVING Count (*) > 1
-- GO


-----------------------------------------------------
-- STEP 1  look into CreatePoolOfUsers
-- STEP 2  BELOW
-- STEP 3  look into CreatePoolOfUsers
-----------------------------------------------------

DECLARE @step_1 VARCHAR (40) = '1_window_buildings_list_open';
DECLARE @step_2 VARCHAR (40) = '1_2_avaliable';
DECLARE @step_3 VARCHAR (40) = '2_3_avaliable';
DECLARE @step_4 VARCHAR (40) = '3_4_avaliable';
DECLARE @step_5 VARCHAR (40) = '4_5_avaliable';
DECLARE @step_6 VARCHAR (40) = '5_6_avaliable';
DECLARE @step_7 VARCHAR (40) = '6_7_avaliable';
DECLARE @step_8 VARCHAR (40) = '7_8_avaliable';
DECLARE @step_9 VARCHAR (40) = '8_9_avaliable';
DECLARE @step_10 VARCHAR (40) = '9_10_avaliable';
DECLARE @step_11 VARCHAR (40) = '10_tutorial_end';

DROP TABLE IF EXISTS PRP_Pool_TutorialDuplicators;

SELEcT POU.[UserId]
INTO PRP_Pool_TutorialDuplicators
FROM dbo.[ev_Tutorial] AS T 
    RIGHT JOIN dbo.PRP_Pool_Of_Users AS POU 
        ON  POU.[UserId] = T.[UserId]
WHERE T.[UserId] IS  NULL
 --  AND T.[UserId] IS  NULL

SELEcT Count(DISTINCT UserId)  FROM PRP_Pool_TutorialDuplicators

EXEC INSERTer @step_id = @step_1;
EXEC INSERTer @step_id = @step_2;
EXEC INSERTer @step_id = @step_3;
EXEC INSERTer @step_id = @step_4;
EXEC INSERTer @step_id = @step_5;
EXEC INSERTer @step_id = @step_6;
EXEC INSERTer @step_id = @step_7;
EXEC INSERTer @step_id = @step_8;
EXEC INSERTer @step_id = @step_9;
EXEC INSERTer @step_id = @step_10;
EXEC INSERTer @step_id = @step_11;

-- SELECT * FROM PRP_Pool_TutorialDuplicators
SELECT TOP (100) * FROM PRP_Pool_TutorialDuplicators
