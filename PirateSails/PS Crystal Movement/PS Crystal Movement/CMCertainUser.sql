DECLARE  @UID int = 30178
DECLARE @action Varchar(10) = 'Received'; 
--DECLARE @action Varchar(10) = 'Spend';

--SELECT TOP (1000) *
--  FROM [dbo].[CrystalMovement]
--  WHERE [UserId] = @UID
--  AND  [Action] = @action 


SELECT [Action] 
       , [ActionSource] 
       , [ItemCategoryId] 
       , [ItemId] 
       , [CrystalAmount] 
       , [CrystalBalance] 
       , [CaptainLevel] 
       , [MainBuildingLevel] 
       , [Timestamp] 
       , [SessionId] 
       , [ClientVersion] 
       , [BalanceVersion] 
FROM   [dbo].[CrystalMovement] 
WHERE  [UserId] = @UID 
ORDER BY [Timestamp]