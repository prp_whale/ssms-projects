DECLARE @start_period DATETIME = '2019-06-01 00:00:00.000';
DECLARE @end_period DATETIME = '2019-06-14 23:59:59.997';

--SET @start_period  = '2019-06-15 00:00:00.000';
--SET @end_period  = '2019-06-28 23:59:59.997';

SET @start_period  = '2019-07-01 00:00:00.000';
SET @end_period   = '2019-07-14 23:59:59.997';

 --SET @start_period  = '2019-07-14 00:00:00.000';
 --SET @end_period   = '2019-07-28 23:59:59.997';

--SET @start_period  = '2019-08-01 00:00:00.000';
--SET @end_period   = '2019-08-14 23:59:59.997';

DECLARE @whale_limit INT = 500;

SELECT Count (DISTINCT [UserId]) AS 'Registration'
,Sum([LTV1D]) / Count (DISTINCT [UserId]) AS 'DAY1'
,Sum([LTV2D]) / Count (DISTINCT [UserId]) AS 'DAY2'
,Sum([LTV3D]) / Count (DISTINCT [UserId]) AS 'DAY3'
,Sum([LTV4D]) / Count (DISTINCT [UserId]) AS 'DAY4'
,Sum([LTV5D]) / Count (DISTINCT [UserId]) AS 'DAY5'
,Sum([LTV6D]) / Count (DISTINCT [UserId]) AS 'DAY6'
,Sum([LTV7D]) / Count (DISTINCT [UserId]) AS 'DAY7'
,Sum([LTV8D]) / Count (DISTINCT [UserId]) AS 'DAY8'
,Sum([LTV9D]) / Count (DISTINCT [UserId]) AS 'DAY9'
,Sum([LTV10D]) / Count (DISTINCT [UserId]) AS 'DAY10'
,Sum([LTV11D]) / Count (DISTINCT [UserId]) AS 'DAY11'
,Sum([LTV12D]) / Count (DISTINCT [UserId]) AS 'DAY12'
,Sum([LTV13D]) / Count (DISTINCT [UserId]) AS 'DAY13'
,Sum([LTV14D]) / Count (DISTINCT [UserId]) AS 'DAY14'
,Sum([LTV15D]) / Count (DISTINCT [UserId]) AS 'DAY15'
,Sum([LTV16D]) / Count (DISTINCT [UserId]) AS 'DAY16'
,Sum([LTV17D]) / Count (DISTINCT [UserId]) AS 'DAY17'
,Sum([LTV18D]) / Count (DISTINCT [UserId]) AS 'DAY18'
,Sum([LTV19D]) / Count (DISTINCT [UserId]) AS 'DAY19'
,Sum([LTV20D]) / Count (DISTINCT [UserId]) AS 'DAY20'
,Sum([LTV21D]) / Count (DISTINCT [UserId]) AS 'DAY21'
,Sum([LTV22D]) / Count (DISTINCT [UserId]) AS 'DAY22'
,Sum([LTV23D]) / Count (DISTINCT [UserId]) AS 'DAY23'
,Sum([LTV24D]) / Count (DISTINCT [UserId]) AS 'DAY24'
,Sum([LTV25D]) / Count (DISTINCT [UserId]) AS 'DAY25'
,Sum([LTV26D]) / Count (DISTINCT [UserId]) AS 'DAY26'
,Sum([LTV27D]) / Count (DISTINCT [UserId]) AS 'DAY27'
,Sum([LTV28D]) / Count (DISTINCT [UserId]) AS 'DAY28'
,Sum([LTV29D]) / Count (DISTINCT [UserId]) AS 'DAY29'
,Sum([LTV30D]) / Count (DISTINCT [UserId]) AS 'DAY30'
FROM [dbo].[UsersLtvCoeff] 
WHERE  [Created] BETWEEN @start_period AND @end_period 
AND ([LTV] <= @whale_limit OR [LTV] IS NULL)
AND [Platform] = 1