SELECT [EventId] 
       ,[dbo].[EventsInfo].[Name] 
       ,[dbo].[EventsParamsInfo].[Name] AS TypeName 
       ,[Type] 
       ,[TypeNumber] 
FROM   [dbo].[EventsInfo] 
       LEFT JOIN [dbo].[EventsParamsInfo] 
              ON [dbo].[EventsInfo].[Id] = [dbo].[EventsParamsInfo].[EventId] 
ORDER  BY [EventId]
		 ,[name] 
          ,[type] 
          ,typenumber 