DECLARE @start_date Datetime = '2019-02-11 00:00:00.000'; 
DECLARE @end_date Datetime = '2019-02-24 23:59:59.999'; 
DECLARE @cheater Int = 7249; 
DECLARE @test_country Varchar(2) = 'UA'; 

SELECT [UserId]          AS U 
       , Sum ([arrived]) AS ARRIVED 
       , Sum ([sends])   AS SENDS 
FROM   [dbo].[airport] 
WHERE  [Timestamp] BETWEEN @start_date AND @end_date 
   AND [UserId] != @cheater 
   AND [LTV] = 0 
   AND [RegistrationCountry] != @test_country 
GROUP  BY [UserId] 
ORDER  BY SENDS DESC 