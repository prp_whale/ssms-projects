SELECT OPENS_AUTO 
	  ,( SELECT Count ( [UserId])  
			FROM [dbo].[PRP_OpensToPurchase_March_May]  AS OTP 
			 WHERE OTP.OPENS IS NULL
			 AND OTP.OPENS_AUTO = [dbo].[PRP_OpensToPurchase_March_May].OPENS_AUTO) AS '0'
       ,( SELECT Count ( [UserId])  
			FROM [dbo].[PRP_OpensToPurchase_March_May]  AS OTP 
			 WHERE OTP.OPENS = 1 
			 AND OTP.OPENS_AUTO = [dbo].[PRP_OpensToPurchase_March_May].OPENS_AUTO) AS '1'
       ,( SELECT Count ( [UserId])  
			FROM [dbo].[PRP_OpensToPurchase_March_May]  AS OTP 
			 WHERE OTP.OPENS = 2 
			 AND OTP.OPENS_AUTO = [dbo].[PRP_OpensToPurchase_March_May].OPENS_AUTO) AS '2'
	  ,( SELECT Count ( [UserId])  
			FROM [dbo].[PRP_OpensToPurchase_March_May]  AS OTP 
			 WHERE OTP.OPENS = 3 
			 AND OTP.OPENS_AUTO = [dbo].[PRP_OpensToPurchase_March_May].OPENS_AUTO) AS '3'
,( SELECT Count ( [UserId])  
			FROM [dbo].[PRP_OpensToPurchase_March_May]  AS OTP 
			 WHERE OTP.OPENS = 4 
			 AND OTP.OPENS_AUTO = [dbo].[PRP_OpensToPurchase_March_May].OPENS_AUTO) AS '4'
,( SELECT Count ( [UserId])  
			FROM [dbo].[PRP_OpensToPurchase_March_May]  AS OTP 
			 WHERE OTP.OPENS = 5 
			 AND OTP.OPENS_AUTO = [dbo].[PRP_OpensToPurchase_March_May].OPENS_AUTO) AS '5'
,( SELECT Count ( [UserId])  
			FROM [dbo].[PRP_OpensToPurchase_March_May]  AS OTP 
			 WHERE OTP.OPENS = 6 
			 AND OTP.OPENS_AUTO = [dbo].[PRP_OpensToPurchase_March_May].OPENS_AUTO) AS '6'
,( SELECT Count ( [UserId])  
			FROM [dbo].[PRP_OpensToPurchase_March_May]  AS OTP 
			 WHERE OTP.OPENS = 7 
			 AND OTP.OPENS_AUTO = [dbo].[PRP_OpensToPurchase_March_May].OPENS_AUTO) AS '7'
,( SELECT Count ( [UserId])  
			FROM [dbo].[PRP_OpensToPurchase_March_May]  AS OTP 
			 WHERE OTP.OPENS = 8 
			 AND OTP.OPENS_AUTO = [dbo].[PRP_OpensToPurchase_March_May].OPENS_AUTO) AS '8'
,( SELECT Count ( [UserId])  
			FROM [dbo].[PRP_OpensToPurchase_March_May]  AS OTP 
			 WHERE OTP.OPENS = 9 
			 AND OTP.OPENS_AUTO = [dbo].[PRP_OpensToPurchase_March_May].OPENS_AUTO) AS '9'
,( SELECT Count ( [UserId])  
			FROM [dbo].[PRP_OpensToPurchase_March_May]  AS OTP 
			 WHERE OTP.OPENS = 10 
			 AND OTP.OPENS_AUTO = [dbo].[PRP_OpensToPurchase_March_May].OPENS_AUTO) AS '10'
,( SELECT Count ( [UserId])  
			FROM [dbo].[PRP_OpensToPurchase_March_May]  AS OTP 
			 WHERE OTP.OPENS = 11 
			 AND OTP.OPENS_AUTO = [dbo].[PRP_OpensToPurchase_March_May].OPENS_AUTO) AS '11'
,( SELECT Count ( [UserId])  
			FROM [dbo].[PRP_OpensToPurchase_March_May]  AS OTP 
			 WHERE OTP.OPENS = 12 
			 AND OTP.OPENS_AUTO = [dbo].[PRP_OpensToPurchase_March_May].OPENS_AUTO) AS '12'
,( SELECT Count ( [UserId])  
			FROM [dbo].[PRP_OpensToPurchase_March_May]  AS OTP 
			 WHERE OTP.OPENS = 13 
			 AND OTP.OPENS_AUTO = [dbo].[PRP_OpensToPurchase_March_May].OPENS_AUTO) AS '13'
,( SELECT Count ( [UserId])  
			FROM [dbo].[PRP_OpensToPurchase_March_May]  AS OTP 
			 WHERE OTP.OPENS = 14 
			 AND OTP.OPENS_AUTO = [dbo].[PRP_OpensToPurchase_March_May].OPENS_AUTO) AS '14'
			 ,( SELECT Count ( [UserId])  
			FROM [dbo].[PRP_OpensToPurchase_March_May]  AS OTP 
			 WHERE OTP.OPENS >= 15 
			 AND OTP.OPENS_AUTO = [dbo].[PRP_OpensToPurchase_March_May].OPENS_AUTO) AS '15+'

FROM   [dbo].[PRP_OpensToPurchase_March_May] 
GROUP  BY OPENS_AUTO  
ORDER  BY OPENS_AUTO 


--SELECT OPENS 
--       ,Count(*) 
--FROM   [dbo].[PRP_OpensToPurchase_March_May_Full] 
--WHERE  OPENS_AUTO IS NULL
--GROUP  BY OPENS 
--ORDER  BY OPENS 

