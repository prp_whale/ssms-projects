DECLARE @start_period DATETIME = '2019-04-09 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-06-13 23:59:59.997'; 


SELECT spent_source_id
		,Count (DISTINCT [UserId]) AS Users 
       ,Count (*)                AS Amount 
FROM [dbo].[ev_friendies]
WHERE  [TimeStamp] BETWEEN @start_period AND @end_period 
GROUP BY [spent_source_id]
ORDER BY Users DESC