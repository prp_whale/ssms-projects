DECLARE @start_registration DATETIME = '2019-03-01 00:00:00.000';
DECLARE @end_registration DATETIME = '2019-08-01 23:59:59.997';

SELECT L.* 
--,All_Users
,[All_Leavers]*1.0 / [All_Users] AS churn
,L1.[Leavers]*1.0 / U1.[Users] AS churn_month_1
FROM
(SELECT [GPUType]
, Count(*) AS All_Leavers
FROM PRP_Pool_Of_Deviators_Android
GROUP BY [GPUType]
 ) AS L
INNER JOIN 
(SELECT [GPUType]
, Count(*) AS All_Users
FROM PRP_Pool_Of_Users_Android
GROUP BY [GPUType]
HAVING Count(*) > 5) AS U
ON U.[GPUType] =  L.[GPUType]

LEFT JOIN
(SELECT [GPUType]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN @start_registration AND DATEADD(mm,1,@start_registration)
GROUP BY [GPUType]
 ) AS L1 ON L.[GPUType] = L1.[GPUType]
LEFT JOIN 
(SELECT [GPUType]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN @start_registration AND DATEADD(mm,1,@start_registration)
GROUP BY [GPUType]
HAVING Count(*) > 5) AS U1
ON L.[GPUType] = U2.[GPUType] 


ORDER BY churn DESC
