-- SELECT TOP (10) * 
-- FROM PRP_Tutorial_Step_Seconds
-- WHERE sec_step_10 IS NOT NULL


-- SELECT 
-- AVG(sec_step_1) AS st_1
-- ,AVG(sec_step_2)	AS st_2
-- ,AVG(sec_step_3)	AS st_3
-- ,AVG(sec_step_4)	AS st_4
-- ,AVG(sec_step_5)	AS st_5
-- ,AVG(sec_step_6)	AS st_6
-- ,AVG(sec_step_7)	AS st_7
-- ,AVG(sec_step_8)	AS st_8
-- ,AVG(sec_step_9)	AS st_9
-- ,AVG(sec_step_10)	AS st_10
-- FROM PRP_Tutorial_Step_Seconds

-- SELECT 
-- AVG(sec_step_1) AS st_1
-- ,AVG(sec_step_2)	AS st_2
-- ,AVG(sec_step_3)	AS st_3
-- ,AVG(sec_step_4)	AS st_4
-- ,AVG(sec_step_5)	AS st_5
-- ,AVG(sec_step_6)	AS st_6
-- ,AVG(sec_step_7)	AS st_7
-- ,AVG(sec_step_8)	AS st_8
-- ,AVG(sec_step_9)	AS st_9
-- ,AVG(sec_step_10)	AS st_10
-- FROM PRP_Tutorial_Step_Seconds


-- SELECT 
-- MAX(sec_step_1)/3600 AS st_1
-- ,MAX(sec_step_2)/3600	AS st_2
-- ,MAX(sec_step_3)/3600	AS st_3
-- ,MAX(sec_step_4)/3600	AS st_4
-- ,MAX(sec_step_5)/3600	AS st_5
-- ,MAX(sec_step_6)/3600	AS st_6
-- ,MAX(sec_step_7)/3600	AS st_7
-- ,MAX(sec_step_8)/3600	AS st_8
-- ,MAX(sec_step_9)/3600	AS st_9
-- ,MAX(sec_step_10)/3600	AS st_10
-- FROM PRP_Tutorial_Step_Seconds


DECLARE @pp FLOAT 
DECLARE @pp_size INT;

SET @pp = .05
SET @pp_size = (SELECT @pp * Count(*)  FROM  PRP_Tutorial_Step_Seconds WHERE sec_step_10 IS NOT NULL) ;

SELECT @pp_size

SELECT Avg(sec_step_1 * 1.0) AS TrimmedMeanP 
FROM PRP_Tutorial_Step_Seconds AS TSS
WHERE  (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds T1
        WHERE  T1.sec_step_1 <= TSS.sec_step_1) >= @pp_size
AND    (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds T2
        WHERE  T2.sec_step_1 >= TSS.sec_step_1) >= @pp_size


SELECT Avg(sec_step_2 * 1.0) AS TrimmedMeanP 
FROM PRP_Tutorial_Step_Seconds AS TSS
WHERE  (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds T1
        WHERE  T1.sec_step_2 <= TSS.sec_step_2) >= @pp_size
AND    (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds T2
        WHERE  T2.sec_step_2 >= TSS.sec_step_2) >= @pp_size

SELECT Count(*) AS TrimmedMeanP 
FROM PRP_Tutorial_Step_Seconds AS TSS
WHERE  (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds T1
        WHERE  T1.sec_step_1 <= TSS.sec_step_1) >= @pp_size
AND    (SELECT Count(*) 
        FROM   PRP_Tutorial_Step_Seconds T2
        WHERE  T2.sec_step_1 >= TSS.sec_step_1) >= @pp_size




-- SELECT Count(*) AS TrimmedMeanP 
-- FROM PRP_Tutorial_Step_Seconds AS TSS
-- WHERE  (SELECT Count(*) 
--         FROM   PRP_Tutorial_Step_Seconds T1
--         WHERE  T1.sec_step_2 <= TSS.sec_step_2) >= @pp_size
-- AND    (SELECT Count(*) 
--         FROM   PRP_Tutorial_Step_Seconds T2
--         WHERE  T2.sec_step_2 >= TSS.sec_step_2) >= @pp_size

-- SELECT Count(*) AS TrimmedMeanP 
-- FROM PRP_Tutorial_Step_Seconds AS TSS
-- WHERE  (SELECT Count(*) 
--         FROM   PRP_Tutorial_Step_Seconds T1
--         WHERE  T1.sec_step_5 <= TSS.sec_step_5) >= @pp_size
-- AND    (SELECT Count(*) 
--         FROM   PRP_Tutorial_Step_Seconds T2
--         WHERE  T2.sec_step_5 >= TSS.sec_step_5) >= @pp_size


-- SELECT Count(*) AS TrimmedMeanP 
-- FROM PRP_Tutorial_Step_Seconds AS TSS
-- WHERE  (SELECT Count(*) 
--         FROM   PRP_Tutorial_Step_Seconds T1
--         WHERE  T1.sec_step_10 <= TSS.sec_step_10) >= @pp_size
-- AND    (SELECT Count(*) 
--         FROM   PRP_Tutorial_Step_Seconds T2
--         WHERE  T2.sec_step_10 >= TSS.sec_step_10) >= @pp_size

-- SELECT Count(DISTINCT sec_step_1) FROM PRP_Tutorial_Step_Seconds 
-- SELECT Count(DISTINCT sec_step_2) FROM PRP_Tutorial_Step_Seconds 
-- SELECT Count(DISTINCT sec_step_5) FROM PRP_Tutorial_Step_Seconds 
-- SELECT Count(DISTINCT sec_step_10) FROM PRP_Tutorial_Step_Seconds 
-- SELECT Count(*) FROM PRP_Tutorial_Step_Seconds WHERE sec_step_2 IS NULL
-- SELECT Count(*) FROM PRP_Tutorial_Step_Seconds WHERE sec_step_5 IS NULL
-- SELECT Count(*) FROM PRP_Tutorial_Step_Seconds WHERE sec_step_10 IS NULL

-- SELEcT sec_step_10 
-- , COunt (Distinct UserID) AS Users
-- , Count (*) AS A
-- FROM PRP_Tutorial_Step_Seconds 
-- GROUP BY sec_step_10
-- ORDER BY Users DESC, sec_step_10 ASC