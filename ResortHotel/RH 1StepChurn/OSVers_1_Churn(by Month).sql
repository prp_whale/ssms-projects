DECLARE @start_registration DATETIME = '2019-03-01 00:00:00.000';
DECLARE @end_registration DATETIME = '2019-10-01 23:59:59.997';

SELECT L.* 
,All_Users
,L1.[Leavers] AS leave_month_1
,L2.[Leavers] AS leave_month_2
,L3.[Leavers] AS leave_month_3
,L4.[Leavers] AS leave_month_4
,L5.[Leavers] AS leave_month_5
,L6.[Leavers] AS leave_month_6
,L7.[Leavers] AS leave_month_7
,[All_Leavers]*1.0 / [All_Users] AS churn
,L1.[Leavers]*1.0 / U1.[Users] AS churn_month_1
,L2.[Leavers]*1.0 / U2.[Users] AS churn_month_2
,L3.[Leavers]*1.0 / U3.[Users] AS churn_month_3
,L4.[Leavers]*1.0 / U4.[Users] AS churn_month_4
,L5.[Leavers]*1.0 / U5.[Users] AS churn_month_5
,L6.[Leavers]*1.0 / U6.[Users] AS churn_month_6
,L7.[Leavers]*1.0 / U7.[Users] AS churn_month_7
FROM
(SELECT [OsVersion]
, Count(*) AS All_Leavers
FROM PRP_Pool_Of_Deviators_Android
GROUP BY [OsVersion]
 ) AS L
INNER JOIN 
(SELECT [OsVersion]
, Count(*) AS All_Users
FROM PRP_Pool_Of_Users_Android
GROUP BY [OsVersion]
HAVING Count(*) > 5) AS U
ON U.[OsVersion] =  L.[OsVersion]

LEFT JOIN
(SELECT [OsVersion]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN @start_registration AND DATEADD(mm,1,@start_registration)
GROUP BY [OsVersion]
 ) AS L1 ON L.[OsVersion] = L1.[OsVersion]
LEFT JOIN 
(SELECT [OsVersion]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN @start_registration AND DATEADD(mm,1,@start_registration)
GROUP BY [OsVersion]
) AS U1
ON L.[OsVersion] = U1.[OsVersion] 

LEFT JOIN
(SELECT [OsVersion]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,1,@start_registration) AND DATEADD(mm,2,@start_registration)
GROUP BY [OsVersion]
 ) AS L2 ON L.[OsVersion] = L2.[OsVersion]
LEFT JOIN 
(SELECT [OsVersion]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,1,@start_registration) AND DATEADD(mm,2,@start_registration)
GROUP BY [OsVersion]
) AS U2
ON L.[OsVersion] = U2.[OsVersion] 

LEFT JOIN
(SELECT [OsVersion]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,2,@start_registration) AND DATEADD(mm,3,@start_registration)
GROUP BY [OsVersion]
 ) AS L3 ON L.[OsVersion] = L3.[OsVersion]
LEFT JOIN 
(SELECT [OsVersion]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,2,@start_registration) AND DATEADD(mm,3,@start_registration)
GROUP BY [OsVersion]
) AS U3
ON L.[OsVersion] = U3.[OsVersion] 

LEFT JOIN
(SELECT [OsVersion]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,3,@start_registration) AND DATEADD(mm,4,@start_registration)
GROUP BY [OsVersion]
 ) AS L4 ON L.[OsVersion] = L4.[OsVersion]
LEFT JOIN 
(SELECT [OsVersion]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,3,@start_registration) AND DATEADD(mm,4,@start_registration)
GROUP BY [OsVersion]
) AS U4
ON L.[OsVersion] = U4.[OsVersion] 

LEFT JOIN
(SELECT [OsVersion]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,4,@start_registration) AND DATEADD(mm,5,@start_registration)
GROUP BY [OsVersion]
 ) AS L5 ON L.[OsVersion] = L5.[OsVersion]
LEFT JOIN 
(SELECT [OsVersion]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,4,@start_registration) AND DATEADD(mm,5,@start_registration)
GROUP BY [OsVersion]
) AS U5
ON L.[OsVersion] = U5.[OsVersion] 

LEFT JOIN
(SELECT [OsVersion]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,6-1,@start_registration) AND DATEADD(mm,6,@start_registration)
GROUP BY [OsVersion]
 ) AS L6 ON L.[OsVersion] = L6.[OsVersion]
LEFT JOIN 
(SELECT [OsVersion]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,6-1,@start_registration) AND DATEADD(mm,6,@start_registration)
GROUP BY [OsVersion]
) AS U6
ON L.[OsVersion] = U6.[OsVersion] 

LEFT JOIN
(SELECT [OsVersion]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,7-1,@start_registration) AND DATEADD(mm,7,@start_registration)
GROUP BY [OsVersion]
 ) AS L7 ON L.[OsVersion] = L7.[OsVersion]
LEFT JOIN 
(SELECT [OsVersion]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,7-1,@start_registration) AND DATEADD(mm,7,@start_registration)
GROUP BY [OsVersion]
) AS U7
ON L.[OsVersion] = U7.[OsVersion] 




ORDER BY All_Leavers DESC
