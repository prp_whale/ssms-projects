
DROP TABLE IF EXISTS PRP_Tutorial_Step_Seconds;

SELECT TT.[UserId]
    ,DATEDIFF(SECOND, TT.[step_1] , TT.[step_2]) AS 'sec_step_1'
    ,DATEDIFF(SECOND, TT.[step_2] , TT.[step_3]) AS 'sec_step_2'
    ,DATEDIFF(SECOND, TT.[step_3] , TT.[step_4]) AS 'sec_step_3'
    ,DATEDIFF(SECOND, TT.[step_4] , TT.[step_5]) AS 'sec_step_4'
    ,DATEDIFF(SECOND, TT.[step_5] , TT.[step_6]) AS 'sec_step_5'
    ,DATEDIFF(SECOND, TT.[step_6] , TT.[step_7]) AS 'sec_step_6'
    ,DATEDIFF(SECOND, TT.[step_7] , TT.[step_8]) AS 'sec_step_7'
    ,DATEDIFF(SECOND, TT.[step_8] , TT.[step_9]) AS 'sec_step_8'
    ,DATEDIFF(SECOND, TT.[step_9] , TT.[step_10]) AS 'sec_step_9'
    ,DATEDIFF(SECOND, TT.[step_10] , TT.[step_11]) AS 'sec_step_10'
INTO PRP_Tutorial_Step_Seconds
FROM PRP_Tutorial_Timestamps AS TT
WHERE TT.[step_1] IS NOT NULL

SELECT Count(*) FROM PRP_Tutorial_Step_Seconds 
SELECT TOP (100) * FROM PRP_Tutorial_Step_Seconds

SELEcT DATEDIFF(SECOND, NULL , Getdate())