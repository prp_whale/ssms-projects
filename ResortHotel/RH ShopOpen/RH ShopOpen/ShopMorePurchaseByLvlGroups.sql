--DECLARE @start_lvl int = 0;	 DECLARE @end_lvl int = 100;
--DECLARE @start_lvl int = 100;	 DECLARE @end_lvl int = 150;
--DECLARE @start_lvl int = 150;	 DECLARE @end_lvl int = 200;
--DECLARE @start_lvl int = 200;	 DECLARE @end_lvl int = 250;
--DECLARE @start_lvl int = 250;	 DECLARE @end_lvl int = 300;
--DECLARE @start_lvl int = 300;	 DECLARE @end_lvl int = 400;
--DECLARE @start_lvl int = 400;	 DECLARE @end_lvl int = 500;
DECLARE @start_lvl int = 500;	 DECLARE @end_lvl int = 1000;



SELECT [full_shop]            AS IsFullShop
       ,Count (DISTINCT [UserId]) AS Users 
       ,Count (*)                 AS Amount 
FROM   [dbo].[PRP_ShopPurchase_March_May] 
WHERE  [level_id] BETWEEN @start_lvl AND @end_lvl
 --  AND [new_depositor] = 0 
GROUP  BY [full_shop] 
ORDER  BY [full_shop] desc


--SELECT Count (DISTINCT [UserId]) AS Users 
--       ,Count (*)                 AS Amount 
--FROM   [dbo].[PRP_ShopPurchase_March_May] 
--WHERE  [level_id] BETWEEN @start_lvl AND @end_lvl
-- --  AND [new_depositor] = 0 
 

--SELECT [level_id]
--  ,Count (DISTINCT [UserId]) AS Users 
--  FROM   [dbo].[PRP_ShopPurchase_March_May] 
--  GROUP  BY [level_id] 
--  ORDER  BY [level_id] 