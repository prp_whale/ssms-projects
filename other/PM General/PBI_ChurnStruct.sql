declare @days int = 28;

declare @days_multi int = 3;
declare @st_per DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days*@days_multi, GETDATE())));
declare @end_per DATETIME  = DATEADD(ms, -3, CONVERT(datetime, CONVERT(date, GETDATE())));

declare @st_tail DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days*@days_multi, @st_per)));

declare @versa_limit int = 100;

If(OBJECT_ID('tempdb..#temp_dates') Is Null)
Begin
    CREATE TABLE #temp_dates ( [date] DATE PRIMARY KEY );
    INSERT #temp_dates([date]) SELECT d FROM ( 
        SELECT d = DATEADD(DAY, rn - 1, @st_per) FROM  (
                SELECT TOP (DATEDIFF(DAY, @st_per, @end_per+1)) rn = ROW_NUMBER() OVER (ORDER BY s1.[object_id])
                FROM sys.all_objects AS s1 CROSS JOIN sys.all_objects AS s2
                ORDER BY s1.[object_id] ) AS x ) AS y;
End;

WITH Versa
AS (
    SELECT registrationclientversion AS RegVer
    FROM  dbo.Users
   -- WHERE  created BETWEEN @st_per AND @end_per
    GROUP BY registrationclientversion
    HAVING Count(*) >=  @versa_limit
)

,GameOpen 
AS (
    SELECT UserID
    , Max(LevelId) AS lvl
    , Convert(date, Max([Timestamp])) AS last_play
    FROM EventsLevelAttempts
    WHERE [ClientVersion] IN (SELECT * FROM Versa)
    AND [Timestamp] BETWEEN @st_tail AND @end_per
    AND [LevelId] IS NOT NULL
    GROUP BY UserId
)

-- , Churn3D
-- AS (
--     SELECT [date] , lvl
--     , Count(UserID) AS Users
--     FROM GameOpen CROSS JOIN #temp_dates
--     WHERE last_login = DATEADD(dd, -3,  [date])
--     GROUP BY [date] , lvl
-- )
, Churn7D
AS (
    SELECT [date] , lvl
    , Count(UserID) AS Users
    FROM GameOpen CROSS JOIN #temp_dates
    WHERE last_play = DATEADD(dd, -3,  [date])
    GROUP BY [date] , lvl
)

-- , Churn14D
-- AS (
--     SELECT [date] , lvl
--     , Count(UserID) AS Users
--     FROM GameOpen CROSS JOIN #temp_dates
--     WHERE last_login = DATEADD(dd, -14,  [date])
--     GROUP BY [date] , lvl
-- )

-- , Churn28D
-- AS (
--     SELECT [date] , lvl
--     , Count(UserID) AS Users
--     FROM GameOpen CROSS JOIN #temp_dates
--     WHERE last_login = DATEADD(dd, -28,  [date])
--     GROUP BY [date] , lvl
-- )

SELECT [date] , lvl 
, Users AS Churn7D
    FROM Churn7D AS T
ORDER BY [date] , lvl

If(OBJECT_ID('tempdb..#temp_dates') Is Not Null)
Begin Drop Table #temp_dates End