 DECLARE @startperiod datetime,
        @endperiod datetime,
		@platform int

SET @startperiod = '2019-08-14 00:00:00.000';
SET @endperiod = '2019-11-14 23:59:59.999';
SET @platform = 2;

 
With lvls as (
    SELECT DISTINCT UserId as U_ID
    FROM  dbo.[EventsLevelAttempts]
    WHERE  LevelId > 30	
    AND [Platform] = @platform
)
, joined  as (
SELECT  UserId AS U_ID
    ,Min (Convert(date, Timestamp)) AS JoinTime
FROM  dbo.[JoinTeam] 
WHERE [TIMESTAMP] BETWEEN @startperiod AND  @endperiod
AND Platform = @platform
GROUP BY UserID
)



SELECT Convert(date, Timestamp) 
, Count  (distinct (iif(JoinTime < Convert(date, Timestamp) ,UserId, NULL))) AS Users
 FROM  dbo.[EventsAppOpen] AS ao INNER JOIN joined ON joined.U_ID = ao.UserId
    INNER JOIN lvls on lvls.U_ID = ao.UserId
    WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
    AND [Platform] = @platform
GROUP BY Convert(date, Timestamp)
ORDER BY Convert(date, Timestamp)


-- SEL
