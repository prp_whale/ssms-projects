
--[dbo].[ev_ShopOpen]
SELECT        UserId, Timestamp, ClientTime, BackOfficeTime, ClientVersion, Int1 AS bundle_ver, Int2 AS is_auto, Int3 AS is_client_depositor, Int4 AS level_id, Int5 AS segment_1, Int6 AS segment_2, Int7 AS segment_3, 
                         Int8 AS special_offer_available, Int9 AS time_to_end, String1 AS offers_showed_1, String2 AS offers_showed_2, String3 AS offers_showed_3, String4 AS offers_showed_4, String5 AS offers_showed_5, 
                         String6 AS offers_showed_6, String7 AS trigger_id, String8 AS trigger_source_id
FROM            dbo.Events
WHERE        (EventId = 86)

--[dbo].[ev_ShopOpenMore]
SELECT        UserId, Timestamp, ClientTime, BackOfficeTime, ClientVersion, Int1 AS bundle_ver, Int2 AS is_auto, Int3 AS is_client_depositor, Int4 AS level_id, Int5 AS segment_1, Int6 AS segment_2, Int7 AS segment_3, 
                         Int8 AS special_offer_available, Int9 AS time_to_end, String1 AS trigger_id, String2 AS trigger_source_id
FROM            dbo.Events
WHERE        (EventId = 89)


--[dbo].[ev_ShopPurchase]
SELECT        UserId, Timestamp, ClientTime, BackOfficeTime, ClientVersion, Double1 AS price, Int1 AS bundle_ver, Int2 AS full_shop, Int3 AS is_auto, Int4 AS is_client_depositor, Int5 AS level_id, Int6 AS new_depositor, Int7 AS segment_1, 
                         Int8 AS segment_2, Int9 AS segment_3, Int10 AS special_offer_available, Int11 AS time_to_end, String1 AS purchase_name, String2 AS result, String3 AS trigger_id, String4 AS trigger_source_id
FROM            dbo.Events
WHERE        (EventId = 90)

--[dbo].[ev_PiggyBankShow] 
SELECT UserId 
       ,Timestamp 
       ,ClientTime 
       ,BackOfficeTime 
       ,ClientVersion 
       ,Int1    AS bundle_ver 
       ,Int2    AS is_auto 
       ,Int3    AS is_avaliable 
       ,Int4    AS is_client_depositor 
       ,Int5    AS level_id 
       ,Int6    AS progress 
       ,Int7    AS special_offer_available 
       ,Int8    AS time_to_end 
       ,String1 AS type 
FROM    dbo.Events  
WHERE  ( EventId = 94 ) 

--[dbo].[ev_PiggyBankPurchase] 
SELECT UserId 
       ,Timestamp 
       ,ClientTime 
       ,BackOfficeTime 
       ,ClientVersion 
       ,Double1 AS price 
       ,Int1    AS bundle_ver 
       ,Int2    AS is_auto 
       ,Int3    AS is_client_depositor 
       ,Int4    AS level_id 
       ,Int5    AS new_depositor 
       ,Int6    AS progress 
       ,Int7    AS special_offer_available 
       ,Int8    AS time_to_end 
       ,String1 AS result 
       ,String2 AS type 
FROM   dbo.Events 
WHERE  ( EventId = 95 ) 

--[dbo].[ev_OfferShowUp] 
SELECT UserId 
       ,Timestamp 
       ,ClientTime 
       ,BackOfficeTime 
       ,ClientVersion 
       ,Int1    AS bundle_ver 
       ,Int2    AS is_auto 
       ,Int3    AS is_client_depositor 
       ,Int4    AS level_id 
       ,Int5    AS segment_1 
       ,Int6    AS segment_2 
       ,Int7    AS segment_3 
       ,Int8    AS time_to_end 
       ,String1 AS offers_showed_1 
       ,String2 AS offers_showed_2 
FROM   dbo.Events 
WHERE  ( EventId = 88 ) 

--[dbo].[ev_OfferPurchase] 
SELECT UserId 
       ,Timestamp 
       ,ClientTime 
       ,BackOfficeTime 
       ,ClientVersion 
       ,Double1 AS price 
       ,Int1    AS bundle_ver 
       ,Int2    AS is_auto 
       ,Int3    AS is_client_depositor 
       ,Int4    AS level_id 
       ,Int5    AS new_depositor 
       ,Int6    AS segment_1 
       ,Int7    AS segment_2 
       ,Int8    AS segment_3 
       ,Int9    AS time_to_end 
       ,String1 AS offers_showed_1 
       ,String2 AS offers_showed_2 
        ,String3 AS purchase_name 
         ,String4 AS result 
FROM   dbo.Events 
WHERE  ( EventId = 92 ) 