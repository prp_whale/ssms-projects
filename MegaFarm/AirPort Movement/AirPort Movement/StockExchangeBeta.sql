DECLARE @start_date Datetime = '2019-02-11 00:00:00.000'; 
DECLARE @end_date Datetime = '2019-02-24 23:59:59.999'; 
DECLARE @cheater Int = 7249; 
DECLARE @test_country Varchar(2) = 'UA'; 


SELECT TOP (1000) [EventId]
	  ,[UserId]
	  ,[source_type_id]
	  ,[item_id]
       ,[Timestamp]
      ,[ClientTime]
      ,[BackOfficeTime]
      ,[Order]
      ,[ClientVersion]
      ,[IsOffline]
      ,[Platform]
      ,[Created]
      ,[LTV]
      ,[Retention1D]
      ,[Retention2D]
      ,[Retention7D]
      ,[FirstDepositDate]
      ,[LTV7D]
      ,[LTV2D]
      ,[LTV1D]
      ,[LTV30D]
      ,[RegistrationCountry]
      ,[cash]
      ,[cash_balance]
      ,[coins]
      ,[coins_balance]
      ,[level_id]
      ,[gems1]
      ,[gems2]
      ,[gems3]
      ,[gems4]
      
  FROM [dbo].[currency_received]

  WHERE  [Timestamp] BETWEEN @start_date AND @end_date 
   AND [UserId] != @cheater 
   AND [LTV] = 0 
   AND [RegistrationCountry] != @test_country 
   AND [source_type_id] = 'order_stock_exch'
