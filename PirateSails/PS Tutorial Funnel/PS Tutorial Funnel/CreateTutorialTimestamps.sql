
DECLARE @step_1 VARCHAR (40) = '1_window_buildings_list_open';
DECLARE @step_2 VARCHAR (40) = '1_2_avaliable';
DECLARE @step_3 VARCHAR (40) = '2_3_avaliable';
DECLARE @step_4 VARCHAR (40) = '3_4_avaliable';
DECLARE @step_5 VARCHAR (40) = '4_5_avaliable';
DECLARE @step_6 VARCHAR (40) = '5_6_avaliable';
DECLARE @step_7 VARCHAR (40) = '6_7_avaliable';
DECLARE @step_8 VARCHAR (40) = '7_8_avaliable';
DECLARE @step_9 VARCHAR (40) = '8_9_avaliable';
DECLARE @step_10 VARCHAR (40) = '9_10_avaliable';
DECLARE @step_11 VARCHAR (40) = '10_tutorial_end';

DROP TABLE IF EXISTS PRP_Tutorial_Timestamps;

CREATE TABLE PRP_Tutorial_Timestamps 
    (UserId int primary key
    ,step_1 DATETIME
    ,step_2 DATETIME
    ,step_3 DATETIME
    ,step_4 DATETIME
    ,step_5 DATETIME
    ,step_6 DATETIME
    ,step_7 DATETIME
    ,step_8 DATETIME
    ,step_9 DATETIME
    ,step_10 DATETIME
    ,step_11 DATETIME
    );


INSERT INTO PRP_Tutorial_Timestamps ([UserId])
SELECT [UserId] FROM PRP_Pool_Of_NoDuplicators


-- SELEcT Count (*) FROM PRP_Tutorial_Timestamps 
-- SELECT TOP (100) * FROM PRP_Tutorial_Timestamps


UPDATE TT
SET TT.[step_1] = T.[Timestamp]
FROM PRP_Tutorial_Timestamps AS TT LEFT JOIN [dbo].[ev_Tutorial] AS T ON T.[UserId] = TT.[UserId]
WHERE T.[step_key] = @step_1

UPDATE TT
SET TT.[step_2] = T.[Timestamp]
FROM PRP_Tutorial_Timestamps AS TT LEFT JOIN [dbo].[ev_Tutorial] AS T ON T.[UserId] = TT.[UserId]
WHERE T.[step_key] = @step_2

UPDATE TT
SET TT.[step_3] = T.[Timestamp]
FROM PRP_Tutorial_Timestamps AS TT LEFT JOIN [dbo].[ev_Tutorial] AS T ON T.[UserId] = TT.[UserId]
WHERE T.[step_key] = @step_3

UPDATE TT
SET TT.[step_4] = T.[Timestamp]
FROM PRP_Tutorial_Timestamps AS TT LEFT JOIN [dbo].[ev_Tutorial] AS T ON T.[UserId] = TT.[UserId]
WHERE T.[step_key] = @step_4
 
 
UPDATE TT
SET TT.[step_5] = T.[Timestamp]
FROM PRP_Tutorial_Timestamps AS TT LEFT JOIN [dbo].[ev_Tutorial] AS T ON T.[UserId] = TT.[UserId]
WHERE T.[step_key] = @step_5

UPDATE TT
SET TT.[step_6] = T.[Timestamp]
FROM PRP_Tutorial_Timestamps AS TT LEFT JOIN [dbo].[ev_Tutorial] AS T ON T.[UserId] = TT.[UserId]
WHERE T.[step_key] = @step_6

UPDATE TT
SET TT.[step_7] = T.[Timestamp]
FROM PRP_Tutorial_Timestamps AS TT LEFT JOIN [dbo].[ev_Tutorial] AS T ON T.[UserId] = TT.[UserId]
WHERE T.[step_key] = @step_7

UPDATE TT
SET TT.[step_8] = T.[Timestamp]
FROM PRP_Tutorial_Timestamps AS TT LEFT JOIN [dbo].[ev_Tutorial] AS T ON T.[UserId] = TT.[UserId]
WHERE T.[step_key] = @step_8

UPDATE TT
SET TT.[step_9] = T.[Timestamp]
FROM PRP_Tutorial_Timestamps AS TT LEFT JOIN [dbo].[ev_Tutorial] AS T ON T.[UserId] = TT.[UserId]
WHERE T.[step_key] = @step_9

UPDATE TT
SET TT.[step_10] = T.[Timestamp]
FROM PRP_Tutorial_Timestamps AS TT LEFT JOIN [dbo].[ev_Tutorial] AS T ON T.[UserId] = TT.[UserId]
WHERE T.[step_key] = @step_10

UPDATE TT
SET TT.[step_11] = T.[Timestamp]
FROM PRP_Tutorial_Timestamps AS TT LEFT JOIN [dbo].[ev_Tutorial] AS T ON T.[UserId] = TT.[UserId]
WHERE T.[step_key] = @step_11

--SELECT TOP (100) * FROM PRP_Tutorial_Timestamps
SELECT Count(*) FROM PRP_Tutorial_Timestamps  WHERE step_1 IS NULL

