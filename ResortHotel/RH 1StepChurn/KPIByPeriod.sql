DECLARE @start_period DATETIME = '2019-09-01 00:00:00.000'; 
DECLARE @end_period DATETIME = '2019-10-01 23:59:59.997';  -- конец анализированного периода


SELECT  COUNT(*) AS USERS
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 1, IIF([Retention1D] = 1, 1.0, 0.0), null)) as 'RetentionDay1'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 3, IIF([Retention3D] = 1, 1.0, 0.0), null)) as 'RetentionDay3'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 7, IIF([Retention7D] = 1, 1.0, 0.0), null)) as 'RetentionDay7'
	--	,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 14, IIF([Retention14D] = 1, 1.0, 0.0), null)) as 'RetentionDay14'
	--	,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 30, IIF([Retention30D] = 1, 1.0, 0.0), null)) as 'RetentionDay30'

FROM [dbo].[Users]
WHERE [Created] BETWEEN @start_period AND @end_period 
AND [RegistrationCountry] !=  'UA' 
AND [Platform] = 1
AND [RAMSize] > 1500

SELECT  COUNT(*) AS USERS
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 1, IIF([LTV1D] > 0, 1.0, 0.0), null)) as 'Conversion_Day1'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 3, IIF([LTV3D] > 0, 1.0, 0.0), null)) as 'Conversion_Day3'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 7, IIF([LTV7D] > 0, 1.0, 0.0), null)) as 'Conversion_Day7'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 14, IIF([LTV14D] > 0, 1.0, 0.0), null)) as 'Conversion_Day14'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 30, IIF([LTV30D] > 0, 1.0, 0.0), null)) as 'Conversion_Day30'
FROM [dbo].[Users]
WHERE [Created] BETWEEN @start_period AND @end_period 
AND [RegistrationCountry] !=  'UA' 
AND [Platform] = 1
AND [RAMSize] > 1500

SELECT  COUNT(*)  AS USERS
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 1, [LTV1D], null)) as 'CARPU_Day1'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 3, [LTV3D], null)) as 'CARPU_Day3'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 7, [LTV7D], null)) as 'CARPU_Day7'
	--	,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 14, [LTV14D], null)) as 'CARPU_Dy14'
	--	,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 30, [LTV30D], null)) as 'CARPU_Day30'
FROM [dbo].[Users]
WHERE [Created] BETWEEN @start_period AND @end_period 
AND [RegistrationCountry] !=  'UA' 
AND [Platform] = 1
AND [RAMSize] > 1500


