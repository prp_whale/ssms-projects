DECLARE @DateFrom datetime = '2019-11-03 00:00:00.000';--начало анализированного периода
DECLARE @DateTo datetime = DATEADD(ms, -3, DATEADD(dd,  DATEDIFF(dd,0,GETDATE()),0)) -- конец анализированного периода


SELECT  CONVERT(date, [Created]) as date
		,COUNT(*) AS 'Players'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 1, IIF([Retention1D] = 1, 1.0, 0.0), null)) as 'Day1'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 2, IIF([Retention2D] = 1, 1.0, 0.0), null)) as 'Day2'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 3, IIF([Retention3D] = 1, 1.0, 0.0), null)) as 'Day3'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 4, IIF([Retention4D] = 1, 1.0, 0.0), null)) as 'Day4'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 5, IIF([Retention5D] = 1, 1.0, 0.0), null)) as 'Day5'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 6, IIF([Retention6D] = 1, 1.0, 0.0), null)) as 'Day6'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 7, IIF([Retention7D] = 1, 1.0, 0.0), null)) as 'Day7'

		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 8, IIF([Retention8D] = 1, 1.0, 0.0), null)) as 'Day8'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 9, IIF([Retention9D] = 1, 1.0, 0.0), null)) as 'Day9'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 10, IIF([Retention10D] = 1, 1.0, 0.0), null)) as 'Day10'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 11, IIF([Retention11D] = 1, 1.0, 0.0), null)) as 'Day11'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 12, IIF([Retention12D] = 1, 1.0, 0.0), null)) as 'Day12'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 13, IIF([Retention13D] = 1, 1.0, 0.0), null)) as 'Day13'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 14, IIF([Retention14D] = 1, 1.0, 0.0), null)) as 'Day14'

		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 15, IIF([Retention15D] = 1, 1.0, 0.0), null)) as 'Day15'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 16, IIF([Retention16D] = 1, 1.0, 0.0), null)) as 'Day16'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 17, IIF([Retention17D] = 1, 1.0, 0.0), null)) as 'Day17'
        ,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 18, IIF([Retention18D] = 1, 1.0, 0.0), null)) as 'Day18'
        ,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 19, IIF([Retention19D] = 1, 1.0, 0.0), null)) as 'Day19'
        ,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 20, IIF([Retention20D] = 1, 1.0, 0.0), null)) as 'Day20'
        ,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 21, IIF([Retention21D] = 1, 1.0, 0.0), null)) as 'Day21'
        ,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 22, IIF([Retention22D] = 1, 1.0, 0.0), null)) as 'Day22'
        ,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 23, IIF([Retention23D] = 1, 1.0, 0.0), null)) as 'Day23'
        ,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 24, IIF([Retention24D] = 1, 1.0, 0.0), null)) as 'Day24'
        ,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 25, IIF([Retention25D] = 1, 1.0, 0.0), null)) as 'Day25'
        ,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 26, IIF([Retention26D] = 1, 1.0, 0.0), null)) as 'Day26'
        ,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 27, IIF([Retention27D] = 1, 1.0, 0.0), null)) as 'Day27'
        ,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 28, IIF([Retention28D] = 1, 1.0, 0.0), null)) as 'Day28'
        ,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 29, IIF([Retention29D] = 1, 1.0, 0.0), null)) as 'Day29'
        ,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 30, IIF([Retention30D] = 1, 1.0, 0.0), null)) as 'Day30'

		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 60, IIF([Retention2M] = 1, 1.0, 0.0), null)) as 'Day60'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 120, IIF([Retention4M] = 1, 1.0, 0.0), null)) as 'Day120'
		,AVG(IIF(DATEDIFF(day, [Created], GETDATE()) > 180, IIF([Retention6M] = 1, 1.0, 0.0), null)) as 'Day180'
FROM [dbo].[Users] 

WHERE [Created] BETWEEN @DateFrom AND @DateTo 
AND[Platform] = 1 
--AND [RegistrationCountry] = 'DE'

AND [MediaSource] = 'Facebook Ads'
--AND [Campaign] = 'mohicanlab_family_zoo_barel_lal_inp_01'
GROUP BY CONVERT(date, [Created])
ORDER BY CONVERT(date, [Created])
