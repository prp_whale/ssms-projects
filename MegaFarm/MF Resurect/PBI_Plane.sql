/*Самолет
В данной таблице интересует статистика выполнения заказов на Самолете. 
Это нужно для анализа выполняемых заказов, их наполнение и ресурсы, получаемые за них. 
Таким образом, мы сможем проанализировать сложность заказов и получаемые ресурсы
кол-во выполняемых самолетов на уровень (сам заказ и получаемая награда)
кол-во выполняемых самолетов за сессию (сам заказ и получаемая награда)
кол-во ускоренных самолетов в разрезе уровня
кол-во ускоренных самолетов в разрезе сессии
кол-во слотов для отправки самолета в разрезе уровня
разделение на депозиторов и недепозиторов
возможность выбирать версии и временные промежутки

*/

declare @days int = 28;

declare @days_multi int = 1;
declare @st_per DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days*@days_multi, GETDATE())));
declare @end_per DATETIME  = DATEADD(ms, -3, CONVERT(datetime, CONVERT(date, GETDATE())));

declare @versa_limit int = 100;


WITH Versa
AS (
    SELECT registrationclientversion AS RegVer
    FROM  dbo.Users
   -- WHERE  created BETWEEN @st_per AND @end_per
    GROUP BY registrationclientversion
    HAVING Count(*) >=  @versa_limit
)




SELECT CONVERT(date, [TimeStamp]) AS 'Date', ClientVersion, level_id, open_slots
    ,CASE   WHEN ltv > 0 THEN 'Depos'  ELSE 'ND'  END AS 'IsPay'

  --  ,CASE   WHEN order_status = 1 THEN 'Completed'  WHEN order_status = 2 THEN 'Canceled'  ELSE 'Err'  END AS 'Orders Status'

    , Count(Distinct UserID) AS Users
    , Count(Distinct SessionId) AS Sessionss
    , Count(UserID) AS Events

FROM airport 
WHERE ClientVersion IN (SELECT * FROM Versa)
AND [Timestamp] BETWEEN @st_per AND @end_per

AND sends = 1
AND [level_id] IS NOT NULL

GROUP BY CONVERT(date, [TimeStamp]) , ClientVersion,  level_id, open_slots
,CASE   WHEN ltv > 0 THEN 'Depos'  ELSE 'ND'  END 

ORDER BY CONVERT(date, [TimeStamp]), ClientVersion, level_id, [Events]

