DECLARE @start_period DATETIME = '2019-05-01 00:00:00.000';
DECLARE @end_period DATETIME = '2019-07-24 23:59:59.997';


--SET @start_period  = '2019-06-15 00:00:00.000';
--SET @end_period  = '2019-06-28 23:59:59.997';

--SET @start_period  = '2019-07-01 00:00:00.000';
--SET @end_period   = '2019-07-14 23:59:59.997';


  SELECT cast([Created] as DATE) as 'DAY'
        ,Count (DISTINCT P.UserId) AS 'Registration'
        ,Sum(Cast([Retention1D] AS INT))*1.0 / Count (DISTINCT P.UserId) AS 'Ret Day1'
        ,Sum(Cast([Retention7D] AS INT))*1.0 / Count (DISTINCT P.UserId) AS 'Ret Day7'
        ,Sum(Cast([Retention14D] AS INT))*1.0 / Count (DISTINCT P.UserId) AS 'Ret Day14'
        ,Sum(Cast([Retention30D] AS INT)) *1.0 / Count (DISTINCT P.UserId) AS 'Ret Day30' 
        ,Count(Iif([LTV1D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT P.UserId) AS 'C1 Day1'
        ,Count(Iif([LTV7D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT P.UserId) AS 'C1 Day7'
        ,Count(Iif([LTV14D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT P.UserId) AS 'C1 Day14'
        ,Count(Iif([LTV30D]> 0,  [UserId], NULL) )*1.0 / Count (DISTINCT P.UserId) AS 'C1 Day30'
    
    FROM [dbo].[Users] AS P
    WHERE  [Created] BETWEEN @start_period AND @end_period 
    AND [Platform] = 1
    GROUP BY cast([Created] as DATE)
    ORDER BY 'DAY' ASC   
