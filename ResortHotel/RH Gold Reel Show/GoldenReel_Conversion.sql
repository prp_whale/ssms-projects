DECLARE @startperiod datetime,
        @endperiod datetime,
	    @platform int;
        
    SET @startperiod = '2019-08-15 00:00:00.000';
    SET @endperiod = '2019-11-07 23:59:59.999';
    SET @platform = 1;



WITH PoolOfUsers
AS (
     SELECT UserId as U_ID
     FROM dbo.[Users] 
     WHERE  [RegistrationCountry] = 'JP'
     AND [platform] = @platform
    -- AND [RegistrationCountry] IN ('US', 'GB', 'DE','FR')
   )

SELECT  t.U  AS Purchase_U
, t3.U  AS Golden_Purchase_U
, t2.U  AS Converse_U
, t6.U  AS Golden_Convers_U
, t2.U*1.0 / t.U AS 'C/P'
, t6.U*1.0 / t3.U AS 'C/P golden reel'
FROM 
(    Select '1' as link 
    ,Count(Distinct UserId) AS U
    FROM [dbo].[EventsPurchase] AS T 
        INNER JOIN PoolOfUsers AS POU ON T.[UserId] = POU.[U_ID]
    WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
   
)
AS t 
INNER JOIN 
(
    Select '1' as link 
    ,Count(Distinct UserId) AS U
    FROM [dbo].[EventsPurchase] AS T 
        INNER JOIN PoolOfUsers AS POU ON T.[UserId] = POU.[U_ID]
    WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
    AND ProductId LIKE 'golden_ors_%'
   
)
AS t3 ON t.link = t3.link 
INNER JOIN 
(
    Select '1' as link 
    ,Count(Distinct UserId) AS U
    FROM [dbo].[EventsPurchase] AS T 
        INNER JOIN PoolOfUsers AS POU ON T.[UserId] = POU.[U_ID]
    WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
     AND [PaymentsCount] = 1 
)
AS t2 ON t.link = t2.link 
INNER JOIN 
(
    Select '1' as link 
    ,Count(Distinct UserId) AS U
    FROM [dbo].[EventsPurchase] AS T 
        INNER JOIN PoolOfUsers AS POU ON T.[UserId] = POU.[U_ID]
    WHERE [Timestamp] BETWEEN @startperiod AND @endperiod
    AND ProductId LIKE 'golden_ors_%'
     AND [PaymentsCount] = 1 
)
AS t6 ON t.link = t6.link 