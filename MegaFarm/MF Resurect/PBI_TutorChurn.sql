declare @days int = 28;

declare @days_multi int = 1;
declare @st_per DATETIME = CONVERT(datetime, CONVERT(date, DATEADD(DD, -@days*@days_multi, GETDATE())));
declare @end_per DATETIME  = DATEADD(ms, -3, CONVERT(datetime, CONVERT(date, GETDATE())));

declare @versa_limit int = 100;


WITH Versa
AS (
    SELECT registrationclientversion AS RegVer
    FROM  dbo.Users
   -- WHERE  created BETWEEN @st_per AND @end_per
    GROUP BY registrationclientversion
    HAVING Count(*) >=  @versa_limit
)

--     SELECT UserId
--         , CONVERT(date,Timestamp) as lvlup_date
--         , TriggerId AS lvl
--         , ClientVersion
--         ,CASE   WHEN ltv > 0 THEN 'Yes'  ELSE 'No'  END AS 'IsPay'
--         , FIRST_VALUE (TriggerId) OVER (PARTITION BY UserId, CONVERT(date,Timestamp) ORDER BY Timestamp DESC) as last_lvlup
--         , ProductBalance AS 'GoldBalanse'
--     FROM EventsProductMovement
--     WHERE ClientVersion IN (SELECT * FROM Versa)
--         AND [Timestamp] BETWEEN @st_per AND @end_per
--         AND ProductType = 'coins'
--         AND [Action] = 'received'
--         AND [TriggerSourceId] = 'level_up'
-- )

, LvlUp
AS (
    SELECT UserId
        , CONVERT(date,Timestamp) as step_date
        , step_id
        , ClientVersion
        , FIRST_VALUE (step_id) OVER (PARTITION BY UserId, CONVERT(date,Timestamp) ORDER BY Timestamp DESC) as last_step_id
    FROM Tutorial
    WHERE ClientVersion IN (SELECT * FROM Versa)
        AND [Timestamp] BETWEEN @st_per AND @end_per
       
)

,GameOpen 
AS (
    SELECT UserID AS U_ID
            , CONVERT(date,Timestamp) as first_date
            ,  LEAD (CONVERT(date,Timestamp),1,CONVERT(date,getdate())) OVER (PARTITION BY UserId ORDER BY CONVERT(date,Timestamp) ASC)  as next_date
    FROM balance 
   
    WHERE [ClientVersion] IN (SELECT * FROM Versa)
        AND [Timestamp] BETWEEN @st_per AND @end_per
        AND [level] IS NOT NULL
    GROUP BY UserId , CONVERT(date,Timestamp)
)



SELECT step_date   , ClientVersion , step_id
, COUNT(Distinct UserId) AS Users
, COUNT(Distinct (iif (datediff(dd, step_date, next_date)>1, UserID, null))) AS Churn_1D
, COUNT(Distinct (iif (datediff(dd, step_date, next_date)>3, UserID, null))) AS Churn_3D
, COUNT(Distinct (iif (datediff(dd, step_date, next_date)>7, UserID, null))) AS Churn_7D

FROM LvlUp LEFT JOIN GameOpen ON UserId = U_ID AND step_date = first_date AND step_id = last_step_id
WHERE step_Id NOT IN (510,520, 580,590,600,610,620,690,700,710,720, 999)
GROUP BY step_date   , ClientVersion , step_id
ORDER BY step_date   , ClientVersion , step_id


