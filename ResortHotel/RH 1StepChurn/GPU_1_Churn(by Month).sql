DECLARE @start_registration DATETIME = '2019-03-01 00:00:00.000';
DECLARE @end_registration DATETIME = '2019-10-01 23:59:59.997';

SELECT L.* 
--,All_Users
,L1.[Leavers] AS leave_month_1
,L2.[Leavers] AS leave_month_2
,L3.[Leavers] AS leave_month_3
,L4.[Leavers] AS leave_month_4
,L5.[Leavers] AS leave_month_5
,L6.[Leavers] AS leave_month_6
,[All_Leavers]*1.0 / [All_Users] AS churn
,L1.[Leavers]*1.0 / U1.[Users] AS churn_month_1
,L2.[Leavers]*1.0 / U2.[Users] AS churn_month_2
,L3.[Leavers]*1.0 / U3.[Users] AS churn_month_3
,L4.[Leavers]*1.0 / U4.[Users] AS churn_month_4
,L5.[Leavers]*1.0 / U5.[Users] AS churn_month_5
,L6.[Leavers]*1.0 / U6.[Users] AS churn_month_6
FROM
(SELECT [GPUType]
, Count(*) AS All_Leavers
FROM PRP_Pool_Of_Deviators_Android
GROUP BY [GPUType]
 ) AS L
INNER JOIN 
(SELECT [GPUType]
, Count(*) AS All_Users
FROM PRP_Pool_Of_Users_Android
GROUP BY [GPUType]
HAVING Count(*) > 5) AS U
ON U.[GPUType] =  L.[GPUType]

LEFT JOIN
(SELECT [GPUType]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN @start_registration AND DATEADD(mm,1,@start_registration)
GROUP BY [GPUType]
 ) AS L1 ON L.[GPUType] = L1.[GPUType]
LEFT JOIN 
(SELECT [GPUType]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN @start_registration AND DATEADD(mm,1,@start_registration)
GROUP BY [GPUType]
) AS U1
ON L.[GPUType] = U1.[GPUType] 

LEFT JOIN
(SELECT [GPUType]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,1,@start_registration) AND DATEADD(mm,2,@start_registration)
GROUP BY [GPUType]
 ) AS L2 ON L.[GPUType] = L2.[GPUType]
LEFT JOIN 
(SELECT [GPUType]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,1,@start_registration) AND DATEADD(mm,2,@start_registration)
GROUP BY [GPUType]
) AS U2
ON L.[GPUType] = U2.[GPUType] 

LEFT JOIN
(SELECT [GPUType]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,2,@start_registration) AND DATEADD(mm,3,@start_registration)
GROUP BY [GPUType]
 ) AS L3 ON L.[GPUType] = L3.[GPUType]
LEFT JOIN 
(SELECT [GPUType]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,2,@start_registration) AND DATEADD(mm,3,@start_registration)
GROUP BY [GPUType]
) AS U3
ON L.[GPUType] = U3.[GPUType] 

LEFT JOIN
(SELECT [GPUType]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,3,@start_registration) AND DATEADD(mm,4,@start_registration)
GROUP BY [GPUType]
 ) AS L4 ON L.[GPUType] = L4.[GPUType]
LEFT JOIN 
(SELECT [GPUType]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,3,@start_registration) AND DATEADD(mm,4,@start_registration)
GROUP BY [GPUType]
) AS U4
ON L.[GPUType] = U4.[GPUType] 

LEFT JOIN
(SELECT [GPUType]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,4,@start_registration) AND DATEADD(mm,5,@start_registration)
GROUP BY [GPUType]
 ) AS L5 ON L.[GPUType] = L5.[GPUType]
LEFT JOIN 
(SELECT [GPUType]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,4,@start_registration) AND DATEADD(mm,5,@start_registration)
GROUP BY [GPUType]
) AS U5
ON L.[GPUType] = U5.[GPUType] 

LEFT JOIN
(SELECT [GPUType]
, Count(*) AS Leavers
FROM PRP_Pool_Of_Deviators_Android
WHERE [Created] BETWEEN DATEADD(mm,5,@start_registration) AND DATEADD(mm,6,@start_registration)
GROUP BY [GPUType]
 ) AS L6 ON L.[GPUType] = L6.[GPUType]
LEFT JOIN 
(SELECT [GPUType]
, Count(*) AS Users
FROM PRP_Pool_Of_Users_Android
WHERE [Created] BETWEEN DATEADD(mm,5,@start_registration) AND DATEADD(mm,6,@start_registration)
GROUP BY [GPUType]
) AS U6
ON L.[GPUType] = U6.[GPUType] 
ORDER BY All_Leavers DESC
